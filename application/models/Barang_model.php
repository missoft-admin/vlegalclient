<?php

class Barang_model extends CI_Model {
	function __construct()
	{
		
   	}

   	
	function add_record($data) 
	{
		$this->db->insert('mbarang', $data);
		return $this->db->insert_id();		
	}

	function get_barang($kodebarang)
    {	
		$this->db->select('mbarang.kodebarang,mbarang.namabarang,mbarang.hargabeli,mbarang.hargajualstokis,mbarang.hargajualstokis,
				mbarang.hargajualmember,mbarang.jenis,mbarang.ket,mbarang.createdby,userc.nama AS namac,mbarang.editedby,usere.nama AS namae');
		$this->db->from('mbarang');
		$this->db->where('kodebarang',$kodebarang);
		$this->db->join("zusers AS userc", "mbarang.createdby = userc.userid", 'left');
		$this->db->join("zusers AS usere", "mbarang.editedby = usere.userid", 'left');
		$query = $this->db->get();
		return $query->row();
    	
    	
    	//$this->db->where('kodebarang',$noid);
		//$query = $this->db->get('mbarang');
        //return $query->row();
    }
    
	function check_namabarang($noid,$nama)
    {	
    	$query = $this->db->query("SELECT COUNT(kodebarang) as hasil,deleted FROM mbarang WHERE kodebarang!='$noid' AND REPLACE(mbarang.namabarang,' ','') = REPLACE('$nama',' ','') LIMIT 1");    	
		if ($row = $query->row()){
			$hasil=$row->hasil + $row->deleted ;
		}else{
			$hasil=0;
		}
		return $hasil;
    }     

	function update_record($noid,$data) 
	{	
		$this->db->where('kodebarang',$noid);
		$this->db->update('mbarang',$data);
		return;
	}    

	function count($deleted = false)
	{
		if ($this->session->userdata('cabang')){
			$this->db->from('mbarang_zona');
			$this->db->join("mbarang", "mbarang_zona.kodebarang = mbarang.kodebarang");
			$this->db->join("mcabang", "mbarang_zona.idpropinsi = mcabang.idpropinsi");
			$this->db->join("mcabangstok", "mcabang.noidcabang = mcabangstok.noidcabang AND mbarang_zona.kodebarang = mcabangstok.kodebarang");
			$this->db->where('mcabang.noidcabang',$this->session->userdata('cabang'));
	        if($deleted){
	        	$this->db->where('mbarang_zona.deleted',1);
	        }else{ 
	        	$this->db->where('mbarang_zona.deleted',0);
	        }
			$query = $this->db->count_all_results();
			return $query;    	
    	}else{		
	        if($deleted){
	        	$this->db->where('deleted',1);
	        }else{ 
	        	$this->db->where('deleted',0);
	        }
			$this->db->from('mbarang');
			$query = $this->db->count_all_results();
	        return $query;
		}
	}
	function get_listbarang()
    {	
		if ($this->session->userdata('cabang')){
			$this->datatables->select('mbarang_zona.kodebarang,mbarang.namabarang,mbarang.jenis,mbarang_zona.hargajualstokiszona As hargajualstokis,
					mbarang_zona.hargajualmemberzona As hargajualmember,mcabangstok.stok,mcabangstok.progress');
			$this->datatables->from('mbarang_zona');
			$this->datatables->join("mbarang", "mbarang_zona.kodebarang = mbarang.kodebarang");
			$this->datatables->join("mcabang", "mbarang_zona.idpropinsi = mcabang.idpropinsi");
			$this->datatables->join("mcabangstok", "mcabang.noidcabang = mcabangstok.noidcabang AND mbarang_zona.kodebarang = mcabangstok.kodebarang");
			$this->datatables->where('mcabang.noidcabang',$this->session->userdata('cabang'));
			$this->datatables->add_column('action', '', 'id');
	        // if($deleted){
	        	// $this->db->where('mbarang_zona.deleted',1);
	        // }else{ 
	        	// $this->db->where('mbarang_zona.deleted',0);
	        // }
			// ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
			// $this->datatables->order_by('mbarang.namabarang','ASC');
			// $query = $this->db->get();
			// return $query->result();    	
			 return $this->datatables->generate();
    	}else{
			// print_r('Datang Kesini');exit();
	        // if($deleted){
	        	// $this->db->where('mbarang.deleted',1);
	        // }else{ 
	        	// $this->db->where('mbarang.deleted',0);
	        // }
			// ($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
			$this->datatables->select('kodebarang,namabarang,hargabeli,hargajualstokis,hargajualmember,stok,jenis');
			$this->datatables->from('mbarang');
			$this->datatables->add_column('var_jenis', '', 'id');
			$this->datatables->add_column('action', '', 'id');
			// $this->datatables->order_by('mbarang.namabarang','ASC');
			// $query = $this->db->get();
	       return $this->datatables->generate();
    	}
    }
    
	function deletebarang($id)
	{
		$this->db->where('kodebarang',$id);
		$this->db->update('mbarang',array('deleted' => 1,'deletedby' => $this->session->userdata('userid')));
		return;
	}
	
	function get_stokcabang($kodebarang)
    {	$this->db->select('mcabang.namacabang,mcabang.otonom,mkota.kota,mbarang.namabarang,mcabangstok.stok,,mcabangstok.progress');
		$this->db->from('mcabang');
		$this->db->join("mcabangstok", "mcabang.noidcabang = mcabangstok.noidcabang");
		$this->db->join("mbarang", "mcabangstok.kodebarang = mbarang.kodebarang");
		$this->db->join("mkota", "mcabang.kota = mkota.idkota");
		$this->db->where('mcabangstok.kodebarang',$kodebarang);
		$this->db->order_by('mcabang.otonom','ASC');
		$this->db->order_by('mcabang.namacabang','ASC');
		$query = $this->db->get();
        return $query->result();
    }	
// NewMp14
	function get_cabang()
    {	
    	$this->db->select('mcabang.namacabang,mkota_zona.propinsi');
    	$this->db->from('mcabang');
    	$this->db->join("mkota_zona", "mcabang.idpropinsi = mkota_zona.idpropinsi");
    	$this->db->where('mcabang.noidcabang',$this->session->userdata('cabang'));
		$query = $this->db->get();
		return $query->row();
    }
}
