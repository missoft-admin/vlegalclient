<?php defined('BASEPATH') or exit('No direct script access allowed');

class DataTable_model extends CI_Model
{
    /**
     * Server Side Method Binding.
     * Modified @gunalirezqimauludi
     */

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        if(count($this->select) != 0){
          $this->db->select($this->select);
        }

        if(count($this->from) != 0){
          $this->db->from($this->from);
        }

        if(count($this->join) != 0){
          foreach ($this->join as $item) {
            $this->db->join($item[0], $item[1], $item[2]);
          }
        }

        if(count($this->where) != 0){
          $this->db->where($this->where);
        }

        if(isset($this->where_not_in)){
          if(count($this->where_not_in) != 0){
            $this->db->where_not_in($this->where_not_in);
          }
        }

        if(isset($this->or_where)){
          if(count($this->or_where) != 0){
            $this->db->or_where($this->or_where);
          }
        }

        $i = 0;

        foreach ($this->column_search as $item) {
          if ($_POST['search']['value']) {
            if ($i===0) {
              $this->db->group_start();
              $this->db->like($item, $_POST['search']['value']);
            } else {
              $this->db->or_like($item, $_POST['search']['value']);
            }

            if (count($this->column_search) - 1 == $i) {
              $this->db->group_end();
            }
          }
          $i++;
        }

        if(isset($_POST['order'])){
          $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else{
          $order = $this->order;
          $this->db->order_by(key($order), $order[key($order)]);
        }

        if(count($this->group) != 0){
          $this->db->group_by($this->group);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
          $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        // print_r($this->db->last_query());exit();
        return $query->result();
    }


    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->_get_datatables_query();
        return $this->db->count_all_results();
    }
}

/* End of file DataTable_model.php */
/* Location: ./application/models/DataTable_model.php */
