<?php

class Infobar_model extends CI_Model {
	function __construct()
	{
//		$this->config->set_item('infobar',$this->get_infobar());
   	}

   	
	function get_infobar()
	{	$psebelum=date("ym",strtotime(date("Y-m-1", strtotime(date("Y-m-1"))) . "-1 month"));
		$periode=date("ym");
		
		$query = $this->db->query("SELECT sum(mutasi.penerimaan) as sterima FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode = '".$psebelum."' AND mbarang.jenis = 1 LIMIT 1");
		$row = $query->row();
		if ($row->sterima){
			$buffs = $row->sterima;
		}else{
			$buffs = 0;
		}
		$query = $this->db->query("SELECT sum(mutasi.penerimaan) as terima FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode = '".$periode."' AND mbarang.jenis = 1 LIMIT 1");
		$row = $query->row();
		if ($row->terima){
			$data['iterima']= number_format($row->terima,0,",",".");
			$data['iterimaold']= number_format($buffs,0,",",".");
			if ($buffs>0){
				$data['cterima']= get_range($row->terima,$buffs);
			}else{
				$data['cterima']= 'stag-counter';
			}
			$data['priterima']=2*(strlen($data['iterima'])*3);
		}else{
			$data['iterima']= '0';
			$data['iterimaold']='0';
			$data['cterima']= 'zero-count';
			$data['priterima']=4;
		}
	
		$query = $this->db->query("SELECT sum(mutasi.penjualan) as sjualm FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode = '".$psebelum."' AND mbarang.jenis = 1 LIMIT 1");
		$row = $query->row();
		if ($row->sjualm){
			$buffs = $row->sjualm;
		}else{
			$buffs = 0;
		}
		$query = $this->db->query("SELECT sum(mutasi.penjualan) as jualm FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode = '".$periode."' AND mbarang.jenis = 1 LIMIT 1");
		$row = $query->row();
		if ($row->jualm){
			$data['ijualm']= number_format($row->jualm,0,",",".");
			$data['ijualmold']= number_format($buffs,0,",",".");
			if ($buffs>0){
				$data['cjualm']= get_range($row->jualm,$buffs);
			}else{
				$data['cjualm']= 'stag-counter';
			}		
			$data['prijualm']=2*(strlen($data['ijualm'])*3);
		}else{
			$data['ijualm']= '0';
			$data['ijualmold']='0';
			$data['cjualm']= 'zero-count';
			$data['prijualm']=4;
		}
		
		$query = $this->db->query("SELECT sum(mutasi.penjualan2stokies) as sjuals FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode = '".$psebelum."' AND mbarang.jenis = 1 LIMIT 1");
		$row = $query->row();
		if ($row->sjuals){
			$buffs = $row->sjuals;
		}else{
			$buffs = 0;
		}	
		$query = $this->db->query("SELECT sum(mutasi.penjualan2stokies) as juals FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode = '".$periode."' AND mbarang.jenis = 1 LIMIT 1");
		$row = $query->row();
		if ($row->juals){
			$data['ijuals']= number_format($row->juals,0,",",".");
			$data['ijualsold']= number_format($buffs,0,",",".");
			if ($buffs>0){
				$data['cjuals']= get_range($row->juals,$buffs);
			}else{
				$data['cjuals']= 'stag-counter';
			}
			$data['prijuals']=2*(strlen($data['ijuals'])*3);
		}else{
			$data['ijuals']= '0';
			$data['ijualsold']='0';
			$data['cjuals']= 'zero-count';
			$data['prijuals']=4;
		}
		
		$psebelum=date("Y-m",strtotime(date("Y-m-1", strtotime(date("Y-m-1"))) . "-1 month"));
		$periode=date("Y-m");
		$query = $this->db->query("SELECT Count(noid) AS sinewm FROM mmembers WHERE tgldaftar like '".$psebelum."%' LIMIT 1");
		$row = $query->row();
		if ($row->sinewm){
			$buffs = $row->sinewm;
		}else{
			$buffs = 0;
		}	
		$query = $this->db->query("SELECT Count(noid) AS inewm FROM mmembers WHERE tgldaftar like '".$periode."%' LIMIT 1");
		$row = $query->row();
		if ($row->inewm){
			$data['inewm']= number_format($row->inewm,0,",",".");
			$data['inewmold']= number_format($buffs,0,",",".");
			if ($buffs>0){
				$data['cnewm']= get_range($row->inewm,$buffs);
			}else{
				$data['cnewm']= 'stag-counter';
			}
			$data['prinewm']=2*(strlen($data['inewm'])*3);
		}else{
			$data['inewm']= '0';
			$data['inewmold']='0';
			$data['cnewm']= 'zero-count';
			$data['prinewm']=4;
		}
        
        $running_text = "";
        $this->db->where('deleted',0);
        $this->db->where('publish',1);
        $this->db->order_by('publish_date','DESC');
        $query = $this->db->get('running_text');
        foreach($query->result() as $row){
            $running_text.="<li data-title=\"".str_replace('"',"'",$row->content)."\">&nbsp;</li>";
        }
        $data['running_text'] = $running_text;
        
		return $data;
	}
	function cek_setting()
	{	if($this->session->userdata('noid')<>'Super Admin'){
		$query = $this->db->query("SELECT nilai FROM zsetting WHERE setting='onlineAdmin' LIMIT 1");
		$row = $query->row();
		if ($row->nilai){
			return $row->nilai;
		}else{
			return 0;
		}}else{
			return 1;
		}
	}
}
