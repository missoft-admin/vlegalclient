<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mmember_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function data_list()
    {
        $this->datatables->select('mmembers.noid,namamembers,mkota.kota as namakota,DATE_FORMAT(tgldaftar,"%d/%m/%Y") AS tgldaftar,noupline,akumulasi.downline',false);		
		$this->datatables->from('mmembers');
		$this->datatables->join('mkota','mkota.idkota=mmembers.kota');
		$this->datatables->join('akumulasi', 'mmembers.noid = akumulasi.noid');
		$this->datatables->add_column('nilai', '', 'id');
		$this->datatables->add_column('action', '', 'id');
		
        return $this->datatables->generate();
        
    }
	function data_list_new()
    {
		$where = "mmembers.tgldaftar LIKE '".date("Y-m")."%'";
        $this->datatables->select('mmembers.noid,namamembers,mkota.kota as namakota,DATE_FORMAT(tgldaftar,"%d/%m/%Y") AS tgldaftar,noupline,akumulasi.downline',false);		
		$this->datatables->from('mmembers');
		$this->datatables->join('mkota','mkota.idkota=mmembers.kota');
		$this->datatables->join('akumulasi', 'mmembers.noid = akumulasi.noid');
	    $this->datatables->where($where);
		
		$this->datatables->add_column('nilai', '', 'id');
		$this->datatables->add_column('action', '', 'id');
		
        return $this->datatables->generate();
        
    }
	function data_belanja($noid,$periode)
    {
        // $this->datatables->select('mmembers.noid,namamembers,mkota.kota as namakota,DATE_FORMAT(tgldaftar,"%d/%m/%Y") AS tgldaftar,noupline,akumulasi.downline',false);		
		// $this->datatables->from('mmembers');
		// $this->datatables->join('mkota','mkota.idkota=mmembers.kota');
		// $this->datatables->join('akumulasi', 'mmembers.noid = akumulasi.noid');
		// $this->datatables->add_column('nilai', '', 'id');
		// $this->datatables->add_column('action', '', 'id');
		
		$this->datatables->select('ttransjualhead.nojual,ttransjualhead.tgljual,ttransjualdetail.kodebarang,
			mbarang.namabarang,mbarang.hargajualmember,ttransjualdetail.jumlah
			,(mbarang.hargajualmember*ttransjualdetail.jumlah) as total');
		$this->datatables->from('ttransjualhead');
		$this->datatables->join("ttransjualdetail", "ttransjualhead.nojual = ttransjualdetail.nojual");
		$this->datatables->join("mbarang", "ttransjualdetail.kodebarang = mbarang.kodebarang");
		$this->datatables->where('ttransjualhead.noid',$noid);
		$this->datatables->where('DATE_FORMAT(ttransjualhead.tgljual,"%y%m")',$periode);
		$this->datatables->add_column('action', '', 'id');
		// $this->datatables->order_by('ttransjualhead.nojual','DESC');
		// $query = $this->datatables->get();
		// return $query->result();
		
		
        return $this->datatables->generate();
        
    }
	function data_member(){
		$q="select *from mmembers where noid='DOM00666'";
		$query=$this->db->query($q);
		return $query->row();
	}
	function get_members($noid)
    {	
		$this->db->select('mmembers.*,mstokies.namastokies,msponsor.namamembers as namasponsor,mupline.namamembers as namaupline,
					userc.nama AS namac,usere.nama AS namae,mkota.propinsi,mkota.kota as nama_kota');//mkota.kota as kota,
		$this->db->from('mmembers');
		$this->db->where('mmembers.noid',$noid);
		$this->db->join("mstokies", "mmembers.noidstokies = mstokies.noidstokies");
		$this->db->join("mmembers AS mupline", "mmembers.noupline = mupline.noid");
		$this->db->join("mmembers AS msponsor", "mmembers.nosponsor = msponsor.noid");
		$this->db->join("zusers AS userc", "mmembers.createdby = userc.userid", 'left');
		$this->db->join("zusers AS usere", "mmembers.editedby = usere.userid", 'left');
		$this->db->join("mkota", "mkota.idkota = mmembers.kota", 'left');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->row();
    }
	function update_record($noid,$data) 
	{	
		
		// print_r($data);exit();
		// $this->db->update('mmembers',$data);
		// $this->db->affected_rows();exit();
		// print_r($this->db->last_query());exit();
		$this->db->where('noid',$noid);
		if ($this->db->update('mmembers',$data)){
			return true;
		}else{
			print_r('Kenapa Kesini');exit();
			return false;
		}
		// return true;
	}  
	function cek_upline($noid)
	{
		$this->db->select('namamembers');
		$this->db->from('mmembers');
		$this->db->where('noid', $noid);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();
        return $query->row();		
	}
	function get_infonet($noid)
    {
        $this->db->select('akumulasi.downline AS jdownline,akumulasi.nilai AS jnilai,akumulasi.grade AS jgrade,akumulasi.level1 AS jlevel1,akumulasi.level2 AS jlevel2,akumulasi.level3 AS jlevel3,akumulasi.level4 AS jlevel4,
        		akumulasi.level5 AS jlevel5,akumulasi.level6 AS jlevel6,akumulasi.level7 AS jlevel7,akumulasi.level8 AS jlevel8,akumulasi.level9 AS jlevel9,akumulasi.level10 AS jlevel10');
        $this->db->from('akumulasi');
        $this->db->where('akumulasi.noid',$noid);
		$query = $this->db->get();
        return $query->row();
    }
	function check_value($newtot=0,$curgrade=0,$noid=1)
	{	$newvalue=number_format($newtot/12207025*100,10);
		$newgrade=cek_grade($newtot);
		if ($newvalue>$curgrade){
			$this->db->query("Update akumulasi set nilaibefore = nilai,nilai =".$newvalue.",periode='".date('ym')."',gradebefore = grade,grade=".$newgrade." WHERE noid='".$noid."'");
		}else{
			$this->db->query("Update akumulasi set nilaibefore = nilai,nilai =".$newvalue." WHERE noid='".$noid."'");
		}
		return $newgrade;
	}
	function get_royalty($noid,$periode,$passup)
    {	
		//$periode=substr($periode,2,4);
		//print_r(substr($periode,2,4));exit();
		//print_r($passup);exit();
		if ($passup){
	        $this->db->select('mkomisi.personal,mkomisi.personalval,
			mkomisi_pp.salelevel1,mkomisi_pp.salelevel1val,mkomisi_pp.salelevel2,mkomisi_pp.salelevel2val,
			mkomisi_pp.salelevel3,mkomisi_pp.salelevel3val,mkomisi_pp.salelevel4,mkomisi_pp.salelevel4val,
			mkomisi_pp.salelevel5,mkomisi_pp.salelevel5val,mkomisi_pp.salelevel6,mkomisi_pp.salelevel6val,
			mkomisi_pp.salelevel7,mkomisi_pp.salelevel7val,mkomisi_pp.salelevel8,mkomisi_pp.salelevel8val,
			mkomisi_pp.salelevel9,mkomisi_pp.salelevel9val,mkomisi_pp.salelevel10,mkomisi_pp.salelevel10val,
			mkomisi.salelevel1 AS normal1,mkomisi.salelevel2 AS normal2,
			mkomisi.salelevel3 AS normal3,mkomisi.salelevel4 AS normal4,
			mkomisi.salelevel5 AS normal5,mkomisi.salelevel6 AS normal6,
			mkomisi.salelevel7 AS normal7,mkomisi.salelevel8 AS normal8,
			mkomisi.salelevel9 AS normal9,mkomisi.salelevel10 AS normal10,');
			$this->db->from('mkomisi');
	        $this->db->join("mkomisi_pp", "mkomisi.periode = mkomisi_pp.periode AND mkomisi.noid = mkomisi_pp.noid");
    	}else{
	        $this->db->select('mkomisi.personal,mkomisi.personalval,
			mkomisi.salelevel1,mkomisi.salelevel1val,mkomisi.salelevel2,mkomisi.salelevel2val,
			mkomisi.salelevel3,mkomisi.salelevel3val,mkomisi.salelevel4,mkomisi.salelevel4val,
			mkomisi.salelevel5,mkomisi.salelevel5val,mkomisi.salelevel6,mkomisi.salelevel6val,
			mkomisi.salelevel7,mkomisi.salelevel7val,mkomisi.salelevel8,mkomisi.salelevel8val,
			mkomisi.salelevel9,mkomisi.salelevel9val,mkomisi.salelevel10,mkomisi.salelevel10val,');
			$this->db->from('mkomisi');
    	}
        $this->db->where('mkomisi.periode',$periode);
    	$this->db->where('mkomisi.noid',$noid);
		$query = $this->db->get();
		//ieu mastaer
		//print_r($this->db->last_query());exit();
		//print_r($query->row());exit();
        return $query->row();
    }
	function get_newnoid() 
	{	$init="MM".date("ym");
    	$query = $this->db->query("SELECT noid FROM mmembers WHERE noid like '$init%' ORDER BY noid DESC LIMIT 1");    	
		if ($row = $query->row()){
			$hasil=$row->noid ;
			$hasil = substr($hasil,6,5);
			$hasil = $hasil + 1;
			$hasil = $init.substr("00000",0, 5 - strlen($hasil)).$hasil;			
		}else{
			$hasil=$init."00001";
		}
		return $hasil;	
	} 
	function get_royaltynet($noid,$periode)
    {
        $this->db->select('periode,noid,personal,sponsor,sponsorval,netlevel1,netlevel1val,netlevel2,netlevel2val,netlevel3,netlevel3val,netlevel4,netlevel4val,
				netlevel5,netlevel5val,netlevel6,netlevel6val,netlevel7,netlevel7val,netlevel8,netlevel8val,netlevel9,netlevel9val,netlevel10,netlevel10val');
    	$this->db->where('periode',$periode);
    	$this->db->where('noid',$noid);
		$query = $this->db->get('mkomisi');
        return $query->row();
    }
	function get_omzet($periode)
	{
		$query = $this->db->query("SELECT SUM(mutasi.penjualan) as omzet FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode='$periode' AND mbarang.jenis = 1 LIMIT 1");
		if ($row = $query->row()){$omzet=$row->omzet;}else{$omzet=0;}
		return $omzet;	
	}
	function get_omzeta($periode)
	{	$query = $this->db->query("SELECT SUM(mkomisiroyalti.personal)+SUM(mkomisiroyalti.salejalur1)+SUM(mkomisiroyalti.salejalur2)+
				SUM(mkomisiroyalti.salejalur3)+SUM(mkomisiroyalti.salejalur4)+SUM(mkomisiroyalti.salejalur5) AS totalall 
				FROM mkomisiroyalti WHERE mkomisiroyalti.periode = '".$periode."'");
		$row = $query->row();
		if ($row->totalall){
			$omzeta=$row->totalall;
		}else{
			$omzeta=0;
		}
		return $omzeta;
	}
	function check_passup($periode)
	{	$query = $this->db->query("SELECT passup FROM mkomisi_pp WHERE periode='$periode' LIMIT 1");
		if ($row = $query->row()){
			$passup=$row->passup;
		}else{
			$passup=0;
		}
		return $passup;
	}
	function get_komisiother($noid,$periode)
    {
        $this->db->select('mmembers.noid,mkomisi.personal,mkomisi.salejalur1,mkomisi.salejalur2,mkomisi.salejalur3,mkomisi.salejalur4,mkomisi.salejalur5');
		$this->db->from('mmembers');
		$this->db->join("mkomisi", "mmembers.noid = mkomisi.noid");
        $this->db->where('mkomisi.periode',$periode);
    	$this->db->where('mmembers.noid',$noid);
		$query = $this->db->get();
        return $query->row();
    }
	function get_omzeta_pu($periode)
	{	$query = $this->db->query("SELECT SUM(mkomisiroyalti_pu.personal)+SUM(mkomisiroyalti_pu.salejalur1)+SUM(mkomisiroyalti_pu.salejalur2)+
				SUM(mkomisiroyalti_pu.salejalur3)+SUM(mkomisiroyalti_pu.salejalur4)+SUM(mkomisiroyalti_pu.salejalur5) AS totalall 
				FROM mkomisiroyalti_pu WHERE mkomisiroyalti_pu.periode = '".$periode."'");
		$row = $query->row();
		if ($row->totalall){
			$omzeta=$row->totalall;
		}else{
			$omzeta=0;
		}
		return $omzeta;
	}
	function get_recordjaminan_pu($periode,$noid)
	{
		$this->db->select('salejalur1,salejalur2,salejalur3,salejalur4,salejalur5,(personal+salejalur1+salejalur2+salejalur3+salejalur4+salejalur5) as omzetg');
		$this->db->from('mkomisi_pp');
		$this->db->where('periode', $periode);
		$this->db->where('noid', $noid);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->row();
	}
	function countbelanja($noid,$periode)
	{
		$this->db->select('ttransjualhead.nojual');
		$this->db->from('ttransjualhead');
		$this->db->join("ttransjualdetail", "ttransjualhead.nojual = ttransjualdetail.nojual");
		$this->db->where('ttransjualhead.noid',$noid);
		$this->db->where('DATE_FORMAT(ttransjualhead.tgljual,"%y%m")',$periode);
		$query = $this->db->count_all_results();
        return $query;
	}
	function countbelanjanet($noid,$periode)
	{ 	$this->db->select('mmembersjual.periode');
		$this->db->from('mmembersjual');
//		$this->db->join("mmembers", "mmembersjual.noid = mmembers.noid");
//		$this->db->join("mmembersjual AS mmembersjualupline", "mmembersjual.nouplinebuy = mmembersjualupline.noidbuy");
//		$this->db->join("mmembers AS mmembersupline", "mmembersjualupline.noid = mmembersupline.noid");
		$this->db->join("ttransjualhead", "mmembersjual.nojual = ttransjualhead.nojual");
		$this->db->where('mmembersjual.noid',$noid);
		$this->db->where('mmembersjual.periode',$periode);
		$query = $this->db->count_all_results();
        return $query;
	}
	function get_recordjual($off,$lim,$noid,$periode)
	{
		$this->db->select('ttransjualhead.nojual,ttransjualhead.tgljual,ttransjualdetail.kodebarang,
			mbarang.namabarang,mbarang.hargajualmember,ttransjualdetail.jumlah');
		$this->db->from('ttransjualhead');
		$this->db->join("ttransjualdetail", "ttransjualhead.nojual = ttransjualdetail.nojual");
		$this->db->join("mbarang", "ttransjualdetail.kodebarang = mbarang.kodebarang");
		$this->db->where('ttransjualhead.noid',$noid);
		$this->db->where('DATE_FORMAT(ttransjualhead.tgljual,"%y%m")',$periode);
		$this->db->order_by('ttransjualhead.nojual','DESC');
		$this->db->limit($lim, $off);
		$query = $this->db->get();
		return $query->result();
	}
}
