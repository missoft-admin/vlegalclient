<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tproduk_model extends CI_Model {

	var $tableName = "penjualan";
    var $fieldPrefix = "";
	
	var $tableNameExport = "ekspor";
	var $tableNameExportDetail = "ekspor_detail";
	
	var $tableNameProses = "proses_lanjut";
	var $tableNameLain 	= "pengeluaran_lain";

	var $tblEksporRealisasi = "realisasiekspor";
	var $tblEksporRealisasiDetail = "realisasiekspor_detail";

	public function __construct()
	{
		$this->load->database();
	}
	
	function showTables()
	{
		$query = "show tables";
		$rs = $this->db->query($query);
		return $rs->result_array();
	}

  function delAkun($id){ 
        $data = array('stdelete' => 0);
        return $this->db->where('idjual',$id)
                 ->update($this->tableName,$data);
  }
  function delEkspor($id){ 
        $data = array('stdelete' => 0);
        return $this->db->where('idekspor',$id)
                 ->update($this->tblEksporRealisasi,$data);
  }

	function getAllItems()
	{
		$this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->order_by($this->fieldPrefix."nama","DESC");
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}

	function countItems()
	{
		$this->db->select("COUNT(*) as count");
		$this->db->from($this->tableName);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array['count'];
	}

	function countItemByClientId($client)
	{
		$this->db->select("COUNT(*) as count");
		$this->db->from($this->tableName);
		$this->db->where("client_id",$client);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array['count'];
	}

	function getLimitedItems($offset,$limit)
	{
		$this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->order_by($this->fieldPrefix."nick","ASC");
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getLokalByLastPeriode(){
		$this->db->select("p.*");
		$this->db->from($this->tableName . " p ");
		$this->db->where("client_id",$_SESSION['client_id']); 	
		$this->db->order_by("periode_awal","DESC");
		$this->db->order_by("periode_akhir","DESC");
		$this->db->limit(1);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}

	function getItemById($id)
	{
		$this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("idjual",$id);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getLokalUpdate($id){
		return $this->db
			->select('t1.*,
					concat(t4.idbuyer,\',\',concat(t4.buyer,\' [\', idnegara  ,\']\')) as buyer,
					concat(t2.propid,\',\',t2.nama) as provinsi,
					concat(t3.kabid,\',\',t3.nama) as kabupaten,
					concat(t5.idproduk,\',\',t5.produk) as produk,
					concat(kayu1.idkayu,\',\',kayu1.nama) as kayu1,
					concat(kayu2.idkayu,\',\',kayu2.nama) as kayu2,
					concat(kayu3.idkayu,\',\',kayu3.nama) as kayu3,
					')
			->from($this->tableName.' as t1')
			->join('kit_propinsi as t2','t2.propid = t1.idpropinsi','left')
			->join('kit_kabupaten as t3','t3.kabid = t1.idkota','left')
			->join('kit_buyer as t4','idbuyer','left')
			->join('kit_produk AS t5','idproduk','left')
			->join('kit_jeniskayu as kayu1','kayu1.idkayu = t1.idkayu','left')
			->join('kit_jeniskayu as kayu2','kayu2.idkayu = t1.idkayu2','left')
			->join('kit_jeniskayu as kayu3','kayu3.idkayu = t1.idkayu3','left')
			->where('idjual',$id)
			->where('stdelete',1)
			->where('client_id',$_SESSION['client_id'])
			->get()
			->row_array();
	}

	function getEksporUpdate($id)
	{
		return $this->db
			->select("t1.*,
					t2.iddischarge,t2.uraian dis_uraian,t2.kode dis_kode,
                    t3.idloading,t3.uraian loading_uraian,t3.kode loading_kode,
                    t4.negara nama_negara,
                    t5.iduang,t5.kode kode_uang,t5.nama nama_uang,
                    t6.idbuyer,t6.buyer,t6.idnegara negarabuyer
                    	")
			->from($this->tblEksporRealisasi.' t1')
			->join('kit_discharge t2','t1.discharge = t2.iddischarge','left')
			->join('kit_loading t3','t1.loading = t3.idloading','left')
			->join('kit_negara t4','idnegara','left')
			->join('kit_matauang t5','t1.valuta = t5.kode','left')
			->join('kit_buyer t6','idbuyer','left')
			->where("idekspor",$id)
			->get()
			->row_array();
    }

	function getItemEksporById($id)
	{
		$this->db->select("*");
		$this->db->from($this->tblEksporRealisasi);
		$this->db->where("idekspor",$id);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}

function addUpLokalItem($where=array())
{
		$data['client_id']		= $_SESSION['client_id'];
		$data['bulan']			= date('m');
		$data['tahun']			= date('Y');
		$data['periode_awal']	= decode_date($this->input->post('new-periode_awal'));
		$data['periode_akhir']	= decode_date($this->input->post('new-periode_akhir'));
		$data['idbuyer']		= $this->input->post('new-buyer');
		$data['idpropinsi']		= $this->input->post('new-provinsi');
		$data['idkota']			= $this->input->post('new-kabupaten');
		$data['invoice']		= $this->input->post('new-invoice');
		$data['nilai']			= $this->input->post('new-nilai');
		$data['tglterbit']		= decode_date($this->input->post('new-tglterbit'));
		$data['idproduk']		= $this->input->post('new-produk');
		$data['jumlah']			= $this->input->post('new-jumlah');
		$data['satuan_jumlah']	= $this->input->post('new-satjumlah');
		$data['volume']			= $this->input->post('new-volume');
		$data['satuan_volume']	= $this->input->post('new-satvolume');
		$data['berat']			= $this->input->post('new-berat');
		$data['satuan_berat']	= $this->input->post('new-satberat');
		$data['idkayu']			= $this->input->post('new-listkayu');
		$data['idkayu2']		= $this->input->post('new-listkayu2');
		$data['idkayu3']		= $this->input->post('new-listkayu3');
		$data['nonota']			= $this->input->post('new-nonota');
		$data['packinglist']	= $this->input->post('new-packinglist');
		$data['stdelete']		= 1;
if(empty($where)){
    $this->db->insert($this->tableName, $data);
    	return $this->db->insert_id();
	}else{
    $this->db->update($this->tableName, $data, $where);
	    return true;
	}
}

	function deletelokal($id)
	{
		$this->db->where('idjual', $id);
		$this->db->delete($this->tableName);
		return true;
	}

	function countItemEksportByClientId($client,$keyword)
	{
		$this->db->select("COUNT(*) as count");
		$this->db->from($this->tableNameExport);
		$this->db->where("client_id",$client);
		if (!empty($keyword)) {
			$this->db->like("invoice",$keyword);
		}
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array['count'];
	}
	
	function getEksporList($client,$keyword='',$offset,$limit){
		$this->db->select("e.idekspor, e.client_id, e.bulan, e.tahun, e.iso, b.buyer, n.negara, e.invoice, l.uraian as loading, 
							d.uraian as discharge, e.invoice, e.tglinvoice, e.peb, e.tglship, e.vessel, e.bl, e.packinglist,
							e.etpik,e.received_date,e.status_dokumen, e.sertifikat, e.npwp, e.keterangan, e.no_urut, e.submitdate, e.status, e.status_liu,vs.vid,vs.downloaded,vs.no_dokumen");
		$this->db->from($this->tableNameExport . " e ");
		$this->db->join("kit_negara n","n.idnegara=e.idnegara ","LEFT");
		$this->db->join("kit_loading l","l.idloading=e.loading ","LEFT");
		$this->db->join("kit_discharge d","d.iddischarge=e.discharge ","LEFT");
		$this->db->join("kit_buyer b","b.idbuyer=e.idbuyer","LEFT");
		$this->db->join("vlegal_status vs","vs.idekspor=e.idekspor AND vs.status = 'send'","LEFT");
		$this->db->where("client_id",$client);
		if (!empty($keyword)) {
			$this->db->like("e.invoice",$keyword);
		}
        
        $this->db->order_by("tahun","DESC");
        $this->db->order_by("no_urut","DESC");
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
    
    function getVlegalList($idekspor){
		$this->db->select("vlegal_status.*");
		$this->db->from("vlegal_status");
		$this->db->where("idekspor",$idekspor);
		$this->db->where_in("status",array('send','cancel'));
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
    
    function get_invoice($idekspor)
    {
        $this->db->select('invoice');
        $this->db->where('idekspor',$idekspor);
        $query = $this->db->get('ekspor');
        return $query->row('invoice');
    }
	
	function getLastEksporData($client){
		$this->db->select("e.*");
		$this->db->from($this->tableNameExport . " e ");
		$this->db->where("client_id",$client);
		$this->db->order_by("no_urut","DESC");
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getEksporByIdEskpor($id){
		$this->db->select("e.idekspor, e.client_id, e.bulan, e.tahun, e.iso, b.buyer, n.negara, e.invoice, l.uraian as loading, 
							d.uraian as discharge, e.invoice, e.tglinvoice, e.peb, e.tglship, e.vessel, e.bl, e.packinglist,e.alamat,
							e.etpik, e.sertifikat, e.npwp, e.keterangan, e.no_urut, e.submitdate, e.status, e.periode_awal,e.periode_akhir,
							c.client_nama, e.status_liu, e.valuta, m.nama as matauang, e.status_dokumen, e.stuffing");
		$this->db->from($this->tableNameExport . " e ");
		$this->db->join("kit_negara n","n.idnegara=e.idnegara ","LEFT");
		$this->db->join("kit_loading l","l.idloading=e.loading ","LEFT");
		$this->db->join("kit_discharge d","d.iddischarge=e.discharge ","LEFT");
		$this->db->join("kit_buyer b","b.idbuyer=e.idbuyer","LEFT");
		$this->db->join("kit_matauang m","m.kode=e.valuta","LEFT");
		$this->db->join("clients c","c.client_id=e.client_id","LEFT");
		$this->db->where("idekspor",$id); 	
		$this->db->order_by("no_urut","ASC");
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getExporToWord($id){
		$this->db->select("e.idekspor, e.client_id, e.iso, b.buyer, n.negara,n.idnegara, e.invoice, l.kode as kode_loading, l.uraian as loading, 
							d.kode as kode_discharge,d.uraian as discharge, e.invoice, e.tglinvoice, e.peb, e.tglship, e.vessel, e.bl, e.packinglist,e.alamat,
							e.etpik, e.sertifikat, e.npwp, e.keterangan, e.no_urut, e.submitdate, e.status, e.periode_awal,e.periode_akhir,e.valuta,
							c.client_nama, c.client_nick, (select sum(nilai) from ".$this->tableNameExportDetail." de where de.idekspor = ".$id.") as totalnilai,
							(select sum(volume) from ".$this->tableNameExportDetail." de where de.idekspor = ".$id.") as totalvolume,
							(select sum(berat) from ".$this->tableNameExportDetail." de where de.idekspor = ".$id.") as totalberat,
							(select sum(jumlah) from ".$this->tableNameExportDetail." de where de.idekspor = ".$id.") as totaljumlah");
		$this->db->from($this->tableNameExport . " e ");
		$this->db->join("kit_negara n","n.idnegara=e.idnegara ","LEFT");
		$this->db->join("kit_loading l","l.idloading=e.loading ","LEFT");
		$this->db->join("kit_discharge d","d.iddischarge=e.discharge ","LEFT");
		$this->db->join("kit_buyer b","b.idbuyer=e.idbuyer","LEFT");
		$this->db->join("clients c","c.client_id=e.client_id","LEFT");
		$this->db->where("idekspor",$id); 	
		$this->db->order_by("no_urut","ASC");
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getEksporDetailByIdEskpor($id){
		$this->db->select("ed.*, pr.produk, n.negara,n.idnegara, jk.nama as kayu, 
			(select negara from kit_negara where idnegara = idnegaraasal2) as negara2, idnegaraasal2 as idnegara2,
			(select negara from kit_negara where idnegara = idnegaraasal3) as negara3, idnegaraasal3 as idnegara3,
			(select negara from kit_negara where idnegara = idnegaraasal4) as negara4, idnegaraasal4 as idnegara4,
			(select negara from kit_negara where idnegara = idnegaraasal5) as negara5, idnegaraasal5 as idnegara5,
			(select negara from kit_negara where idnegara = idnegaraasal6) as negara6, idnegaraasal6 as idnegara6,
			(select negara from kit_negara where idnegara = idnegaraasal7) as negara7, idnegaraasal7 as idnegara7,
			(select negara from kit_negara where idnegara = idnegaraasal8) as negara8, idnegaraasal8 as idnegara8,
			(select negara from kit_negara where idnegara = idnegaraasal9) as negara9, idnegaraasal9 as idnegara9,
			(select negara from kit_negara where idnegara = idnegaraasal10) as negara10, idnegaraasal10 as idnegara10,
			(select nama from kit_jeniskayu where idkayu = idkayu2) as kayu2,
			(select nama from kit_jeniskayu where idkayu = idkayu3) as kayu3,
			(select nama from kit_jeniskayu where idkayu = idkayu4) as kayu4,
			(select nama from kit_jeniskayu where idkayu = idkayu5) as kayu5,
			(select nama from kit_jeniskayu where idkayu = idkayu6) as kayu6,
			(select nama from kit_jeniskayu where idkayu = idkayu7) as kayu7,
			(select nama from kit_jeniskayu where idkayu = idkayu8) as kayu8,
			(select nama from kit_jeniskayu where idkayu = idkayu9) as kayu9,
			(select nama from kit_jeniskayu where idkayu = idkayu10) as kayu10
		");
		$this->db->from($this->tableNameExportDetail . " ed ");
		$this->db->join("kit_negara n","n.idnegara=ed.idnegaraasal ","LEFT");
		$this->db->join("kit_produk pr","pr.idproduk=ed.idproduk","LEFT");
		$this->db->join("kit_jeniskayu jk","jk.idkayu=ed.idkayu","LEFT");
		$this->db->where("idekspor",$id); 	
		$this->db->order_by("ed.iddetekspor","ASC");
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function addUpEkspor($where=array())
	{
		$d=$this->getItemEksporById(decryptURL($_GET['id']));
		if(!empty($where)){
		 $nourut=$d['no_urut'];
		}else{
		 $nourut=$this->getNoUrut($_SESSION['client_id']);
		}
		// print_r($this->getNoUrut($_SESSION['client_id']));exit();
		$data = array(
			'client_id'		=> $_SESSION['client_id'],
			'bulan'			=> date('m'),
			'tahun'			=> date('Y'),
			'periode_awal'	=> decode_date($this->input->post('new-periode_awal')),
			'periode_akhir'	=> decode_date($this->input->post('new-periode_akhir')),
			'idbuyer'		=> $this->input->post('new-buyer'),
			'alamat'		=> $this->input->post('new-alamat'),
			'idnegara'		=> $this->input->post('new-negara'),
			'iso'			=> $this->input->post('new-iso'),
			'loading'		=> $this->input->post('new-loading'),
			'discharge'		=> $this->input->post('new-discharge'),
			'keterangan'	=> $this->input->post('new-keterangan'),
			'invoice'		=> $this->input->post('new-invoice'),
			'tglinvoice'	=> decode_date($this->input->post('new-tglinvoice')),
			'peb'			=> $this->input->post('new-peb'),
			'bl'			=> $this->input->post('new-bl'),
			'packinglist'	=> $this->input->post('new-packinglist'),
			'vessel'		=> $this->input->post('new-vessel'),
			'tglship'		=> decode_date($this->input->post('new-tglship')),
			'etpik'			=> $this->input->post('new-etpik'),
			'sertifikat'	=> $this->input->post('new-sertifikat'),
			'npwp'			=> $this->input->post('new-npwp'),
			'valuta'		=> $this->input->post('new-matauang'),
			'no_urut'		=> $nourut,
			'submitdate'	=> date("Y-m-d H:i:s"),
			'status'		=> $this->input->post('new-status'),
			'stuffing'		=> $this->input->post('new-stuffing'),
			'stdelete'		=>1
		);
		$tipeE=$this->input->post('new-status_edit');
		switch($tipeE){
			case "edit"	:
				$data['status_liu']		= $this->input->post('new-status_liu');
				$data['status_dokumen']	= $d['status_dokumen'];
				break;
			case "replace"	:
				$data['status_liu']		= "replace";
				$data['status_dokumen']	= "Editing";
				break;
			case "extend"	:
				$data['status_liu']		= "extend";
				$data['status_dokumen']	= "Editing";
				break;
			case "cancel"	:
				$data['status_liu']		= "cancel";
				$data['status_dokumen']	= "Editing";
				break;
		}
if(empty($where)){
    $this->db->insert($this->tblEksporRealisasi, $data);
    	return $this->db->insert_id();
	}else{
    $this->db->update($this->tblEksporRealisasi, $data, $where);
	    return true;
	}
	}

	function updateExportItemById($rs,$id)
	{
		$data = array(
			'client_id'		=> $rs['client_id'],
			'bulan'			=> $rs['bulan'],
			'tahun'			=> $rs['tahun'],
			'periode_awal'	=> $rs['periode_awal'],
			'periode_akhir'	=> $rs['periode_akhir'],
			'idbuyer'		=> $rs['idbuyer'],
			'alamat'		=> $rs['alamat'],
			'idnegara'		=> $rs['idnegara'],
			'iso'			=> $rs['iso'],
			'loading'		=> $rs['loading'],
			'discharge'		=> $rs['discharge'],
			'keterangan'	=> $rs['keterangan'],
			'invoice'		=> $rs['invoice'],
			'tglinvoice'	=> $rs['tglinvoice'],
			'peb'			=> $rs['peb'],
			'bl'			=> $rs['bl'],
			'packinglist'	=> $rs['packinglist'],
			'vessel'		=> $rs['vessel'],
			'tglship'		=> $rs['tglship'],
			'etpik'			=> $rs['etpik'],
			'sertifikat'	=> $rs['sertifikat'],
			'npwp'			=> $rs['npwp'],
			'keterangan'	=> $rs['keterangan'],
			'valuta'		=> $rs['valuta'],
            'no_urut'		=> $rs['no_urut'],
			'status'		=> $rs['status'],
			'status_liu'	=> $rs['status_liu'],
			'status_dokumen'=> $rs['status_dokumen'],
			'stuffing'		=> $rs['stuffing']
		);
        $this->db->update($this->tableNameExport, $data, array('idekspor' => $id));
	}
	
	function getdetaileksporbyid($id){
		$this->db->select("*");
		$this->db->from($this->tableNameExportDetail);
		$this->db->where("iddetekspor",$id);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function deleteekspor($id)
	{
		$this->db->where('idekspor', $id);
		$this->db->delete($this->tableNameExport);
		
		$this->db->where('idekspor', $id);
		$this->db->delete($this->tableNameExportDetail);
		return true;
	}
	
	function deletedetailekspor($id){
		$this->db->where('iddetekspor', $id);
		$this->db->delete($this->tableNameExportDetail);
		return true;
	}
	
	function getNoUrut($client){
		
		$this->db->select("( no_urut + 1 ) as no_urut");
		$this->db->from($this->tblEksporRealisasi);
		$this->db->where("client_id",$client); //dimatiin dulu by cliennya
		// $this->db->where("bulan",date("m"));
		// $this->db->where("tahun",date("Y"));
		$this->db->order_by("no_urut","DESC");
		$this->db->limit(1);
		$query = $this->db->get();
		$array = $query->row_array();
        return $array['no_urut'];
        // return $this->db->last_query();
	}
	
	function addNewEksporDetailItem($rs)
	{
		$data = array(
			'fako'			=> $rs['fako'],
			'hscode'		=> $rs['hscode'],
			'idproduk'		=> $rs['idproduk'],
			'satuan_jumlah'	=> $rs['satuan_jumlah'],
			'idkayu'		=> $rs['idkayu'],
			'idnegaraasal'	=> $rs['negaraasal'],
			'idkayu2'		=> $rs['idkayu2'],
			'idnegaraasal2'	=> $rs['negaraasal2'],
			'idkayu3'		=> $rs['idkayu3'],
			'idnegaraasal3'	=> $rs['negaraasal3'],
            'idkayu4'		=> $rs['idkayu4'],
			'idnegaraasal4'	=> $rs['negaraasal4'],
            'idkayu5'		=> $rs['idkayu5'],
			'idnegaraasal5'	=> $rs['negaraasal5'],
            'idkayu6'		=> $rs['idkayu6'],
			'idnegaraasal6'	=> $rs['negaraasal6'],
            'idkayu7'		=> $rs['idkayu7'],
			'idnegaraasal7'	=> $rs['negaraasal7'],
            'idkayu8'		=> $rs['idkayu8'],
			'idnegaraasal8'	=> $rs['negaraasal8'],
            'idkayu9'		=> $rs['idkayu9'],
			'idnegaraasal9'	=> $rs['negaraasal9'],
            'idkayu10'		=> $rs['idkayu10'],
			'idnegaraasal10' => $rs['negaraasal10'],
			'nilai'			=> $rs['nilai'],
			'nilai_cnf'		=> $rs['nilai_cnf'],
			'nilai_cif'		=> $rs['nilai_cif'],
			'jumlah'		=> $rs['jumlah'],
			'volume'		=> $rs['volume'],
			'satuan_volume'	=> $rs['satuan_volume'],
			'berat'			=> $rs['berat'],
			'satuan_berat'	=> $rs['satuan_berat'],
			'idekspor'		=> $rs['idekspor']
			
		);
	    $this->db->insert($this->tableNameExportDetail, $data);
	    return $this->db->insert_id();
	}
	
	function UpdateEksporDetailItem($rs,$id)
	{
		$data = array(
			'fako'			=> $rs['fako'],
			'hscode'		=> $rs['hscode'],
			'idproduk'		=> $rs['idproduk'],
			'satuan_jumlah'	=> $rs['satuan_jumlah'],
			'idkayu'		=> $rs['idkayu'],
			'idnegaraasal'	=> $rs['negaraasal'],
			'idkayu2'		=> $rs['idkayu2'],
			'idnegaraasal2'	=> $rs['negaraasal2'],
			'idkayu3'		=> $rs['idkayu3'],
			'idnegaraasal3'	=> $rs['negaraasal3'],
            'idkayu4'		=> $rs['idkayu4'],
			'idnegaraasal4'	=> $rs['negaraasal4'],
            'idkayu5'		=> $rs['idkayu5'],
			'idnegaraasal5'	=> $rs['negaraasal5'],
            'idkayu6'		=> $rs['idkayu6'],
			'idnegaraasal6'	=> $rs['negaraasal6'],
            'idkayu7'		=> $rs['idkayu7'],
			'idnegaraasal7'	=> $rs['negaraasal7'],
            'idkayu8'		=> $rs['idkayu8'],
			'idnegaraasal8'	=> $rs['negaraasal8'],
            'idkayu9'		=> $rs['idkayu9'],
			'idnegaraasal9'	=> $rs['negaraasal9'],
            'idkayu10'		=> $rs['idkayu10'],
			'idnegaraasal10' => $rs['negaraasal10'],
			'nilai'			=> $rs['nilai'],
			'nilai_cnf'		=> $rs['nilai_cnf'],
			'nilai_cif'		=> $rs['nilai_cif'],
			'jumlah'		=> $rs['jumlah'],
			'volume'		=> $rs['volume'],
			'satuan_volume'	=> $rs['satuan_volume'],
			'berat'			=> $rs['berat'],
			'satuan_berat'	=> $rs['satuan_berat'],
			'idekspor'		=> $rs['idekspor']
			
		);
	    $this->db->update($this->tableNameExportDetail, $data, array('iddetekspor' => $id));
	}
	
	function UpdateStatusDokumen($rs,$id)
	{
		$data = array(
			'status_dokumen'	=> $rs['status_dokumen'],
			'received_date'	=> date('Y-m-d H:i:s'),
			'hide'	=> 0
			
		);
	    $this->db->update($this->tableNameExport, $data, array('idekspor' => $id));
	}

	function countItemLainByClientId($client)
	{
		$this->db->select("COUNT(*) as count");
		$this->db->from($this->tableNameLain);
		$this->db->where("client_id",$client);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array['count'];
	}
	
	function getLainList($client,$offset,$limit){
		$this->db->select("p.*, produk, kodehs, jk.nama as kayu");
		$this->db->from($this->tableNameLain . " p ");
		$this->db->join("kit_produk pr","pr.idproduk=p.idproduk ","LEFT");
		$this->db->join("kit_jeniskayu jk","jk.idkayu=p.idkayu ","LEFT");
		$this->db->where("client_id",$client); 	
		$this->db->order_by("submitdate","DESC");
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getLainByLastPeriode($client){
		$this->db->select("p.*, pr.produk, pr.kodehs, jk.nama as kayu");
		$this->db->from($this->tableNameLain . " p ");
		$this->db->join("kit_produk pr","pr.idproduk=p.idproduk ","LEFT");
		$this->db->join("kit_jeniskayu jk","jk.idkayu=p.idkayu ","LEFT");
		$this->db->where("client_id",$client); 	
		$this->db->order_by("periode_awal","DESC");
		$this->db->limit(1);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getLainByID($id){
		$this->db->select("p.*");
		$this->db->from($this->tableNameLain . " p ");
		$this->db->where("idlain",$id);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
    
    function getSendVlegalByIdEkspor($id){
		$this->db->select("*");
		$this->db->from("vlegal_status");
		$this->db->where("downloaded",0); 	
		$this->db->where("idekspor",$id); 	
		$this->db->where("status",'send');
		$this->db->order_by("vid","DESC");
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
    
    function getSendVlegalByVid($id){
		$this->db->select("*");
		$this->db->from("vlegal_status");
		$this->db->where("vid",$id); 	
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function addNewLainItem($rs)
	{
		$data = array(
			'client_id'		=> $rs['client_id'],
			'bulan'			=> $rs['bulan'],
			'tahun'			=> $rs['tahun'],
			'periode_awal'	=> $rs['periode_awal'],
			'periode_akhir'	=> $rs['periode_akhir'],
			'idproduk'		=> $rs['idproduk'],
			'idkayu'		=> $rs['idkayu'],
			'jumlah'		=> $rs['jumlah'],
			'satuan_jumlah'	=> $rs['satuan_jumlah'],
			'volume'		=> $rs['volume'],
			'satuan_volume'	=> $rs['satuan_volume'],
			'berat'				=> $rs['berat'],
			'satuan_berat'		=> $rs['satuan_berat'],
			'keterangan'	=> $rs['keterangan'],
			'submitdate'	=> $rs['submitdate'],
			'status'		=> $rs['status']
			
		);
	    $this->db->insert($this->tableNameLain, $data);
	    return $this->db->insert_id();
	}

	function updateLainItemById($rs,$id)
	{
		$data = array(
			'client_id'		=> $rs['client_id'],
			'bulan'			=> $rs['bulan'],
			'tahun'			=> $rs['tahun'],
			'periode_awal'	=> $rs['periode_awal'],
			'periode_akhir'	=> $rs['periode_akhir'],
			'idproduk'		=> $rs['idproduk'],
			'idkayu'		=> $rs['idkayu'],
			'jumlah'		=> $rs['jumlah'],
			'satuan_jumlah'	=> $rs['satuan_jumlah'],
			'volume'		=> $rs['volume'],
			'satuan_volume'	=> $rs['satuan_volume'],
			'berat'				=> $rs['berat'],
			'satuan_berat'		=> $rs['satuan_berat'],
			'keterangan'	=> $rs['keterangan'],
			'submitdate'	=> $rs['submitdate'],
			'status'		=> $rs['status']
		);
        $this->db->update($this->tableNameLain, $data, array('idlain' => $id));
	}
    
    function updateDownloadStatus($vid)
    {
         $this->db->update('vlegal_status', array('downloaded' => 1), array('vid' => $vid));
         return true;
    }
	
	function deleteLain($id){
		$this->db->where('idlain', $id);
		$this->db->delete($this->tableNameLain);
		return true;
	}

	function checkSettingPortLoading(){
		return $this->db
			->select("*")
			->from("setting_portloading")
			->where("client_id",$_SESSION['client_id'])
			->get()
			->result_array();
	}
	
	function checkStuffingById($id){
		$this->db->select("*");
		$this->db->from("kit_stuffing");
		$this->db->where("client_id",$id);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
    
    function check_invoice($inv_no){
		$this->db->where("invoice",$inv_no);
		$count = $this->db->count_all_results('ekspor');
        if($count > 0){
            return false;
        }else{
            return true;
        }
	}
    
    function check_hscode($hscode){
        $hscode = trim(str_replace('.','',$hscode));
        if(strlen($hscode) == 8){
            $this->db->where("hscode_8",$hscode);
            $count = $this->db->count_all_results('hscode');
            if($count > 0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
	}
    
    
    function validasi_replace($idekspor){
        $this->db->where("idekspor",$idekspor);
        $this->db->where("status_liu","replace");
        $this->db->where_in("client_id",array(2,3,4,5,6,7,8,9));
        $this->db->where("tglship < CURDATE()",null,false);
        $count = $this->db->count_all_results('ekspor');
        if($count > 0){
            return false;
        }else{
            return true;
        }
	}
    
    function validasi_shipment($idekspor){
        $this->db->where("idekspor",$idekspor);
        $this->db->where("status_liu","Draft");
        $this->db->where_in("client_id",array(2,3,4,5,6,7,8,9));
        $this->db->where("tglship",'0000-00-00');
        $count = $this->db->count_all_results('ekspor');
        if($count > 0){
            return false;
        }else{
            return true;
        }
	}
    
     
    function getDokumenReplace($vid)
    {
        $this->db->select("vlegal_status.invoice,DATE_FORMAT(CURDATE(),'%d %M %Y') postupdate,client_petugas.kota,clients.client_nama, vlegal_status.no_dokumen,client_petugas.nama_petugas,client_petugas.jabatan_petugas",false);
		$this->db->from('vlegal_status');
		$this->db->join("clients","vlegal_status.clientid = clients.client_id");
		$this->db->join("ekspor","vlegal_status.idekspor = ekspor.idekspor");
		$this->db->join("client_petugas","client_petugas.client_id = clients.client_id AND (client_petugas.invoice_prefix = '0' OR client_petugas.invoice_prefix = LEFT(vlegal_status.invoice,4))");
		$this->db->where("vlegal_status.vid",$vid);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function getNegaraTujuan($negara)
    {
        $this->db->from('kit_negara');
        $this->db->like('kit_negara.negara',$negara);
        $this->db->or_like('kit_negara.idnegara',$negara,'after');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function getMataUang($matauang)
    {
        $this->db->from('kit_matauang');
        $this->db->like('kit_matauang.kode',$matauang,'after');
        $this->db->or_like('kit_matauang.nama',$matauang,'after');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function getLoading($loading)
    {
        $this->db->from('kit_loading');
        $this->db->where("(kit_loading.kode LIKE '%$loading%' OR kit_loading.uraian LIKE '%$loading%')",null, false);
        $this->db->where("xstatus",1);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function getPortDischarge($negara,$discharge)
    {
        $this->db->from('kit_discharge');
        $this->db->like('kit_discharge.kode',$negara,'after');
        $this->db->where("(kit_discharge.kode LIKE '%$discharge%' OR kit_discharge.uraian LIKE '%$discharge%')",null, false);
        $query = $this->db->get();
        return $query->result_array();
    }
	
	
}

?>