<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tbahanbaku_model extends CI_Model
{
    private $tblPenerimaan  = "penerimaan";
	private $tblPengeluaran = "pengeluaran";

    public function __construct()
    {
        parent::__construct();
    }

    function dataStatus($id){
        return $this->db->select("xstatus")
                        ->where("idpenerimaan",$id)
                        ->get($this->tblPenerimaan)
                        ->row()
                        ->xstatus;
    }
  function gantiStatus($id){ 
        $status=$this->dataStatus($id);
    
  if($status==0){$status=1;}else if($status==1){$status=0;}

        $data = array('xstatus' => $status);
        return $this->db->where('idpenerimaan',$id)
                 ->update($this->tblPenerimaan,$data);
  }
  function delAkun($id){ 
        $data = array('stdelete' => 0);
        return $this->db->where('idpenerimaan',$id)
                 ->update($this->tblPenerimaan,$data);
  }

    function getItemsByLastPeriodeIN($type)
    {
        return $this->db
            ->select("bb.*, sp.supplier, h.hutan, sf.sertifikasi, s.sortimen, jd.dok, n.negara, jk1.nama as dokumenkayu, jk2.nama as fisikkayu")
            ->join("kit_supplier sp","sp.idsupplier=bb.idsupplier","LEFT")
            ->join("kit_hutan h","h.idhutan=bb.idhutan","LEFT")
            ->join("kit_sertifikasi sf","sf.idsertifikasi=bb.idsertifikasi","LEFT")
            ->join("kit_sortimen s","s.idsortimen=bb.idsortimen","LEFT")
            ->join("kit_jenisdokumen jd","jd.iddok=bb.iddokumen","LEFT")
            ->join("kit_negara n","n.idnegara=bb.idnegara","LEFT")
            ->join("kit_jeniskayu jk1","jk1.idkayu=bb.datadokumen_kayu","LEFT")
            ->join("kit_jeniskayu jk2","jk2.idkayu=bb.datafisik_kayu","LEFT")
            ->where("type_penerimaan",$type)
            ->where("client_id",$_SESSION['client_id'])
            ->order_by("periode_awal","DESC")
            ->limit(1)
            ->get($this->tblPenerimaan . ' bb')
            ->row_array();
    }

function getItemsByLastPeriodeOUT($type)
{
return $this->db
        ->select("bb.*, pr.produk, jk.nama as kayu, s.sortimen, jp.pengeluaran")
        ->from($this->tblPengeluaran . ' bb')
        ->join("kit_produk pr","pr.idproduk=bb.idproduk","LEFT")
        ->join("kit_jeniskayu jk","jk.idkayu=bb.idkayu","LEFT")
        ->join("kit_sortimen s","s.idsortimen=bb.idsortimen","LEFT")
        ->join("kit_jenispengeluaran jp","jp.idpeng=bb.jenis_pengeluaran","LEFT")
        ->where("type_pengeluaran",$type)
        ->where("client_id",$_SESSION['client_id'])
        ->order_by("periode_awal","DESC")
        ->limit(1)
        ->get()
        ->row_array();
}

function showUpdateIN($type,$id){
    return $this->db
        ->select("bb.*, sp.supplier, h.hutan, sf.sertifikasi, s.sortimen, jd.dok, jk1.nama as dokumenkayu, jk2.nama as fisikkayu")
        ->from($this->tblPenerimaan . ' bb')
        ->join("kit_supplier sp","sp.idsupplier=bb.idsupplier","LEFT")
        ->join("kit_hutan h","h.idhutan=bb.idhutan","LEFT")
        ->join("kit_sertifikasi sf","sf.idsertifikasi=bb.idsertifikasi","LEFT")
        ->join("kit_sortimen s","s.idsortimen=bb.idsortimen","LEFT")
        ->join("kit_jenisdokumen jd","jd.iddok=bb.iddokumen","LEFT")
        ->join("kit_jeniskayu jk1","jk1.idkayu=bb.datadokumen_kayu","LEFT")
        ->join("kit_jeniskayu jk2","jk2.idkayu=bb.datafisik_kayu","LEFT")
        ->where("type_penerimaan",$type)
        ->where("stdelete",1)
        ->where("idpenerimaan",$id)
        ->where("client_id",$_SESSION['client_id'])
        ->order_by("idpenerimaan","DESC")
        ->get()
        ->row_array();
}

function showUpdateOUT($type,$id){
    return $this->db
        ->select("bb.*, pr.produk, jk.nama as kayu, s.sortimen, jp.pengeluaran")
        ->from($this->tblPengeluaran . ' bb')
        ->join("kit_produk pr","pr.idproduk=bb.idproduk","LEFT")
        ->join("kit_jeniskayu jk","jk.idkayu=bb.idkayu","LEFT")
        ->join("kit_sortimen s","s.idsortimen=bb.idsortimen","LEFT")
        ->join("kit_jenispengeluaran jp","jp.idpeng=bb.jenis_pengeluaran","LEFT")
        ->where("type_pengeluaran",$type)
        ->where("stdelete",1)
        ->where("idpengeluaran",$id)
        ->where("client_id",$_SESSION['client_id'])
        ->order_by("idpengeluaran","DESC")
        ->get()
        ->row_array();
}

    function InUpItem($where=array())
    {
$string=$_POST['new-kabupaten'];
$validate=strpos($string, ",");
if($_GET['tipe']=='lokal'){
    if($validate){
        $idnegara=explode(',', $string)[1];
    }else{
        $idnegara=$string;
    }
}else{
        $idnegara=$_POST['new-negara'];
}
// print_r($idnegara);exit();
        $data = array();
        $data['periode_awal']               = decode_date($this->input->post('new-periode_awal'));
        $data['periode_akhir']              = decode_date($this->input->post('new-periode_akhir'));
        $data['idsupplier']                 = $this->input->post('new-supplier');
        $data['idsertifikasi']              = $this->input->post('new-sertifikasi');
        $data['idnegara']                   = $idnegara;
        $data['idhutan']                    = $this->input->post('new-statushutan');
        $data['iddokumen']                  = $this->input->post('new-dokumen');
        $data['idsortimen']                 = $this->input->post('new-sortimen');
        $data['datadokumen_kayu']           = $this->input->post('new-dokumenkayu');
        $data['datadokumen_jumlah']         = $this->input->post('new-dokjumlah');
        $data['datadokumen_satuan_jumlah']  = $this->input->post('new-doksatjumlah');
        $data['datadokumen_volume']         = $this->input->post('new-dokvolume');
        $data['datadokumen_satuan_volume']  = $this->input->post('new-doksatvolume');
        $data['datadokumen_berat']          = $this->input->post('new-dokberat');
        $data['datadokumen_satuan_berat']   = $this->input->post('new-doksatberat');
        $data['datafisik_kayu']             = $this->input->post('new-fisikkayu');
        $data['datafisik_jumlah']           = $this->input->post('new-fisjumlah');
        $data['datafisik_satuan_jumlah']    = $this->input->post('new-fissatjumlah');
        $data['datafisik_volume']           = $this->input->post('new-fisvolume');
        $data['datafisik_satuan_volume']    = $this->input->post('new-fissatvolume');
        $data['datafisik_berat']            = $this->input->post('new-fisberat');
        $data['datafisik_satuan_berat']     = $this->input->post('new-fissatberat');
        $data['type_penerimaan']            = $_GET['tipe'];
        $data['iduser']                     = $_SESSION['user_id'];
        $data['client_id']                  = $_SESSION['client_id'];
        $data['postdate']                   = date('Y-m-d H:i:s');
        $data['stdelete']            = 1;
        if(empty($where)){
            $this->db->insert($this->tblPenerimaan, $data);
            return $this->db->insert_id();
        }else{
            $this->db->update($this->tblPenerimaan, $data, $where);
        }
    }

    function InUpItemOUT($where=array())
    {
// print_r($idnegara);exit();
        $data = array();
        $data['periode_awal']       = decode_date($this->input->post('new-periode_awal'));
        $data['periode_akhir']      = decode_date($this->input->post('new-periode_akhir'));
        $data['jenis_pengeluaran']  = $this->input->post('new-jenispengeluaran');
        $data['idproduk']           = $this->input->post('new-produk');
        $data['idkayu']             = $this->input->post('new-listkayu');
        $data['idsortimen']         = $this->input->post('new-sortimen');
        $data['jumlah']             = $this->input->post('new-jumlah');
        $data['satuan_jumlah']      = $this->input->post('new-satjumlah');
        $data['volume']             = $this->input->post('new-volume');
        $data['satuan_volume']      = $this->input->post('new-satvolume');
        $data['berat']              = $this->input->post('new-berat');
        $data['satuan_berat']       = $this->input->post('new-satberat');
        $data['type_pengeluaran']   = $_GET['tipe'];
        $data['iduser']             = $_SESSION['user_id'];
        $data['client_id']          = $_SESSION['client_id'];
        $data['postdate']           = date('Y-m-d H:i:s');
        $data['stdelete']           = 1;
        if(empty($where)){
            $this->db->insert($this->tblPengeluaran, $data);
            return $this->db->insert_id();
        }else{
            $this->db->update($this->tblPengeluaran, $data, $where);
        }
    }


}