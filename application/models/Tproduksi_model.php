<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tproduksi_model extends CI_Model
{
    private $table = 'produksi';
    public function __construct()
    {
        parent::__construct();
    }

  function delAkun($id){ 
        $data = array('stdelete' => 0);
        return $this->db->where('idproduksi',$id)
                 ->update($this->table,$data);
  }

  function InUpItem($where=array()){ 
parse_str($this->input->post('data'), $post);
      $data['periode_awal']   = decode_date($post['new-periode_awal']);
      $data['periode_akhir']  = decode_date($post['new-periode_akhir']);
      $data['idproduk']       = $post['new-produk'];
      $data['idkayu']         = $post['new-listkayu'];
      $data['jumlah']         = $post['new-jumlah'];
      $data['satuan_jumlah']  = $post['new-satjumlah'];
      $data['volume']         = $post['new-volume'];
      $data['satuan_volume']  = $post['new-satvolume'];
      $data['berat']          = $post['new-berat'];
      $data['satuan_berat']   = $post['new-satberat'];
      $data['client_id']      = $_SESSION['client_id'];
      $data['bulan']          = decode_date_m($post['new-periode_awal']);
      $data['tahun']          = decode_date_Y($post['new-periode_akhir']);
      $data['tanggal']        = date('Y-m-d');
      $data['stdelete']       =1;
    if(empty($where)){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }else{
        $this->db->update($this->table, $data, $where);
        return true;
    }
  }

  function getItemByClient($id='')
  {
    $this->db
      ->select("p.*, kp.produk, kp.kodehs, jk.nama as kayu,
                CONCAT(`kp`.`idproduk`,\",\",`kp`.`produk`) as selectproduk,
                CONCAT(`jk`.`idkayu`,\",\",`jk`.`nama`) as selectkayu,
                DATE_FORMAT(p.periode_awal, \"%d-%m-%Y\") periode_awal,
                DATE_FORMAT(p.periode_akhir, \"%d-%m-%Y\") periode_akhir")
      ->from($this->table ." p")
      ->join("kit_produk kp","p.idproduk=kp.idproduk","LEFT")
      ->join("kit_jeniskayu jk","p.idkayu=jk.idkayu","LEFT")
      ->where("client_id",$_SESSION['client_id']);
    if(!empty($id)){
    return $this->db
      ->where("idproduksi",$id)
      ->get()
      ->row_array();
    }else{
    return $this->db
      ->get()
      ->result_array();
    }
  }
  
  function getItemByLastPeriode()
  {
    $query = $this->db
      ->select("DATE_FORMAT(p.periode_awal, \"%d-%m-%Y\") periode_awal,DATE_FORMAT(p.periode_akhir, \"%d-%m-%Y\") periode_akhir")
      ->from($this->table ." p")
      ->where("client_id",$_SESSION['client_id'])
      ->order_by("p.periode_awal","DESC")
      ->order_by("p.periode_akhir","DESC")
      ->limit(1)
      ->get();
    $array = $query->row_array();
        return $array;
  }

}
