<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tlainlain_model extends CI_Model {

	private $tblLainlain = "pengeluaran_lain";

	public function __construct()
	{
		$this->load->database();
	}
	
  function delAkun($id){ 
        $data = array('stdelete' => 0);
        return $this->db->where('idlain',$id)
                 ->update($this->tblLainlain,$data);
  }
 
	function getDataList($id){
		return $this->db
				->select("
					t1.idlain,
					t1.jumlah,
					t1.satuan_jumlah,
					t1.volume,
					t1.satuan_volume,
					t1.berat,
					t1.satuan_berat,
					t1.keterangan,
	                DATE_FORMAT(t1.periode_awal, \"%d-%m-%Y\") periode_awal,
	                DATE_FORMAT(t1.periode_akhir, \"%d-%m-%Y\") periode_akhir,
	                CONCAT(COALESCE(`t2`.`idproduk`,''), \",\", COALESCE (`t2`.`produk`,'')) selectproduk,
	                CONCAT(`t3`.`idkayu`,\",\",`t3`.`nama`) selectkayu
						")
				->from($this->tblLainlain." t1")
				->join("kit_produk t2","idproduk","LEFT")
				->join("kit_jeniskayu t3","idkayu","LEFT")
				->where("client_id",$_SESSION['client_id'])
				->where("idlain",$id)
				->order_by("periode_awal","DESC")
				->get()
				->row_array();
	}
	
	function getByLastPeriode(){
		return $this->db
				->select("DATE_FORMAT(periode_awal, \"%d-%m-%Y\") periode_awal,DATE_FORMAT(periode_akhir, \"%d-%m-%Y\") periode_akhir")
				->from($this->tblLainlain)
				->where("client_id",$_SESSION['client_id']) 	
				->order_by("periode_awal","DESC")
				->order_by("periode_akhir","DESC")
				->limit(1)
				->get()
				->row_array();
	}

	function InUpItem($where=array())
	{
parse_str($this->input->post('data'), $post);
		$data = array(
			'client_id'			=> $_SESSION['client_id'],
			'bulan'				=> decode_date_m($post['new-periode_awal']),
			'tahun'				=> decode_date_m($post['new-periode_awal']),
			'periode_awal'		=> decode_date($post['new-periode_awal']),
			'periode_akhir'		=> decode_date($post['new-periode_akhir']),
			'idproduk'			=> $post['new-produk'],
			'idkayu'			=> $post['new-listkayu'],
			'jumlah'			=> $post['new-jumlah'],
			'satuan_jumlah'		=> $post['new-satjumlah'],
			'volume'			=> $post['new-volume'],
			'satuan_volume'		=> $post['new-satvolume'],
			'berat'				=> $post['new-berat'],
			'satuan_berat'		=> $post['new-satberat'],
			'keterangan'	=> $post['new-keterangan'],
			'submitdate'		=> date("Y-m-d H:i:s"),
			'status'			=> 1,
			'stdelete'			=> 1,
			
		);
			if(empty($where)){
			    $this->db->insert($this->tblLainlain, $data);
			    return $this->db->insert_id();
			}else{
        		$this->db->update($this->tblLainlain, $data, $where);
        		return true;
    		}
	}

	function updateProsesItemById($rs,$id)
	{
		$data = array(
			'client_id'			=> $rs['client_id'],
			'bulan'				=> $rs['bulan'],
			'tahun'				=> $rs['tahun'],
			'periode_awal'		=> $rs['periode_awal'],
			'periode_akhir'		=> $rs['periode_akhir'],
			'idproduk'			=> $rs['idproduk'],
			'idkayu'			=> $rs['idkayu'],
			'jumlah'			=> $rs['jumlah'],
			'satuan_jumlah'		=> $rs['satuan_jumlah'],
			'volume'			=> $rs['volume'],
			'satuan_volume'		=> $rs['satuan_volume'],
			'berat'				=> $rs['berat'],
			'satuan_berat'		=> $rs['satuan_berat'],
			'untuk_produksi'	=> $rs['untuk_produksi'],
			'submitdate'		=> $rs['submitdate'],
			'status'			=> $rs['status']
		);
        $this->db->update($this->tblLainlain, $data, array('idlain' => $id));
	}
	
	function deleteProses($id){
		$this->db->where('idlain', $id);
		$this->db->delete($this->tblLainlain);
		return true;
	}
}