<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tproseslanjut_model extends CI_Model {

	private $tblProses = "proses_lanjut";

	public function __construct()
	{
		$this->load->database();
	}
	
  function delAkun($id){ 
        $data = array('stdelete' => 0);
        return $this->db->where('idproses',$id)
                 ->update($this->tblProses,$data);
  }
	
	function getDataList($id){
		return $this->db
				->select("
					t1.idproses,
					t1.jumlah,
					t1.satuan_jumlah,
					t1.volume,
					t1.satuan_volume,
					t1.berat,
					t1.satuan_berat,
	                DATE_FORMAT(t1.periode_awal, \"%d-%m-%Y\") periode_awal,
	                DATE_FORMAT(t1.periode_akhir, \"%d-%m-%Y\") periode_akhir,
	                CONCAT(COALESCE(`t2`.`idproduk`,''), \",\", COALESCE (`t2`.`produk`,'')) selectproduk,
	                CONCAT(`t3`.`idkayu`,\",\",`t3`.`nama`) selectkayu,
	                CONCAT(COALESCE(`t4`.`idproduk`,''), \",\", COALESCE (`t4`.`produk`,'')) select_untukproduksi
						")
				->from($this->tblProses." t1")
				->join("kit_produk t2","idproduk","LEFT")
				->join("kit_jeniskayu t3","idkayu","LEFT")
				->join("kit_produk t4","t4.idproduk=t1.untuk_produksi ","LEFT")
				->where("client_id",$_SESSION['client_id'])
				->where("idproses",$id)
				->order_by("periode_awal","DESC")
				->get()
				->row_array();
	}
	
	function getByLastPeriode(){
		return $this->db
				->select("DATE_FORMAT(periode_awal, \"%d-%m-%Y\") periode_awal,DATE_FORMAT(periode_akhir, \"%d-%m-%Y\") periode_akhir")
				->from($this->tblProses)
				->where("client_id",$_SESSION['client_id']) 	
				->order_by("periode_awal","DESC")
				->order_by("periode_akhir","DESC")
				->limit(1)
				->get()
				->row_array();
	}
	
	function getProsesByID($id){
		$this->db->select("p.*");
		$this->db->from($this->tblProses . " p ");
		$this->db->where("idproses",$id);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function InUpItem($where=array())
	{
parse_str($this->input->post('data'), $post);
		$data = array(
			'client_id'			=> $_SESSION['client_id'],
			'bulan'				=> decode_date_m($post['new-periode_awal']),
			'tahun'				=> decode_date_m($post['new-periode_awal']),
			'periode_awal'		=> decode_date($post['new-periode_awal']),
			'periode_akhir'		=> decode_date($post['new-periode_akhir']),
			'idproduk'			=> $post['new-produk'],
			'idkayu'			=> $post['new-listkayu'],
			'jumlah'			=> $post['new-jumlah'],
			'satuan_jumlah'		=> $post['new-satjumlah'],
			'volume'			=> $post['new-volume'],
			'satuan_volume'		=> $post['new-satvolume'],
			'berat'				=> $post['new-berat'],
			'satuan_berat'		=> $post['new-satberat'],
			'untuk_produksi'	=> $post['new-untukproduksi'],
			'submitdate'		=> date("Y-m-d H:i:s"),
			'status'			=> 1,
			'stdelete'			=> 1,
			
		);
			if(empty($where)){
			    $this->db->insert($this->tblProses, $data);
			    return $this->db->insert_id();
			}else{
        		$this->db->update($this->tblProses, $data, $where);
        		return true;
    		}
	}

	function deleteProses($id){
		$this->db->where('idproses', $id);
		$this->db->delete($this->tblProses);
		return true;
	}
}