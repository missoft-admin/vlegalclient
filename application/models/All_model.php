<?php
defined('BASEPATH') or exit('No direct script access allowed');

class All_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function get_propinsi(){
		$this->db->select('mkota.propinsi');
		$this->db->from('mkota');
		$this->db->order_by('propinsi','ASC');
		$this->db->group_by('propinsi');
		$query=$this->db->get();
		return $query->result();
	}
	function get_kota($propinsi){
		$this->db->select('mkota.*');
		$this->db->where('mkota.propinsi',$propinsi);
		$this->db->from('mkota');
		$this->db->order_by('propinsi','ASC');		
		$query=$this->db->get();
		//print_r($query->result());exit();
		return $query->result();
	}
	function s2_stokis($searchText){
		$this->db->select("mstokies.*");
		$this->db->from('mstokies');
		$this->db->like('mstokies.namastokies',$searchText);
		$query = $this->db->get();
		return $query->result_array();
	}
	function s2_kota($searchText){
		$this->db->select("mkota.*");
		$this->db->from('mkota');
		$this->db->like('mkota.kota',$searchText);
		$this->db->or_like('mkota.propinsi',$searchText);
		$query = $this->db->get();
		return $query->result_array();
	}
	function s2_member($searchText){
		$this->db->select("mmembers.*");
		$this->db->from('mmembers');
		$this->db->like('mmembers.noid',$searchText);
		$this->db->or_like('mmembers.namamembers',$searchText);
		$query = $this->db->get();
		return $query->result_array();
	}
	function header_jual_member($nojual){
		$this->db->select('ttransjualhead.*,mcabang.namacabang,mstokies.namastokies,mmembers.namamembers');
		$this->db->where('ttransjualhead.nojual',$nojual);
		$this->db->from('ttransjualhead');
		$this->db->join('mstokies','mstokies.noidstokies=ttransjualhead.noidstokies');
		$this->db->join('mmembers','mmembers.noid=ttransjualhead.noid');
		$this->db->join('mcabang','mcabang.noidcabang=ttransjualhead.noidcabang');
		$query=$this->db->get();
		// print_r($this->db->last_query());exit();
		 return $query->row();		
	}
	function detail_jual_member($nojual){
		$this->db->select('ttransjualdetail.nojual,mbarang.namabarang,mbarang_zona.hargajualmemberzona AS hargajualmember,
			ttransjualdetail.jumlah,(mbarang_zona.hargajualmemberzona*ttransjualdetail.jumlah) as total');
		$this->db->from('ttransjualdetail');
		$this->db->where('ttransjualdetail.nojual',$nojual);
		$this->db->join("mbarang", "ttransjualdetail.kodebarang = mbarang.kodebarang");
		$this->db->join("ttransjualhead", "ttransjualdetail.nojual = ttransjualhead.nojual");
		$this->db->join("mcabang", "ttransjualhead.noidcabang = mcabang.noidcabang");
		$this->db->join("mbarang_zona", "mcabang.idpropinsi = mbarang_zona.idpropinsi AND ttransjualdetail.kodebarang = mbarang_zona.kodebarang");
		$this->db->order_by('mbarang.namabarang','ASC');
		$query = $this->db->get();
		return $query->result();
	}
}
