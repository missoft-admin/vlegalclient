<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mstock_model extends CI_Model {

	var $tableName = 'stok';
    var $fieldPrefix = '';

	public function __construct()
	{
		$this->load->database();
	}

	function countItemsBahanBakuByClientId($jenis,$client)
	{
		$this->db->select('COUNT(*) as count');
		$this->db->from($this->tableName);
		$this->db->where('jenis',$jenis);
		$this->db->where('client_id',$client);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array['count'];
	}
	
	function getItemStockBahanBakuByBulanTahun($jenis,$client,$offset,$limit)
	{
		$this->db->select('s.*, jk.nama as kayu, sr.sortimen');
		$this->db->from($this->tableName .' s');
		$this->db->join('kit_jeniskayu jk','s.idkayu=jk.idkayu','LEFT');
		$this->db->join('kit_sortimen sr','sr.idsortimen=s.idsortimen','LEFT');
		$this->db->where('client_id',$client);
		$this->db->where('jenis',$jenis);
		$this->db->limit($limit,$offset);
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getItemStockProdukJadi($jenis,$client)
	{
		$this->db->select('s.*, kp.produk, kp.kodehs, ky.nama as kayu');
		$this->db->from($this->tableName .' s');
		$this->db->join('kit_produk kp','s.idproduk=kp.idproduk','LEFT');
		$this->db->join('kit_jeniskayu ky','s.idkayu=ky.idkayu','LEFT');
		$this->db->where('client_id',$client);
		#$this->db->where('bulan',$bulan);
		#$this->db->where('tahun',$tahun);
		$this->db->where('jenis',$jenis);
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}

	
function getItemStock($id)
{
	if($_GET['jenis']=='wip'){
		return $this->db
			->select('idstok,
					DATE_FORMAT( periode_awal, \'%d-%m-%Y\' ) as periode_awal,
					DATE_FORMAT( periode_akhir, \'%d-%m-%Y\' ) as periode_akhir,
					concat( satuan_stokawal_volume, \',\', ucfirst(satuan_stokawal_volume) ) AS satuan_volume,
					concat( satuan_stokawal_berat, \',\', ucfirst(satuan_stokawal_berat) ) AS satuan_berat,
					concat( satuan_stokawal_jumlah, \',\', ucfirst(satuan_stokawal_jumlah) ) AS satuan_jumlah,
					concat( satuan_stokakhir_volume, \',\', ucfirst(satuan_stokakhir_volume) ) AS satuan_volume_akhir,
					concat( satuan_stokakhir_berat, \',\', ucfirst(satuan_stokakhir_berat) ) AS satuan_berat_akhir,
					concat( satuan_stokakhir_jumlah, \',\', ucfirst(satuan_stokakhir_jumlah) ) AS satuan_jumlah_akhir,
					concat( idwip, \',\', wip) AS wip,
					concat( t2.idproduk, \',\', produk,\' [\',kodehs,\']\') AS produk,
					stokakhir_volume,
					stokakhir_berat,
					stokakhir_jumlah,
					stokawal_volume,
					stokawal_berat,
					stokawal_jumlah,
					kodehs')
			->from($this->tableName .' t1')
			->join('kit_produk t2','`idproduk`','LEFT')
			->join('kit_wip t3','t1.idkayu=t3.idwip','LEFT')
			->where('client_id',$_SESSION['client_id'])
			->where('jenis','W')
			->where('idstok',decryptURL($id))
			->get()
			->row_array();
	}else
	if($_GET['jenis']=='bahan-baku'){
		return $this->db
			->select('idstok,
					DATE_FORMAT( periode_awal, \'%d-%m-%Y\' ) as periode_awal,
					DATE_FORMAT( periode_akhir, \'%d-%m-%Y\' ) as periode_akhir,
					concat( satuan_stokawal_volume,\',\', ucfirst(satuan_stokawal_volume) ) as satuan_volume,
					concat( satuan_stokawal_berat,\',\', ucfirst(satuan_stokawal_berat) ) as satuan_berat,
					concat( satuan_stokawal_jumlah,\',\', ucfirst(satuan_stokawal_jumlah) ) as satuan_jumlah,
					concat( idsortimen, \',\', sortimen) AS sortimen,
					concat( idkayu, \',\', `t2`.`nama`) AS kayu,
					stokawal_volume,
					stokawal_berat,
					stokawal_jumlah')
			->from($this->tableName .' t1')
			->join('kit_jeniskayu t2','`idkayu`','LEFT')
			->join('kit_sortimen t3','`idsortimen`','LEFT')
			->where('client_id',$_SESSION['client_id'])
			->where('jenis','B')
			->where('idstok',decryptURL($id))
			->get()
			->row_array();
	}else
	if($_GET['jenis']=='produk-jadi'){
		return $this->db
			->select('idstok,
					DATE_FORMAT( periode_awal, \'%d-%m-%Y\' ) as periode_awal,
					DATE_FORMAT( periode_akhir, \'%d-%m-%Y\' ) as periode_akhir,
					concat( t3.idproduk, \',\', produk,\' [\',kodehs,\']\') AS produk,
					concat( `t2`.`idkayu`, \',\', `t2`.`nama`) AS kayu,
					stokawal_volume')
			->from($this->tableName .' t1')
			->join('kit_jeniskayu t2','`idkayu`','LEFT')
			->join('kit_produk t3','`idproduk`','LEFT')
			->where('client_id',$_SESSION['client_id'])
			->where('jenis','P')
			->where('idstok',decryptURL($id))
			->get()
			->row_array();
	}
}
	
	function getReportStockProdukJadi($rs,$client,$jenis)
	{
		$this->db->select('s.*, kp.produk, kp.kodehs, ky.nama as kayu');
		$this->db->from($this->tableName .' s');
		$this->db->join('kit_produk kp','s.idproduk=kp.idproduk','LEFT');
		$this->db->join('kit_jeniskayu ky','s.idkayu=ky.idkayu','LEFT');
		$this->db->where('client_id',$client);
		$this->db->where('s.periode_awal',$rs['periode_awal']);
		$this->db->where('s.periode_akhir',$rs['periode_akhir']);
		$this->db->where('s.idproduk',$rs['idproduk']);
		$this->db->where('s.idkayu',$rs['idkayu']);
		$this->db->where('jenis',$jenis);
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getStokWIP($rs,$jenis)
	{
		return $this->db
			->select('s.stokawal_volume,
					s.stokawal_jumlah,
					s.stokawal_berat,
					s.satuan_stokawal_volume,
					s.satuan_stokawal_jumlah,
					s.satuan_stokawal_berat,
					s.stokakhir_volume,
					s.stokakhir_jumlah,
					s.stokakhir_berat,
					s.satuan_stokakhir_volume,
					s.satuan_stokakhir_jumlah,
					s.satuan_stokakhir_berat')
			->from($this->tableName .' s')
			->where('client_id',$_SESSION['client_id'])
			->where('s.periode_awal < ',$rs['periode_awal'])
			->where('s.periode_akhir < ',$rs['periode_akhir'])
			->where('s.idproduk',$rs['idproduk'])
			->where('s.idkayu',$rs['idkayu'])
			->where('jenis',$jenis)
			->order_by('periode_awal','DESC')
			->get()
			->row_array();
	}

	function InUpItem($rs,$where=array())
	{
		$data = array(
			$this->fieldPrefix . 'idproduk'			=> $rs['idproduk'],
			$this->fieldPrefix . 'idkayu'			=> $rs['idkayu'],
			$this->fieldPrefix . 'idsortimen'		=> $rs['idsortimen'],
			$this->fieldPrefix . 'jenis'			=> $rs['jenis'],
			$this->fieldPrefix . 'stokawal_jumlah'	=> $rs['stokawal_jumlah'],
			$this->fieldPrefix . 'stokawal_volume'	=> $rs['stokawal_volume'],
			$this->fieldPrefix . 'stokawal_berat'	=> $rs['stokawal_berat'],
			$this->fieldPrefix . 'satuan_stokawal_jumlah'	=> $rs['satuan_stokawal_jumlah'],
			$this->fieldPrefix . 'satuan_stokawal_volume'	=> $rs['satuan_stokawal_volume'],
			$this->fieldPrefix . 'satuan_stokawal_berat'	=> $rs['satuan_stokawal_berat'],
			$this->fieldPrefix . 'pemasukan_jumlah'	=> $rs['pemasukan_jumlah'],
			$this->fieldPrefix . 'pemasukan_volume'	=> $rs['pemasukan_volume'],
			$this->fieldPrefix . 'pemasukan_berat'	=> $rs['pemasukan_berat'],
			$this->fieldPrefix . 'satuan_pemasukan_jumlah'	=> $rs['satuan_pemasukan_jumlah'],
			$this->fieldPrefix . 'satuan_pemasukan_volume'	=> $rs['satuan_pemasukan_volume'],
			$this->fieldPrefix . 'satuan_pemasukan_berat'	=> $rs['satuan_pemasukan_berat'],
			$this->fieldPrefix . 'pengeluaran_jumlah'	=> $rs['pengeluaran_jumlah'],
			$this->fieldPrefix . 'pengeluaran_volume'	=> $rs['pengeluaran_volume'],
			$this->fieldPrefix . 'pengeluaran_berat'	=> $rs['pengeluaran_berat'],
			$this->fieldPrefix . 'satuan_pengeluaran_jumlah'	=> $rs['satuan_pengeluaran_jumlah'],
			$this->fieldPrefix . 'satuan_pengeluaran_volume'	=> $rs['satuan_pengeluaran_volume'],
			$this->fieldPrefix . 'satuan_pengeluaran_berat'	=> $rs['satuan_pengeluaran_berat'],
			$this->fieldPrefix . 'stokakhir_jumlah'	=> $rs['stokakhir_jumlah'],
			$this->fieldPrefix . 'stokakhir_volume'	=> $rs['stokakhir_volume'],
			$this->fieldPrefix . 'stokakhir_berat'	=> $rs['stokakhir_berat'],
			$this->fieldPrefix . 'satuan_stokakhir_jumlah'	=> $rs['satuan_stokakhir_jumlah'],
			$this->fieldPrefix . 'satuan_stokakhir_volume'	=> $rs['satuan_stokakhir_volume'],
			$this->fieldPrefix . 'satuan_stokakhir_berat'	=> $rs['satuan_stokakhir_berat'],
			$this->fieldPrefix . 'jk'				=> $rs['jk'],
			$this->fieldPrefix . 'fk'				=> $rs['fk'],
			$this->fieldPrefix . 'client_id'		=> $rs['client_id'],
			$this->fieldPrefix . 'bulan'			=> $rs['bulan'],
			$this->fieldPrefix . 'tahun'			=> $rs['tahun'],
			$this->fieldPrefix . 'tanggal'			=> $rs['tanggal'],
			$this->fieldPrefix . 'periode_awal'		=> $rs['periode_awal'],
			$this->fieldPrefix . 'periode_akhir'	=> $rs['periode_akhir'],
			$this->fieldPrefix . 'contentid'		=> $rs['contentid'],
			$this->fieldPrefix . 'stdelete'		=> 1
		);
        if(empty($where)){
    	    $this->db->insert($this->tableName, $data);
    	    return $this->db->insert_id();
    	}else{
        $this->db->update($this->tableName, $data, $where);
        return true;
    	}
	}

	function updateItemByContentId($rs,$id)
	{
		if($rs['stoknya']=='stokawal'){
			$data = array(
				$this->fieldPrefix . 'idproduk'			=> $rs['idproduk'],
				$this->fieldPrefix . 'idkayu'			=> $rs['idkayu'],
				$this->fieldPrefix . 'idsortimen'		=> $rs['idsortimen'],
				$this->fieldPrefix . 'jenis'			=> $rs['jenis'],
				$this->fieldPrefix . 'stokawal_jumlah'	=> $rs['stokawal_jumlah'],
				$this->fieldPrefix . 'stokawal_volume'	=> $rs['stokawal_volume'],
				$this->fieldPrefix . 'stokawal_berat'	=> $rs['stokawal_berat'],
				$this->fieldPrefix . 'satuan_stokawal_jumlah'	=> $rs['satuan_stokawal_jumlah'],
				$this->fieldPrefix . 'satuan_stokawal_volume'	=> $rs['satuan_stokawal_volume'],
				$this->fieldPrefix . 'satuan_stokawal_berat'	=> $rs['satuan_stokawal_berat'],
				$this->fieldPrefix . 'client_id'		=> $rs['client_id'],
				$this->fieldPrefix . 'periode_awal'		=> $rs['periode_awal'],
				$this->fieldPrefix . 'periode_akhir'	=> $rs['periode_akhir']
			);
		}elseif($rs['stoknya']=='stokakhir'){
			$data = array(
				$this->fieldPrefix . 'idproduk'			=> $rs['idproduk'],
				$this->fieldPrefix . 'idkayu'			=> $rs['idkayu'],
				$this->fieldPrefix . 'idsortimen'		=> $rs['idsortimen'],
				$this->fieldPrefix . 'jenis'			=> $rs['jenis'],
				$this->fieldPrefix . 'stokakhir_jumlah'	=> $rs['stokakhir_jumlah'],
				$this->fieldPrefix . 'stokakhir_volume'	=> $rs['stokakhir_volume'],
				$this->fieldPrefix . 'stokakhir_berat'	=> $rs['stokakhir_berat'],
				$this->fieldPrefix . 'satuan_stokakhir_jumlah'	=> $rs['satuan_stokakhir_jumlah'],
				$this->fieldPrefix . 'satuan_stokakhir_volume'	=> $rs['satuan_stokakhir_volume'],
				$this->fieldPrefix . 'satuan_stokakhir_berat'	=> $rs['satuan_stokakhir_berat'],
				$this->fieldPrefix . 'client_id'		=> $rs['client_id'],
				$this->fieldPrefix . 'periode_awal'		=> $rs['periode_awal'],
				$this->fieldPrefix . 'periode_akhir'	=> $rs['periode_akhir']
			);
		}elseif($rs['stoknya']=='pemasukan'){
			$data = array(
				$this->fieldPrefix . 'idproduk'			=> $rs['idproduk'],
				$this->fieldPrefix . 'idkayu'			=> $rs['idkayu'],
				$this->fieldPrefix . 'idsortimen'		=> $rs['idsortimen'],
				$this->fieldPrefix . 'jenis'			=> $rs['jenis'],
				$this->fieldPrefix . 'pemasukan_jumlah'	=> $rs['pemasukan_jumlah'],
				$this->fieldPrefix . 'pemasukan_volume'	=> $rs['pemasukan_volume'],
				$this->fieldPrefix . 'pemasukan_berat'	=> $rs['pemasukan_berat'],
				$this->fieldPrefix . 'satuan_pemasukan_jumlah'	=> $rs['satuan_pemasukan_jumlah'],
				$this->fieldPrefix . 'satuan_pemasukan_volume'	=> $rs['satuan_pemasukan_volume'],
				$this->fieldPrefix . 'satuan_pemasukan_berat'	=> $rs['satuan_pemasukan_berat'],
				$this->fieldPrefix . 'client_id'		=> $rs['client_id'],
				$this->fieldPrefix . 'periode_awal'		=> $rs['periode_awal'],
				$this->fieldPrefix . 'periode_akhir'	=> $rs['periode_akhir']
			);
		}else{
			$data = array(
				$this->fieldPrefix . 'idproduk'			=> $rs['idproduk'],
				$this->fieldPrefix . 'idkayu'			=> $rs['idkayu'],
				$this->fieldPrefix . 'idsortimen'		=> $rs['idsortimen'],
				$this->fieldPrefix . 'jenis'			=> $rs['jenis'],
				$this->fieldPrefix . 'pengeluaran_jumlah'	=> $rs['pengeluaran_jumlah'],
				$this->fieldPrefix . 'pengeluaran_volume'	=> $rs['pengeluaran_volume'],
				$this->fieldPrefix . 'pengeluaran_berat'	=> $rs['pengeluaran_berat'],
				$this->fieldPrefix . 'satuan_pengeluaran_jumlah'	=> $rs['satuan_pengeluaran_jumlah'],
				$this->fieldPrefix . 'satuan_pengeluaran_volume'	=> $rs['satuan_pengeluaran_volume'],
				$this->fieldPrefix . 'satuan_pengeluaran_berat'	=> $rs['satuan_pengeluaran_berat'],
				$this->fieldPrefix . 'client_id'		=> $rs['client_id'],
				$this->fieldPrefix . 'periode_awal'		=> $rs['periode_awal'],
				$this->fieldPrefix . 'periode_akhir'	=> $rs['periode_akhir']
			);
		}
			
        $this->db->update($this->tableName, $data, array('contentid' => $id));
	}

    function dataStatus($id){
        return $this->db->select('xstatus')
                        ->where('id'.$this->field,$id)
                        ->get($this->table)
                        ->row()
                        ->xstatus;
    }
  function gantiStatus($id){ 
        $status=$this->dataStatus($id);
    
  if($status==0){$status=1;}else if($status==1){$status=0;}

        $data = array('xstatus' => $status);
        return $this->db->where('id'.$this->field,$id)
                 ->update($this->table,$data);
  }
  function delAkun($id){ 
        $data = array('stdelete' => 0);
        return $this->db->where('idstok',$id)
                 ->update($this->tableName,$data);
  }
	function selectRendemenPengeluaran($client,$jenis,$periode_awal,$periode_akhir){
		$this->db->select('idproduk,sum(pengeluaran_volume) as pengeluaran');
		$this->db->from($this->tableName .' s');
		$this->db->where('client_id',$client);
		$this->db->where('s.periode_awal >= ',$periode_awal);
		$this->db->where('s.periode_akhir <= ',$periode_akhir);
		$this->db->where('jenis',$jenis);
		$this->db->group_by('idproduk');
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function selectRendemenPemasukan($client,$jenis,$periode_awal,$periode_akhir){
		$this->db->select('idproduk,sum(pemasukan_volume) as pemasukan');
		$this->db->from($this->tableName .' s');
		$this->db->where('client_id',$client);
		$this->db->where('s.periode_awal >= ',$periode_awal);
		$this->db->where('s.periode_akhir <= ',$periode_akhir);
		$this->db->where('jenis',$jenis);
		$this->db->group_by('idproduk');
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function selectRendemenByWipFK_ori($client,$jenis,$jk,$periode_awal,$periode_akhir){
		$this->db->select('idproduk,sum(pemasukan_volume-pengeluaran_volume)*fk as sisa');
		$this->db->from($this->tableName .' s');
		$this->db->where('client_id',$client);
		$this->db->where('s.periode_awal >= '.$periode_awal);
		$this->db->where('s.periode_akhir <= ',$periode_akhir);
		$this->db->where('jenis',$jenis);
		$this->db->where('jk',$jk);
		$this->db->group_by('idproduk');
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function selectRendemenByWipFK_diffop($client,$jenis,$jk,$periode_awal,$periode_akhir){
		//if ($jk=='I'){
			$this->db->select('idproduk, sum(pemasukan_volume-pengeluaran_volume) * case when (fki IS NULL OR fki==0) then fki else fko end as sisa');
		//}else{
		//	$this->db->select('idproduk,sum(pemasukan_volume-pengeluaran_volume) * fko as sisa');
		//}
		$this->db->from($this->tableName .' s');
		$this->db->join('kit_wip w','s.idkayu=w.idwip','LEFT');
		$this->db->where('client_id',$client);
		$this->db->where('s.periode_awal >= '.$periode_awal);
		$this->db->where('s.periode_akhir <= ',$periode_akhir);
		$this->db->where('jenis',$jenis);
		//$this->db->where('jk',$jk);
		$this->db->group_by('idproduk');
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function selectRendemenByWipFK($client,$jenis,$jk,$periode_awal,$periode_akhir){
		if ($jk=='I'){
			//$this->db->select('idproduk, sum(stokawal_volume) * (case when fki is null then fki else fko end) as sisa');
			$this->db->select('idproduk, sum((stokakhir_volume-stokawal_volume*fki)) sisa');
		}else{
			$this->db->select('idproduk,sum((stokakhir_volume-stokawal_volume*fko)) as sisa');
		}
		$this->db->from($this->tableName .' s');
		$this->db->join('kit_wip w','s.idkayu=w.idwip','LEFT');
		$this->db->where('client_id',$client);
		$this->db->where('s.periode_awal >= '.$periode_awal);
		$this->db->where('s.periode_akhir <= ',$periode_akhir);
		$this->db->where('jenis',$jenis);
		//$this->db->where('jk',$jk);
		$this->db->group_by('idproduk');
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
  
}

?>
