<?php
defined('BASEPATH') OR exit('Hacking Attempt : Keluar dari sistem..!!');
class auth_model extends CI_Model {

    var $tableName = "users";
    var $fieldPrefix = "user";
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	/**
	 * create_user function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function create_user($username, $email, $password) {
		
		$data = array(
			'username'   => $username,
			'email'      => $email,
			'passkey'   => hash('sha256',md5($password)),
			'tanggaldaftar' => date('Y-m-j'),
		);
		
		return $this->db->insert('manggota', $data);
		
	}
	
	/**
	 * resolve_user_login function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function resolve_user_login($where) {
		return $this->db->get_where('manggota',$where);
	}
	
	/**
	 * get_user_id_from_username function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
	public function get_user_id_from_username($username) {
		
		$this->db->select('idanggota');
		$this->db->from('manggota');
		$this->db->where('username', $username);
		return $this->db->get()->row('idanggota');
		
	}
	
	/**
	 * get_user function.
	 * 
	 * @access public
	 * @param mixed $user_id
	 * @return object the user object
	 */
	public function get_user($user_id) {
		
		$this->db->from('manggota');
		$this->db->where('idanggota', $user_id);
		return $this->db->get()->row();
		
	}
	
	/**
	 * hash_password function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($password) {
		
		return password_hash($password, PASSWORD_BCRYPT);
		
	}
	
	/**
	 * verify_password_hash function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
	private function verify_password_hash($password, $hash) {
		
		return password_verify($password, $hash);
		
	}



    function getAuthItem($username, $password) {
     return $this->db->select('user_id,
								user_name,
								`name`,
								client_id,
								client_nama,
								client_alamat,
								client_nick,
								client_email,
								client_telp,
								ugroup_id,
								ugroup_modules,
								ugroup_title')
     				 ->join('users_group','users.group=ugroup_id','left')
     				 ->join('clients','users.user_id=client_userid','left')
     				 ->where($this->tableName.'.user_name',$username)
     				 ->where($this->tableName .'.user_password',$password)
     				 ->where('status','1')
     				 ->get($this->tableName)
     				 ->row_array();
    }
    
    function getAuthItemLogin($username) {
     return $this->db->select('user_id,
								user_name,
								`name`,
								client_id,
								client_nama,
								client_alamat,
								client_nick,
								client_email,
								client_telp,
								ugroup_id,
								ugroup_modules,
								ugroup_title')
     				 ->join('users_group','users.group=ugroup_id','left')
     				 ->join('clients','users.user_id=client_userid','left')
     				 ->where($this->tableName.'.user_name',$username)
     				 ->get($this->tableName)
     				 ->result_array();
    }
	
}