<?php
defined('BASEPATH') or exit('No direct script access allowed');

class A_json_model extends CI_Model
{
	private $tableName 		= 'penerimaan';
	private $tableSatuan 	= 'kit_satuan';
	private $tableJenisKayu = 'kit_jeniskayu';
    public function __construct()
    {
        parent::__construct();
    }

function getSertifikasi($id=''){
		$this->db
			->select('idsertifikasi as id, sertifikasi as name')
			->from('kit_sertifikasi')
			->where('xstatus','1')
			->order_by('idsertifikasi','ASC');
		if(!empty($id)){
	return $this->db->get()->row_array();
		}else{
	return $this->db->get()->result_array();
		}
	}

function getHutan($id=''){
		$this->db
			->select('idhutan as id, hutan as name')
			->from('kit_hutan')
			->where('xstatus','1')
			->order_by('idhutan','ASC');
		if(!empty($id)){
	return $this->db->get()->row_array();
		}else{
	return $this->db->get()->result_array();
		}
	}
function getDokumen($id=''){
		$this->db
			->select('iddok as id, dok as name')
			->from('kit_jenisdokumen')
			->where('xstatus','1')
			->order_by('iddok','ASC');
		if(!empty($id)){
	return $this->db->get()->row_array();
		}else{
	return $this->db->get()->result_array();
		}
	}

function getMataUang($id='',$like=''){
		$this->db
			->select('kode as id, concat(kode,\' - \',nama) as name')
			->from('kit_matauang')
			->order_by('kode','ASC');
	if(!empty($like)){
		$this->db->like('nama', $like);
	}
		if(!empty($id)){
	return $this->db->get()->row_array();
		}else{
	return $this->db->get()->result_array();
		}
}

function checkStuffingById(){
	return $this->db
		->select('stuff_data as name')
		->from('kit_stuffing')
		->where('client_id',$_SESSION['client_id'])
		->get()
		->row_array();
}

function getDischargeByNegara($kode=0)
{
	$this->db
		->select('iddischarge as id, concat(uraian,\' [\', kode ,\']\') as name',FALSE)
		->from('kit_discharge')
		->where('SUBSTRING(kode, 1,2) = "' . $kode.'"')
		->where('xstatus','1')
		->order_by('kode','ASC');
		if(!empty($id)){
	return $this->db->get()->row_array();
		}else{
	return $this->db->get()->result_array();
		}
}

function getAsalKayu($id=''){
		return $this->db
			->select('*')
			->from('kit_negara')
			->where('xstatus','1')
			->order_by('negara','ASC')
			->get()
			->result_array();
	}
	
function getSatuanByOption($option){
$satuan_kode='';
if(!empty($_GET['satuan_kode'])){
	$satuan_kode.=$_GET['satuan_kode'];}

		if(!empty($satuan_kode)){
			$this->db->where('satuan_kode',$satuan_kode);
		}
		return $this->db
			->select('satuan_kode as id,CONCAT(UCASE(MID(satuan_kode,1,1)),MID(satuan_kode,2)) as name')
			->from($this->tableSatuan)
			->where('satuan_status','1')
			->where('satuan_jenis',$option)
			->order_by('satuan_kode','ASC')
			->get()
			->result_array();
}

function getDataPengeluaran($id=''){
	$this->db
		->select('idpeng as id, pengeluaran as name')
		->from('kit_jenispengeluaran')
		->where('xstatus','1') 	
		->order_by('idpeng','ASC');
	$query = $this->db->get();
	$result = $query->result_array();
	$row = $query->row_array();
	if(!empty($id)){
        return $row;
    }else{
        return $result;
    }
}

function getSortimen($like='',$limit=array(),$id=''){
		$this->db
			->select('sr.idsortimen as id,sr.sortimen as name')
			->from('kit_sortimen sr')
			->join('kit_setting s','sr.idsortimen=s.idsortimen','LEFT')
			->where('xstatus','1')
			->where('sortimenstdelete','1')
			->where('idclient',$_SESSION['client_id']);
		if(!empty($like)){
			$this->db->like('sortimen',$like);
		}
		// if(!empty($limit)){
		// 	$this->db->limit($limit['limit'],$limit['start']);
		// }
		if(!empty($id)){
			$this->db->where('sr.idsortimen IN ('.$id.')');
		}
		$this->db->order_by('sr.sortimen','ASC');
		if(!empty($id)){
	return $this->db->get()->row_array();
		}else{
	return $this->db->get()->result_array();
		}
}
	
	function getClientbyID()
	{
		return $this->db
			->select('c.*,p.nama as propinsi, k.nama as kabupaten, u.user_name as username')
			->from('clients c ')
			->join('kit_propinsi p','p.propid=c.client_propinsi','LEFT')
			->join('kit_kabupaten k','k.kabid=c.client_kabupaten','LEFT')
			->join('users u','u.user_id=c.client_userid','LEFT')
			->where('client_id',$_SESSION['client_id'])
			->get()
			->row_array();
	}

	function getKayu($like='',$limit=array(),$id=''){
		$this->db
			->select('idkayu as id,nama as name')
			->from($this->tableJenisKayu)
			->where('xstatus','1')
			->where('kayustdelete','1');
		if(!empty($like)){
			$this->db->like('nama',$like);
		}
		if(!empty($id)){
			$this->db->where('idkayu IN ('.$id.')');
		}
		// if(!empty($limit)){
		// 	$this->db->limit($limit['limit'],$limit['start']);
		// }
		$this->db
			->order_by('nama','ASC');
		if(!empty($id)){
	return $this->db->get()->row_array();
		}else{
	return $this->db->get()->result_array();
		}
	}
	
	function getSupplier($like='',$limit=array(),$id=''){
		$this->db->select('sp.idsupplier as id,concat(supplier,\' [\', idnegara ,\']\') as name',FALSE);
		$this->db->from('kit_supplier sp');
		$this->db->join('kit_setting s','sp.idsupplier=s.idsupplier','LEFT');
		$this->db->where('xstatus','1');
		$this->db->where('supplierstdelete','1');
		$this->db->where('idclient',$_SESSION['client_id']);
		if(!empty($like)){
			$this->db->like('supplier',$like);
		}
		if(!empty($id)){
			$this->db->where('sp.idsupplier IN ('.$id.')');
		}
		// if(!empty($limit)){
		// 	$this->db->limit($limit['limit'],$limit['start']);
		// }
		$this->db->order_by('supplier','ASC');
		$query 	= $this->db->get();
		$result = $query->result_array();
		$row 	= $query->row_array();
		if(!empty($id)){
	        return $row;
	    }else{
	        return $result;
	    }
	}

	function getProduk($like='',$limit=array(),$id=''){
		$this->db
			->select('p.idproduk as id,concat(produk,\' [\', COALESCE(kodehs_8,kodehs) ,\']\') as name',FALSE)
			->from('kit_produk p')
			->where('xstatus','1')
			->where('produkstdelete','1');
		if(!empty($like)){
			$this->db->like('produk',$like);
		}
		if(!empty($id)){
			$this->db->where('p.idproduk IN ('.$id.')');
		}
		// if(!empty($_SESSION['client_id'])){
			$this->db->join('kit_setting s','s.idproduk=p.idproduk','LEFT')
					 ->where('idclient',$_SESSION['client_id']);
		// }
		// if(!empty($limit)){
		// 	$this->db->limit($limit['limit'],$limit['start']);
		// }
		$this->db->order_by('produk','ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		$row = $query->row_array();
		if(!empty($id)){
	        return $row;
	    }else{
	        return $result;
	    }
	}

	function getBuyer($like='',$limit=array(),$id=''){
		$this->db
			->select('b.idbuyer as id,concat(buyer,\' [\', idnegara  ,\']\') as name, alamat',FALSE)
			->from('kit_buyer b')
			->join('kit_setting s','b.idbuyer=s.idbuyer','LEFT')
			->where('xstatus','1')
			->where('buyerstdelete','1')
			->where('idclient',$_SESSION['client_id']);
		if(!empty($like)){
			$this->db->like('buyer',$like);
		}
		if(!empty($id)){
			$this->db->where('b.idbuyer IN ('.$id.')');
		}
		if(!empty($limit)){
			$this->db->limit($limit['limit'],$limit['start']);
		}
		$this->db->order_by('buyer','ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		$row = $query->row_array();
		if(!empty($id)){
	        return $row;
	    }else{
	        return $result;
	    }
	}
	
	function getLoading($id=''){
		if(count($this->getLoading_byClient())>0){
			return $this->getLoading_byClient();
		}else{
			$this->db
				->select('idloading as id, concat(uraian,\' [\', kode ,\']\') as name',FALSE)
				->from('kit_loading')
				->where('xstatus','1')
				->where('loadingstdelete','1');
			if(!empty($id)){
				$this->db->where('idloading IN ('.$id.')');
			}
			// if(!empty($limit)){
			// 	$this->db->limit($limit['limit'],$limit['start']);
			// }
			$this->db->order_by('kode','ASC');
			$query = $this->db->get();
			$result = $query->result_array();
			$row = $query->row_array();
			if(!empty($id)){
		        return $row;
		    }else{
		        return $result;
		    }
		}
	}
	
	function getLoading_byClient($id=''){
		$this->db
			->select('t2.idloading as id, concat(t2.uraian,\' [\', t2.kode ,\']\') as name',FALSE)
			->from('kit_loading t2')
			->join('setting_portloading t1','idloading','left')
			->where('t2.xstatus','1')
			->where('t2.loadingstdelete','1')
			->where('t1.client_id',$_SESSION['client_id']);
		if(!empty($id)){
			$this->db->where('t2.idloading IN ('.$id.')');
		}
		// if(!empty($limit)){
		// 	$this->db->limit($limit['limit'],$limit['start']);
		// }
		$this->db->order_by('t2.kode','ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		$row = $query->row_array();
		if(!empty($id)){
	        return $row;
	    }else{
	        return $result;
	    }
	}
	
    
}