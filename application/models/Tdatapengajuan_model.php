<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tdatapengajuan_model extends CI_Model
{
    private $tblEkspor = 'ekspor';
    private $field = 'wip';
    public function __construct()
    {
        parent::__construct();
    }

    function dataStatus($id){
        return $this->db->select('xstatus')
                        ->where('id'.$this->field,$id)
                        ->get($this->tblEkspor)
                        ->row()
                        ->xstatus;
    }
  function gantiStatus($id){ 
        $status=$this->dataStatus($id);
    
  if($status==0){$status=1;}else if($status==1){$status=0;}

        $data = array('xstatus' => $status);
        return $this->db->where('id'.$this->field,$id)
                 ->update($this->tblEkspor,$data);
  }
  function delAkun($id){ 
        $data = array($this->field.'stdelete' => 0);
        return $this->db->where('id'.$this->field,$id)
                 ->update($this->tblEkspor,$data);
  }

  function getStatus_dokumen($id)
  {
    return $this->db
      ->select('status_dokumen,no_urut')
      ->from($this->tblEkspor)
      ->where('idekspor',$id)
      ->get()
      ->row_array();
  }

  function getDetail_Edit($id)
  {
    return $this->db
                ->SELECT('
                      t1.volume,t1.berat,t1.jumlah,
                      CONCAT(\'[\',
                        (CONCAT(\'{"id":"\',COALESCE(t1.idkayu,\'\'),\'","name":"\',COALESCE(kayu1.nama,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idkayu2,\'\'),\'","name":"\',COALESCE(kayu2.nama,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idkayu3,\'\'),\'","name":"\',COALESCE(kayu3.nama,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idkayu4,\'\'),\'","name":"\',COALESCE(kayu4.nama,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idkayu5,\'\'),\'","name":"\',COALESCE(kayu5.nama,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idkayu6,\'\'),\'","name":"\',COALESCE(kayu6.nama,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idkayu7,\'\'),\'","name":"\',COALESCE(kayu7.nama,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idkayu8,\'\'),\'","name":"\',COALESCE(kayu8.nama,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idkayu9,\'\'),\'","name":"\',COALESCE(kayu9.nama,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idkayu10,\'\'),\'","name":"\',COALESCE(kayu10.nama,\'\'),\'"}\')),
                      \']\') as jenis_kayu,
                      CONCAT(\'[\',
                        (CONCAT(\'{"id":"\',COALESCE(t1.idnegaraasal,\'\'),\'","name":"\',COALESCE(asalkayu1.negara,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idnegaraasal2,\'\'),\'","name":"\',COALESCE(asalkayu2.negara,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idnegaraasal3,\'\'),\'","name":"\',COALESCE(asalkayu3.negara,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idnegaraasal4,\'\'),\'","name":"\',COALESCE(asalkayu4.negara,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idnegaraasal5,\'\'),\'","name":"\',COALESCE(asalkayu5.negara,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idnegaraasal6,\'\'),\'","name":"\',COALESCE(asalkayu6.negara,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idnegaraasal7,\'\'),\'","name":"\',COALESCE(asalkayu7.negara,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idnegaraasal8,\'\'),\'","name":"\',COALESCE(asalkayu8.negara,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idnegaraasal9,\'\'),\'","name":"\',COALESCE(asalkayu9.negara,\'\'),\'"},\')),
                        (CONCAT(\'{"id":"\',COALESCE(t1.idnegaraasal10,\'\'),\'","name":"\',COALESCE(asalkayu10.negara,\'\'),\'"}\')),
                      \']\') as negara_asal,
                      t2.produk as namaproduk,t2.idproduk
        ')
  ->FROM('ekspor_detail t1')
  //Join Kayu
  ->JOIN('kit_jeniskayu kayu1', 'idkayu','left') 
  ->JOIN('kit_jeniskayu kayu2', 't1.idkayu2 = kayu2.idkayu','left') 
  ->JOIN('kit_jeniskayu kayu3', 't1.idkayu3 = kayu3.idkayu','left') 
  ->JOIN('kit_jeniskayu kayu4', 't1.idkayu4 = kayu4.idkayu','left') 
  ->JOIN('kit_jeniskayu kayu5', 't1.idkayu5 = kayu5.idkayu','left') 
  ->JOIN('kit_jeniskayu kayu6', 't1.idkayu6 = kayu6.idkayu','left') 
  ->JOIN('kit_jeniskayu kayu7', 't1.idkayu7 = kayu7.idkayu','left') 
  ->JOIN('kit_jeniskayu kayu8', 't1.idkayu8 = kayu8.idkayu','left') 
  ->JOIN('kit_jeniskayu kayu9', 't1.idkayu9 = kayu9.idkayu','left') 
  ->JOIN('kit_jeniskayu kayu10', 't1.idkayu10 = kayu10.idkayu','left') 
  //Join Negara
  ->JOIN('kit_negara asalkayu1', 't1.idnegaraasal = asalkayu1.idnegara','left') 
  ->JOIN('kit_negara asalkayu2', 't1.idnegaraasal2 = asalkayu2.idnegara','left') 
  ->JOIN('kit_negara asalkayu3', 't1.idnegaraasal3 = asalkayu3.idnegara','left') 
  ->JOIN('kit_negara asalkayu4', 't1.idnegaraasal4 = asalkayu4.idnegara','left') 
  ->JOIN('kit_negara asalkayu5', 't1.idnegaraasal5 = asalkayu5.idnegara','left') 
  ->JOIN('kit_negara asalkayu6', 't1.idnegaraasal6 = asalkayu6.idnegara','left') 
  ->JOIN('kit_negara asalkayu7', 't1.idnegaraasal7 = asalkayu7.idnegara','left') 
  ->JOIN('kit_negara asalkayu8', 't1.idnegaraasal8 = asalkayu8.idnegara','left') 
  ->JOIN('kit_negara asalkayu9', 't1.idnegaraasal9 = asalkayu9.idnegara','left') 
  ->JOIN('kit_negara asalkayu10', 't1.idnegaraasal10 = asalkayu10.idnegara','left') 
  //Join Produk
  ->JOIN('kit_produk t2','idproduk','left')
  ->WHERE('iddetekspor',$id)
  ->get()
  ->row_array();
  }

  function getEksporUpdate($id)
  {
    return $this->db
      ->select('t1.*,
              t2.iddischarge,t2.uraian as dis_uraian,t2.kode dis_kode,
              t3.idloading,t3.uraian as loading_uraian,t3.kode loading_kode,
              t4.negara nama_negara,
              t5.iduang,t5.kode kode_uang,t5.nama nama_uang,
              t6.idbuyer,t6.buyer,t6.idnegara negarabuyer
              ')
      ->from($this->tblEkspor.' t1')
      ->join('kit_discharge t2','t2.iddischarge = t1.discharge','left')
      ->join('kit_loading t3','t3.idloading = t1.loading','left')
      ->join('kit_negara t4','idnegara','left')
      ->join('kit_matauang t5','t5.kode = t1.valuta','left')
      ->join('kit_buyer t6','idbuyer','left')
      ->where('idekspor',$id)
      ->get()
      ->row_array();
    }

  function getEksporByIdEskpor($id){
    $this->db->select("e.idekspor, e.client_id, e.bulan, e.tahun, e.iso, b.buyer, n.negara, e.invoice, l.uraian as loading, 
              d.uraian as discharge, e.invoice, e.tglinvoice, e.peb, e.tglship, e.vessel, e.bl, e.packinglist,e.alamat,
              e.etpik, e.sertifikat, e.npwp, e.keterangan, e.no_urut, e.submitdate, e.status, e.periode_awal,e.periode_akhir,
              c.client_nama, e.status_liu, e.valuta, m.nama as matauang, e.status_dokumen, e.stuffing");
    $this->db->from($this->tblEkspor . " e ");
    $this->db->join("kit_negara n","n.idnegara=e.idnegara ","LEFT");
    $this->db->join("kit_loading l","l.idloading=e.loading ","LEFT");
    $this->db->join("kit_discharge d","d.iddischarge=e.discharge ","LEFT");
    $this->db->join("kit_buyer b","b.idbuyer=e.idbuyer","LEFT");
    $this->db->join("kit_matauang m","m.kode=e.valuta","LEFT");
    $this->db->join("clients c","c.client_id=e.client_id","LEFT");
    $this->db->where("idekspor",$id);   
    $this->db->order_by("no_urut","ASC");
    $query = $this->db->get();
    $array = $query->row_array();
    $query->free_result();
    unset($query);
        return $array;
  }

function addUpEkspor($where=array())
  {
    $d=$this->getStatus_dokumen(decryptURL($_GET['id']));
    if(!empty($where)){
     $nourut=$d['no_urut'];
    }else{
     $nourut=$this->getNoUrut();
    }

    $data = array(
      'client_id'     => $_SESSION['client_id'],
      'bulan'         => date('m'),
      'tahun'         => date('Y'),
      'periode_awal'  => decode_date($this->input->post('new-periode_awal')),
      'periode_akhir' => decode_date($this->input->post('new-periode_akhir')),
      'idbuyer'       => $this->input->post('new-buyer'),
      'alamat'        => $this->input->post('new-alamat'),
      'idnegara'      => $this->input->post('new-negara'),
      'iso'           => $this->input->post('new-iso'),
      'loading'       => $this->input->post('new-loading'),
      'discharge'     => $this->input->post('new-discharge'),
      'keterangan'    => $this->input->post('new-keterangan'),
      'invoice'       => $this->input->post('new-invoice'),
      'tglinvoice'    => decode_date($this->input->post('new-tglinvoice')),
      'peb'           => $this->input->post('new-peb'),
      'bl'            => $this->input->post('new-bl'),
      'packinglist'   => $this->input->post('new-packinglist'),
      'vessel'        => $this->input->post('new-vessel'),
      'tglship'       => decode_date($this->input->post('new-tglship')),
      'etpik'         => $this->input->post('new-etpik'),
      'sertifikat'    => $this->input->post('new-sertifikat'),
      'npwp'          => $this->input->post('new-npwp'),
      'valuta'        => $this->input->post('new-matauang'),
      'no_urut'       => $nourut,
      'submitdate'    => date('Y-m-d H:i:s'),
      'status'        => $this->input->post('new-status'),
      'stuffing'      => $this->input->post('new-stuffing'),
      'stdelete'      => 1
    );
    $tipeE=$this->input->post('new-status_edit');
    switch($tipeE){
      case 'edit' :
        $data['status_liu']   = $this->input->post('new-status_liu');
        $data['status_dokumen'] = $d['status_dokumen'];
        break;
      case 'replace'  :
        $data['status_liu']   = 'replace';
        $data['status_dokumen'] = 'Editing';
        break;
      case 'extend' :
        $data['status_liu']   = 'extend';
        $data['status_dokumen'] = 'Editing';
        break;
      case 'cancel' :
        $data['status_liu']   = 'cancel';
        $data['status_dokumen'] = 'Editing';
        break;
    }
if(empty($where)){
    $this->db->insert($this->tblEkspor, $data);
      return $this->db->insert_id();
  }else{
    $this->db->update($this->tblEkspor, $data, $where);
      return true;
  }
  }

function getNoUrut(){
    return $this->db
      ->select('( no_urut + 1 ) as no_urut')
      ->from($this->tblEkspor)
      ->where('client_id',$_SESSION['client_id'])
      ->order_by('no_urut','DESC')
      ->limit(1)
      ->get()
      ->row_array()['no_urut'];
  }

function pagination_Table($id,$limit='',$start=''){
  $output='';
          $this->db
              ->select('
                    t1.nilai,t1.fako,t1.hscode,t1.volume,t1.berat,t1.iddetekspor,t1.jumlah,
                    t2.produk,
concat(kayu1.nama,\'<br>\',kayu2.nama,\'<br>\',kayu3.nama,\'<br>\',kayu4.nama,\'<br>\',kayu5.nama,\'<br>\',
        kayu6.nama,\'<br>\',kayu7.nama,\'<br>\',kayu8.nama,\'<br>\',kayu9.nama,\'<br>\',kayu10.nama) jenis_kayu,
concat(asalkayu1.negara,\'<br>\',asalkayu2.negara,\'<br>\',asalkayu3.negara,\'<br>\',asalkayu4.negara,\'<br>\',asalkayu5.negara,\'<br>\',
        asalkayu6.negara,\'<br>\',asalkayu7.negara,\'<br>\',asalkayu8.negara,\'<br>\',asalkayu9.negara,\'<br>\',asalkayu10.negara) negara_asal
                      ')
              ->FROM('ekspor_detail t1')
              //Join Kayu
              ->JOIN('kit_jeniskayu kayu1', 'idkayu','left') 
              ->JOIN('kit_jeniskayu kayu2', 't1.idkayu2 = kayu2.idkayu','left') 
              ->JOIN('kit_jeniskayu kayu3', 't1.idkayu3 = kayu3.idkayu','left') 
              ->JOIN('kit_jeniskayu kayu4', 't1.idkayu4 = kayu4.idkayu','left') 
              ->JOIN('kit_jeniskayu kayu5', 't1.idkayu5 = kayu5.idkayu','left') 
              ->JOIN('kit_jeniskayu kayu6', 't1.idkayu6 = kayu6.idkayu','left') 
              ->JOIN('kit_jeniskayu kayu7', 't1.idkayu7 = kayu7.idkayu','left') 
              ->JOIN('kit_jeniskayu kayu8', 't1.idkayu8 = kayu8.idkayu','left') 
              ->JOIN('kit_jeniskayu kayu9', 't1.idkayu9 = kayu9.idkayu','left') 
              ->JOIN('kit_jeniskayu kayu10', 't1.idkayu10 = kayu10.idkayu','left') 
              //Join Negara
              ->JOIN('kit_negara asalkayu1', 't1.idnegaraasal = asalkayu1.idnegara','left') 
              ->JOIN('kit_negara asalkayu2', 't1.idnegaraasal2 = asalkayu2.idnegara','left') 
              ->JOIN('kit_negara asalkayu3', 't1.idnegaraasal3 = asalkayu3.idnegara','left') 
              ->JOIN('kit_negara asalkayu4', 't1.idnegaraasal4 = asalkayu4.idnegara','left') 
              ->JOIN('kit_negara asalkayu5', 't1.idnegaraasal5 = asalkayu5.idnegara','left') 
              ->JOIN('kit_negara asalkayu6', 't1.idnegaraasal6 = asalkayu6.idnegara','left') 
              ->JOIN('kit_negara asalkayu7', 't1.idnegaraasal7 = asalkayu7.idnegara','left') 
              ->JOIN('kit_negara asalkayu8', 't1.idnegaraasal8 = asalkayu8.idnegara','left') 
              ->JOIN('kit_negara asalkayu9', 't1.idnegaraasal9 = asalkayu9.idnegara','left') 
              ->JOIN('kit_negara asalkayu10', 't1.idnegaraasal10 = asalkayu10.idnegara','left') 
              //Join Produk
              ->JOIN('kit_produk t2','idproduk','left')
              ->WHERE('idekspor',$id)
              ->order_by('t1.iddetekspor','ASC');
    if(!empty($limit)||!empty($start)){
              $this->db->limit($limit,$start);
      $query =$this->db->get()->result_array();

  $output.='<table width="100%" class="table table-bordered table-striped table-responsive" id="list-DT">
              <thead>
                <tr>
                   <th rowspan="2">No.</th>
                   <th rowspan="2">FAKO</th>
                   <th rowspan="2">Nama Produk Jadi</th>
                   <th rowspan="2">HS Code</th>
                   <th colspan="3" width="20%" style="text-align:center;">Jumlah</th>
                   <th rowspan="2">Jenis Kayu</th>
                   <th rowspan="2">Negara Asal</th>
                   <th rowspan="2">Nilai</th>
                   <th rowspan="2">Option</th>
                </tr>
                <tr>
                   <th width="7%">m3</th>
                   <th width="7%">kg</th>
                   <th width="7%">unit</th>
                </tr>
              </thead>
              <tbody>';
$no=1;
$mm3='';
$kkg='';
$ppcs='';
$total = '';
foreach ($query as $show) {
$total += $show['nilai'];
$m3    = $show['volume'];
$mm3  += $m3; 
$kg    = $show['berat'];
$kkg  += $kg; 
$pcs   = $show['jumlah'];
$ppcs += $pcs;

      $output.='<tr>
                  <td>'.$no++.'</td>
                  <td>'.$show['fako'].'</td>
                  <td>'.$show['produk'].'</td>
                  <td>'.$show['hscode'].'</td>
                  <td>'.$show['volume'].'</td>
                  <td>'.$show['berat'].'</td>
                  <td>'.$show['jumlah'].'</td>
                  <td>'.$show['jenis_kayu'].'</td>
                  <td>'.$show['negara_asal'].'</td>
                  <td>'.number_format($show['nilai'],2).'</td>
                  <td>'.$show['iddetekspor'].'</td>
                </tr>';
}
    $output.='</tbody>
              <tfoot>
                <tr>
                  <td colspan="4">&nbsp;</td>
                  <td>'.$mm3.'</td>
                  <td>'.$kkg.'</td>
                  <td>'.$ppcs.'</td>
                  <td colspan="2">&nbsp;</td>
                  <td>'.number_format($total,2).'</td>
                  <td>&nbsp;</td>
                </tr>
              </tfoot>
            </table>';
    return $output;
    }else{
      return $this->db->get()->num_rows();
    }
}

}
