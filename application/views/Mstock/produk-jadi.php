<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?php
$uri=$this->uri->segment(1);
	$uri2=$this->uri->segment(2);
$awaltext  	=encode_date($Laper['periode_awal']);
$akhirtext 	=encode_date($Laper['periode_akhir']);
            ?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="All {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List" id="refresh_list" type="button"><i class="si si-refresh"></i></button>
            </li>
            <li>
                <button title="Tambah Stock Awal" id="btnTambahStock" type="button" style="border-radius: 25px;border: 2px solid #ffffff;width: 125px">Tambah Stock Awal</button>
            </li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
	<form action="" method="post" enctype="multipart/form-data" id="form-insertNew">
<input type="hidden" value="" id="hide-satjumlah" name="hide-satjumlah">
<input type="hidden" value="" id="hide-satvolume" name="hide-satvolume">
<input type="hidden" value="" id="hide-satberat" name="hide-satberat">
<input type="hidden" value="" id="hide-produk" name="hide-produk">
<input type="hidden" value="" id="hide-listkayu" name="hide-listkayu">
<input type="hidden" id="hide-ID" name="hide-ID">
<div class="row">
	<div class="col-sm-6">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		<div class="form-group">
			<label>Periode Awal - Akhir</label>
			<div class="row">
				<div class="col-sm-3">
					<input type="text" class="form-control" name="new-periode_awal" id="new-periode_awal" value="">
			</div>
				<div class="col-sm-3">
					<sub style="margin-left: -20px">s/d</sub>
					<input type="text" style="margin-left: 0px;margin-top: -18.5px" class="form-control" name="new-periode_akhir" id="new-periode_akhir" value="">
			</div>
		</div>
		</div>
        <div class="form-group">
            <label>Nama Produk</label>
            <select class="form-control newproduk" id="new-produk" name="new-produk">
            </select>
        </div>
	</div>
	<div class="col-sm-6">
        <div class="form-group">
            <label>Jenis Kayu</label>
            <select class="form-control newlistkayu" id="new-listkayu" name="new-listkayu">
            </select>
        </div>
        <div class="form-group stock_awal-input" style="display: none;">
            <label>Jumlah Stock Awal</label>
			<input type="text" class="form-control" name="new-volume" id="new-volume" value="">
			<input type="hidden" name="stocknya" id="stocknya" value="stockawal">
        </div>
	</div>
</div>
	<div class="text-right">
		<button type="reset" class="btn btn-danger faa-parent animated-hover btnBatal">Batal&nbsp;
			<i class="fas fa-times faa-spin"></i>
		</button>
		<button style="display: none;width: 135px" type="reset" class="btn btn-primary faa-parent animated-hover btnUpdate" id="btnUpdate">Update 
			<i class="fas fa-check faa-wrench"></i>
		</button>
		<button style="display: none;width: 135px" type="reset" class="btn btn-primary faa-parent animated-hover btnSimpan" id="btnSimpan">Save&nbsp;
			<i class="fas fa-check faa-wrench"></i>
		</button>
		<button type="button" class="btn btn-primary faa-parent animated-hover btnFilter" style="width: 135px" id="btnFilter">Lihat Laporan&nbsp;
			<i class="fa fa-filter faa-wrench"></i>
		</button>
	</div>
</form>
<br>
<div class="progress progress-mini">
    <div class="progress-bar progress-bar-info" role="progressbar" style="width: 100%"></div>
</div>
			<table width="150%" class="table table-bordered table-striped table-responsive" id="list-DT">
				<thead>
					<tr>
					   <th width='1%'>No.</th>
					   <th width='25%'>Nama Produk</th>
					   <th width='15%'>hs code</th>
					   <th width='20%'>Jenis Kayu</th>
					   <th width='10%'>stock awal (m3/ton)</th>
					   <th width='10%'>Penerimaan (m3/ton)</th>
					   <th width='10%'>Penggunaan (m3/ton)</th>
					   <th width='10%'>Stock Akhir (m3/ton)</th>
					   <th width='3%'>aksi</th>
					</tr>

				</thead>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
<script type="text/javascript">
$(function(){
$hideproduk     	= $('#hide-produk').val().split(',');
$hidelistkayu     	= $('#hide-listkayu').val().split(',');

$hidesatjumlah = $('#hide-satjumlah').val().split(',');
$hidesatvolume = $('#hide-satvolume').val().split(',');
$hidesatberat  = $('#hide-satberat').val().split(',');
// var filtering=`new-periode_awal=`+$('#new-periode_awal').val()+`&new-periode_akhir=`+$('#new-periode_akhir').val()+`&new-produk=`+$('#new-produk').val()+`&new-listkayu=`+$('#new-listkayu').val();
// $ta=$('#form-insertNew').serialize();
    $(document).ready(function() {
// getDataSSP("#list-DT","{url_ajax}&"+filtering,true);
getDataSSP("#list-DT","{url_ajax}",true,'',$('#form-insertNew').serialize());
getDatePicker('#new-periode_awal')
getDatePicker('#new-periode_akhir')
getPeriode()
getSelect2Search($('.newproduk'),'{json_produk}',$hideproduk,'Produk')
getSelect2Search($('.newlistkayu'),'{json_datakayu}',$hidelistkayu,'Jenis Kayu')
// Select Satuan
getSelect2noSearch($('.new-satjumlah'),'{json_satuanbyjumlah}',$hidesatjumlah,'Satuan')
getSelect2noSearch($('.new-satvolume'),'{json_satuanbyvolume}',$hidesatvolume,'Satuan')
getSelect2noSearch($('.new-satberat'),'{json_satuanbyberat}',$hidesatberat,'Satuan')

// end of document
})

$(document).on("click",".btnBatal",function(){
	$('.stock_awal-input').hide(500)
	resetValue()
})
$(document).on("click",".btnSimpan",function(){
// console.log($('#form-insertNew').serialize())
	$.ajax({
	url:"{url_insert}",
	data:{"data":$('#form-insertNew').serialize()},
	dataType:'script',
	success: function(respon){
		$('#list-DT').each(function() {
	  	dt = $(this).dataTable();
	  		dt.fnDraw();
		})
		resetValue();
		return respon;
	}
	});
})
$(document).on("click",".btnUpdate",function(){
	$.ajax({
	url:"{url_insert}",
	data:{"data":$('#form-insertNew').serialize()},
	dataType:'script',
	success: function(respon){
		$('#list-DT').each(function() {
	  	dt = $(this).dataTable();
	  		dt.fnDraw();
		})
		resetValue();
		return respon;
	}
	});
})

$(document).on("click","#refresh_list",function(){
 $('#list-DT').each(function() {
  	dt = $(this).dataTable();
  		dt.fnDraw();
		})
		resetValue();
})

$(document).on("click","#btnTambahStock",function(){
	resetValue();
	$('.stock_awal-input').show(500)
	$('#btnSimpan').show();
	$('#btnUpdate').hide();
	$('#btnFilter').hide();
})

$(document).on("click",".btnFilter",function(){
	if($('#new-periode_awal').val()==''){
		Toastr('Harap Isi Periode Awal','Peringatan!')
	}else 
	if($('#new-periode_akhir').val()==''){
		Toastr('Harap Isi Periode Akhir','Peringatan!')
	}else 
	if($('#new-produk').val()==''){
		Toastr('Harap Pilih Produk','Peringatan!')
	}else 
	if($('#new-listkayu').val()==''){
		Toastr('Harap Pilih Jenis Kayu','Peringatan!')
	}else{
 		$('#list-DT').dataTable().api().destroy()
		getDataSSP("#list-DT","{url_ajax}",true,'',$('#form-insertNew').serialize());
 		// dt = $('#list-DT').dataTable();
		// dt.api().ajax.reload();
	}
})

$(document).on("click",".edit-row",function(){
var me 	 				=$(this),
	id 	 				=me.attr("data-id"),
	tr 	 				=me.closest('tr');

	$.ajax({
	url:"{get_4update}",
	data:{"idstock":id},
	dataType:'json',
	beforeSend:function(){
		$('#hide-ID').val(id)
		$('.stock_awal-input').show(500)
		$('#btnFilter').hide();
		$('#btnSimpan').hide();
		$('#btnUpdate').show();
	},
	success: function(data){
	var $newhideproduk			=data.produk.split(','),
		$newhidelistkayu		=data.kayu.split(',');
			$('#new-periode_awal').val(data.periode_awal)
			$('#new-periode_akhir').val(data.periode_akhir)
			getSelect2Search($('.newproduk'),'{json_produk}',$newhideproduk,'Produk')
			getSelect2Search($('.newlistkayu'),'{json_datakayu}',$newhidelistkayu,'Jenis Kayu')
			$('#new-volume').val(data.stokawal_volume)
		}
	});
})
$(document).on("click",".delete-row",function(){
var id=$(this).attr("data-id");
var buton=$(this);
	$.ajax({
	url:"{url_delete}",
	data:{"id":id},
	dataType:'text',
	success: function(data){
		$('#list-DT').each(function() {
	  	dt = $(this).dataTable();
	  		dt.fnDraw();
		})
		ToastrSukses("{tJudul} "+data+" berhasil dihapus","Info")
	}
	});
})

function getPeriode(){
	$.ajax({
		//url:'{url_periode}',
		dataType:'json',
		success:function(data){
			$('#new-periode_awal').val(data.periode_awal)
			$('#new-periode_akhir').val(data.periode_akhir)
		}
	})
}

function resetValue() {
	getPeriode()
$('#hide-ID').val('');
	$('.newproduk').empty()
	$('.newlistkayu').empty()
	$('.new-satjumlah').empty()
	$('.new-satvolume').empty()
	$('.new-satberat').empty()
$('#hide-satjumlah').val('')
$('#hide-satvolume').val('')
$('#hide-satberat').val('')
$('#hide-produk').val('')
$('#hide-listkayu').val('')
$('#btnFilter').show();
$('#btnSimpan').hide();
$('#btnUpdate').hide();
}

// end of function
})
</script>