<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?php
$uri=$this->uri->segment(1);
	$uri2=$this->uri->segment(2);
$awaltext  	=encode_date($Laper['periode_awal']);
$akhirtext 	=encode_date($Laper['periode_akhir']);
            ?>
<style type="text/css">
	.disabled-select {
  background-color: #d5d5d5;
  opacity: 0.5;
  border-radius: 3px;
  cursor: not-allowed;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
}

select[readonly].select2-hidden-accessible + .select2-container {
  pointer-events: none;
  touch-action: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
  background: #eee;
  box-shadow: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow,
select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
  display: none;
}

</style>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="All {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List" id="refresh_list" type="button"><i class="si si-refresh"></i></button>
            </li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
	<form action="" method="post" enctype="multipart/form-data" id="form-insertNew">
<input type="hidden" value="" id="hide-satjumlah" name="hide-satjumlah">
<input type="hidden" value="" id="hide-satvolume" name="hide-satvolume">
<input type="hidden" value="" id="hide-satberat" name="hide-satberat">
<?php
if($_GET['jenis']=='bahan-baku'){?>
<input type="hidden" value="" id="hide-sortimen" name="hide-sortimen">
<input type="hidden" value="" id="hide-listkayu" name="hide-listkayu">
<?php }else{?>
<input type="hidden" value="" id="hide-satjumlah_akhir" name="hide-satjumlah_akhir">
<input type="hidden" value="" id="hide-satvolume_akhir" name="hide-satvolume_akhir">
<input type="hidden" value="" id="hide-satberat_akhir" name="hide-satberat_akhir">
<input type="hidden" value="" id="hide-produk" name="hide-produk">
<input type="hidden" value="" id="hide-wip" name="hide-wip">
<?php }?>
<input type="hidden" id="hide-ID" name="hide-ID">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
<div class="row">
    <div class="col-sm-6">
		<div class="form-group">
			<label>Periode Awal - Akhir</label>
			<div class="row">
				<div class="col-sm-3">
					<input type="text" class="form-control" name="new-periode_awal" id="new-periode_awal" value="<?=$awaltext?>">
			</div>
				<div class="col-sm-3">
					<sub style="margin-left: -20px">s/d</sub><input type="text" style="margin-left: 0px;margin-top: -18.5px" class="form-control" name="new-periode_akhir" id="new-periode_akhir" value="<?=$akhirtext?>">
			</div>
		</div>
		</div>
        <div class="form-group">
        	<?php
        	$div='';
    		$display='block';
        	if($_GET['jenis']=='wip'){
        		$display='none';
        		$div.='<div class="form-group"><label id="teks-stock"></label></div>';
        		?>
        	<label>Jenis Input</label><br>
        	<select class="select-jenis form-control" name="stocknya" style="width: 50%;">
                <option value="">-- Pilih Jenis Input --</option>
        		<option value="stockawal">Stock Awal</option>
        		<option value="stockakhir">Stock Akhir</option>
            </select>
            <script type="text/javascript">
            	$(document).on('change','.select-jenis',function(){
            		switch($(this).val()){
            			case "stockawal":
            		$('#input-angka').hide(100)
            		$('#input-angka').show(400)
            		$('#div-awal').show(400)
            		$('#div-akhir').hide(100)
        			$('#teks-stock').html('Stock Awal (Hanya diisi satu kali untuk setiap WIP)');
            			break;
            			case "stockakhir":
            		$('#input-angka').hide(100)
            		$('#input-angka').show(400)
            		$('#div-awal').hide(100)
            		$('#div-akhir').show(400)
        			$('#teks-stock').html('Stock Akhir');
            			break;
            			default:
        			$('#teks-stock').html('');
        			$('#input-angka').hide(200)
        			$('#div-awal').hide(200)
        			$('#div-akhir').hide(200)
            			break;
            		} 
            	})
            </script>
        <?php }else{?>
        	<label>Stock Awal</label>
        	<input type="hidden" name="stocknya" value="stockawal">
        <?php }?>
        </div>
        <div id="input-angka" style="display: <?=$display?>">
<?=$div?>
<div id="div-awal" style="display: block;">
        <div class="form-group">
            <label>Jumlah Awal</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-jumlah" id="new-jumlah" value="" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satjumlah" id="new-satjumlah" name="new-satjumlah" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Volume Awal</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-volume" id="new-volume" value="" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satvolume" id="new-satvolume" name="new-satvolume" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <input class="form-control" type="email" id="example-input2-group2" name="example-input2-group2" placeholder="Email">
                <span class="input-group-addon">
                   <select class="form-control new-satberat" id="new-satberat" name="new-satberat" required>
                    </select>
                </span>
            </div>
        </div>
        <div class="form-group">
            <label>Berat Awal</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-berat" id="new-berat" value="" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satberat" id="new-satberat" name="new-satberat" required>
                    </select>
                </div>
            </div>
        </div>
</div>
<div id="div-akhir" style="display: none;">
        <div class="form-group">
            <label>Jumlah Akhir</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-jumlah_akhir" id="new-jumlah_akhir" value="" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satjumlah_akhir" id="new-satjumlah_akhir" name="new-satjumlah_akhir" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Volume Akhir</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-volume_akhir" id="new-volume_akhir" value="" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satvolume_akhir" id="new-satvolume_akhir" name="new-satvolume_akhir" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Berat Akhir</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-berat_akhir" id="new-berat_akhir" value="" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satberat_akhir" id="new-satberat_akhir" name="new-satberat_akhir" required>
                    </select>
                </div>
            </div>
        </div>
</div>
	</div>
	</div>
	<div class="col-sm-6">
	<?php
	if($_GET['jenis']=='bahan-baku'){?>
        <div class="form-group">
            <label>Jenis Sortimen</label>
            <select class="form-control newsortimen" id="new-sortimen" name="new-sortimen">
            </select>
        </div>
        <div class="form-group">
            <label>Jenis Kayu</label>
            <select class="form-control newlistkayu" id="new-listkayu" name="new-listkayu">
            </select>
        </div>
    <?php }else{?>
        <div class="form-group">
            <label>WIP</label>
			<select class="form-control newwip" id="new-wip" name="new-wip">
			</select>
        </div>
        <div class="form-group">
            <label>Menjadi Produk</label>
			<select class="form-control newproduk" id="new-produk" name="new-produk">
			</select>
        </div>
    <?php }?>
	</div>
</div>
	<div class="text-right">
		<button type="reset" class="btn btn-danger faa-parent animated-hover btnBatal">Batal 
			<i class="fas fa-times faa-spin"></i>
		</button>
        <button style="display: none;" type="reset" class="btn btn-primary faa-parent animated-hover btnUpdate" id="btnUpdate">Update 
            <i class="fas fa-check faa-wrench"></i>
        </button>
        <button type="reset" class="btn btn-primary faa-parent animated-hover btnSimpan" id="btnSimpan">Save 
            <i class="fas fa-check faa-wrench"></i>
        </button>
    </div>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
</form>
<br>
<div class="progress progress-mini">
    <div class="progress-bar progress-bar-info" role="progressbar" style="width: 100%"></div>
</div>
            <table width="<?=($_GET['jenis']=='bahan-baku')?'120%':'100%'?>" class="table table-bordered table-striped table-responsive" id="list-DT">
                <thead>
                    <tr>
                       <?php if($_GET['jenis']=='bahan-baku'){?>
                       <th rowspan="2" width="5%">No.</th>
                       <th rowspan="2" width="25%">Sortimen Bahan Baku</th>
                       <th rowspan="2" width="35%">Kayu</th>
                       <th colspan="3" width="12%" class="text-center">Stock Awal</th>
                       <th colspan="3" width="12%" class="text-center">Penerimaan</th>
                       <th colspan="3" width="12%" class="text-center">Penggunaan</th>
                       <th colspan="3" width="12%" class="text-center">Stock Akhir</th>
                       <th rowspan="2" width="5%">Aksi</th>
                        <?php }else{?>
                       <th rowspan="2" width="5%">No.</th>
                       <th rowspan="2" width="20%">WIP</th>
                       <th rowspan="2" width="30%">Produk</th>
                       <th rowspan="2" width="15%">Hs code</th>
                       <th colspan="3" width="12%" class="text-center">Stock Awal</th>
                       <th colspan="3" width="12%" class="text-center">Stock Akhir</th>
                       <th rowspan="2" width="5%">Aksi</th>
                        <?php }?>
                    </tr>
                    <tr>
                       <th width="4%" class="text-center">Volume</th>
                       <th width="4%" class="text-center">Berat</th>
                       <th width="4%" class="text-center">Jumlah</th>
                       <?php if($_GET['jenis']=='bahan-baku'){?>
                       <th width="4%" class="text-center">Volume</th>
                       <th width="4%" class="text-center">Berat</th>
                       <th width="4%" class="text-center">Jumlah</th>
                       <th width="4%" class="text-center">Volume</th>
                       <th width="4%" class="text-center">Berat</th>
                       <th width="4%" class="text-center">Jumlah</th>
                        <?php }?>
                       <th width="4%" class="text-center">Volume</th>
                       <th width="4%" class="text-center">Berat</th>
                       <th width="4%" class="text-center">Jumlah</th>
                    </tr>
                </thead>
            </table>
    </div>
</div>
<script type="text/javascript">
$(function(){
	$('.select-jenis').select2({minimumResultsForSearch: -1})
<?php
if($_GET['jenis']=='bahan-baku'){?>
	$hidesortimen     = $('#hide-sortimen').val().split(',');
	$hidelistkayu     = $('#hide-listkayu').val().split(',');
<?php }else{?>
	$hideproduk = $('#hide-produk').val().split(',');
	$hidewip 	= $('#hide-wip').val().split(',');
$hidesatjumlah_akhir = $('#hide-satjumlah_akhir').val().split(',');
$hidesatvolume_akhir = $('#hide-satvolume_akhir').val().split(',');
$hidesatberat_akhir  = $('#hide-satberat_akhir').val().split(',');
<?php }?>
$hidesatjumlah = $('#hide-satjumlah').val().split(',');
$hidesatvolume = $('#hide-satvolume').val().split(',');
$hidesatberat  = $('#hide-satberat').val().split(',');
    $(document).ready(function() {
getDataSSP("#list-DT","{url_ajax}",<?=($_GET['jenis']=='bahan-baku')?'true':''?>);
getDatePicker('#new-periode_awal')
getDatePicker('#new-periode_akhir')
<?php
if($_GET['jenis']=='bahan-baku'){?>
getSelect2Search($('.newsortimen'),'{json_sortimen}',$hidesortimen,'Sortimen')
getSelect2Search($('.newlistkayu'),'{json_datakayu}',$hidelistkayu,'Jenis Kayu')
<?php }else{?>
getSelect2Search($('.newwip'),'{json_wip}',$hidewip,'WIP')
getSelect2Search($('.newproduk'),'{json_produk}',$hideproduk,'Produk')
// Select Satuan
getSelect2noSearch($('.new-satjumlah_akhir'),'{json_satuanbyjumlah}',$hidesatjumlah_akhir,'Satuan')
getSelect2noSearch($('.new-satvolume_akhir'),'{json_satuanbyvolume}',$hidesatvolume_akhir,'Satuan')
getSelect2noSearch($('.new-satberat_akhir'),'{json_satuanbyberat}',$hidesatberat_akhir,'Satuan')
<?php }?>

// Select Satuan
getSelect2noSearch($('.new-satjumlah'),'{json_satuanbyjumlah}',$hidesatjumlah,'Satuan')
getSelect2noSearch($('.new-satvolume'),'{json_satuanbyvolume}',$hidesatvolume,'Satuan')
getSelect2noSearch($('.new-satberat'),'{json_satuanbyberat}',$hidesatberat,'Satuan')

// end of document
})

$(document).on("click",".btnBatal",function(){
	resetValue()
})
$(document).on("click",".btnSimpan",function(){
// console.log($('#form-insertNew').serialize())
	$.ajax({
	url:"{url_insert}",
	data:{"data":$('#form-insertNew').serialize()},
	dataType:'script',
	success: function(respon){
		$('#list-DT').each(function() {
	  	dt = $(this).dataTable();
	  		dt.fnDraw();
		})
		resetValue();
		return respon;
	}
	});
})
$(document).on("click",".btnUpdate",function(){
	$.ajax({
	url:"{url_insert}",
	data:{"data":$('#form-insertNew').serialize()},
	dataType:'script',
	success: function(respon){
		$('#list-DT').each(function() {
	  	dt = $(this).dataTable();
	  		dt.fnDraw();
		})
		resetValue();
		return respon;
	}
	});
})

$(document).on("click","#refresh_list",function(){
 $('#list-DT').each(function() {
  	dt = $(this).dataTable();
  		dt.fnDraw();
		})
})

$(document).on("click",".edit-row",function(){
var me 	 				=$(this),
	id 	 				=me.attr("data-id"),
	tr 	 				=me.closest('tr');
$('#hide-ID').val(id);
	$.ajax({
	url:"{get_4update}",
	data:{"idstock":id},
	dataType:'json',
	success: function(get){
	<?php if($_GET['jenis']=='bahan-baku'){?>
	var $a=$('#hide-sortimen').val(get.sortimen),
	    $b=$('#hide-listkayu').val(get.kayu),
		$newhidesortimen	=$a.val().split(','),
		$newhidelistkayu	=$b.val().split(','),
		$d=$('#hide-satvolume').val(get.satuan_volume),
	    $e=$('#hide-satberat').val(get.satuan_berat),
	    $c=$('#hide-satjumlah').val(get.satuan_jumlah),
		$newhidesatjumlah	=$c.val().split(','),
		$newhidesatvolume	=$d.val().split(','),
		$newhidesatberat	=$e.val().split(',');
$('#new-volume').val(get.stokawal_volume)
$('#new-berat').val(get.stokawal_berat)
$('#new-jumlah').val(get.stokawal_jumlah)
getSelect2Search($('.newsortimen'),'{json_sortimen}',$newhidesortimen,'Sortimen')
getSelect2Search($('.newlistkayu'),'{json_datakayu}',$newhidelistkayu,'Jenis Kayu')
// Select Satuan
getSelect2noSearch($('.new-satjumlah'),'{json_satuanbyjumlah}',$newhidesatjumlah,'Satuan')
getSelect2noSearch($('.new-satvolume'),'{json_satuanbyvolume}',$newhidesatvolume,'Satuan')
getSelect2noSearch($('.new-satberat'),'{json_satuanbyberat}',$newhidesatberat,'Satuan')
	<?php }else{?>
	var $a=$('#hide-wip').val(get.wip),
	    $b=$('#hide-produk').val(get.produk),
		$newhidewip		=$a.val().split(','),
		$newhideproduk	=$b.val().split(',');
		// alert(get.stokakhir_volume)
if(get.stokakhir_volume>=0 && get.stokakhir_berat>=0 && get.stokakhir_jumlah>=0 || get.stokakhir_volume!=null && get.stokakhir_berat!=null && get.stokakhir_jumlah!=null){
	var $d=$('#hide-satvolume_akhir').val(get.satuan_volume_akhir),
	    $e=$('#hide-satberat_akhir').val(get.satuan_berat_akhir),
	    $c=$('#hide-satjumlah_akhir').val(get.satuan_jumlah_akhir),
		$newhidesatjumlah_akhir	=$c.val().split(','),
		$newhidesatvolume_akhir	=$d.val().split(','),
		$newhidesatberat_akhir	=$e.val().split(',');
	$('.select-jenis').val('stockakhir').trigger('change');
	$('#new-volume_akhir').val(get.stokakhir_volume)
	$('#new-berat_akhir').val(get.stokakhir_berat)
	$('#new-jumlah_akhir').val(get.stokakhir_jumlah)
// Select Satuan
getSelect2noSearch($('.new-satjumlah_akhir'),'{json_satuanbyjumlah}',$newhidesatjumlah_akhir,'Satuan')
getSelect2noSearch($('.new-satvolume_akhir'),'{json_satuanbyvolume}',$newhidesatvolume_akhir,'Satuan')
getSelect2noSearch($('.new-satberat_akhir'),'{json_satuanbyberat}',$newhidesatberat_akhir,'Satuan')
}else{
	var $d=$('#hide-satvolume').val(get.satuan_volume),
	    $e=$('#hide-satberat').val(get.satuan_berat),
	    $c=$('#hide-satjumlah').val(get.satuan_jumlah),
		$newhidesatjumlah	=$c.val().split(','),
		$newhidesatvolume	=$d.val().split(','),
		$newhidesatberat	=$e.val().split(',');
	$('.select-jenis').val('stockawal').trigger('change');
	$('#new-volume').val(get.stokawal_volume)
	$('#new-berat').val(get.stokawal_berat)
	$('#new-jumlah').val(get.stokawal_jumlah)
// Select Satuan
getSelect2noSearch($('.new-satjumlah'),'{json_satuanbyjumlah}',$newhidesatjumlah,'Satuan')
getSelect2noSearch($('.new-satvolume'),'{json_satuanbyvolume}',$newhidesatvolume,'Satuan')
getSelect2noSearch($('.new-satberat'),'{json_satuanbyberat}',$newhidesatberat,'Satuan')
}

$('.select-jenis').attr('readonly','readonly');
getSelect2Search($('.newwip'),'{json_wip}',$newhidewip,'WIP')
getSelect2Search($('.newproduk'),'{json_produk}',$newhideproduk,'Produk')
	<?php }?>
    // var 
$('#new-periode_awal').val(get.periode_awal)
$('#new-periode_akhir').val(get.periode_akhir)
	}
	});

$('#btnSimpan').hide();
$('#btnUpdate').show();
})
$(document).on("click",".delete-row",function(){
var id=$(this).attr("data-id");
var buton=$(this);
	$.ajax({
	url:"{url_delete}",
	data:{"id":id},
	dataType:'text',
	success: function(data){
		$('#list-DT').each(function() {
	  	dt = $(this).dataTable();
	  		dt.fnDraw();
		})
		ToastrSukses("{tJudul} "+data+" berhasil dihapus","Info")
	}
	});
})

function resetValue() {
	$('#periode_awal').val('<?=$awaltext?>')
	$('#periode_akhir').val('<?=$akhirtext?>')
	<?php if($_GET['jenis']=='wip'){?>
$('.select-jenis').val('').trigger('change');
$('.select-jenis').removeAttr('readonly');
	$('.newwip').empty()
	$('.newproduk').empty()
	$('.new-satjumlah_akhir').empty()
	$('.new-satvolume_akhir').empty()
	$('.new-satberat_akhir').empty()
<?php }else{?>
	$('.newsortimen').empty()
	$('.newlistkayu').empty()
<?php }?>
$('#hide-ID').val('');
$('#btnSimpan').show();
$('#btnUpdate').hide();
	$('.new-satjumlah').empty()
	$('.new-satvolume').empty()
	$('.new-satberat').empty()
$('#hide-satjumlah').val('')
$('#hide-satvolume').val('')
$('#hide-satberat').val('')
$('#hide-sortimen').val('')
$('#hide-listkayu').val('')
}

// end of function
})
</script>