<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?php
$uri=$this->uri->segment(1);
    $uri2=$this->uri->segment(2);
	 	$uri3=$this->uri->segment(3);
?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="<?=($uri2=='new-pengajuan')?'New':'Update'?> {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<?if ($uri2==$url_kedua){?>
						<li class="dropdown-header">New {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_index}">All {tJudul}</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All {tJudul} </li>
						<li>
							<!-- <a tabindex="-1" id="testing" href="#">New {tJudul}</a> -->
							<a tabindex="-1" href="{url_addnew}">New {tJudul}</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		<form method="post" action="{url_proses}">
<input type="hidden" value="{newbuyer}" id="hide-buyer" name="hide-buyer">

<input type="hidden" value="{newnegara}" id="hide-negara" name="hide-negara">
<input type="hidden" value="{newmatauang}" id="hide-matauang" name="hide-matauang">
<input type="hidden" value="{newloading}" id="hide-loading" name="hide-loading">
<input type="hidden" value="{newdischarge}" id="hide-discharge" name="hide-discharge">
<input type="hidden" value="{newidnegara_dis}" id="hide-idnegara_dis" name="hide-idnegara_dis">
<input type="hidden" value="{newstatus_liu}" id="hide-status_liu" name="hide-status_liu">
<input type="hidden" value="<?=(!empty($uri3))?$uri3:''?>" id="hide-ID" name="hide-ID">
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Nama Pembeli</label>
            <select class="form-control newbuyer" id="new-buyer" name="new-buyer">
            </select>
        </div>
        <div class="form-group">
            <label>Alamat Pembeli</label>
            <textarea class="form-control" rows="5" name="new-alamat">{newalamat}</textarea>
        </div>
        <div class="form-group">
            <label>Negara Tujuan</label>
            <div class="row">
                <div class="col-sm-6">
            <select class="form-control new-negara" id="new-negara" name="new-negara">
            </select>
            </div>
            </div>
        </div>
        <div class="form-group">
            <label>ISO Code</label>
            <input type="text" name="new-iso" class="form-control new-iso" id="new-iso" value="{newiso}">
        </div>
        <div class="form-group">
            <label>Mata Uang</label>
            <select class="form-control new-matauang" id="new-matauang" name="new-matauang">
            </select>
        </div>
        <div class="form-group">
            <label>Port of Loading</label>
            <select class="form-control new-loading" id="new-loading" name="new-loading">
            </select>
        </div>
        <div class="form-group">
            <label>Port of Discharge</label>
            <select class="form-control new-discharge" id="new-discharge" name="new-discharge">
            </select>
        </div>
        <div class="form-group">
            <label>Keterangan</label>
            <textarea class="form-control" rows="5" name="new-keterangan">{newketerangan}</textarea>
        </div>
        <div class="form-group">
            <label>NPWP</label>
            <div class="row">
                <div class="col-sm-6" style="display: none;">
            <input type="text" name="new-etpik" class="form-control" value="{newetpik}">
                </div>
                <div class="col-sm-6">
            <input type="text" readonly name="new-npwp" class="form-control" value="{newnpwp}">
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-sm-6">
        <div class="form-group">
            <label>Status Dokumen</label>
            <select class="form-control new-status" id="new-status" name="new-status">
                <option value=""></option>
                <option value="vlegal" <?php echo ($newstatus=='vlegal')?'selected':'' ?>>Dokumen V-Legal</option>
                <option value="non-vlegal" <?php echo ($newstatus=='non-vlegal')?'selected':'' ?>>Dokumen Non V-Legal</option>
            </select>
                </div>
            </div>
            <?php if(!empty($uri3)){?>
                <div class="col-sm-6">
            <div class="form-group">
                <label>Status Edit Dokumen</label> 
                <select class="form-control new-status_edit" id="new-status_edit" name="new-status_edit">
                    <option value=""></option>
                    <option value="edit">Penambahan/Perubahan Data</option>
                    <option value="replace">Perubahan Dokumen V-Legal</option>
                    <option value="extend">Perpanjangan Dokumen V-Legal</option>
                    <option value="cancel">Pembatalan Dokumen V-Legal</option>
                </select>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>No. Invoice</label>
            <input type="text" name="new-invoice" id="new-invoice" class="form-control" value="{newinvoice}">
        </div>
        <div class="form-group">
            <label>Tanggal Invoice</label>
            <input type="text" name="new-tglinvoice" id="new-tglinvoice" class="form-control" value="{newtglinvoice}">
        </div>
        <div class="form-group">
            <label>Nomor PEB</label>
            <input type="text" name="new-peb" id="new-peb" class="form-control" value="{newpeb}">
        </div>
        <div class="form-group">
            <label>Nomor B/L</label>
            <input type="text" name="new-bl" id="new-bl" class="form-control" value="{newbl}">
        </div>
        <div class="form-group">
            <label>Packing List</label>
            <input type="text" name="new-packinglist" id="new-packinglist" class="form-control" value="{newpackinglist}">
        </div>
        <div class="form-group">
            <label>Alat Angkut</label>
            <select class="form-control new-vessel" id="new-vessel" name="new-vessel">
                <option value=""></option>
                <option value="1" <?php echo ($newvessel=='1')?'selected':'' ?>>By Sea</option>
                <option value="2" <?php echo ($newvessel=='2')?'selected':'' ?>>By Air</option>
                <option value="3" <?php echo ($newvessel=='3')?'selected':'' ?>>By Land</option>
            </select>
        </div>
        <div class="form-group">
            <label>Tanggal Shipment</label>
            <input type="text" name="new-tglship" id="new-tglship" class="form-control" value="{newtglship}">
        </div>
        <div class="form-group">
            <label>No. Sertifikat</label>
            <input type="text" readonly name="new-sertifikat" id="new-sertifikat" class="form-control" value="{newsertifikat}">
        </div>
        <div class="form-group">
            <label>Lokasi Stuffing</label>
            <input type="text" name="new-stuffing" id="new-stuffing" class="form-control" value="{newstuffing}">
        </div>
    </div> 
</div>
        <div class="text-right">
            <button class="btn btn-primary faa-parent animated-hover">Save <i class="fas fa-check faa-wrench"></i></button>
        </div>
    </form>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
		<div class="block-footer">&nbsp;</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
$hidebuyer        = $('#hide-buyer').val().split(',');

$negaraS        = $('#hide-negara').val().split(',');
$negara         = $('.new-negara');
$hidematauang   = $('#hide-matauang').val().split(',');
$hideloading    = $('#hide-loading').val().split(',');
$hidedischarge  = $('#hide-discharge').val().split(',');
$idnegara_dis   = $('#hide-idnegara_dis').val();

    $(document).ready(function() {
getSelect2Search($('.newbuyer'),'{json_buyer}',$hidebuyer,'Pembeli')
getDatePicker('#new-tglship')
getDatePicker('#new-tglinvoice')
getSelect2Search($('.new-matauang'),'{json_matauang}',$hidematauang,'Mata Uang')
getSelect2Search($('.new-loading'),'{json_loading}',$hideloading,'Port of Loading')
getSelect2Search($('.new-discharge'),'{json_discharge}?kode='+$idnegara_dis,$hidedischarge,'Port of Discharge')
getSelect2Search($('.new-negara'),'{json_negara}',$negaraS,'Negara')
$('#new-status_edit').select2({
    minimumResultsForSearch:-1,
    placeholder: "-- Pilih Status Edit Dokumen --"})
$('#new-status').select2({
    minimumResultsForSearch:-1,
    placeholder: "-- Pilih Status Dokumen --"})
$('#new-vessel').select2({
    minimumResultsForSearch:-1,
    placeholder: "-- Pilih Alat Angkut --"})

$negara.change(function(){
$('.new-discharge').css('width','100%')
var a=$('.new-negara option:selected').val();
$('#new-iso').val(a)
getSelect2noSearch($('.new-discharge'),'{json_discharge}?kode='+a,$hidedischarge,'Port of Discharge')
})
$('.newbuyer').change(function(){
    var idbuyer=$(this).find(':selected').val();
$.ajax({
    url: '{site_url}json/buyer',
    type: 'get',
    dataType: 'json',
    data: {id: idbuyer,limitnya:1},
    success:function(data){
        $('textarea[name=new-alamat]').val(data.alamat)
    }
})

}) 

/** End Document */
})

})
</script>

