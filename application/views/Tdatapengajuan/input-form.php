	<div class="col-sm-6">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
        <div class="form-group">
            <label>Nama produk</label>
            <select class="form-control newproduk" id="new-produk" name="new-produk">
            </select>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-sm-6">
                    <label>Jenis Kayu</label>
                    <select class="form-control newlistkayu" id="new-listkayu" name="new-listkayu">
                    </select>
                </div>
                <div class="col-sm-6">
                    <label>Asal Kayu</label>
                    <select class="form-control new-negara" id="new-negara" name="new-negara">
                    </select>
                </div>
            </div>
        </div>
     <!--    <div class="form-group pull-right" style="margin-top: -15px">
            <button type="button" class="btn btn-info btn-xs" id="dup-kayu">Tambah Kayu</button>
        </div> -->
	</div>
	<div class="col-sm-6">
        <div class="form-group">
            <label>Jumlah</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-jumlah" id="new-jumlah" value="{newjumlah}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satjumlah" id="new-satjumlah" name="new-satjumlah" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Volume</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-volume" id="new-volume" value="{newvolume}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satvolume" id="new-satvolume" name="new-satvolume" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Berat</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-berat" id="new-berat" value="{newberat}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satberat" id="new-satberat" name="new-satberat" required>
                    </select>
                </div>
            </div>
        </div>
	</div>
</div>
	<div class="text-right">
		<button type="reset" class="btn btn-danger faa-parent animated-hover btnBatal">Batal 
			<i class="fas fa-times faa-spin"></i>
		</button>
		<button style="display: none;" type="reset" class="btn btn-primary faa-parent animated-hover btnUpdate" id="btnUpdate">Update 
			<i class="fas fa-check faa-wrench"></i>
		</button>
		<button type="reset" class="btn btn-primary faa-parent animated-hover btnSimpan" id="btnSimpan">Save 
			<i class="fas fa-check faa-wrench"></i>
		</button>
	</div>
</form>