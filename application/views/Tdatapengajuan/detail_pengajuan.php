<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<style type="text/css">
	.blueIcon{
		color:#09f;
	}
	.blueIcon:hover{
		color:#0cf;
	}
	.greenIcon{
		color:#0c0;
	}
	.greenIcon:hover{
		color:#0f0;
	}
</style>
<?php
$uri=$this->uri->segment(1);
	$uri2=$this->uri->segment(2);
            ?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="All {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List" id="refresh_list" type="button"><i class="si si-refresh"></i></button>
            </li>
            
		</ul>
		<h3 class="block-title">Detail {title}</h3>
	</div>
	<div class="block-content">
	<form action="" method="post" enctype="multipart/form-data" id="form-insertNew">
<input type="hidden" value="{newsatjumlah}" id="hide-satjumlah" name="hide-satjumlah">
<input type="hidden" value="{newsatvolume}" id="hide-satvolume" name="hide-satvolume">
<input type="hidden" value="{newsatberat}" id="hide-satberat" name="hide-satberat">
<input type="hidden" value="{newproduk}" id="hide-produk" name="hide-produk">
<input type="hidden" value='{newlistkayu}' id="hide-listkayu" name="hide-listkayu">
<input type="hidden" value="{newnegara}" id="hide-negara" name="hide-negara">
<input type="hidden" id="hide-ID" name="hide-ID" value="{hide-idekspor}">
<div class="row">
<div class="col-sm-12">
	<table class="table table-striped table-bordered table-responsive" id="list-DT">
		<tbody>
			<tr>
			   <th width="16%">No. Invoice & Tgl. Invoice</th>
			   <td>: <?=$get['invoice'].' & '.encode_date($get['tglinvoice']) ?></td>
			   <td width="1%">&nbsp;</td>
			   <th width="16%">Loading / Discharge</th>
			   <td>: <?=$get['loading'].' / '.$get['discharge'] ?></td>
			</tr>
			<tr>
			   <th width="16%">Pembeli / Negara / ISO</th>
			   <td>: <?=$get['buyer'].' / '.$get['negara'].' / '.$get['iso'] ?></td>
			   <td width="1%">&nbsp;</td>
			   <th width="16%">Alamat</th>
			   <td>: <?=$get['alamat'] ?></td>
			</tr>
			<tr>
			   <th width="16%">No. PEB / No. BL</th>
			   <td>: <?=$get['peb'].' / '.$get['bl'] ?></td>
			   <td width="1%">&nbsp;</td>
			   <th width="16%">Packing List</th>
			   <td>: <?=$get['packinglist'] ?></td>
			</tr>
			<tr>
			   <th width="16%">Tgl. Shipment</th>
			   <td>: <?=encode_date($get['tglship']) ?></td>
			   <td width="1%">&nbsp;</td>
			   <th width="16%">Vessel</th>
			   <td>: <?=get_vessel($get['vessel']) ?></td>
			</tr>
			<tr>
			   <th width="16%">	NPWP<br>
			   					Sertifikat</th>
			   <td>	: <?=$get['npwp'] ?><br>
			   		: <?=$get['sertifikat'] ?></td>
			   <td width="1%">&nbsp;</td>
			   <th width="16%">Keterangan</th>
			   <td>: <?=$get['keterangan'] ?></td>
			</tr>
			<tr>
			   <th width="16%">Status Dokumen LIU</th>
			   <td>: <?=$get['status_liu'] ?></td>
			   <td width="1%">&nbsp;</td>
			   <th width="16%">Mata Uang</th>
			   <td>: <?=$get['valuta'].' / '.$get['matauang'] ?></td>
			</tr>
			<tr>
			   <th width="16%">Status Dokumen</th>
			   <td>: <?=$get['status_dokumen'] ?></td>
			   <td width="1%">&nbsp;</td>
			   <th width="16%">&nbsp;</th>
			   <td>
			   	<a href="" class="btn-lg" style="margin-right: -20px"><i class="fas fa-edit fa-3x blueIcon"></i></a>
			   	<a href="" class="btn-lg" style="margin-right: -20px;"><i class="far fa-file-word fa-3x blueIcon"></i></a>
			   	<a href="" class="btn-lg"><i class="far fa-check-circle fa-3x greenIcon"></i></a>
			   </td>
			</tr>
			<tr>
			   <th width="16%">Lokasi Stuffing</th>
			   <td colspan="4">: <?=$get['stuffing'] ?></td>
			</tr>
		</tbody>
	</table>
</div>
	<?php $this->load->view($this->folder.'/input-form');?>
<br>
<div class="progress progress-mini">
    <div class="progress-bar progress-bar-info" role="progressbar" style="width: 100%"></div>
</div>
<div id="tbl-Pagination"></div>
<nav id="pagination_link" class="text-center"></nav>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
<script type="text/javascript">
$(function(){
$hideproduk     = $('#hide-produk').val().split(',');
$hidelistkayu     = $('#hide-listkayu').val().split('#');

$negaraS = $('#hide-negara').val().split('#');
$negara  = $('.new-negara');

$hidesatjumlah = $('#hide-satjumlah').val().split(',');
$hidesatvolume = $('#hide-satvolume').val().split(',');
$hidesatberat  = $('#hide-satberat').val().split(',');
    $(document).ready(function() {
// getDataSSP("#list-DT","{url_ajax}",true);
getDatePicker('#new-periode_awal')
getDatePicker('#new-periode_akhir')
getPeriode()
getSelect2Search($('.newproduk'),'{json_produk}',$hideproduk,'produk')
// getSelect2Search($('.newlistkayu'),'{json_datakayu}',$hidelistkayu,'Jenis Kayu')
// getSelect2Search($('.new-negara'),'{json_negara}',$negaraS,'Asal Kayu')
// Select Satuan
getSelect2noSearch($('.new-satjumlah'),'{json_satuanbyjumlah}',$hidesatjumlah,'Satuan')
getSelect2noSearch($('.new-satvolume'),'{json_satuanbyvolume}',$hidesatvolume,'Satuan')
getSelect2noSearch($('.new-satberat'),'{json_satuanbyberat}',$hidesatberat,'Satuan')
getMultipleSelect($('.newlistkayu'),'{json_datakayu}',$hidelistkayu,'Jenis Kayu',10);
getMultipleSelect($('.new-negara'),'{json_negara}',$negaraS,'Asal Kayu',10);

function load_dataTable(page)
 {
  $.ajax({
   url:'<?=base_url(); ?>dokumen/page-detail/'+page,
   method:'GET',
   data:{'idekspor':$('#hide-ID').val()},
   dataType:'json',
   success:function(data)
   {
    $('#tbl-Pagination').html(data.dataTable);
    $('#pagination_link').html(data.pagination_link);
   }
  });
 }
 
 load_dataTable(1);
 $(document).on("click", ".pagination li a", function(event){
  event.preventDefault();
  var page = $(this).data("ci-pagination-page");
  load_dataTable(page);
 });
// end of document
})

$(document).on("click","#dup-kayu",function(){

})

$(document).on("click",".btnBatal",function(){
$('#btnSimpan').show();
$('#btnUpdate').hide();
	resetValue()
})
$(document).on("click",".btnSimpan",function(){
// console.log($('#form-insertNew').serialize())
	$.ajax({
	url:"{url_insert}",
	data:{"data":$('#form-insertNew').serialize()},
	dataType:'script',
	success: function(respon){
		$('#list-DT').each(function() {
	  	dt = $(this).dataTable();
	  		dt.fnDraw();
		})
		resetValue();
		return respon;
	}
	});
})
$(document).on("click",".btnUpdate",function(){
// alert('{url_update}')
// console.log($('#form-insertNew').serialize())
	$.ajax({
	url:"{url_insert}",
	data:{"data":$('#form-insertNew').serialize()},
	dataType:'script',
	success: function(respon){
		$('#list-DT').each(function() {
	  	dt = $(this).dataTable();
	  		dt.fnDraw();
		})
$('#btnSimpan').show();
$('#btnUpdate').hide();
		resetValue();
		return respon;
	}
	});
})

$(document).on("click","#refresh_list",function(){
 $('#list-DT').each(function() {
  	dt = $(this).dataTable();
  		dt.fnDraw();
		})
		resetValue();
})

$(document).on("click",".edit-row",function(){
var me 	 				=$(this),
	id 	 				=me.attr("data-id"),
	tr 	 				=me.closest('tr');
	$.ajax({
	url:"{site_url}produksi/realisasi/edit",
	data:{"id":id},
	dataType:'json',
	success: function(data){
	var $j=$('#hide-satjumlah').val(data.satuan_jumlah+`,`+ucFirst(data.satuan_jumlah)),
		$v=$('#hide-satvolume').val(data.satuan_volume+`,`+ucFirst(data.satuan_volume)),
		$b=$('#hide-satberat').val(data.satuan_berat+`,`+ucFirst(data.satuan_berat)),
		$newhideproduk		=data.selectproduk.split(','),
		$newhidelistkayu	=data.selectkayu.split(',')
		$newhidesatjumlah	=$j.val().split(','),
		$newhidesatvolume	=$v.val().split(','),
		$newhidesatberat	=$b.val().split(',');
			$('#new-periode_awal').val(data.periode_awal)
			$('#new-periode_akhir').val(data.periode_akhir)
			getSelect2Search($('.newproduk'),'{json_produk}',$newhideproduk,'Produk')
			getSelect2Search($('.newlistkayu'),'{json_datakayu}',$newhidelistkayu,'Jenis Kayu')
			// Select Satuan
			getSelect2noSearch($('.new-satjumlah'),'{json_satuanbyjumlah}',$newhidesatjumlah,'Satuan')
			getSelect2noSearch($('.new-satvolume'),'{json_satuanbyvolume}',$newhidesatvolume,'Satuan')
			getSelect2noSearch($('.new-satberat'),'{json_satuanbyberat}',$newhidesatberat,'Satuan')

			$('#new-jumlah').val(data.jumlah)
			$('#new-volume').val(data.volume)
			$('#new-berat').val(data.berat)
		}
	});
$('#hide-ID').val(id)
$('#btnSimpan').hide();
$('#btnUpdate').show();
})
$(document).on("click",".delete-row",function(){
var id=$(this).attr("data-id");
var buton=$(this);
	$.ajax({
	url:"{url_delete}",
	data:{"id":id},
	dataType:'text',
	success: function(data){
		$('#list-DT').each(function() {
	  	dt = $(this).dataTable();
	  		dt.fnDraw();
		})
		ToastrSukses("{tJudul} "+data+" berhasil dihapus","Info")
	}
	});
})

function getPeriode(){
	$.ajax({
		url:'{url_periode}',
		dataType:'json',
		success:function(data){
			$('#new-periode_awal').val(data.periode_awal)
			$('#new-periode_akhir').val(data.periode_akhir)
		}
	})
}

function resetValue() {
	getPeriode()
$('#hide-ID').val('');
	$('.newproduk').empty()
	$('.newlistkayu').empty()
	$('.new-satjumlah').empty()
	$('.new-satvolume').empty()
	$('.new-satberat').empty()
$('#hide-satjumlah').val('')
$('#hide-satvolume').val('')
$('#hide-satberat').val('')
$('#hide-produk').val('')
$('#hide-listkayu').val('')
}

// end of function
})
</script>