<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?php
$uri=$this->uri->segment(1);
    $uri2=$this->uri->segment(2);
	 	$uri3=$this->uri->segment(3);
$tipe=$_GET['tipe'];
$page=(!empty($_GET['p']))?$_GET['p']:'';
?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="<?=(!empty($page)?'Update':'New')?> {tJudul} <?=ucfirst($tipe)?>"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<?if ($page==$url_kedua){?>
						<li class="dropdown-header">New {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_index}">All {tJudul}</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All {tJudul} </li>
						<li>
							<!-- <a tabindex="-1" id="testing" href="#">New {tJudul}</a> -->
							<a tabindex="-1" href="{url_addnew}">New {tJudul}</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		<form method="post" action="{url_proses}">
<input type="hidden" value="{newsatjumlah}" id="hide-satjumlah" name="hide-satjumlah">
<input type="hidden" value="{newsatvolume}" id="hide-satvolume" name="hide-satvolume">
<input type="hidden" value="{newsatberat}" id="hide-satberat" name="hide-satberat">
<input type="hidden" value="{newsortimen}" id="hide-sortimen" name="hide-sortimen">
<input type="hidden" value="{newproduk}" id="hide-produk" name="hide-produk">
<input type="hidden" value="{newlistkayu}" id="hide-listkayu" name="hide-listkayu">
<input type="hidden" value="{newjenispengeluaran}" id="hide-jenispengeluaran" name="hide-jenispengeluaran">
<input type="hidden" value="<?=(!empty($uri3))?$uri3:''?>" id="hide-ID" name="hide-ID">
<div class="row">
	<div class="col-sm-6">
        <?php
        if(empty($_GET['id'])){
            if($Laper['periode_awal']=='0000-00-00'&&$Laper['periode_akhir']=='0000-00-00'){
                $awal='';
                $awaltext='';
                $akhir='';
                $akhirtext='';
            }else{
                $awal  =tsi_date($Laper['periode_awal']);
                $awaltext  =encode_date($Laper['periode_awal']);
                $akhir =tsi_date($Laper['periode_akhir']);
                $akhirtext =encode_date($Laper['periode_akhir']);
            }
            ?>
        <div class="form-group">
            <label class="text-danger">Data Input Terakhir adalah Periode : <?=$awal.' - '.$akhir?></label>
        </div>
        <?php }?>
		<div class="form-group">
			<label>Periode</label>
			<div class="row">
				<div class="col-sm-6">
					<input type="text" class="form-control" name="new-periode_awal" id="new-periode_awal" value="<?=(empty($_GET['id']))?$awaltext:$newperiode_awal?>">
			</div>
				<div class="col-sm-6">
					<sub style="margin-left: -20px">s/d</sub><input type="text" style="margin-left: 0px;margin-top: -18.5px" class="form-control" name="new-periode_akhir" id="new-periode_akhir" value="<?=(empty($_GET['id']))?$akhirtext:$newperiode_akhir?>">
			</div>
		</div>
		</div>
        <div class="form-group">
            <label>Jenis Pengeluaran</label>
            <div class="row">
                <div class="col-sm-6">
                    <select class="form-control new-jenispengeluaran" id="new-jenispengeluaran" name="new-jenispengeluaran" required>
                    </select>
                </div>
            </div>
        </div>
		<div class="form-group">
			<label>Nama Produk</label>
			<select class="form-control newproduk" id="new-produk" name="new-produk">
			</select>
		</div>
        <div class="form-group">
            <label>Jenis Kayu</label>
            <select class="form-control newlistkayu" id="new-listkayu" name="new-listkayu">
            </select>
        </div>
	</div>
	<div class="col-sm-6">
        <div class="form-group">
            <label>Jenis Sortimen</label>
            <select class="form-control newsortimen" id="new-sortimen" name="new-sortimen">
            </select>
        </div>
        <div class="form-group">
            <label>Jumlah</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-jumlah" value="{newjumlah}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satjumlah" id="new-satjumlah" name="new-satjumlah" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Volume</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-volume" value="{newvolume}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satvolume" id="new-satvolume" name="new-satvolume" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Berat</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-berat" value="{newberat}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satberat" id="new-satberat" name="new-satberat" required>
                    </select>
                </div>
            </div>
        </div>
	</div>
</div>
		<div class="text-right">
			<button class="btn btn-primary faa-parent animated-hover">Save <i class="fas fa-check faa-wrench"></i></button>
		</div>
    </form>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
		<div class="block-footer">&nbsp;</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
$hidesortimen     = $('#hide-sortimen').val().split(',');
$hideproduk       = $('#hide-produk').val().split(',');
$hidelistkayu     = $('#hide-listkayu').val().split(',');

$hidesatjumlah = $('#hide-satjumlah').val().split(',');
$hidesatvolume = $('#hide-satvolume').val().split(',');
$hidesatberat  = $('#hide-satberat').val().split(',');
$hidejenispengeluaran  = $('#hide-jenispengeluaran').val().split(',');


    $(document).ready(function() {
getDatePicker('#new-periode_awal')
getDatePicker('#new-periode_akhir')
getSelect2Search($('.newsortimen'),'{json_sortimen}',$hidesortimen,'Sortimen')
getSelect2Search($('.newproduk'),'{json_produk}',$hideproduk,'Produk')
getSelect2Search($('.newlistkayu'),'{json_datakayu}',$hidelistkayu,'Jenis Kayu')
// Select Satuan
getSelect2noSearch($('.new-satjumlah'),'{json_satuanbyjumlah}',$hidesatjumlah,'Satuan')
getSelect2noSearch($('.new-satvolume'),'{json_satuanbyvolume}',$hidesatvolume,'Satuan')
getSelect2noSearch($('.new-satberat'),'{json_satuanbyberat}',$hidesatberat,'Satuan')
getSelect2noSearch($('.new-jenispengeluaran'),'{json_datapengeluaran}',$hidejenispengeluaran,'Jenis Pengeluaran')


/** End Document */
})


})
</script>

