<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?php
$uri=$this->uri->segment(1);
    $uri2=$this->uri->segment(2);
	 	$uri3=$this->uri->segment(3);
$tipe=$_GET['tipe'];
$page=(!empty($_GET['p']))?$_GET['p']:'';
?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="<?=(!empty($page)?'Update':'New')?> {tJudul} <?=ucfirst($tipe)?>"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<?if ($page==$url_kedua){?>
						<li class="dropdown-header">New {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_index}">All {tJudul}</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All {tJudul} </li>
						<li>
							<!-- <a tabindex="-1" id="testing" href="#">New {tJudul}</a> -->
							<a tabindex="-1" href="{url_addnew}">New {tJudul}</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		<form method="post" action="{url_proses}">
<?php
if($_GET['tipe']=='lokal'){
?>
<input type="hidden" value="{newclient_propinsi}" id="hide-provinsi" name="hide-provinsi">
<input type="hidden" value="{newclient_kabupaten}" id="hide-kabupaten" name="hide-kabupaten">
<input type="hidden" value="{newclient_statushutan}" id="hide-statushutan" name="hide-statushutan">
<input type="hidden" value="{newclient_dokumen}" id="hide-dokumen" name="hide-dokumen">
<input type="hidden" value="{newsertifikasi}" id="hide-sertifikasi" name="hide-sertifikasi">
<?php }else{?>
<input type="hidden" value="{newnegara}" id="hide-negara" name="hide-negara">
<?php }?>
<input type="hidden" value="{newdoksatjumlah}" id="hide-doksatjumlah" name="hide-doksatjumlah">
<input type="hidden" value="{newdoksatvolume}" id="hide-doksatvolume" name="hide-doksatvolume">
<input type="hidden" value="{newdoksatberat}" id="hide-doksatberat" name="hide-doksatberat">
<input type="hidden" value="{newfissatjumlah}" id="hide-fissatjumlah" name="hide-fissatjumlah">
<input type="hidden" value="{newfissatvolume}" id="hide-fissatvolume" name="hide-fissatvolume">
<input type="hidden" value="{newfissatberat}" id="hide-fissatberat" name="hide-fissatberat">
<input type="hidden" value="{newsortimen}" id="hide-sortimen" name="hide-sortimen">
<input type="hidden" value="{newsupplier}" id="hide-supplier" name="hide-supplier">
<input type="hidden" value="{newdokumenkayu}" id="hide-dokumenkayu" name="hide-dokumenkayu">
<input type="hidden" value="{newfisikkayu}" id="hide-fisikkayu" name="hide-fisikkayu">
<input type="hidden" value="<?=(!empty($uri3))?$uri3:''?>" id="hide-ID" name="hide-ID">
<div class="row">
	<div class="col-sm-6">
        <?php
        if(empty($_GET['id'])){
            if($Laper['periode_awal']=='0000-00-00'&&$Laper['periode_akhir']=='0000-00-00'){
                $awal='';
                $awaltext='';
                $akhir='';
                $akhirtext='';
            }else{
                $awal  =tsi_date($Laper['periode_awal']);
                $awaltext  =encode_date($Laper['periode_awal']);
                $akhir =tsi_date($Laper['periode_akhir']);
                $akhirtext =encode_date($Laper['periode_akhir']);
            }
            ?>
        <div class="form-group">
            <label class="text-danger">Data Input Terakhir adalah Periode : <?=$awal.' - '.$akhir?></label>
        </div>
        <?php }?>
        <div class="form-group">
            <label>Periode</label>
            <div class="row">
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="new-periode_awal" id="new-periode_awal" value="<?=(empty($_GET['id']))?$awaltext:$newperiode_awal?>">
            </div>
                <div class="col-sm-6">
                    <sub style="margin-left: -20px">s/d</sub><input type="text" style="margin-left: 0px;margin-top: -18.5px" class="form-control" name="new-periode_akhir" id="new-periode_akhir" value="<?=(empty($_GET['id']))?$akhirtext:$newperiode_akhir?>">
            </div>
        </div>
        </div>
		<div class="form-group">
			<label>Supplier</label>
			<select class="form-control newsupplier" id="new-supplier" name="new-supplier">
			</select>
		</div>
        <?php
        if($_GET['tipe']=='lokal'){
        ?>
        <div class="form-group">
            <label>Sertifikasi</label>
            <div class="row">
                <div class="col-sm-6">
            <select class="form-control new-sertifikasi" id="new-sertifikasi" name="new-sertifikasi" required>
            </select>
            </div>
            </div>
        </div>
        <div class="form-group">
            <label>Provinsi & Kabupaten</label>
            <div class="row">
                <div class="col-sm-6">
            <select class="form-control new-provinsi" id="new-provinsi" name="new-provinsi" required>
            </select>
            </div>
                <div class="col-sm-6">
            <select class="form-control new-kabupaten" id="new-kabupaten" name="new-kabupaten" placeholder="Pilih Kabupaten Dulu">
            </select>
            </div>
        </div>
        </div>
		<?php
        }else
        if($_GET['tipe']=='import'){
        ?>
        <div class="form-group">
			<label>Asal Kayu</label>
			<div class="row">
				<div class="col-sm-6">
            <select class="form-control new-negara" id="new-negara" name="new-negara">
            </select>
			</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<div class="col-sm-6">
        <?php
        if($_GET['tipe']=='lokal'){
        ?>
        <div class="form-group">
            <label>Status Hutan</label>
            <div class="row">
                <div class="col-sm-6">
                    <select class="form-control new-statushutan" id="new-statushutan" name="new-statushutan" required>
                    </select>
                </div>
            </div>
        </div>
		<div class="form-group">
            <div class="row">
				<div class="col-sm-6">
			<label>Jenis Dokumen Angkutan</label>
					<select class="form-control new-dokumen" id="new-dokumen" name="new-dokumen" required>
					</select>
				</div>
			</div>
		</div>
        <?php }?>
        <div class="form-group">
            <label>Jenis Sortimen</label>
            <select class="form-control newsortimen" id="new-sortimen" name="new-sortimen">
            </select>
        </div>
	</div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="block block-themed block-rounded block-bordered">
            <div class="block-header bg-smooth-dark">
                <h3 class="block-title">DATA DOKUMEN {dJudul}</h3>
            </div>
            <div class="block-content">
        <div class="form-group">
            <label>Jenis Kayu</label>
            <select class="form-control newdokumenkayu" id="new-dokumenkayu" name="new-dokumenkayu">
            </select>
        </div>
        <div class="form-group">
            <label>Jumlah</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-dokjumlah" value="{newdokjumlah}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-doksatjumlah" id="new-doksatjumlah" name="new-doksatjumlah" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Volume</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-dokvolume" value="{newdokvolume}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-doksatvolume" id="new-doksatvolume" name="new-doksatvolume" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Berat</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-dokberat" value="{newdokberat}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-doksatberat" id="new-doksatberat" name="new-doksatberat" required>
                    </select>
                </div>
            </div>
        </div>
                
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="block block-themed block-rounded block-bordered">
            <div class="block-header bg-smooth-dark">
                <h3 class="block-title">DATA FISIK {dJudul}</h3>
            </div>
            <div class="block-content">
        <div class="form-group">
            <label>Jenis Kayu</label>
            <select class="form-control newfisikkayu" id="new-fisikkayu" name="new-fisikkayu">
            </select>
        </div>
        <div class="form-group">
            <label>Jumlah</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-fisjumlah" value="{newfisjumlah}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-fissatjumlah" id="new-fissatjumlah" name="new-fissatjumlah" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Volume</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-fisvolume" value="{newfisvolume}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-fissatvolume" id="new-fissatvolume" name="new-fissatvolume" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Berat</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-fisberat" value="{newfisberat}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-fissatberat" id="new-fissatberat" name="new-fissatberat" required>
                    </select>
                </div>
            </div>
        </div>
                
                
            </div>
        </div>        
    </div>
</div>
		<div class="text-right">
			<button class="btn btn-primary faa-parent animated-hover">Save <i class="fas fa-check faa-wrench"></i></button>
		</div>
    </form>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
		<div class="block-footer">&nbsp;</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
<?php
if($_GET['tipe']=='lokal'){
?>
$hideProvinsi 		= $('#hide-provinsi').val().split(',');
$hideKabupaten 		= $('#hide-kabupaten').val().split(',');
$hideSertifikasi	= $('#hide-sertifikasi').val().split(',');
$hideStatushutan	= $('#hide-statushutan').val().split(',');
$hideDokumen        = $('#hide-dokumen').val().split(',');
$provinsi  			= $('.new-provinsi');
$kabupaten  		= $('.new-kabupaten');
$sertifikasi  		= $('.new-sertifikasi');
$hutan  			= $('.new-statushutan');
$dokumen            = $('.new-dokumen');
<?php }else{?>
$negaraS = $('#hide-negara').val().split(',');
$negara  = $('.new-negara');
<?php }?>
$hidesortimen        = $('#hide-sortimen').val().split(',');
$hidesupplier        = $('#hide-supplier').val().split(',');
$hidedokumenkayu     = $('#hide-dokumenkayu').val().split(',');
$hidefisikkayu       = $('#hide-fisikkayu').val().split(',');

$hidedoksatjumlah = $('#hide-doksatjumlah').val().split(',');
$hidedoksatvolume = $('#hide-doksatvolume').val().split(',');
$hidedoksatberat  = $('#hide-doksatberat').val().split(',');

$hidefissatjumlah = $('#hide-fissatjumlah').val().split(',');
$hidefissatvolume = $('#hide-fissatvolume').val().split(',');
$hidefissatberat  = $('#hide-fissatberat').val().split(',');


    $(document).ready(function() {
getDatePicker('#new-periode_awal')
getDatePicker('#new-periode_akhir')
<?php
if($_GET['tipe']=='lokal'){
?>
getProvinsi();
getKabupaten($hideProvinsi[0])
getSelect2noSearch($hutan,'{json_datahutan}',$hideStatushutan,'Status Hutan')
getSelect2noSearch($dokumen,'{json_datadokumen}',$hideDokumen,'Jenis Dokumen')
getSelect2noSearch($sertifikasi,'{json_sertifikasi}',$hideSertifikasi,'Sertifikasi')
<?php }else{?>
getSelect2Search($('.new-negara'),'{json_negara}',$negaraS,'Asal Kayu')
<?php }?>
getSelect2Search($('.newsortimen'),'{json_sortimen}',$hidesortimen,'Sortimen')
getSelect2Search($('.newsupplier'),'{json_supplier}',$hidesupplier,'Supplier')
getSelect2Search($('.newdokumenkayu'),'{json_datakayu}',$hidedokumenkayu,'Jenis Kayu')
getSelect2Search($('.newfisikkayu'),'{json_datakayu}',$hidefisikkayu,'Jenis Kayu')
// Select Satuan Dokumen
getSelect2noSearch($('.new-doksatjumlah'),'{json_satuanbyjumlah}',$hidedoksatjumlah,'Satuan')
getSelect2noSearch($('.new-doksatvolume'),'{json_satuanbyvolume}',$hidedoksatvolume,'Satuan')
getSelect2noSearch($('.new-doksatberat'),'{json_satuanbyberat}',$hidedoksatberat,'Satuan')
// Select Satuan Fisik
getSelect2noSearch($('.new-fissatjumlah'),'{json_satuanbyjumlah}',$hidefissatjumlah,'Satuan')
getSelect2noSearch($('.new-fissatvolume'),'{json_satuanbyvolume}',$hidefissatvolume,'Satuan')
getSelect2noSearch($('.new-fissatberat'),'{json_satuanbyberat}',$hidefissatberat,'Satuan')


$provinsi.change(function(){
$kabupaten.css('width','100%')
var a=$('.new-provinsi option:selected').val().split(',');
$('#hide-provinsi').val(a[1])
getKabupaten(a[0])
})

$kabupaten.change(function(){
var a=$('.new-kabupaten option:selected').val().split(',');
$('#hide-kabupaten').val(a[1])
})

/** End Document */
})

function getProvinsi($kode){
$provinsi.select2({
    minimumInputLength: 2,
    tags: false,
    triggerChange: true,
   placeholder: "-- Pilih Provinsi --",
    ajax: {
        url: '{json_provinsi}',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        type: 'public',
        kode: $kode
        // id: $('#hide-provinsi').val()
      }
      return query;
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.kode+','+item.propid
                    }
                })
            };
        }
    },
    })
        var teks = new Option($hideProvinsi[1], $hideProvinsi[0], true, true);
        $provinsi.append(teks).trigger('change');
      }

function getKabupaten($kode){
$kabupaten.select2({
    minimumInputLength: 2,
    placeholder: "-- Pilih Provinsi Dulu --",
    tags: false,
    triggerChange: true,
    ajax: {
        url: '{json_kabupaten}',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        type: 'public',
        // id: $('#hide-kabupaten').val(),
        kode: $kode
      }
      return query;
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.kode+','+item.kabid
                    }
                })
            };
        }
    },
    })
        var teks = new Option($hideKabupaten[1], $hideKabupaten[0], true, true);
        $kabupaten.append(teks).trigger('change');
      }

function getAsalKayuData(){
$asalkayu.select2({
    minimumResultsForSearch: -1,
    placeholder: "-- Pilih --",
    triggerChange: true,
    ajax: {
        url: '{json_dataasalkayu}',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        id: item.idhutan,
                        text: item.hutan
                    }
                })
            };
        }
    },
    })
        var teks = new Option($hideStatushutan[1], $hideStatushutan[0], true, true); 
        $asalkayu.append(teks).trigger('change');
      }


})
</script>

