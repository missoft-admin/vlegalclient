<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?$uri=$this->uri->segment(1);
	 $uri2=$this->uri->segment(2);
	 	$uri3=$this->uri->segment(3);
$tipe=$_GET['tipe'];
$page=(!empty($_GET['p']))?$_GET['p']:'';
?>
<div class="block block-themed" id="wow">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="All {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<?if ($uri2==$url_kedua){?>
						<li class="dropdown-header">New {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_index}">All {tJudul}</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All {tJudul} </li>
						<li>
							<!-- <a tabindex="-1" id="testing" href="#">New {tJudul}</a> -->
							<a tabindex="-1" href="{url_addnew}">New {tJudul}</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="120%" class="table table-bordered table-striped table-responsive" id="list-DT">
				<thead>
					<tr>
						<th rowspan="3" width="3%">No.</th>
						<th rowspan="3" width="20%">periode</th>
						<th rowspan="3" width="15%">supplier</th>
						<th rowspan="3" width="15%">sortimen</th>
						<th rowspan="3" width="15%">asal kayu</th>
						<?php if($_GET['tipe']=='lokal'){ ?>
						<th rowspan="3" width="15%">Jenis Kayu</th>
						<th rowspan="3" width="5%">Sertifikasi</th>
						<?php } ?>
						<th colspan="6" class="text-center" width="25%">Jumlah Penerimaan</th>
						<th rowspan="3" width="5%">action</th>
					</tr>
					<tr>
						<th colspan="3" class="text-center">Berdasar Dokumen</th>
						<th colspan="3" class="text-center">Berdasar Fisik</th>
					</tr>
					<tr>
						<th align="center">Jumlah</th>
						<th align="center">Volume</th>
						<th align="center">Berat</th>
						<th align="center">Jumlah</th>
						<th align="center">Volume</th>
						<th align="center">Berat</th>
					</tr>
				</thead>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
<script type="text/javascript">
$(function(){

    $(document).ready(function() {
     	// without numbering
     // getDataTable("#list-DT","{url_ajax}");
    	// with numbering
     getDataSSP("#list-DT","{url_ajax}",true);
     // $('#testing').click(function(){
     // 	manage()
     // })
	})

   $(document).on("click","#refresh_list",function(){
     $('#list-DT').each(function() {
      dt = $(this).dataTable();
      dt.fnDraw();
      })
   })
   $(document).on("click",".delete-row",function(){
var id=$(this).attr("data-id");
var buton=$(this);
$.ajax({

    url:"{url_delete}",
    data:{"id":id},
    dataType:'text',
    success: function(data){
     $('#list-DT').each(function() {
      dt = $(this).dataTable();
      dt.fnDraw();
 ToastrSukses("{tJudul} berhasil dihapus","Info")
  })
}
}); 
   })
   $(document).on("click",".ubahstatus",function(){
var id=$(this).attr("data-id");
var buton=$(this);
$.ajax({

    url:"{url_uStatus}",
    data:{"id":id},
    dataType:'html',
    success: function(data){ 
 ToastrSukses("Status berhasil diubah","Info")
 buton.empty()
 buton.html(data)
}
}); 

  });
    // get detail modal 1
    function manage(id='') {
        var obj = document;
        var url = '{url_addnew}' + id;
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.open("GET", url);

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                obj.innerHTML = xmlhttp.responseText;
            } else {
                obj.innerHTML = "<div></div>";
            }
        }
        xmlhttp.send(null);
    }
})
</script>

