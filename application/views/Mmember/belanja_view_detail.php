

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}mmember" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mmember/save','class="form-horizontal push-12-t"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">No Penjualan</label>
				<div class="col-md-4">
					<input  type="text" readonly class="form-control input-sm" name="nojual" id="nojual" placeholder="nojual" value="{nojual}" />
				</div>
				<label class="col-md-2 control-label" for="nama">Tanggal</label>
				<div class="col-md-4">
					<input  type="text" readonly class="form-control input-sm" name="tgljual" id="tgljual" placeholder="" value="{tgljual}" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">NoID Stokies</label>
				<div class="col-md-4">
					<input  type="text" readonly class="form-control input-sm" name="noidstokies" id="noidstokies" placeholder="noidstokies" value="{noidstokies}" />
				</div>
				<label class="col-md-2 control-label" for="nama">Stokies</label>
				<div class="col-md-4">
					<input  type="text" readonly class="form-control input-sm" name="namastokies" id="namastokies" placeholder="" value="{namastokies}" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">NoID Member</label>
				<div class="col-md-4">
					<input  type="text" readonly class="form-control input-sm" name="noid" id="noid" placeholder="Noid" value="{noid}" />
				</div>
				<label class="col-md-2 control-label" for="nama">Nama</label>
				<div class="col-md-4">
					<input  type="text" readonly class="form-control input-sm" name="namamembers" id="namamembers" value="{namamembers}" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Input Dicabang</label>
				<div class="col-md-4">
					<input  type="text" readonly class="form-control input-sm" name="namacabang" id="namacabang" placeholder="namacabang" value="{namacabang}" />
				</div>
				
			</div>
			
			
			<?php echo form_close() ?>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
				<thead>
					<tr>                                    
						<th width="5%">No</th>
						<th width="25%">No Nama Barang</th>
						<th width="15%">Harga</th>
						<th width="10%">Jumlah</th>					
						<th width="15%">Total</th>					
					</tr>
				</thead>
				<tbody>
					<?
						$i=0;
						$total=0;
						foreach($query as $row){
						$i=$i+1;
						$total=$total+$row->total;
						?>
						<tr>
							<td><?=$i?></td>
							<td><?=$row->namabarang?></td>
							<td align="right"><?='Rp. '. number_format($row->hargajualmember,0).',-'?></td>
							<td align="right"><?=$row->jumlah?></td>
							<td align="right"><?='Rp. '.number_format($row->total,0).',-'?></td>
						</tr>
						
					<?}?>
					<tr>
						<td></td><td></td><td></td><td></td><td></td>
					</tr>
					<tr>
						<td colspan="4" align="right"><strong>Grand Total : </strong></td>
						<td align="right"><strong><?='Rp. '.number_format($total,0).',-'?></strong></td>
					</tr>
				</tbody>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
