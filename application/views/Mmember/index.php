<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)
	
?>
<?$uri=$this->uri->segment(1);
	 $uri2=$this->uri->segment(2);	?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="All Member"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu">
					<?if ($uri2=='newmembers'){?>
						<li class="dropdown-header">New Member </li>
						<li>
							<a tabindex="-1" href="{base_url}mmember"><span class="badge pull-right">3</span>All Member</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All Member </li>
						<li>
							<a tabindex="-1" href="{base_url}mmember/newmembers"><span class="badge pull-right">3</span>New Member</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
				<thead>
					<tr>                                    
						<th width="5%">No id</th>
						<th width="20%">Nama Member</th>
						<th width="15%">Kota</th>
						<th width="20">Aktivasi</th>					
						<th width="5%">Mitra</th>					
						<th width="15%">Nilai</th>					
						<th width="5%">No Upline</th>					
						<th width="15%">Tools</th>					
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
<input type="hidden" id="datatable_search" value="{datatable_search}"/>
<input type="hidden" id="datatable_page" value="{datatable_page}"/>
<input type="hidden" id="datatable_order" value="{datatable_order}"/>


