<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}mmember" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mmember/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Noid</label>
				<div class="col-md-3">
					<input  type="text" readonly class="form-control input-sm" name="noid" id="noid" placeholder="Noid" value="{noid}" />
				</div>
				<label class="col-md-2 control-label" for="nama">Tanggal Aktifasi</label>
				<div class="col-md-5">
					<input  type="text" readonly class="form-control input-sm" name="tgldaftar" id="tgldaftar" placeholder="Nama Point Distribusi" value="{tgldaftar}" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Sponsor:</label>
				<div class="col-md-5">
					<input type="text" readonly class="form-control input-sm" name="nosponsor" id="nosponsor" placeholder="Kepala Point Distribusi" value="{nosponsor}" />
				</div>
				<div class="col-md-5">
					<input type="text" readonly class="form-control input-sm" name="namasponsor" id="namasponsor" placeholder="Kepala Point Distribusi" value="{namasponsor}" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="noupline">Upline:</label>
				<div class="col-md-5">
					<input type="text" readonly class="form-control input-sm" name="noupline" id="noupline" placeholder="" value="{noupline}" />
				</div>
				<div class="col-md-5">
					<input type="text" readonly class="form-control input-sm" name="namaupline" id="namaupline" placeholder="" value="{namaupline}" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="namamembers">Nama Member:</label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="namamembers" id="namamembers" placeholder="" value="{namamembers}" />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="noidstokies">Stokis Referensi:</label>
				<div class="col-md-10">
					<select name="noidstokies" tabindex="1" id="noidstokies" data-placeholder="Cari Stokis Referensi" class="form-control  input-sm">
						<? if($noidstokies){?>
							<option selected value="{noidstokies}"><?=$namastokies?></option>
						<?}?>
					</select>
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="email">Email:</label>
				<div class="col-md-5">
					<input class="form-control input-sm" type="email" id="email" name="email" value="{email}" placeholder="Email..">
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="alamat">Alamat:</label>
				<div class="col-md-10">
					<input type="text"  name="alamat" id="alamat" value="{alamat}" class="form-control input-sm" placeholder=""  />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="desa">Desa:</label>
				<div class="col-md-4">
					<input type="text"  name="desa" id="desa" value="{desa}" class="form-control input-sm" placeholder=""  />
				</div>
				<label class="col-md-2 control-label" for="kecamatan">Kecamatan:</label>
				<div class="col-md-4">
					<input type="text"  name="kecamatan" id="kecamatan" value="{kecamatan}" class="form-control input-sm" placeholder=""  />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="kota">Kota:</label>
				<div class="col-md-10">
					<select name="kota" tabindex="1" id="kota" data-placeholder="Cari Stokis Referensi" class="form-control  input-sm">
						<? if($kota){?>
							<option selected value="{kota}"><?=$nama_kota.' - ('.$propinsi.')'?></option>
						<?}?>
					</select>
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="kodepos">Kode Pos:</label>
				<div class="col-md-2">
					<input type="text"  name="kodepos" id="kodepos" value="{kodepos}" class="form-control input-sm number" placeholder=""  />
				</div>
						
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="telepon">Telepon:</label>
				<div class="col-md-4">
					<input type="text"  name="telepon" id="telepon" value="{telepon}" class="form-control input-sm number" placeholder=""  />
				</div>
				<label class="col-md-2 control-label" for="hp">Hand Phone:</label>
				<div class="col-md-4">
					<input type="text"  name="hp" id="hp" value="{hp}" class="form-control input-sm number" placeholder=""  />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="noktp">No KTP.:</label>
				<div class="col-md-4">
					<input type="text"  name="noktp" id="noktp" value="{noktp}" class="form-control input-sm number" placeholder=""  />
				</div>
								
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="tempatlahir">Tempat Lahir:</label>
				<div class="col-md-4">
					<input type="text"  name="tempatlahir" id="tempatlahir" value="{tempatlahir}" class="form-control input-sm" placeholder=""  />
				</div>
				<label class="col-md-2 control-label" for="tgllahir">Tanggal Lahir:</label>
				<div class="col-md-3">
					<div class="js-datetimepicker input-group date">
						<input class="js-datepicker form-control" type="text" id="tgllahir" name="tgllahir" value="{tgllahir}" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>				
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="jeniskelamin">Jenis Kelamin:</label>
				<div class="col-md-4">
					<select class="form-control input-sm" id="jeniskelamin" name="jeniskelamin" size="1">
						<option value="0" <?=($jeniskelamin=='0'?'selected':'')?>>Laki-laki</option>
						<option value="1" <?=($jeniskelamin=='1'?'selected':'')?>>Perempuan</option>
					</select>
				</div>
							
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="agama">Agama:</label>
				<div class="col-md-4">
					<select class="form-control input-sm" id="agama" name="agama" size="1">
						<option value="0" <?php if ($agama == '0') echo 'selected' ?>> Islam</option>
						<option value="1" <?php if ($agama == '1') echo 'selected' ?>> Katholik</option>
						<option value="2" <?php if ($agama == '2') echo 'selected' ?>> Propestan</option>
						<option value="3" <?php if ($agama == '3') echo 'selected' ?>> Hindu</option>
						<option value="4" <?php if ($agama == '4') echo 'selected' ?>> Budha</option>
						<option value="5" <?php if ($agama == '5') echo 'selected' ?>> Lainnya</option>
					</select>
				</div>
							
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="status">Status:</label>
				<div class="col-md-4">
					<select class="form-control input-sm" id="status" name="status" size="1">
						<option value="0" <?php if ($status == '0') echo 'selected' ?>> Menikah</option>
						<option value="1" <?php if ($status == '1') echo 'selected' ?>> Belum Menikah</option>
						<option value="2" <?php if ($status == '2') echo 'selected' ?>> Lainnya</option>
					</select>
				</div>							
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="tempatlahir">Tanggungan:</label>
				<div class="col-md-4">
					<div class="input-group">
					<input type="text"  name="tanggungan" id="tanggungan" value="{tanggungan}" class="form-control input-sm number" placeholder=""  />
					<span class="input-group-addon">Orang</span>
					</div>
				</div>
							
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="ahliwaris">Ahli Waris:</label>
				<div class="col-md-4">
					<input type="text"  name="ahliwaris" id="ahliwaris" value="{ahliwaris}" class="form-control input-sm" placeholder=""  />
				</div>
				<label class="col-md-2 control-label" for="hubungan">Hubungan:</label>
				<div class="col-md-4">
					<input type="text"  name="hubungan" id="hubungan" value="{hubungan}" class="form-control input-sm" placeholder=""  />
				</div>
			</div>
			<div class="form-group">
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mmember" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('edit', $edit); ?>
			<?php echo form_hidden('noid', $noid); ?>
			<?php echo form_close() ?>
	</div>
</div>
