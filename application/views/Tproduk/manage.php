<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?php
$uri=$this->uri->segment(1);
    $uri2=$this->uri->segment(2);
	 	$uri3=$this->uri->segment(3);
$tipe=$_GET['tipe'];
$page=(!empty($_GET['p']))?$_GET['p']:'';
?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="<?=($page=='new-penjualan')?'New':'Update'?> {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<?if ($page==$url_kedua){?>
						<li class="dropdown-header">New {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_index}">All {tJudul}</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All {tJudul} </li>
						<li>
							<!-- <a tabindex="-1" id="testing" href="#">New {tJudul}</a> -->
							<a tabindex="-1" href="{url_addnew}">New {tJudul}</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		<form method="post" action="{url_proses}">
<input type="hidden" value="{newbuyer}" id="hide-buyer" name="hide-buyer">
<?php
if($_GET['tipe']=='lokal'){
?>
<input type="hidden" value="{newsatjumlah}" id="hide-satjumlah" name="hide-satjumlah">
<input type="hidden" value="{newsatvolume}" id="hide-satvolume" name="hide-satvolume">
<input type="hidden" value="{newsatberat}" id="hide-satberat" name="hide-satberat">
<input type="hidden" value="{newproduk}" id="hide-produk" name="hide-produk">
<input type="hidden" value="{newlistkayu}" id="hide-listkayu" name="hide-listkayu">
<?php
#if(empty($_GET['id'])){?>
<input type="hidden" value="{newlistkayu2}" id="hide-listkayu2" name="hide-listkayu2">
<input type="hidden" value="{newlistkayu3}" id="hide-listkayu3" name="hide-listkayu3">
<?php #}?>
<input type="hidden" value="{newclient_propinsi}" id="hide-provinsi" name="hide-provinsi">
<input type="hidden" value="{newclient_kabupaten}" id="hide-kabupaten" name="hide-kabupaten">
<?php }else{?>
<input type="hidden" value="{newnegara}" id="hide-negara" name="hide-negara">
<input type="hidden" value="{newmatauang}" id="hide-matauang" name="hide-matauang">
<input type="hidden" value="{newloading}" id="hide-loading" name="hide-loading">
<input type="hidden" value="{newdischarge}" id="hide-discharge" name="hide-discharge">
<input type="hidden" value="{newidnegara_dis}" id="hide-idnegara_dis" name="hide-idnegara_dis">
<input type="hidden" value="{newstatus_liu}" id="hide-status_liu" name="hide-status_liu">
<?php }?>
<input type="hidden" value="<?=(!empty($uri3))?$uri3:''?>" id="hide-ID" name="hide-ID">
<div class="row">
        <?php
        if($_GET['tipe']=='lokal'){
        ?>
	<div class="col-sm-6">
        <?php
        if(empty($_GET['id'])){
            if($Laper['periode_awal']=='0000-00-00'&&$Laper['periode_akhir']=='0000-00-00'){
                $awal='';
                $awaltext='';
                $akhir='';
                $akhirtext='';
            }else{
                $awal  =tsi_date($Laper['periode_awal']);
                $awaltext  =encode_date($Laper['periode_awal']);
                $akhir =tsi_date($Laper['periode_akhir']);
                $akhirtext =encode_date($Laper['periode_akhir']);
            }
            ?>
        <div class="form-group">
            <label class="text-danger">Data Input Terakhir adalah Periode : <?=$awal.' - '.$akhir?></label>
        </div>
        <?php }?>
		<div class="form-group">
			<label>Periode</label>
			<div class="row">
				<div class="col-sm-6">
					<input type="text" class="form-control" name="new-periode_awal" id="new-periode_awal" value="<?=(empty($_GET['id']))?$awaltext:$newperiode_awal?>">
			</div>
				<div class="col-sm-6">
					<sub style="margin-left: -20px">s/d</sub><input type="text" style="margin-left: 0px;margin-top: -18.5px" class="form-control" name="new-periode_akhir" id="new-periode_akhir" value="<?=(empty($_GET['id']))?$akhirtext:$newperiode_akhir?>">
			</div>
		</div>
		</div>
        <div class="form-group">
            <label>Nama Pembeli</label>
            <select class="form-control newbuyer" id="new-buyer" name="new-buyer">
            </select>
        </div>
        <div class="form-group">
            <label>Provinsi & Kabupaten</label>
            <div class="row">
                <div class="col-sm-6">
            <select class="form-control new-provinsi" id="new-provinsi" name="new-provinsi" required>
            </select>
            </div>
                <div class="col-sm-6">
            <select class="form-control new-kabupaten" id="new-kabupaten" name="new-kabupaten" placeholder="Pilih Kabupaten Dulu">
            </select>
            </div>
        </div>
        </div>
        <div class="form-group">
            <label>Invoice</label>
            <input type="text" name="new-invoice" value="{newinvoice}" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Nilai (Rp)</label>
            <input type="text" name="new-nilai" value="{newnilai}" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Tanggal Terbit</label>
            <input type="text" name="new-tglterbit" value="{newtglterbit}" class="form-control new-tglterbit" required>
        </div>
	</div>
	<div class="col-sm-6">
        <div class="form-group">
            <label>Nama Produk</label>
            <select class="form-control newproduk" id="new-produk" name="new-produk">
            </select>
        </div>
        <div class="form-group">
            <label>Jumlah</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-jumlah" value="{newjumlah}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satjumlah" id="new-satjumlah" name="new-satjumlah" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Volume</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-volume" value="{newvolume}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satvolume" id="new-satvolume" name="new-satvolume" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Berat</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-berat" value="{newberat}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satberat" id="new-satberat" name="new-satberat" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Jenis Kayu</label>
            <select class="form-control newlistkayu" id="new-listkayu" name="new-listkayu">
            </select>
        </div>
    <?php
    #if(!empty($_GET['id'])){?>
        <div class="form-group">
            <label>Jenis Kayu 2</label>
            <select class="form-control newlistkayu2" id="new-listkayu2" name="new-listkayu2">
            </select>
        </div>
        <div class="form-group">
            <label>Jenis Kayu 3</label>
            <select class="form-control newlistkayu3" id="new-listkayu3" name="new-listkayu3">
            </select>
        </div>
    <?php #}?>
        <div class="form-group">
            <label>No. Fako / Nota</label>
            <input type="text" name="new-nonota" value="{newnonota}" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Packing List</label>
            <input type="text" name="new-packinglist" value="{newpackinglist}" class="form-control" required>
        </div>
	</div>
        <?php
        }else{
        ?>
    <div class="col-sm-6">
        <div class="form-group">
            <label>Nama Pembeli</label>
            <select class="form-control newbuyer" id="new-buyer" name="new-buyer">
            </select>
        </div>
        <div class="form-group">
            <label>Alamat Pembeli</label>
            <textarea class="form-control" rows="5" name="new-alamat">{newalamat}</textarea>
        </div>
        <div class="form-group">
            <label>Negara Tujuan</label>
            <div class="row">
                <div class="col-sm-6">
            <select class="form-control new-negara" id="new-negara" name="new-negara">
            </select>
            </div>
            </div>
        </div>
        <div class="form-group">
            <label>ISO Code</label>
            <input type="text" name="new-iso" class="form-control new-iso" id="new-iso" value="{newiso}">
        </div>
        <div class="form-group">
            <label>Mata Uang</label>
            <select class="form-control new-matauang" id="new-matauang" name="new-matauang">
            </select>
        </div>
        <div class="form-group">
            <label>Port of Loading</label>
            <select class="form-control new-loading" id="new-loading" name="new-loading">
            </select>
        </div>
        <div class="form-group">
            <label>Port of Discharge</label>
            <select class="form-control new-discharge" id="new-discharge" name="new-discharge">
            </select>
        </div>
        <div class="form-group">
            <label>Keterangan</label>
            <textarea class="form-control" rows="5" name="new-keterangan">{newketerangan}</textarea>
        </div>
        <?php
        if(!empty($_GET['id'])){?>
        <div class="form-group">
            <label>No. ETPIK / NPWP</label>
            <div class="row">
                <div class="col-sm-6">
            <input type="text" readonly name="new-etpik" class="form-control" value="{newetpik}">
                </div>
                <div class="col-sm-6">
            <input type="text" readonly name="new-npwp" class="form-control" value="{newnpwp}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Status Dokumen</label>
            <div class="row">
                <div class="col-sm-6">
            <select class="form-control new-status" id="new-status" name="new-status">
                <option value=""></option>
                <option value="vlegal" <?php echo ($newstatus=='vlegal')?'selected':'' ?>>Dokumen V-Legal</option>
                <option value="non-vlegal" <?php echo ($newstatus=='non-vlegal')?'selected':'' ?>>Dokumen Non V-Legal</option>
            </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Status Edit Dokumen</label>
            <div class="row">
                <div class="col-sm-6">
            <select class="form-control new-status_edit" id="new-status_edit" name="new-status_edit">
                <option value=""></option>
                <option value="edit">Penambahan/Perubahan Data</option>
                <option value="replace">Perubahan Dokumen V-Legal</option>
                <option value="extend">Perpanjangan Dokumen V-Legal</option>
                <option value="cancel">Pembatalan Dokumen V-Legal</option>
            </select>
                </div>
            </div>
        </div>
        <?php }?>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label>No. Invoice</label>
            <input type="text" name="new-invoice" id="new-invoice" class="form-control" value="{newinvoice}">
        </div>
        <div class="form-group">
            <label>Tanggal Invoice</label>
            <input type="text" name="new-tglinvoice" id="new-tglinvoice" class="form-control" value="{newtglinvoice}">
        </div>
        <div class="form-group">
            <label>Nomor PEB</label>
            <input type="text" name="new-peb" id="new-peb" class="form-control" value="{newpeb}">
        </div>
        <div class="form-group">
            <label>Nomor B/L</label>
            <input type="text" name="new-bl" id="new-bl" class="form-control" value="{newbl}">
        </div>
        <div class="form-group">
            <label>Packing List</label>
            <input type="text" name="new-packinglist" id="new-packinglist" class="form-control" value="{newpackinglist}">
        </div>
        <div class="form-group">
            <label>Alat Angkut</label>
            <select class="form-control new-vessel" id="new-vessel" name="new-vessel">
                <option value=""></option>
                <option value="1" <?php echo ($newvessel=='1')?'selected':'' ?>>By Sea</option>
                <option value="2" <?php echo ($newvessel=='2')?'selected':'' ?>>By Air</option>
                <option value="3" <?php echo ($newvessel=='3')?'selected':'' ?>>By Land</option>
            </select>
        </div>
        <div class="form-group">
            <label>Tanggal Shipment</label>
            <input type="text" name="new-tglship" id="new-tglship" class="form-control" value="{newtglship}">
        </div>
        <div class="form-group">
            <label>Lokasi Stuffing</label>
            <input type="text" name="new-stuffing" id="new-stuffing" class="form-control" value="{newstuffing}">
        </div>
    </div>
        <?php } ?>
</div>
        <div class="text-right">
            <button class="btn btn-primary faa-parent animated-hover">Save <i class="fas fa-check faa-wrench"></i></button>
        </div>
    </form>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
		<div class="block-footer">&nbsp;</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
$hidebuyer        = $('#hide-buyer').val().split(',');
<?php
if($_GET['tipe']=='lokal'){
?>
$hideProvinsi       = $('#hide-provinsi').val().split(',');
$hideKabupaten      = $('#hide-kabupaten').val().split(',');
$provinsi           = $('.new-provinsi');
$kabupaten          = $('.new-kabupaten');
$hideproduk       = $('#hide-produk').val().split(',');
$hidelistkayu     = $('#hide-listkayu').val().split(',');
<?php
#if(!empty($_GET['id'])){?>
$hidelistkayu2     = $('#hide-listkayu2').val().split(',');
$hidelistkayu3     = $('#hide-listkayu3').val().split(',');
<?php #}?>

$hidesatjumlah = $('#hide-satjumlah').val().split(',');
$hidesatvolume = $('#hide-satvolume').val().split(',');
$hidesatberat  = $('#hide-satberat').val().split(',');
<?php }else{?>
$negaraS        = $('#hide-negara').val().split(',');
$negara         = $('.new-negara');
$hidematauang   = $('#hide-matauang').val().split(',');
$hideloading    = $('#hide-loading').val().split(',');
$hidedischarge  = $('#hide-discharge').val().split(',');
$idnegara_dis   = $('#hide-idnegara_dis').val();
<?php }?>


    $(document).ready(function() {
getSelect2Search($('.newbuyer'),'{json_buyer}',$hidebuyer,'Pembeli')
<?php
if($_GET['tipe']=='lokal'){
?>
getDatePicker('#new-periode_awal')
getDatePicker('#new-periode_akhir')
getDatePicker('.new-tglterbit')
getProvinsi();
getKabupaten($hideProvinsi[0])
getSelect2Search($('.newproduk'),'{json_produk}',$hideproduk,'Produk')
getSelect2Search($('.newlistkayu'),'{json_datakayu}',$hidelistkayu,'Jenis Kayu')
<?php
#if(!empty($_GET['id'])){?>
getSelect2Search($('.newlistkayu2'),'{json_datakayu}',$hidelistkayu2,'Jenis Kayu 2')
getSelect2Search($('.newlistkayu3'),'{json_datakayu}',$hidelistkayu3,'Jenis Kayu 3')
<?php #}?>
// Select Satuan
getSelect2noSearch($('.new-satjumlah'),'{json_satuanbyjumlah}',$hidesatjumlah,'Satuan')
getSelect2noSearch($('.new-satvolume'),'{json_satuanbyvolume}',$hidesatvolume,'Satuan')
getSelect2noSearch($('.new-satberat'),'{json_satuanbyberat}',$hidesatberat,'Satuan')

$provinsi.change(function(){
$kabupaten.css('width','100%')
var a=$('.new-provinsi option:selected').val().split(',');
$('#hide-provinsi').val(a[1])
getKabupaten(a[0])
})

$kabupaten.change(function(){
var a=$('.new-kabupaten option:selected').val().split(',');
$('#hide-kabupaten').val(a[1])
})

<?php }else{?>
getDatePicker('#new-tglship')
getDatePicker('#new-tglinvoice')
getSelect2Search($('.new-matauang'),'{json_matauang}',$hidematauang,'Mata Uang')
getSelect2Search($('.new-loading'),'{json_loading}',$hideloading,'Port of Loading')
getSelect2Search($('.new-discharge'),'{json_discharge}?kode='+$idnegara_dis,$hidedischarge,'Port of Discharge')
getSelect2Search($('.new-negara'),'{json_negara}',$negaraS,'Negara')
$('#new-status_edit').select2({
    minimumResultsForSearch:-1,
    placeholder: "-- Pilih Status Edit Dokumen --"})
$('#new-status').select2({
    minimumResultsForSearch:-1,
    placeholder: "-- Pilih Status Dokumen --"})
$('#new-vessel').select2({
    minimumResultsForSearch:-1,
    placeholder: "-- Pilih Alat Angkut --"})


$negara.change(function(){
$('.new-discharge').css('width','100%')
var a=$('.new-negara option:selected').val();
$('#new-iso').val(a)
getSelect2noSearch($('.new-discharge'),'{json_discharge}?kode='+a,$hidedischarge,'Port of Discharge')
})
$('.newbuyer').change(function(){
    var idbuyer=$(this).find(':selected').val();
$.ajax({
    url: '{site_url}json/buyer',
    type: 'get',
    dataType: 'json',
    data: {id: idbuyer,limitnya:1},
    success:function(data){
        $('textarea[name=new-alamat]').val(data.alamat)
    }
})

})
<?php }?>

/** End Document */
})

<?php
if($_GET['tipe']=='lokal'){
?>
function getProvinsi($kode){
$provinsi.select2({
    minimumInputLength: 2,
    tags: false,
    triggerChange: true,
   placeholder: "-- Pilih Provinsi --",
    ajax: {
        url: '{json_provinsi}',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        type: 'public',
        kode: $kode
        // id: $('#hide-provinsi').val()
      }
      return query;
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.kode+','+item.propid
                    }
                })
            };
        }
    },
    })
        var teks = new Option($hideProvinsi[1], $hideProvinsi[0], true, true);
        $provinsi.append(teks).trigger('change');
      }

function getKabupaten($kode){
$kabupaten.select2({
    minimumInputLength: 2,
    placeholder: "-- Pilih Provinsi Dulu --",
    tags: false,
    triggerChange: true,
    ajax: {
        url: '{json_kabupaten}',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        type: 'public',
        // id: $('#hide-kabupaten').val(),
        kode: $kode
      }
      return query;
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.kode+','+item.kabid
                    }
                })
            };
        }
    },
    })
        var teks = new Option($hideKabupaten[1], $hideKabupaten[0], true, true);
        $kabupaten.append(teks).trigger('change');
      }
<?php }?>


})
</script>

