<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-amethyst">
		<ul class="block-options">
			<li>
				<a href="{base_url}stokies" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('stokies/simpan','class="js-validation-bootstrap  form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">No id Stokis <span class="text-danger">*</span></label>
				<div class="col-md-3">
					<input  type="text" readonly class="form-control input-sm" name="noidstokies" id="noidstokies" placeholder="noidstokies" value="{noidstokies}" />
				</div>
				
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="namastokies">Nama Stokis <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<input type="text" class="form-control input-sm" name="namastokies" id="namastokies" placeholder="" value="{namastokies}" />
					
				</div>	
				<div id="div_loading" hidden>					
					<i class="fa fa-2x fa-asterisk fa-spin"></i>
					<button class="js-swal-success btn btn-success" type="button"> Searching ..</button>
				</div>
				<div id="div_ok" hidden>
					<button class="js-swal-success btn btn-success" type="button"><i class="fa fa-check push-5-r"></i> Nama Stokis diperbolehkan</button>
				</div>
				<div id="div_error" hidden>
					<button class="js-swal-error btn btn-danger" type="button"><i class="fa fa-times push-5-r"></i> Nama Stokis ini sudah ada</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="noidcabang">Cabang <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<select name="noidcabang" tabindex="1" id="noidcabang" data-placeholder="Cabang" class="form-control  input-sm">
						<option value="" <?=($noidcabang=='0'?'selected':'')?>>- Pilih Cabang -</option>
						<? foreach($reccabang as $row){?>
							<option value="<?=$row->noidcabang?>" <?=($row->noidcabang==$noidcabang)?'selected':''?>><?=$row->namacabang?></option>
						<?}?>
					</select>
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="noid">NOID Pemilik <span class="text-danger">*</span></label>
				<div class="col-md-2">
					<input class="form-control" type="text" id="noid" name="noid" value="{noid}" placeholder="No ID">
				</div>
				<div id="div_info">
					
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="alamat">Alamat <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text"  name="alamat" id="alamat" value="{alamat}" class="form-control input-sm" placeholder=""  />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="desa">Desa:</label>
				<div class="col-md-4">
					<input type="text"  name="desa" id="desa" value="{desa}" class="form-control input-sm" placeholder=""  />
				</div>
				<label class="col-md-2 control-label" for="kecamatan">Kecamatan:</label>
				<div class="col-md-4">
					<input type="text"  name="kecamatan" id="kecamatan" value="{kecamatan}" class="form-control input-sm" placeholder=""  />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="kota">Kota <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<select name="kota" tabindex="1" id="kota" data-placeholder="Kota Stokis berada" class="form-control  input-sm">
						<? if($kota){?>
							<option value="{kota}"><?=$nama_kota.' - ('.$propinsi.')'?></option>
						<?}?>
					</select>
				</div>				
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="telepon">Telepon </label>
				<div class="col-md-4">
					<input type="text"  name="telepon" id="telepon" value="{telepon}" class="form-control input-sm angka" placeholder=""  />
				</div>
				<label class="col-md-2 control-label" for="hp">Hand Phone <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<input type="text"  name="hp" id="hp" value="{hp}" class="form-control input-sm angka" placeholder=""  />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="email">Email <span class="text-danger">*</span></label>
				<div class="col-md-5">
					<input class="form-control input-sm" type="email" id="email" name="email" value="{email}" placeholder="Email..">
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="haverec"></label>
				<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="haverec" id="haverec" <?php if ($haverec){echo ' checked="checked"';} ?> value="1"><span></span> <b>Memiliki Rekening Bank</b>
				</label>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="email">Bank <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<label class="css-input css-radio css-radio-sm css-radio-primary">
						<input type="radio" id="kodebank" name="kodebank" value="1"  <?php if ($kodebank==1){echo 'checked=CHECKED';}?> ><span></span> BNI &nbsp;&nbsp;&nbsp;
					</label>
					<label class="css-input css-radio css-radio-sm css-radio-primary">
						<input type="radio" id="kodebank" name="kodebank" value="2"  <?php if ($kodebank==2){echo 'checked=CHECKED';}?> ><span></span> BRI &nbsp;&nbsp;&nbsp;
					</label>
					<label class="css-input css-radio css-radio-sm css-radio-primary">
						<input type="radio" id="kodebank" name="kodebank" value="3"  <?php if ($kodebank==3){echo 'checked=CHECKED';}?> ><span></span> MANDIRI &nbsp;&nbsp;&nbsp;
					</label>
					<label class="css-input css-radio css-radio-sm css-radio-primary">
						<input type="radio" id="kodebank" name="kodebank" value="4"  <?php if ($kodebank==4){echo 'checked=CHECKED';}?> ><span></span> BCA &nbsp;&nbsp;&nbsp;
					</label>
					<label class="css-input css-radio css-radio-sm css-radio-primary">
						<input type="radio" id="kodebank" name="kodebank" value="5"  <?php if ($idbank=='1'){echo 'checked=CHECKED';}?> ><span></span> Lainnya &nbsp;&nbsp;&nbsp;
					</label>
					<label class="css-input css-radio css-radio-sm css-radio-primary">				
					<div id="div_bank_lainnya">
						<select name="kodebank2"  tabindex="1" id="kodebank2" data-placeholder="Bank Lainnya" class="form-control  input-sm">
							<option value="" <?php echo ' selected="selected"';?>>Silahkan Pilih Bank </option>
							<? foreach ($rbank as $row){?>
								<option value="<?php echo $row->id;?>" <?php if ($row->id==$kodebank){echo ' selected="selected"';}?>><?php echo $row->namabank;?></option>
							<?}?>
						</select>
					</div>
					</label>
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="desa">No Rekening:</label>
				<div class="col-md-4">
					<input type="text"  name="norek" id="norek" value="{norek}" class="form-control input-sm angka" placeholder=""  />
				</div>							
			</div>
			<div class="form-group">				
				<label class="col-md-2 control-label" for="atasnama">Atas Nama:</label>
				<div class="col-md-4">
					<input type="text"  name="atasnama" id="atasnama" value="{atasnama}" class="form-control input-sm" placeholder=""  />
				</div>				
			</div>
			<div class="form-group">				
				<label class="col-md-2 control-label" for="cabang">Cabang:</label>
				<div class="col-md-4">
					<input type="text"  name="cabang" id="cabang" value="{cabang}" class="form-control input-sm" placeholder=""  />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="lat">Latitude </label>
				<div class="col-md-4">
					<input type="text"  name="lat" id="lat" value="{lat}" class="form-control input-sm" placeholder=""  />
				</div>
				<label class="col-md-2 control-label" for="lng">Longitude </label>
				<div class="col-md-4">
					<input type="text"  name="lng" id="lng" value="{lng}" class="form-control input-sm" placeholder=""  />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="group">Group </label>
				<div class="col-md-4">
					<input type="text"  name="group" id="group" value="{group}" class="form-control input-sm" placeholder=""  />
				</div>
							
			</div>
			<div class="form-group">
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mmember" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('edit', $edit); ?>
			
			<?php echo form_close() ?>
	</div>
</div>

