<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?php
$uri=$this->uri->segment(1);
	$uri2=$this->uri->segment(2);
$awaltext  	=encode_date($Laper['periode_awal']);
$akhirtext 	=encode_date($Laper['periode_akhir']);
            ?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="All {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List" id="refresh_list" type="button"><i class="si si-refresh"></i></button>
            </li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
	<form action="" method="post" enctype="multipart/form-data" id="form-insertNew">
<input type="hidden" value="{newsatjumlah}" id="hide-satjumlah" name="hide-satjumlah">
<input type="hidden" value="{newsatvolume}" id="hide-satvolume" name="hide-satvolume">
<input type="hidden" value="{newsatberat}" id="hide-satberat" name="hide-satberat">
<input type="hidden" value="{newproduk}" id="hide-produk" name="hide-produk">
<input type="hidden" value="{newuntukproduksi}" id="hide-untukproduksi" name="hide-untukproduksi">
<input type="hidden" value="{newlistkayu}" id="hide-listkayu" name="hide-listkayu">
<input type="hidden" id="hide-ID" name="hide-ID">
<div class="row">
	<div class="col-sm-6">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		<div class="form-group">
			<label>Periode Awal - Akhir</label>
			<div class="row">
				<div class="col-sm-3">
					<input type="text" class="form-control" name="new-periode_awal" id="new-periode_awal" value="">
			</div>
				<div class="col-sm-3">
					<sub style="margin-left: -20px">s/d</sub>
					<input type="text" style="margin-left: 0px;margin-top: -18.5px" class="form-control" name="new-periode_akhir" id="new-periode_akhir" value="">
			</div>
		</div>
		</div>
        <div class="form-group">
            <label>Nama Produk</label>
            <select class="form-control newproduk" id="new-produk" name="new-produk">
            </select>
        </div>
        <div class="form-group">
            <label>Jenis Kayu</label>
            <select class="form-control newlistkayu" id="new-listkayu" name="new-listkayu">
            </select>
        </div>
	</div>
	<div class="col-sm-6">
        <div class="form-group">
            <label>Jumlah</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-jumlah" id="new-jumlah" value="{newjumlah}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satjumlah" id="new-satjumlah" name="new-satjumlah" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Volume</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-volume" id="new-volume" value="{newvolume}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satvolume" id="new-satvolume" name="new-satvolume" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Berat</label>
            <div class="row">
                <div class="col-sm-8">
                    <input type="text" name="new-berat" id="new-berat" value="{newberat}" class="form-control" required>
                </div>
                <div class="col-sm-4">
                    <select class="form-control new-satberat" id="new-satberat" name="new-satberat" required>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Keterangan</label>
            <textarea name="new-keterangan" id="new-keterangan" class="form-control new-keterangan" rows="5"></textarea>
        </div>
	</div>
</div>
	<div class="text-right">
		<button type="reset" class="btn btn-danger faa-parent animated-hover btnBatal">Batal 
			<i class="fas fa-times faa-spin"></i>
		</button>
		<button style="display: none;" type="reset" class="btn btn-primary faa-parent animated-hover btnUpdate" id="btnUpdate">Update 
			<i class="fas fa-check faa-wrench"></i>
		</button>
		<button type="reset" class="btn btn-primary faa-parent animated-hover btnSimpan" id="btnSimpan">Save 
			<i class="fas fa-check faa-wrench"></i>
		</button>
	</div>
</form>
<br>
<div class="progress progress-mini">
    <div class="progress-bar progress-bar-info" role="progressbar" style="width: 100%"></div>
</div>
			<table width="100%" class="table table-bordered table-striped table-responsive" id="list-DT">
				<thead>
					<tr>
					   <th width="1%">No.</th>
					   <th width="15%">Periode</th>
					   <th width="30%">Nama Produk</th>
					   <th width="20%">Jenis Kayu</th>
					   <th width="5%">Jumlah</th>
					   <th width="5%">Volume</th>
					   <th width="5%">Berat</th>
					   <th width="12%">keterangan</th>
					   <th width="7%">aksi</th>
					</tr>

				</thead>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
<script type="text/javascript">
$(function(){
$hideproduk     	= $('#hide-produk').val().split(',');
$hidelistkayu     	= $('#hide-listkayu').val().split(',');

$hidesatjumlah = $('#hide-satjumlah').val().split(',');
$hidesatvolume = $('#hide-satvolume').val().split(',');
$hidesatberat  = $('#hide-satberat').val().split(',');
    $(document).ready(function() {
getDataSSP("#list-DT","{url_ajax}",true);
getDatePicker('#new-periode_awal')
getDatePicker('#new-periode_akhir')
getPeriode()
getSelect2Search($('.newproduk'),'{json_produk}',$hideproduk,'Produk')
getSelect2Search($('.newlistkayu'),'{json_datakayu}',$hidelistkayu,'Jenis Kayu')
// Select Satuan
getSelect2noSearch($('.new-satjumlah'),'{json_satuanbyjumlah}',$hidesatjumlah,'Satuan')
getSelect2noSearch($('.new-satvolume'),'{json_satuanbyvolume}',$hidesatvolume,'Satuan')
getSelect2noSearch($('.new-satberat'),'{json_satuanbyberat}',$hidesatberat,'Satuan')

// end of document
})

$(document).on("click",".btnBatal",function(){
$('#btnSimpan').show();
$('#btnUpdate').hide();
	resetValue()
})
$(document).on("click",".btnSimpan",function(){
// console.log($('#form-insertNew').serialize())
	$.ajax({
	url:"{url_insert}",
	data:{"data":$('#form-insertNew').serialize()},
	dataType:'script',
	success: function(respon){
		$('#list-DT').each(function() {
	  	dt = $(this).dataTable();
	  		dt.fnDraw();
		})
		resetValue();
		return respon;
	}
	});
})
$(document).on("click",".btnUpdate",function(){
// alert('{url_update}')
// console.log($('#form-insertNew').serialize())
	$.ajax({
	url:"{url_insert}",
	data:{"data":$('#form-insertNew').serialize()},
	dataType:'script',
	success: function(respon){
		$('#list-DT').each(function() {
	  	dt = $(this).dataTable();
	  		dt.fnDraw();
		})
$('#btnSimpan').show();
$('#btnUpdate').hide();
		resetValue();
		return respon;
	}
	});
})

$(document).on("click","#refresh_list",function(){
 $('#list-DT').each(function() {
  	dt = $(this).dataTable();
  		dt.fnDraw();
		})
		resetValue();
})

$(document).on("click",".edit-row",function(){
var me 	 				=$(this),
	id 	 				=me.attr("data-id"),
	tr 	 				=me.closest('tr');
	$.ajax({
	url:"{getData_edit}",
	data:{"id":id},
	dataType:'json',
	success: function(data){
	var $j=$('#hide-satjumlah').val(data.satuan_jumlah+`,`+ucFirst(data.satuan_jumlah)),
		$v=$('#hide-satvolume').val(data.satuan_volume+`,`+ucFirst(data.satuan_volume)),
		$b=$('#hide-satberat').val(data.satuan_berat+`,`+ucFirst(data.satuan_berat)),
		$newhideproduk			=data.selectproduk.split(','),
		$newhidelistkayu		=data.selectkayu.split(',')
		$newhidesatjumlah		=$j.val().split(','),
		$newhidesatvolume		=$v.val().split(','),
		$newhidesatberat		=$b.val().split(',');
			$('#new-periode_awal').val(data.periode_awal)
			$('#new-periode_akhir').val(data.periode_akhir)
			getSelect2Search($('.newproduk'),'{json_produk}',$newhideproduk,'Produk')
			getSelect2Search($('.newlistkayu'),'{json_datakayu}',$newhidelistkayu,'Jenis Kayu')
			// Select Satuan
			getSelect2noSearch($('.new-satjumlah'),'{json_satuanbyjumlah}',$newhidesatjumlah,'Satuan')
			getSelect2noSearch($('.new-satvolume'),'{json_satuanbyvolume}',$newhidesatvolume,'Satuan')
			getSelect2noSearch($('.new-satberat'),'{json_satuanbyberat}',$newhidesatberat,'Satuan')

			$('#new-jumlah').val(data.jumlah)
			$('#new-volume').val(data.volume)
			$('#new-berat').val(data.berat)
			$('#new-keterangan').val(data.keterangan)
		}
	});
$('#hide-ID').val(id)
$('#btnSimpan').hide();
$('#btnUpdate').show();
})
$(document).on("click",".delete-row",function(){
var id=$(this).attr("data-id");
var buton=$(this);
	$.ajax({
	url:"{url_delete}",
	data:{"id":id},
	dataType:'text',
	success: function(data){
		$('#list-DT').each(function() {
	  	dt = $(this).dataTable();
	  		dt.fnDraw();
		})
		ToastrSukses("{tJudul} "+data+" berhasil dihapus","Info")
	}
	});
})

function getPeriode(){
	$.ajax({
		url:'{url_periode}',
		dataType:'json',
		success:function(data){
			$('#new-periode_awal').val(data.periode_awal)
			$('#new-periode_akhir').val(data.periode_akhir)
		}
	})
}

function resetValue() {
	getPeriode()
$('#hide-ID').val('');
	$('.newproduk').empty()
	$('.newlistkayu').empty()
	$('.new-satjumlah').empty()
	$('.new-satvolume').empty()
	$('.new-satberat').empty()
$('#hide-satjumlah').val('')
$('#hide-satvolume').val('')
$('#hide-satberat').val('')
$('#hide-produk').val('')
$('#hide-listkayu').val('')
}

// end of function
})
</script>