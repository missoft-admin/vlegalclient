<div class="py-5 text-center w-100">
      <div class="mx-auto w-xxl w-auto-xs">
        <div class="px-3">
          <div>
            <h5>Forgot your password?</h5>
            <p class="text-muted my-3">
              Enter your email below and we will send you instructions on how to change your password.
            </p>
          </div>
          <form name="reset">
            <div class="form-group">
              <input type="email" placeholder="Email" class="form-control" required>
            </div>
            <button type="submit" class="btn primary btn-block p-x-md" >Send</button>
          </form>
          <div class="py-4">
            Return to 
            <a href="SignIn" class="text-primary _600">Sign in</a>
          </div> 
        </div>
      </div>
    </div>