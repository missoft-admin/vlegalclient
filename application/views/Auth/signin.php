<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['status'])){
echo $_SESSION['status'];
$_SESSION['status']='';
}else{
$_SESSION['status']='';
}
?>
})

function InvalidMsg(textbox,tipe) {
  var teks;
  if(tipe==1){
    teks='Username';}else{
      teks='Password';}
  if(textbox.validity.valueMissing){
        textbox.setCustomValidity(teks+' tidak boleh kosong');
  }else if(textbox.validity.patternMismatch){
        textbox.setCustomValidity(teks+' tidak boleh pakai spasi');
    }
    else {
        textbox.setCustomValidity('');
    }
    return true;
}
</script>
<!-- Login Form -->
<form class="form-horizontal push-30-t" action="{site_url}signin_proses" method="post">
<div class="form-group">
<div class="col-xs-12">
<div class="form-material form-material-primary floating">
<input type=text name="username" id="username" pattern="[A-Za-z0-9][^\s]+" oninvalid="InvalidMsg(this,1)" oninput="this.setCustomValidity('');" class=form-control required>
<label for="username">Username</label>
</div>
</div>
</div>
<div class="form-group">
<div class="col-xs-12">
<div class="form-material form-material-primary floating">
<input type="password" name="password" oninvalid="InvalidMsg(this,2)" oninput="this.setCustomValidity('')" class=form-control required>
<label for="password">Password</label>
</div>
</div>
</div>
<div class="form-group">
<div class="col-xs-6">
<label class="css-input switch switch-sm switch-primary">
<input type="checkbox" id="login-remember-me" name="login-remember-me"><span></span> Ijinkan tetap login?
</label>
</div>
<div class="col-xs-6">
<div class="font-s13 text-right push-5-t">
<a href="base_pages_reminder_v2.html">Lupa Password?</a>
</div>
</div>
</div>
<div class="form-group push-30-t">
<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
<button class="btn btn-sm btn-block btn-primary" type="submit">Log in</button>
</div>
</div>
</form>
                            <!-- END Login Form -->