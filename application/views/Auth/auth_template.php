<!DOCTYPE html>
<html lang=en>
<head>
<meta charset=utf-8 />
<title>Kulcimart | {title}</title>
<meta name=description content="Responsive, Bootstrap, BS4"/>
<meta name=viewport content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui"/>
<meta http-equiv=X-UA-Compatible content="IE=edge">
<meta name=apple-mobile-web-app-capable content=yes>
<meta name=apple-mobile-web-app-status-barstyle content=black-translucent>
<link rel=apple-touch-icon href="http://member.kulcimart.com/assets/images/logo-default.png">
<meta name=apple-mobile-web-app-title content=Flatkit>
<meta name=mobile-web-app-capable content=yes>
<link rel=stylesheet id=css-main href="{toastr_path}toastr.min.css">
<link rel="shortcut icon" sizes="196x196" href="{img_path}logo-default.png">
<link rel=stylesheet href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel=stylesheet href="{css_path}app.css" type="text/css" />
<link rel=stylesheet href="{css_path}style.css" type="text/css" />
<link rel=stylesheet href="{custom_path}auth.css" type="text/css" />
<script src="{libs_path}jquery/dist/jquery.min.js"></script>
<style type="text/css">
        img {
  image-rendering: auto;
  image-rendering: crisp-edges;
  image-rendering: pixelated;
}
</style>
</head>
<body>
<div class="d-flex flex-column flex">
<div class="navbar light bg pos-rlt box-shadow">
<div class=mx-auto>
<a href="{site_url}" class="navbar-brand hover"><figure>
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="106px" height="16px" viewBox="0 0 106 16" enable-background="new 0 0 106 16" xml:space="preserve">  <image id="image0" width="106" height="16" x="0" y="0"
    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGoAAAAQCAYAAAD+mOowAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAABmJLR0QA/wD/AP+gvaeTAAAA
CXBIWXMAAC4jAAAuIwF4pT92AAAP90lEQVRYw61ZadAmV1k957m3u9/3/d5vm/lmyTZZhgkhjEgW
EpSQAAFCiBIWg7HEEoyogBhLKaMsKooihSxaFktwSYpNFEEwCEEIEkIWUwnLJBkdCJNMkpnM/m3v
0t33Pscf75dENP4Kt+pWdXV39e37nPOcPs/THP/Ck5glDBtHYYCvmnTOKvDiRcRhlzlnVKXpe/c7
ZmfQPXZDtIOLoxSIerrXsSa5WJrMQK8zDq6Cm59M2XU91V/oIU0ldAt76ZFivHn6hMs+VB79NvrF
QXz5Wyuo2vp1z3zq7OJA/snpUGJ1mKGCEADAYCl3SstvH9VYbaerPw4S28VGMKKMQBmBnhlAYrV1
VAb0yowbl9fjRYdfjXUdQZ4BC0BOoEXICLmDJCYrGaQMmoFugAQZAAZ4rkECAJGHy5NjCYwVgAyw
7Vu5ydXUQxCPO0jC6YACYqcCYJg69hykvbcgZaLJI1gIIIl2WCMWHVRTUxiNVwBPsFgB3sCSoOwQ
CcEg1UI5qNDvT5M0HT28ot6xjvd8POFfbmzPKmd5XRnDpUUR4YKTk30pSwaeP9/jy8OMVM4brI0o
OhEp2EvXefxgb/neC9vRCkaHEi54UvfKc58y/4FRo3c2g3TueKWBBSJAiBCDMgx6tct+x4yvLVNe
X7jUKcgqEkZAWgvGZHJy8P9E7AkMkghlByy6YNkDQzkBK/auAv1ahhBpAY83HYQkPNG3io3rNLmf
RnKPzHdjEI6GQYT1KR7AudOz3Wf7EfZf+5JQzM3yyc1Sfk63KkoSvabVEQu826B721bPrKZs8/4D
eeir/mM2xI7ODJGDIhxTQgfDQ3e+elSHr1q/ujgkvj+5wwJZokgiEYOo5AIAQRC47AJiwALbdLLI
w7GYXHU8BlQWkABVyoD7GnQ/miEJZobY6aN1PUIKAOgIuIjK61BYB+Dq/wUYSLUDAkA9MaAofczA
s0jAwUHBcP1wZvgbvu7Iyzq7p4/rdDtLS0vePecnYokWu0ar+gOaTnLHT4eIxoCcE75O8oTU+t1T
FUfNw+GVo3HasWnbCDgQIhKnZI4QehvB/HQKf5EhmFFwXFlVdocb4XIZgCQQoCTdQ9J9ss2GACgC
8EkYJGUJg+zRwa1wPwD60S5bPJGwMBRQTmTsCEqQHHKAEw5BRlA8C8jPkPgxte0qQvhhfgggtfa+
T5w4MZB/lYiLPOvk0nhu7Ovlhxfyv/Gu7ld6R+2NaSG9IhZM9ZK+D2A3DIeN+Oaw1gEj9ndLJglH
Jez3JdimDUVnuKl7W767Rjho8MYqN21eW29mqhfe7dK2SEL09xZl+Gcl4vBQXG1bTZcBMxGenSA5
DoQnwBPQFhSmQpfDNJbLFWPAqMkA8ftVCG9TzpfDZz51WLMwAoC4RuVHIiW5P3L+cfOHoUIaH0Xo
zKldfpAMheLUAoI7ISmZ4BCC8ApaQOv1rYY18vgkAx8Zxh8NSAAQYxmuCcZrVpdGyAivywvtB+a+
3X1nfM+mPxm/fe/Hw7b8Sh2KCwCegrVlXcBUh4AwFLDPiH0A79OJ6bvpGN+RKvsSO4RqIteYZhdb
JlTFuSRoJIZuNzcroz+c6RcoLKKM0sEBcfeqcMF6R2ojqgqNAzKorWjD4AHJsouqJJUx2ErVm1o3
boa/Dm93reR4y1uWz8ENg+PRzSPAukJuAJggAe6Mna5yWwuPk3O0AqOHv4vRoZ1x4ZzXVSvfvmZQ
zh6H/qmXQOOBaKQoUZgBebHLB7GsvkIAyg5A0Jry0gxck+gfCVAuWNegmlDr/tXSwxLJufzU0SW2
JS9ZsjDRWAiP7c4kiESPwFYQW1XmI+FfZw4Mlq27NJNQ7SpxZDUB4txcL/bWiEY5EKOtlOSbrFeu
jpODMZ/VLTT34Ni+WmcUVQxvJ3jz2PMNfWOaiI5SRkaT2xcbeJXRttRN/uji0YffPj9Xnh9hfsR6
ez5Xn4rdo4CFerSFs/2fzOP0RYvlVrgawO8qO9NhnNK0e2uk9QGUa3M7lHbIyp3dY856odrl35o6
/hl/Y2X/IrT1yKH3ivgehI6Jz6fxNMjvMhaXKbcjb9Nfg1wi/1/7B2jiUB/f8DxukvORmEcKXich
TnUQE36Qc/stGp6jZbu5cPTlmJcmjxF/+FESBAfLBcfeG8pry09Mv2Buyp7SK5vPsnDkjsCAYwR0
H5UD41DCr/YCbmljQFsT7ngdaVc8e337pJy1aix/ryr8O5btMgGHA9gPpkON5ZcFhc8EB9LkG/C2
osSeRrQy8OVk8aoNll9wsDt3edmp1inl86zo3EbpVBTFtFnxM8PF/V9GCN80hk2AhoDmSasgFELe
U2w69UxaGOd66cLepqdfKDlyPUAOfA6Jy032LlAXSQLI7bkdb5d8hbH7DVr8D6VVYwj+KKXzRGhT
m8A4Iq0jeYbWMNDajZ4S8rhFMdN5LB8EgRNwrU0JTdNCOSG1SjR+DQ4U0zrzyGHe0RzX3mGbEzDj
QJgsjEQgk8gkxoRvbA/O1HG5QztbpzTd7rzB5olex9ArbP3/VBkSt49T+sziaIzUZLgcWb4XAtaX
ePpCLxwjAC6No2looDJ0NLlOjm5vdQfG8GvM8vWBEevmZi+IVry4QL5o7Pn8Va8uiFa+xGI8DxJo
di6AeZpFh78h1A07Wf9CYB2AEwB0Ia05NtuS02Br2y7/BxiO5GYAr5e/awHvIrTVFX5J5A+4FkVI
+wi7yRCuZoj3IBiSwx/drgPqEyoJABdC+QuCv2/04C0zHB1C42kbae8S8HkEXhGqAjm13VG9cgKA
82g8CcKz5JqObZsfiSMNUjPK/97pRPjG9CJ8YvYuG8WrauipnM9zxYlpo45NW1BqDi03Y4RT2EcY
ZLzb9xVFtZDPZmM3llYgM03UHKg4qXlqAKWkC0h8zmi/HMgHaawc+gkJyAwZwDMnqcp7HXwoQB0D
7nPhx+U4MwO79w75mtPmu+cdro/cX6T4oSr03hrCKv55+YzZe+oNpxxbDCFpALCGtA7AfwpaBXB2
KnIv+crvxjD7NFq4WDmfn+t8b+wW35PUjeKBCGudGINApr2lLON1qW4/akwm1w6RAvl6CL8NxE+W
RYnQzajTAExOxDBB0mns0znky6J1/l5kCW8vVqpbNMO3sZr/MOTPhZBhdj4L2yXoeQa8EVAH4BBK
G0D7QMzto2qmohJWVvJ/dbtxV3acumHO3pSvWfeilr4Lm9N9fnz7MOd9D40HPamLfl5A30fxrk5r
O6sP2wkJRRvuzGxhMPjEea2fyKT2gPwDAm8LtItC5G1ZurJG/nyBYNGA1dY3RtplVSDkeiBM5LwE
qEiWLsHkHziuE3HrvvqmJY1vesGG2aoWNoHLowd1xgPAppnI/0LycC2odaRdruwfBHmGGc4el4UJ
+LEZ8uI0Gr1ZwW62Ml4NYBpATcZcI08XxHpKCRUfEh1AuDtAcDoy7CilocRdYEabx8g1UI8FuRQn
lQNQwMMBjyyL9+e88pBS+5piavPvZ0+Hl6v5S0vk53qb35zknymK8mqL8WclPx3iekEibcqVajK8
NKb4mC5lJzgd9o9d3+gaT61d4knN9gBsR01gVwf1qo60Yzww043f8dJv8RoPs+PH2bHpypVVfOvz
uw/tec5xfWyemtJ4MEYsdXysCjh1FMBnF8f5jrky/DmlC438x5H7T2WF2yPxvIr2JpAnTVTD7x8n
YaqIPU/eDpN2dbvWBud5MaR/W1c127eUC+8U+WetawXUMnJeAtRdE+hdAH8JrlW438kYfsvd9/Ux
LYIXudVgsGfR7CsWeeHE7Kgiw6Y4kYBKss+pWfpWZgdEyRaBmHRjjoWwj+R/Sl5kz6+Hlf2UO39O
pg6JD0FwiD+voI2EzwLhWivC1+vBkVeZcbGqup/KKX06y98p6HRJHbjfTLPnA9oBYT+ozQS/BPnP
xpE/ZiAFoIGAxJ1TVYRL9yDjOgG7GLRbc/ngw+Ph4gODsb/gqRtmBsNc0tD1qAEabgL9pwL46eB6
mAGomxws8PyJfcVuEuNuYbtoeEnO+blm8dpZFmd3zW5tKBi5baokBg1RhLaXJQilp5TyauO3Fb14
U0Vd2oqXnjJTwSisjNtNDOVGAAcQjjwLHD8Zsp2AAoGnSbizres9ZeyOzaI14/uGwWx32d2CBF6C
SW31VSDPg+WZ8vQs0uYBgMF+UHADchpByDLpkQ7DKsiuoEtovCIUMy/Mg33faBcP/0n/2B//S+bl
yydk09eB5mopPmyxcy6gSyLzlQC/JtcSwW0M4RWl2Ucg/yKUbiOrLZI+S7PzPdXXCzoSY+e4uFAV
P+QH16rD7lon5wZA7wN4fHaeFKCnn7S5t/XkzVMnDEb5ySROhlCGlhCB6SLgF09ftzhq88dCbjG3
KZAp7IV4luT3hGDoMABwmNnXLOBpyORKm0e50A3TRXzezmH7+hOr8kxJ1VQZep5YVWWYPn42gkFX
NSN/h2dubYSN5rg+dsInPeGVEBICjgdYeXbIqr+LZbgwj8ersSj2EPwiYM/w0QODcR7+UxtPuLob
8CsC/sgdf0gUb7BgZ0rcJuAbZuVgOHjowfHwIHozJyLEDkRDiAZ3fIHyN1L5U2CE14P7hg/d+Q5q
CKuP365iCkILpPbSzvpNV7fDwZtTPf6EmV1HM3jyT47H9VfKXvj3sjf/aeV0F6VXeTt8LcBuHq/u
LLqzFw/HRx6CtLOcPbGOhvy/jLsAhgWHAdKvEfwVAVWIk5oR/mgpsB/CbRD2JegBgnuNuqfJuBEk
mlacO7mXhvvbnx8upss6pd9Kw6R8nRCCABYFoE0OBFzRtDj90DB/edt0qCyW1XC57pvh9iS/f/HQ
GBvWlbdn8KLWMdcNtlnyH0yvm23SIL8F4+rn2lx+FKbN7j5G2TkaquIX29E4xE4HAP9USlPl9BlQ
bpC8/V0Gvi817fcNBdJ49NfWTVuQu99z+ae6MzM3Nkt7msGDN6Gz/RcQij4Ek2eHu643+lvJcLmE
j+TB4Y+XMycdDv0NqI8+8Jvdzad/1LM2WKf8m9yModx+JpbxGan108oQl8def1mM6s4s/PThHV/c
Wm4+7R+mj9mu+sj935G0OyN+qYqdq/vd+Zkk7M3ePi8m/99VFkHiDqOGa4G8F9Aed+w0cs8o+55h
9gc2VMVqgpZI1HLBCXTNUBpQZ2AcofFDDVKjlQz/W9ekGWCPNXQEkEkZXkBKvO/Q0O/b1gsweCpn
1g3Gg6No2uaFe5dG5VQwOCLkDiMWASzSJi2bIsbPwuO/Jo8t5FcAiJAA+WEAcs+wEA8S8SAwQGSD
quocze3oqCenRae3SzWKcBXZB01I7WBff8N2dOZOAWMH8nbym8Qd8BaCvxtF773IeRR7c0gEvBkh
9Dfertw+G5KFGPd7bil3VVOdHWC7g9lAEDEWKHuzN4wOfvuGMLcAMBDAbYDOYKc3tljVlU8fqOVo
c3PrfwMSTBiW0ampXwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOC0wNy0xNlQwMzo1OToyMy0wNzow
MFX4fSEAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTgtMDctMTZUMDM6NTk6MjMtMDc6MDAkpcWdAAAA
AElFTkSuQmCC" />
</svg>
</figure>
</a>
</div>
</div>
<div id=content-body>
<?php
$this->load->view($content);
?>
</div>
</div>
<script src="{toastr_path}toastr.min.js"></script>
<script src="{libs_path}popper.js/dist/umd/popper.min.js"></script>
<script src="{libs_path}bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{libs_path}pace-progress/pace.min.js"></script>
<script src="{libs_path}pjax/pjax.js"></script>
<script src="{scripts_path}lazyload.config.js"></script>
<script src="{scripts_path}lazyload.js"></script>
<script src="{scripts_path}plugin.js"></script>
<script src="{scripts_path}nav.js"></script>
<script src="{scripts_path}scrollto.js"></script>
<script src="{scripts_path}toggleclass.js"></script>
<script src="{scripts_path}theme.js"></script>
<script src="{scripts_path}ajax.js"></script>
<script src="{scripts_path}app.js"></script>
<script type="text/javascript">
function Toastr(msg,title){
        toastr.options.timeOut = 4000;
        toastr.options.showDuration = 500;
        toastr.options.closeButton = true;
        toastr.options.showMethod = "slideDown";
        toastr.options.positionClass = "toast-top-right";
        toastr.warning(msg,title).css("width","400px");
}
function ToastrSukses(msg,title){
        toastr.options.timeOut = 5000;
        toastr.options.showDuration = 500;
        toastr.options.closeButton = true;
        toastr.options.showMethod = "slideDown";
        toastr.options.positionClass = "toast-top-right";
        toastr.success(msg,title).css("width","400px");
}
</script>
</body>
</html>
