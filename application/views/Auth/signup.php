<div class="py-5 text-center w-100">
      <div class="mx-auto w-xxl w-auto-xs">
        <div class="px-3">
          <!-- <div>
            <a href="#" class="btn btn-block indigo text-white mb-2">
              <i class="fa fa-facebook float-left"></i>
              Sign up with Facebook
            </a>
            <a href="#" class="btn btn-block red text-white">
              <i class="fa fa-google-plus float-left"></i>
              Sign up with Google+
            </a>
          </div>
          <div class="my-3 text-sm">
            OR
          </div> -->
          <form name="form" action="signup_proses" method="post">
            <div class="form-group">
              <input type="text" class="form-control" name="username" placeholder="Username" required>
            </div>
            <div class="form-group">
              <input type="email" class="form-control" name="email" placeholder="Email" required>
            </div>
            <div class="form-group">
              <input type="password" class="form-control" name="password" placeholder="Password" required>
            </div>
            <div class="form-group">
              <input type="password" class="form-control" name="password_confirm" placeholder="Confirm Password" required>
            </div>
            <div class="mb-3 text-sm">
              <span class="text-muted">By clicking Sign Up, I agree to the</span> 
              <a href="#">Terms of service</a> 
              <span class="text-muted">and</span> 
              <a href="#">Policy Privacy.</a>
            </div>
            <button type="submit" class="btn primary">Sign Up</button>
          </form>
          <div class="py-4 text-center">
            <div>Already have an account? <a href="SignIn" class="text-primary _600">Sign in</a></div>
          </div>
        </div>
      </div>
    </div>