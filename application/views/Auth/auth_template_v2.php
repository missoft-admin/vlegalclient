<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>{title_web} | Sign In</title>

        <meta name="description" content="OneUI - Admin Dashboard Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{favicon_url}">
 
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <!-- Bootstrap and OneUI CSS framework -->
        <link rel="stylesheet" href="{assets_path}css/bootstrap.min.css">
        <link rel="stylesheet" id="css-main" href="{assets_path}css/oneui.css">
        <link rel="stylesheet" id="css-main" href="{toastr_css}">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="{assets_path}css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
        <script src="{assets_path}js/core/jquery.min.js"></script>
    </head>
    <body>
        <!-- Login Content -->
        <div class="bg-white pulldown">
            <div class="content content-boxed overflow-hidden">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                        <div class="push-30-t push-50 animated fadeIn">
                            <!-- Login Title -->
                            <div class="text-center"><center>
<!-- <a href="{site_url}" class="navbar-brand hover"> -->
    <figure>
<?php
$this->load->view('auth/svgfile');
?>
        
<!-- <img src="https://s4.aconvert.com/convert/p3r68-cdx67/cbgxu-ime5j.svg" width="20%"> -->
</figure>
<!-- </a> -->
                                <!-- <p class="text-muted push-15-t">Kuliner Cimahi Mart</p> -->
                         </center>   </div>
                            <!-- END Login Title -->

                            <!-- Login Form -->
                            <!-- jQuery Validation (.js-validation-login class is initialized in js/pages/base_pages_login.js) -->
                            <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
<?php
$this->load->view($content);
?>
                            <!-- END Login Form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Login Content -->

        <!-- Login Footer -->
        <div class="pulldown push-30-t text-center animated fadeInUp">
            <small class="text-muted"><span class="js-year-copy"></span> &copy; OneUI 3.2</small>
        </div>
        <!-- END Login Footer -->

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script src="{assets_path}custom/basic-function.js"></script>
        <script src="{assets_path}js/core/bootstrap.min.js"></script>
        <script src="{assets_path}js/core/jquery.slimscroll.min.js"></script>
        <script src="{assets_path}js/core/jquery.scrollLock.min.js"></script>
        <script src="{assets_path}js/core/jquery.appear.min.js"></script>
        <script src="{assets_path}js/core/jquery.countTo.min.js"></script>
        <script src="{assets_path}js/core/jquery.placeholder.min.js"></script>
        <script src="{assets_path}js/core/js.cookie.min.js"></script>
        <script src="{assets_path}js/app.js"></script>
        <script src="{toastr_js}"></script>

        <!-- Page JS Plugins -->
        <script src="{assets_path}js/plugins/jquery-validation/jquery.validate.min.js"></script>

        <!-- Page JS Code -->
        <script src="{assets_path}js/pages/base_pages_login.js"></script>
    </body>
</html>