<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)
	
?>
<?$uri=$this->uri->segment(1);
	 $uri2=$this->uri->segment(2);	?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			
            
		</ul>
		<h3 class="block-title"><i class="si si-bar-chart"></i> {title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
				<thead>
					<tr>                                    
						<th width="5%">No</th>
						<th width="20%">Nama Barang</th>
						<th width="15%">Harga Stokies</th>
						<th width="15">Harga Member</th>					
						<th width="10%">Jenis</th>					
						<th width="15%">Stok</th>					
						<th width="10%">Tools</th>					
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
<input type="hidden" id="datatable_search" value="{datatable_search}"/>
<input type="hidden" id="datatable_page" value="{datatable_page}"/>
<input type="hidden" id="datatable_order" value="{datatable_order}"/>


