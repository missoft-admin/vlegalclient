
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li>
				<a href="{base_url}barang" class="btn"><i class="fa fa-reply"></i></a>
			</li>
            
		</ul>
		<h3 class="block-title"><i class="si si-bar-chart"></i> {title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
				<thead>
					<tr>                                    
						<th class="align_left">No</th>
							<th class="align_left">Nama Cabang</th>
							<th class="align_left center">Otoritas</th>
							<th class="align_left center">Kota</th>
							<th class="align_left center">Nama Barang</th>
							<th class="align_left center">Stok</th>
							<th class="align_left center">Progress</th>				
					</tr>
				</thead>
				
				<tbody>
					<?php if (isset($records)):?>
						<?php $no = 0;
						foreach($records as $row): ?>		
							<tr>
								<th class="align_left"><?php $no=$no+1;echo $no; ?></th>
								<td class="align_left"><a href="#"><?php echo $row->namacabang; ?></a></td>
								<td class="align_left center"><?php echo getotoritas($row->otonom); ?></td>
								<td class="align_left center"><?php echo $row->kota; ?></td>
								<td class="align_left center"><?php echo $row->namabarang; ?></td>
								<td class="align_left center"><?php echo number_format($row->stok,0,",","."); ?></td>
								<td class="align_left center"><?php echo number_format($row->progress,0,",","."); ?></td>
							</tr>
					<?php endforeach;?>	
					<?php else:?>
					
					<div class="alert alert-danger">
						<strong>Stok Barang di Tiap Cabang Masih Kosong</strong>,  <a href="<?php echo site_url("terima");?>">klik disini untuk lihat penerimaan</a>.
					</div>
					<?php endif;?>	
				</tbody>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>


