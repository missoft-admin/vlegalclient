<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}barang" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-bar-chart"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('barang/simpan','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Nama Barang</label>
				<div class="col-md-6">
					<input  type="text" class="form-control input-sm" name="namabarang" id="namabarang" placeholder="namabarang" value="{namabarang}" />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Harga Beli</label>
				<div class="col-md-6">
					<input  type="text" class="form-control input-sm number"  name="hargabeli" id="hargabeli" placeholder="hargabeli" value="{hargabeli}" />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Harga Jual Stokis</label>
				<div class="col-md-6">
					<input  type="text" class="form-control input-sm number" name="hargajualstokis" id="hargajualstokis" placeholder="hargajualstokis" value="{hargajualstokis}" />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Harga Jual Member</label>
				<div class="col-md-6">
					<input  type="text" class="form-control input-sm number" name="hargajualmember" id="hargajualmember" placeholder="hargajualmember" value="{hargajualmember}" />
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="jenis">Jenis </label>
				<div class="col-md-4">
					<select class="form-control input-sm" id="jenis" name="jenis" size="1">
						<option value="0" <?=($jenis=='0'?'selected':'')?>>Non Komisi</option>
						<option value="1" <?=($jenis=='1'?'selected':'')?>>Komisi</option>
					</select>
				</div>							
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Keterangan</label>
				<div class="col-md-10">
					<input  type="text" class="form-control input-sm" name="ket" id="ket" placeholder="Keterangan" value="{ket}" />
				</div>				
			</div>
			<div class="form-group">
				
			</div>
			<?php //if (getauth('2121',$this->session->userdata('level'))):?>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mmember" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php //endif;?>
			<?php echo form_hidden('noid', $noid); ?>
			<?php echo form_close() ?>
	</div>
</div>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			
			<li>
				<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> Info Operator</h3>
	</div>
	<div class="block-content">
		<form class="form-horizontal push-10-t push-10" action="" method="post" onsubmit="return false;">
			<div class="form-group">
				<div class="col-xs-12">
					<div class="form-material">
						<input class="form-control" type="text" id="namac" name="namac" value="{namac}" placeholder="..">
						<label for="namac">Created By :</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12">
					<div class="form-material">
						<input class="form-control" type="namae"  name="namae" value="{namae}" placeholder="Last Edited">
						<label for="namae">Last Edited By :</label>
					</div>
				</div>
			</div>
			
		</form>
	</div>
</div>
