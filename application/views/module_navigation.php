<?php
$uri1=$this->uri->segment(1);
$uri2=$this->uri->segment(2);
$uri3=$this->uri->segment(3);
$tipe=(!empty($_GET['tipe']))?$_GET['tipe']:'';
$jenis=(!empty($_GET['jenis']))?$_GET['jenis']:'';
$page=(!empty($_GET['p']))?$_GET['p']:'';
?>
<li>
	<a class="active" href="{base_url}dashboard"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
</li>
<li class="nav-main-heading"><span class="sidebar-mini-hide">Transaksi</span></li>
<li <?=menuIsOpen('bahanbaku').getLinkOpen('bahan-baku',$jenis)?>>
	<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Bahan Baku</span></a>
	<ul>
		<li <?=getLinkOpen('lokal',$tipe)?>>
			<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fas fa-share"></i>Lokal</a>
			<ul>
				<li>
					<a <?=getLinkActive('penerimaan','lokal',$tipe)?> href="{base_url}bahanbaku/penerimaan?tipe=lokal">
					<i class="glyphicon glyphicon-option-horizontal"></i>Penerimaan</a></li>
				<li>
					<a <?=getLinkActive('pengeluaran','lokal',$tipe)?> href="{base_url}bahanbaku/pengeluaran?tipe=lokal">
					<i class="glyphicon glyphicon-option-horizontal"></i>Pengeluaran</a></li>
			</ul>
		</li>
		<li <?=getLinkOpen('import',$tipe)?>>
			<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-reply"></i>Import</a>
			<ul>
				<li>
					<a <?=getLinkActive('penerimaan','import',$tipe)?> href="{base_url}bahanbaku/penerimaan?tipe=import">
					<i class="glyphicon glyphicon-option-horizontal"></i>Penerimaan</a></li>
				<li>
					<a <?=getLinkActive('pengeluaran','import',$tipe)?> href="{base_url}bahanbaku/pengeluaran?tipe=import">
					<i class="glyphicon glyphicon-option-horizontal"></i>Pengeluaran</a></li>
			</ul>
		</li>
		<li>
			<a <?=LinkActive('bahan-baku',$jenis)?> href="{base_url}stock/produk?jenis=bahan-baku">
			<i class="fas fa-briefcase"></i>Stock Bahan Baku</a>
		</li>
	</ul>
</li>
<li <?=menuIsOpen('produksi').getLinkOpen('wip',$jenis)?>>
	<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Produksi</span></a>
	<ul>
		<li>
			<a <?=menuIsActive('realisasi')?> href="{base_url}produksi/realisasi">
			<i class="fas fa-briefcase"></i>Realisasi Produksi</a>
		</li>
		<li>
			<a <?=LinkActive('wip',$jenis)?> href="{base_url}stock/produk?jenis=wip">
			<i class="fas fa-briefcase"></i>Stock WIP</a>
		</li>
	</ul>
</li>
<li <?=menuIsOpen('produk').getLinkOpen('produk-jadi',$jenis)?>>
	<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Pengeluaran Produk</span></a>
	<ul>
		<li>
			<a <?=getLinkActive('penjualan','lokal',$tipe)?> href="{base_url}produk/penjualan?tipe=lokal">
			<i class="fa fa-reply"></i>Penjualan Lokal</a>
		</li>
		<li>
			<a <?=getLinkActive('penjualan','ekspor',$tipe)?> href="{base_url}produk/penjualan?tipe=ekspor">
			<i class="fas fa-share"></i>Penjualan Ekspor</a>
		</li>
		<li>
			<a <?=menuIsActive('proses-lebih-lanjut')?> href="{base_url}produk/proses-lebih-lanjut">
			<i class="fas fa-refresh"></i>Proses Lebih Lanjut</a>
		</li>
		<li>
			<a <?=menuIsActive('lain-lain')?> href="{base_url}produk/lain-lain">
			<i class="glyphicon glyphicon-option-horizontal"></i>Lain-lain</a>
		</li>
		<li>
			<a <?=LinkActive('produk-jadi',$jenis)?> href="{base_url}stock/produk?jenis=produk-jadi">
			<i class="fas fa-briefcase"></i>Stock Produk Jadi</a>
		</li>
	</ul>
</li>
<li <?=menuIsOpen('dokumen')?>>
	<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Dokumen V-Legal</span></a>
	<ul>
		<li>
			<a <?=menuIsActive('pengajuan').menuIsActive('detail-pengajuan')?> href="{base_url}dokumen/pengajuan">
			<i class="fas fa-briefcase"></i>Data Pengajuan</a>
		</li>
	</ul>
</li>

<li class="nav-main-heading"><span class="sidebar-mini-hide">Main Menu</span></li>
<li <?=menuIsOpen('setting')?>>
	<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Setting</span></a>
	<ul>
			<a <?=menuIsActive('buyers').menuIsActive('new-buyer')?> href="{base_url}setting/buyers">
				<i class="si si-bar-chart"></i>Buyers
			</a>
			</li>
		<li>
			<a <?=menuIsActive('supplier').menuIsActive('new-supplier')?> href="{base_url}setting/supplier">
				<i class="si si-bar-chart"></i>Suppliers
			</a>
			</li>
		<li>
			<a <?=menuIsActive('produk').menuIsActive('new-produk')?> href="{base_url}setting/produk">
				<i class="si si-bar-chart"></i>Data Produk
			</a>
			</li>
		<li>
			<a <?=menuIsActive('sortimen').menuIsActive('new-sortimen')?> href="{base_url}setting/sortimen">
				<i class="si si-bar-chart"></i>Data Sortimen
			</a>
			</li>
		<li>
			<a <?=menuIsActive('wip').menuIsActive('new-wip')?> href="{base_url}setting/wip">
				<i class="si si-bar-chart"></i>Data WIP
			</a>
			</li>
	</ul>
</li>

