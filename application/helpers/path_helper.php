<?php

function backend_info()
{
    $CI =& get_instance();
    $data = array(
        'site_url'             => site_url(),
        'base_url'             => base_url(),
        'assets_path'          => base_url().'assets/',
        'css_path'             => base_url().'assets/css/',
        'fonts_path'           => base_url().'assets/fonts/',
        'img_path'             => base_url().'assets/img/',
        'js_path'              => base_url().'assets/js/',
        'plugins_path'         => base_url().'assets/js/plugins/',
        'upload_path'          => base_url().'assets/upload/',
        'ajax'                 => base_url().'assets/ajax/',
        'toastr_css'           => base_url().'assets/toastr/toastr.min.css',
        'toastr_js'            => base_url().'assets/toastr/toastr.min.js',

        'user_id'              => $CI->session->userdata('user_id'),
        'user_avatar'          => $CI->session->userdata('user_avatar'),
        'username'             => $CI->session->userdata('user_name'),
        'user_permission'      => $CI->session->userdata('user_permission'),
        'user_last_login'      => human_date($CI->session->userdata('user_last_login')),

        'json_negara'               => base_url().'json/negara',
        'json_provinsi'             => base_url().'json/provinsi',
        'json_kabupaten'            => base_url().'json/kabupaten',
        'json_sertifikasi'          => base_url().'json/sertifikasi',
        'json_produk'               => base_url().'json/produk',
        'json_wip'                  => base_url().'json/wip',
        'json_buyer'                => base_url().'json/buyer',
        'json_sortimen'             => base_url().'json/sortimen',
        'json_supplier'             => base_url().'json/supplier',
        'json_datakayu'             => base_url().'json/datakayu',
        'json_datapengeluaran'      => base_url().'json/DataPengeluaran',
        'json_datahutan'            => base_url().'json/datahutan',
        'json_datadokumen'          => base_url().'json/datadokumen',
        'json_dataasalkayu'         => base_url().'json/dataasalkayu',
        'json_satuanbyvolume'       => base_url().'json/satuan?by=volume',
        'json_satuanbyjumlah'       => base_url().'json/satuan?by=jumlah',
        'json_satuanbyberat'        => base_url().'json/satuan?by=berat',
        'json_matauang'             => base_url().'json/matauang',
        'json_loading'              => base_url().'json/loading',
        'json_loading_byClient'     => base_url().'json/loading_byClient',
        'json_discharge'            => base_url().'json/discharge',

        'title_web'            => 'V-Legal PT TUV Rheinland Indonesia',
        'favicon_url'          => base_url().'assets/img/favicons/ico.png'
 
    );
    return $data;
}
