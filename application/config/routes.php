<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'dashboard';

/**
========================  L I N K  G E T  D A T A  J S O N  ========================
*/
$route['json/negara'] 		 	= 'A_json/getNegara/$1';
$route['json/supplier'] 	 	= 'A_json/getSupplier/$1';
$route['json/clients'] 		 	= 'A_json/getClients/$1';
$route['json/loading'] 		 	= 'A_json/getLoading/$1';
$route['json/loading_byClient'] = 'A_json/getLoading_byClient/$1';
$route['json/groupusers']	 	= 'A_json/getGroupUsers/$1';
$route['json/provinsi']	 	 	= 'A_json/getProvinsi/$1';
$route['json/kabupaten']	 	= 'A_json/getKabupaten/$1';
$route['json/users']	 	 	= 'A_json/getUsers/$1';
$route['json/buyer']	 	 	= 'A_json/getBuyer/$1';
$route['json/sortimen']	 	 	= 'A_json/getSortimen/$1';
$route['json/wip']	 	 	 	= 'A_json/getWip/$1';
$route['json/produk']	 	 	= 'A_json/getProduk/$1';
$route['json/sertifikasi']	 	= 'A_json/getSertifikasi/$1';
$route['json/datahutan']	 	= 'A_json/getHutanData/$1';
$route['json/datadokumen']	 	= 'A_json/getDokumenData/$1';
$route['json/dataasalkayu']	 	= 'A_json/getAsalKayuData/$1';
$route['json/lastperiode']	 	= 'A_json/getItemsByLastPeriode/$1';
$route['json/datakayu']	 	 	= 'A_json/getKayu/$1';
$route['json/DataPengeluaran']	= 'A_json/getDataPengeluaran/$1';
$route['json/satuan?(:any)']	= 'A_json/getSatuanByOption/$1';
$route['json/matauang']	 	 	= 'A_json/getMataUang/$1';
$route['json/discharge']	 	= 'A_json/getDischargeByNegara/$1';

/**
=============================  L I N K  S E T T I N G S =============================
*/
//link authentication
$route['SignIn'] 		= 'auth/signin/$1';
$route['SignUp'] 		= 'auth/signup/$1';
$route['SignOut'] 		= 'auth/do_logout/$1';
$route['Forgot'] 		= 'auth/forgot_password/$1';
	//link proses authentication
	$route['signup_proses'] = 'auth/proses_signup/$1';
	$route['signin_proses'] = 'auth/do_login/$1';
/**
=============================  L I N K  S E T T I N G S =============================
*/

// Link setting buyers
$route['setting/buyers'] 				= 'Mbuyers/showingData/$1';
$route['setting/buyers/(:any)']			= 'Mbuyers/indexUpdate/$1';
$route['setting/new-buyer'] 			= 'Mbuyers/insertBaru/$1';
$route['setting/new-buyer/proses']		= 'Mbuyers/saveNew/$1';
$route['setting/update-buyers/proses']	= 'Mbuyers/FupdateData/$1';
$route['ajax/buyers'] 					= 'Mbuyers/getListDT/$1';
$route['ajax/upStatusBuyer']			= 'Mbuyers/gantiStatus/$1';
$route['ajax/delBuyer'] 				= 'Mbuyers/delAkun/$1';

// Link setting supplier
$route['setting/supplier'] 					= 'Msupplier/showingData/$1';
$route['setting/supplier/(:any)']			= 'Msupplier/indexUpdate/$1';
$route['setting/new-supplier'] 				= 'Msupplier/insertBaru/$1';
$route['setting/new-supplier/proses']		= 'Msupplier/saveNew/$1';
$route['setting/update-supplier/proses']	= 'Msupplier/FupdateData/$1';
$route['ajax/supplier'] 					= 'Msupplier/getListDT/$1';
$route['ajax/upStatusSupplier']				= 'Msupplier/gantiStatus/$1';
$route['ajax/delSupplier'] 					= 'Msupplier/delAkun/$1';

// Link setting produk
$route['setting/produk'] 				= 'Mproduk/showingData/$1';
$route['setting/produk/(:any)']			= 'Mproduk/indexUpdate/$1';
$route['setting/new-produk'] 			= 'Mproduk/insertBaru/$1';
$route['setting/new-produk/proses']		= 'Mproduk/saveNew/$1';
$route['setting/update-produk/proses']	= 'Mproduk/FupdateData/$1';
$route['ajax/produk'] 					= 'Mproduk/getListDT/$1';
$route['ajax/upStatusProduk']			= 'Mproduk/gantiStatus/$1';
$route['ajax/delProduk'] 				= 'Mproduk/delAkun/$1';

// Link setting sortimen
$route['setting/sortimen'] 				= 'Msortimen/showingData/$1';
$route['setting/sortimen/(:any)']		= 'Msortimen/indexUpdate/$1';
$route['setting/new-sortimen'] 			= 'Msortimen/insertBaru/$1';
$route['setting/new-sortimen/proses']	= 'Msortimen/saveNew/$1';
$route['setting/update-sortimen/proses']= 'Msortimen/FupdateData/$1';
$route['ajax/sortimen'] 				= 'Msortimen/getListDT/$1';
$route['ajax/upStatusSortimen']			= 'Msortimen/gantiStatus/$1';
$route['ajax/delSortimen'] 				= 'Msortimen/delAkun/$1';

// Link setting wip
$route['setting/wip'] 				= 'Mwip/showingData/$1';
$route['setting/wip/(:any)']		= 'Mwip/indexUpdate/$1';
$route['setting/new-wip'] 			= 'Mwip/insertBaru/$1';
$route['setting/new-wip/proses']	= 'Mwip/saveNew/$1';
$route['setting/update-wip/proses']	= 'Mwip/FupdateData/$1';
$route['ajax/wip'] 					= 'Mwip/getListDT/$1';
$route['ajax/upStatusWip']			= 'Mwip/gantiStatus/$1';
$route['ajax/delWip'] 				= 'Mwip/delAkun/$1';

/**
=============================  L I N K  T R A N S A K S I  =============================
*/

/** Link bahan baku */
	// Penerimaan
$route['bahanbaku/penerimaan?(:any)'] 			= 'bahanbaku/Tpenerimaan/showingData/$1';
$route['bahanbaku/penerimaan/proses?(:any)'] 	= 'bahanbaku/Tpenerimaan/saveNew/$1';
$route['ajax/penerimaan?(:any)']				= 'bahanbaku/Tpenerimaan/getListDT/$1';
$route['ajax/delPenerimaan'] 					= 'bahanbaku/Tpenerimaan/delAkun/$1';
	// Pengeluaran
$route['bahanbaku/pengeluaran?(:any)'] 			= 'bahanbaku/Tpengeluaran/showingData/$1';
$route['bahanbaku/pengeluaran/proses?(:any)']	= 'bahanbaku/Tpengeluaran/saveNew/$1';
$route['ajax/pengeluaran?(:any)']				= 'bahanbaku/Tpengeluaran/getListDT/$1';
$route['ajax/delPengeluaran'] 					= 'bahanbaku/Tpengeluaran/delAkun/$1';

/** Link Produksi */
$route['produksi/realisasi'] 			= 'Tproduksi/showingData/$1';
$route['produksi/realisasi/edit']		= 'Tproduksi/getData4Update/$1';
$route['produksi/realisasi/proses'] 	= 'Tproduksi/saveNew/$1';
$route['ajax/realisasi?(:any)']			= 'Tproduksi/getListDT/$1';
$route['ajax/delRealisasi'] 			= 'Tproduksi/delAkun/$1';
$route['ajax/periode-realisasi']		= 'Tproduksi/getLastPeriode/$1';

/** Link Produk */
$route['produk/penjualan?(:any)'] 			= 'Tproduk/showingData/$1';
$route['produk/penjualan/proses?(:any)']	= 'Tproduk/saveNew/$1';
$route['ajax/penjualan?(:any)']				= 'Tproduk/getListDT/$1';
$route['ajax/delPenjualan'] 				= 'Tproduk/delAkun/$1';
	//proses lebih lanjut
$route['produk/proses-lebih-lanjut'] 		= 'Tproseslanjut/showingData/$1';
$route['produk/proses-lebih-lanjut/proses']	= 'Tproseslanjut/saveNew/$1';
$route['produk/proses-lebih-lanjut/edit']	= 'Tproseslanjut/getData4Update/$1';
$route['ajax/proses-lebih-lanjut']			= 'Tproseslanjut/getListDT/$1';
$route['ajax/delProses-lebih-lanjut'] 		= 'Tproseslanjut/delAkun/$1';
$route['ajax/periode-proses-lebih-lanjut']	= 'Tproseslanjut/getLastPeriode/$1';
	//lain-lain
$route['produk/lain-lain'] 			= 'Tlainlain/showingData/$1';
$route['produk/lain-lain/proses']	= 'Tlainlain/saveNew/$1';
$route['produk/lain-lain/edit']		= 'Tlainlain/getData4Update/$1';
$route['ajax/lain-lain']			= 'Tlainlain/getListDT/$1';
$route['ajax/delLain-lain'] 		= 'Tlainlain/delAkun/$1';
$route['ajax/periode-lain-lain']	= 'Tlainlain/getLastPeriode/$1';

/** Link Stock */
	// Bahan Baku
$route['stock/produk?(:any)']		 		= 'Mstock/showingData/$1';
$route['stock/produk/edit?(:any)'] 			= 'Mstock/getData4Update/$1';
$route['stock/produk/proses?(:any)'] 		= 'Mstock/saveNew/$1';
$route['ajax/stock-produk?(:any)']			= 'Mstock/getListDT/$1';
$route['ajax/delStock']		 				= 'Mstock/delAkun/$1';

/* Link Dokumen V-Legal */
	// Pengajuan
$route['dokumen/pengajuan'] 				= 'Tdatapengajuan/showingData/$1';
$route['dokumen/pengajuan/(:any)']			= 'Tdatapengajuan/indexUpdate/$1';
$route['dokumen/new-pengajuan'] 			= 'Tdatapengajuan/insertBaru/$1';
$route['dokumen/new-pengajuan/proses']		= 'Tdatapengajuan/saveNew/$1';
$route['dokumen/update-pengajuan/proses']	= 'Tdatapengajuan/FupdateData/$1';
$route['dokumen/detail-pengajuan/(:any)']	= 'Tdatapengajuan/showDetail/$1';
$route['ajax/pengajuan']					= 'Tdatapengajuan/getListDT/$1';
$route['ajax/delPengajuan'] 				= 'Tdatapengajuan/delAkun/$1';
$route['dokumen/page-detail/(:any)'] 			= 'Tdatapengajuan/pagination_tbl/$1';




$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
