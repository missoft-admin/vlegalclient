<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stokies extends CI_Controller {

   function __construct()
   {
   	  parent::__construct();
      // if (!$online=$this->infobar_model->cek_setting()){
		// redirect('offline');
   	  // }
      // if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       // redirect('login');
      // }
      $this->load->model('stokies_model','stokies_model'); 
//      check_infobar();
	  $this->infobar=$this->infobar_model->get_infobar();
   }

	function index()
	{	
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'All Stokies';
		$data['template'] 		= 'stokies/index';
		$data['ajax_url'] = 'stokies/getDataList'; 
		$data['breadcrum'] 	= array(
								array("Area Admin",'#'),
								array("Stokies",'#'),
								array("List",'stokies')
								);
	
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		
	}
	function getDataList()
    {
        $query = $this->stokies_model->data_list(0);
        $this->output->set_output($query);
    }
	function getDataListNew()
    {
        $query = $this->stokies_model->data_list(1);
        $this->output->set_output($query);
    }
	function newstokies(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'New Stokies';
		$data['template'] 		= 'stokies/index';
		$data['ajax_url'] = 'stokies/getDataListNew'; 
		$data['breadcrum'] 	= array(
								array("Area Admin",'#'),
								array("Stokies",'#'),
								array("New",'stokies')
								);
	
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	
	function find_stokis()
	{
		$cari 	= $this->input->post('nama');
		$arr = $this->stokies_model->find_stokis($cari);
		$this->output->set_output(json_encode($arr));
	}
	function find_noid()
	{
		$cari 	= $this->input->post('cari');
		$arr = $this->stokies_model->find_noid($cari);
		$this->output->set_output(json_encode($arr));
	}

	function editor($noidstokies=0,$edit=0,$err=0,$suk=0)
	{//if ( $this->session->userdata('level')!=1 && $this->session->userdata('level')!=3 ) { redirect('dashboard');	}
		$this->session->unset_userdata('searchkey');
		if ($edit && !$err){
			$this->session->set_userdata('page', 'Stokies - Edit');
			if($row = $this->stokies_model->get_stokies($noidstokies))
			{
				// print_r($row);exit();
				$data['noidstokies'] = $noidstokies;
				$data['namastokies'] = $row->namastokies;
				$data['noidcabang'] = $row->noidcabang;
				$data['noid'] = $row->noid;
				$data['namamembers'] = $row->namamembers;
				$data['alamat'] = $row->alamat;
				$data['desa'] = $row->desa;
				$data['kecamatan'] = $row->kecamatan;
				$data['kota'] = $row->kota;
				$data['nama_kota'] = $row->nama_kota;
				$data['propinsi'] = $row->propinsi;
				$data['telepon'] = $row->telepon;
				$data['hp'] = $row->hp;
				$data['tgldaftar'] = $row->tgldaftar;
				$data['lat'] = $row->lat;
				$data['lng'] = $row->lng;
				$data['norek'] = $row->norekening;
				$data['atasnama'] = $row->atasnama;
				$data['idbank'] = $row->idbank;
				$data['kodebank'] = $row->kodebank;
				$data['bank'] = $row->bank;
				$data['cabang'] = $row->cabang;
				$data['email'] = $row->email;
				$data['group'] = $row->group;
				$data['edit'] = '1';
				if ($row->norekening){$data['haverec'] =1;}else{$data['haverec'] =0;} 
				if($row->createdby){$data['namac'] = $row->namac;}else{$data['namac'] = " - "; }
				if($row->editedby){
					if($row->ide<=16){
						$data['namae'] = $row->namae." [ Admin ]";
					}else{$data['namae'] = $row->namae." [ Operator Stokies ".$row->namastokies." ]"; }
				}else{$data['namae'] = " - "; }
				//$data['namaupline'] = $this->stokies_model->get_namastokies($row->noupline);
			}
			// print_r($row);exit();
		}else{
			$this->session->set_userdata('page', 'Stokies - Addnew');
			$data['noidstokies'] = $this->stokies_model->get_newnoidstokies();
			$data['namastokies'] = "";
			$data['noidcabang'] ='0';
			$data['nama_kota'] = "";
			$data['propinsi'] = "";
			$data['noid'] = "";
			$data['alamat'] = "";
			$data['desa'] = "";
			$data['kecamatan'] = "";
			$data['kota'] = "";
			$data['telepon'] = "";
			$data['hp'] = "";
			$data['lat'] = "";
			$data['lng'] = "";
			$data['norek'] = "";
			$data['atasnama'] = "";
			$data['bank'] = "";
			$data['cabang'] = "";
			$data['haverec'] = 0;
			$data['email'] = "";
			$data['group'] = "00000000";
			$data['kodebank'] = '';
			$data['idbank'] = '';
			$data['edit'] ='0';
		}
		$data['rbank']=$this->stokies_model->get_bank();
		$data['rkota']=$this->stokies_model->get_kota();
		$data['reccabang']=$this->stokies_model->get_cabang();
		if ($err){
			$data['error'] ="NO ID : \"<span style=\"color: white; font-weight: bold;\">".$noidstokies."</span>\" sudah ada, gunakan NO ID lain.";
		}else{
			$data['error'] =0;
		}
		$data['suk'] =$suk;
		$data['active'] =232;
		if ($noidstokies){
			$data['breadcrum'] = array(array("Master","#","frames"),array("Stokies",'stokies',"point_right"),array("Edit",'#',"active"));
		}else{
			$data['breadcrum'] = array(array("Master","#","frames"),array("Stokies",'stokies',"point_right"),array("Input Baru",'#',"active"));
		}
		
		$data['title'] = 'Stokies - Input Baru';
		$data['template'] = 'stokies/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function editor_rekening($noidstokies=0,$edit=0,$err=0,$suk=0)
	{//if ( $this->session->userdata('level')!=1 && $this->session->userdata('level')!=3 ) { redirect('dashboard');	}
		$this->session->unset_userdata('searchkey');
		if (!$err){
			$this->session->set_userdata('page', 'Stokies - Edit');
			if($row = $this->stokies_model->get_stokies($noidstokies))
			{
				$data['noidstokies'] = $noidstokies;
				$data['namastokies'] = $row->namastokies;
				$data['noidcabang'] = $row->noidcabang;
				$data['noid'] = $row->noid;
				$data['namamembers'] = $row->namamembers;
				$data['alamat'] = $row->alamat;
				$data['desa'] = $row->desa;
				$data['kecamatan'] = $row->kecamatan;
				$data['kota'] = $row->kota;
				$data['telepon'] = $row->telepon;
				$data['hp'] = $row->hp;
				$data['tgldaftar'] = $row->tgldaftar;
				$data['lat'] = $row->lat;
				$data['lng'] = $row->lng;
				
				$data['email'] = $row->email;
				
			}
			
			if($row = $this->stokies_model->get_stokies_rekening($noidstokies))
			{				
				$data['idbank'] = $row->idbank;
				$data['kodebank'] = $row->kodebank;
				$data['norek2'] = $row->norekening;
				$data['atasnama2'] = $row->atasnama;
				$data['bank2'] = $row->bank;
				$data['cabang2'] = $row->cabang;
				$data['namae'] = $row->namae;
				$data['tgl_edit'] = $row->tgl_edit;
				  // print_r($row);exit();
				//$data['namaupline'] = $this->stokies_model->get_namastokies($row->noupline);
			}else{
				$data['norek2'] = '';
				$data['atasnama2'] = '';
				$data['bank2'] = '';
				$data['cabang2'] = '';
				$data['namae'] = '';
				$data['tgl_edit'] = '';
			}
			
		}
		$data['rbank']=$this->stokies_model->get_bank();
		// print_r($data['rbank']);exit();
		$data['rkota']=$this->stokies_model->get_kota();
		$data['reccabang']=$this->stokies_model->get_cabang();
		if ($err){
			$data['error'] ="NO ID : \"<span style=\"color: white; font-weight: bold;\">".$noidstokies."</span>\" sudah ada, gunakan NO ID lain.";
		}else{
			$data['error'] =0;
		}
		$data['suk'] =$suk;
		$data['active'] =232;
		if ($noidstokies){
			$data['breadcrum'] = array(array("Master","#","frames"),array("Stokies",'stokies',"point_right"),array("Edit",'#',"active"));
		}else{
			$data['breadcrum'] = array(array("Master","#","frames"),array("Stokies",'stokies',"point_right"),array("Input Baru",'#',"active"));
		}
		$data['edit'] =$edit;
		$data['template'] = 'stokies/edit_rekening_view';
		$data = array_merge($data,$this->infobar);
		$this->parser->parse('index',$data); 
	}
	function simpan()
	{	
		$edit=$this->input->post('edit');
		$noidstokies=$this->input->post('noidstokies');
		
		$haverec = (int)($this->input->post('haverec'));
		if ($haverec){
			$datarek = array(
				'norekening' => $this->input->post('norek'),
				'atasnama' => ucwords(strtolower($this->input->post('atasnama'))),
				'bank' => strtoupper($this->input->post('bank')),
				'cabang' => ucwords(strtolower($this->input->post('cabang')))
			);
			if ($this->input->post('kodebank')<5){
				$datarek['kodebank']= $this->input->post('kodebank');
				$datarek['bank']=$this->stokies_model->cek_nama_bank($this->input->post('kodebank'));
				$datarek['idbank']='0';
			}else{
				$datarek['kodebank']= $this->input->post('kodebank2');
				$datarek['bank']=$this->stokies_model->cek_nama_bank($this->input->post('kodebank2'));
				$datarek['idbank']='1';
			}
		}else{
			$datarek = array(
				'norekening' => '',
				'atasnama' => '',
				'bank' => '',
				'cabang' => ''
			);
		}
		if ($edit){
			// print_r($this->input->post('noid'));exit();
			$datarec = array( 
				'namastokies' => ucwords(strtolower($this->input->post('namastokies'))),
				'noidcabang' => $this->input->post('noidcabang'),
				'noid' => strtoupper($this->input->post('noid')),
				'alamat' => ucwords(strtolower($this->input->post('alamat'))),
				'desa' => ucwords(strtolower($this->input->post('desa'))),
				'kecamatan' => ucwords(strtolower($this->input->post('kecamatan'))),
				'kota' => ucwords(strtolower($this->input->post('kota'))),
				'telepon' => $this->input->post('telepon'),
				'hp' => $this->input->post('hp'),
				'email' => $this->input->post('email'),
				'lat' => $this->input->post('lat'),
				'lng' => $this->input->post('lng'),
				'group' => $this->input->post('group'),
				'editedby' => $this->session->userdata('userid')
				);
			$datarec=array_merge($datarec,$datarek);
			$this->stokies_model->update_record($noidstokies,$datarec);
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah disimpan.');
			redirect("stokies/editor/$noidstokies/1/0/1");				
		}else{
			$datarec = array( 
				'noidstokies' => $noidstokies,
				'namastokies' => ucwords(strtolower($this->input->post('namastokies'))),
				'noidcabang' => $this->input->post('noidcabang'),
				'noid' => strtoupper($this->input->post('noid')),
				'alamat' => ucwords(strtolower($this->input->post('alamat'))),
				'desa' => ucwords(strtolower($this->input->post('desa'))),
				'kecamatan' => ucwords(strtolower($this->input->post('kecamatan'))),
				'kota' => ucwords(strtolower($this->input->post('kota'))),
				'telepon' => $this->input->post('telepon'),
				'hp' => $this->input->post('hp'),
				'email' => $this->input->post('email'),
				'lat' => $this->input->post('lat'),
				'lng' => $this->input->post('lng'),
				'passkey' => md5(sha1(md5($noidstokies))),
				'tgldaftar' => date("Y-m-d H:t:s"),
				'group' => $this->input->post('group'),
				// 'createdby' => $this->session->userdata('userid')
				'createdby' =>''
				);
			$datarec=array_merge($datarec,$datarek);
			// print_r($datarec);exit();
			$this->stokies_model->add_record($datarec);
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah disimpan.');
			
			redirect("stokies/editor/$noidstokies/1/0/1");
		}
		
	}
	function simpan_rekening($noidstokies,$edit){
		// print_r('Kesini Bray');exit();
			$datarek = array(
				
				'noidstokies' => $this->input->post('noidstokies'),
				'kodebank' => $this->input->post('kodebank'),
				'norekening' => $this->input->post('norek2'),
				'atasnama' => $this->input->post('atasnama2'),				
				'cabang' => ucwords(strtolower($this->input->post('cabang2'))),
				'edit_by' => $this->session->userdata('userid'),
				'tgl_edit' => date("Y-m-d H:i:s")
			);
		if ($this->input->post('kodebank')<5){
			$datarek['idbank']='0';
			$datarek['bank']=$this->stokies_model->cek_nama_bank($this->input->post('kodebank'));
		}else{
			$datarek['kodebank']= $this->input->post('kodebank2');
			$datarek['bank']=$this->stokies_model->cek_nama_bank($this->input->post('kodebank2'));
			$datarek['idbank']='1';
		}
		
		// print_r($datarek);exit();
			$this->stokies_model->add_record_rekening($datarek);
			redirect("stokies/editor_rekening/$noidstokies/1/0/1");
		
		
	}
	function deletestokies($id,$cari=0,$uri="")
	{
		$this->stokies_model->deletestokies($id);
		if ($uri>=$this->stokies_model->count()){
			$uri=$uri-10;
			if ($uri<1){$uri="";}
		}
		if ($cari){
			redirect("stokies/cari/$uri");
		}else{
			redirect("stokies/index/$uri");
		}
	}	

	function cekid()
	{
		$noid=$this->input->post('no_id');
		if($query = $this->stokies_model->cek_members($noid)){
			$this->output->set_output($query->namamembers);
		}else{
			$this->output->set_output('no');
		}
	}
	function ceknamastokies()
	{
		$namastokies=str_replace(",", "",str_replace("'", "", str_replace("-", "", str_replace(".", "", str_replace(" ", "", trim($this->input->post('nm_s')))))));
		if($query = $this->stokies_model->cek_namas($namastokies)){
			$this->output->set_output('no');
		}else{
			$this->output->set_output('yes');
		}
	}
	function belanja($noidstokies,$page=0,$urlbef=0,$control=0,$bln=0,$thn=0)
	{	
		$this->session->set_userdata('page', 'Stokies - View Belanja');
		if($query = $this->stokies_model->cek_stokies($noidstokies)){
			$data['noidstokies'] = $noidstokies;
			$data['namastokies'] = $query->namastokies;
		}
		if ($bln && $thn){
			$bulan=$bln;
			$tahun=$thn;
			$periode=$tahun.$bulan;
		}else{
			if (isset($_POST['bulan'])){
				$bulan=$this->input->post('bulan');
				$tahun=$this->input->post('tahun');
				$periode=$tahun.$bulan;
			}else{
				$bulan=date("m");
				$tahun=date("y");
				$periode=$tahun.$bulan;
			}
		}
		$data['ajax_url'] = 'stokies/get_data_belanja/'.$noidstokies.'/'.$periode; 
		$data['control'] = $control;
		$data['page'] = $page;
		$data['urlbef'] = $urlbef;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['title'] = 'Stokies - View Belanja';
		
		$data['active'] =234;
		$data['breadcrum'] = array(array("Master","#","frames"),array("Stokies",'stokies',"point_right"),array("View Belanja",'#',"active"));		
		$data['template'] = 'stokies/belanja_view';
		// $data = array_merge($data,$this->infobar);
		// $this->parser->parse('index',$data); 
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_data_belanja($noid,$periode)
    {
        $query = $this->stokies_model->get_recordbelanja($noid,$periode);
        $this->output->set_output($query);
    }
	function penjualan($noidstokies,$page=0,$urlbef=0,$control=0,$bln=0,$thn=0)
	{	$this->session->set_userdata('page', 'Stokies - View Penjualan');
		if($query = $this->stokies_model->cek_stokies($noidstokies)){
			$data['noidstokies'] = $noidstokies;
			$data['namastokies'] = $query->namastokies;
		}
		if ($bln && $thn){
			$bulan=$bln;
			$tahun=$thn;
			$periode=$tahun.$bulan;
		}else{
			if (isset($_POST['bulan'])){
				$bulan=$this->input->post('bulan');
				$tahun=$this->input->post('tahun');
				$periode=$tahun.$bulan;
			}else{
				$bulan=date("m");
				$tahun=date("y");
				$periode=$tahun.$bulan;
			}
		}
		$data['control'] = $control;
		$data['page'] = $page;
		$data['urlbef'] = $urlbef;
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['ajax_url'] = 'stokies/get_data_penjualan/'.$noidstokies.'/'.$periode; 
		// $data['total'] = $this->stokies_model->countpenjualan($noidstokies,$periode);
		// if ($query=$this->stokies_model->get_recordpenjualan($this->uri->segment(9),10,$noidstokies,$periode)){
			// $data['records'] = $query;
			// $this->pagination->initialize(paging_admin($data['total'],"stokies/penjualan/$noidstokies/$page/$urlbef/$control/$bulan/$tahun",9,10));
			// $data['pagination'] =  $this->pagination->create_links();
		// }
			
		$data['active'] =235;
		$data['breadcrum'] = array(array("Master","#","frames"),array("Stokies",'stokies',"point_right"),array("View Penjualan",'#',"active"));		
		$data['template'] = 'stokies/penjualan_view';
		$data = array_merge($data,$this->infobar);
		$this->parser->parse('index',$data); 
	}
	function get_data_penjualan($noid,$periode)
    {
        $query = $this->stokies_model->get_recordpenjualan($noid,$periode);
        $this->output->set_output($query);
    }
	function exportpenjualan($noids,$periode) {
		if($query = $this->stokies_model->cek_stokies($noids)){
			$data['namastokies'] = $query->namastokies;
		}
		if($query = $this->stokies_model->get_recordpenjualanexp($noids,$periode))
		{	$data['records'] = $query;	
			$data['active']=235;
			$data['periode']=$periode;
			$this->load->view('stokies/exportpenjualan',$data);
		}
	}
	function stok($noidstokies,$page='index',$urlbef='')
	{	$this->session->set_userdata('page', 'Stokies - View Stok');
		if ($query=$this->stokies_model->get_liststokbarang($noidstokies,$this->uri->segment(6),20)){
			$data['records'] = $query;
			$this->pagination->initialize(paging_admin($this->stokies_model->countstok($noidstokies,$this->uri->segment(6),20),"stokies/stok/$page/$urlbef"));
			$data['pagination'] =  $this->pagination->create_links();
		}
		$data['page'] = $page;
		$data['urlbef'] = $urlbef;
		if($query = $this->stokies_model->cek_stokies($noidstokies)){
			$data['noidstokies'] = $noidstokies;
			$data['namastokies'] = $query->namastokies;
		}		
		$data['active'] =236;
		$data['breadcrum'] = array(array("Master","#","frames"),array("Stokies",'stokies',"point_right"),array("View Stok",'#',"active"));		
		$data['template'] = 'stokies/stok_view';	
/*		echo "<pre>";
		print_r($records);
		echo "</pre>";
*/		$data = array_merge($data,$this->infobar);
		$this->parser->parse('index',$data);
	} 
	function mapall()
	{	$this->session->set_userdata('page', 'Stokies - View Maps');
		$data['active'] =237;
		$data['breadcrum'] = array(array("Master","#","frames"),array("Stokies",'stokies',"point_right"),array("View Maps",'#',"active"));
		$data['template'] = 'stokies/mapall_view';
		$data = array_merge($data,$this->infobar);
		$this->parser->parse('index',$data); 
	}
	function mapalldet() {
		$data['stokiesmap'] = $this->stokies_model->get_mapstokies();
//		$data['invoice'] = $this->jual2stokies->get_invoice($nojual);
		$this->load->view("stokies/mapall_det",$data);
	}
	
	function setonline($noidstokies,$uri=0,$cari=0) {
//blm pake cek userid
		if($row=$this->stokies_model->get_stokiesdata($noidstokies)){
			$namastokies=$row->namastokies;
			$email=$row->email;
			$noidcabang=$row->noidcabang;
		
			$username="opr".$noidstokies."01";
			$pwd=substr(md5(date('dmY')),0,6);
			$nama="Opr1. ".$namastokies;
			$datastokies = array( 
				'online' => 1,
				'tglonline' => date("Y-m-d H:i:s")
				);
			$this->stokies_model->update_record($noidstokies,$datastokies);
			$datauser = array( 
				'username' => $username,
				'nama' => $nama,
				'noidstokies' => $noidstokies,
				'noidcabang' => $noidcabang,
				'user_level' => 32,
				'email' => $email,
				'passkey' => md5(sha1(md5($pwd))),
				'tglonline' => date("Y-m-d H:i:s")
				);
			$this->stokies_model->add_userrecord($datauser);
			$this->stokies_model->sendmailonline($email,$namastokies,$username,$pwd);
		}
		if ($uri){
			if ($cari){
				redirect("stokies/cari/$uri");
			}else{
				redirect("stokies/index/$uri");
		}}else{
			if ($cari){
				redirect("stokies/cari");
			}else{
			redirect("stokies");
		}}
	}
	function settest($uri=0) {
		for ($i=1;$i<10;$i++){
			sleep(1);	
		}
		
		if ($uri){
			redirect("stokies/index/$uri");	
		}else{
			redirect("stokies/index");
		}
	}
    
    function passkeynya()
    {
        echo "suksessss <br/>";
        // echo md5(sha1(md5(substr(md5(date('dmY')),0,6))));
        // echo "suksessss <br/>";
        // echo substr(md5(date('dmY')),0,6);
        exit();
    }
    
    function resetpassword($id,$cari=0,$uri="")
	{
		$this->stokies_model->resetpassword($id);
		if ($uri>=$this->stokies_model->count()){
			$uri=$uri-10;
			if ($uri<1){$uri="";}
		}
		if ($cari){
			redirect("stokies/cari/$uri");
		}else{
			redirect("stokies/index/$uri");
		}
	}
    
    function resend_lagi() {
        //blm pake cek userid
		if($row=$this->stokies_model->get_stokiesdata('1600101')){
			$namastokies=$row->namastokies;
			$email=$row->email;
			$username="opr160010101";
			//$pwd=substr(md5(date('dmY'),),0,6);
			$pwd=substr(md5(date('dmY',strtotime("2016-03-03"))),0,6);
			
			$this->stokies_model->sendmailonline($email,$namastokies,$username,$pwd);
            echo "suksessss";
            exit();
		}
	}
    function resend_lagi_today($noidstokies) {
        //blm pake cek userid
		if($row=$this->stokies_model->get_stokiesdata($noidstokies)){
			$namastokies=$row->namastokies;
			$email=$row->email;
			$username="opr".$noidstokies."01";
			//$pwd=substr(md5(date('dmY'),),0,6);
			$pwd=substr(md5(date('dmY')),0,6);
			// print_r($email);
			// print_r("<br/>");
			// print_r($namastokies);
            // print_r("<br/>");
			// print_r($username);
            // print_r("<br/>");
			// print_r($pwd);
            // exit();
			if($this->stokies_model->sendmailonline($email,$namastokies,$username,$pwd)){
                echo "suksessss";
            }else{
                echo "Gagal Maning Son!";
            }
            
            
            exit();
		}
	}
    
}