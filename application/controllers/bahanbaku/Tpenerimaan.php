<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Tpenerimaan extends CI_Controller {
private $table      = 'penerimaan';
private $label      = 'penerimaan';
private $folder     = 'bahanbaku';
public  $firstTitle = 'Bahan Baku';
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
            $this->load->model('Tbahanbaku_model','model');
            $this->load->model('Mstock_model','stock');
            $this->load->model('A_json_model','json');
    }
    
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

    function showingData()
    {
// $data array() for basic HTML
        if($_GET['tipe']=='lokal'&&empty($_GET['p'])){
            $link=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'];
            $param=array('page'=>'index','labelLink'=>'List','link'=>$link,'form_url'=>'');
                $data=$this->pageBahanbaku($param);
        }else
        if($_GET['tipe']=='import'&&empty($_GET['p'])){
            $link=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'];
            $param=array('page'=>'index','labelLink'=>'List','link'=>$link,'form_url'=>'');
                $data=$this->pageBahanbaku($param);
        }else
        if($_GET['tipe']=='lokal'&&$_GET['p']=='new-penerimaan'){
            $param['page']='manage';
            $param['form_url']=site_url().$this->folder.'/'.$this->label.'/proses?tipe='.$_GET['tipe'];
            $param['link']=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p='.$_GET['p'];
            $param['labelLink']='Add New';
                $dat=$this->pageBahanbaku($param);
                $datnull=$this->insertBaru();
        $data = array_merge($dat, $datnull);
        }else
        if($_GET['tipe']=='lokal'&&$_GET['p']=='edit-penerimaan'){
            $param['page']='manage';
            $param['form_url']=site_url().$this->folder.'/'.$this->label.'/proses?tipe='.$_GET['tipe'].'&id='.$_GET['id'];
            $param['link']=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p='.$_GET['p'].'&id='.$_GET['id'];
            $param['labelLink']='Edit';
                $dat=$this->pageBahanbaku($param);
                $data=$this->indexUpdate();
        $data = array_merge($dat, $data);
        }else
        if($_GET['tipe']=='import'&&$_GET['p']=='new-penerimaan'){
            $param['page']='manage';
            $param['form_url']=site_url().$this->folder.'/'.$this->label.'/proses?tipe='.$_GET['tipe'];
            $param['link']=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p='.$_GET['p'];
            $param['labelLink']='Add New';
                $dat=$this->pageBahanbaku($param);
                $datnull=$this->insertBaru();
        $data = array_merge($dat, $datnull);
        }else
        if($_GET['tipe']=='import'&&$_GET['p']=='edit-penerimaan'){
            $param['page']='manage';
            $param['form_url']=site_url().$this->folder.'/'.$this->label.'/proses?tipe='.$_GET['tipe'].'&id='.$_GET['id'];
            $param['link']=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p='.$_GET['p'].'&id='.$_GET['id'];
            $param['labelLink']='Edit';
                $dat=$this->pageBahanbaku($param);
                $data=$this->indexUpdate();
        $data = array_merge($dat, $data);
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function pageBahanbaku($param=array('page'=>'index','labelLink'=>'List','link'=>'','form_url'=>'')){
if(!empty($_GET['p'])&&$_GET['p']=='new-penerimaan'){
    $t=' New ';
}else
if(!empty($_GET['p'])&&$_GET['p']=='edit-penerimaan'){
    $t=' Edit ';
}else{
    $t=' ';
}
    $data = array();
    $data['title']        = $this->firstTitle.' -'.$t.$this->label.' '.$_GET['tipe'];
    $data['template']     = $this->folder.'/'.$this->label.'/'.$param['page'];
    $data['tJudul']       = ucfirst($this->label);
    $data['dJudul']       = $this->label;
    $data['url_index']    = site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'];
    $data['url_addnew']   = site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p=new-'.$this->label;
    $data['url_kedua']    = (!empty($_GET['p']))?$_GET['p']:'';
    $data['url_ajax']     = site_url().'ajax/'.$this->label.'?tipe='.$_GET['tipe'];
    $data['url_delete']   = site_url().'ajax/del'.ucfirst($this->label);
    $data['url_uStatus']  = site_url().'ajax/upStatus'.ucfirst($this->label);
    $data['url_proses']   = $param['form_url'];
    $data['breadcrum']    = array(
                            array($this->firstTitle,'#'),
                            array(ucfirst($this->label),'#'),
                            array($param['labelLink'],$param['link'])
                          );
    $data['json_supplier']     = site_url().'json/supplier';
    $lastperiode=$this->model->getItemsByLastPeriodeIN($_GET['tipe']);
    $data['Laper']          = (count($lastperiode)>0)?$lastperiode:array('periode_awal'=>'0000-00-00','periode_akhir'=>'0000-00-00');
    $data['json_sortimen']       = site_url().'json/sortimen';
    $data['json_datakayu']       = site_url().'json/datakayu';
    return $data;
}

    function insertBaru()
    {
// $data array() for value database
$data['newperiode_awal']='';
$data['newperiode_akhir']='';
$data['newclient_propinsi']='';
$data['newclient_kabupaten']='';
$data['newclient_statushutan']='';
$data['newclient_dokumen']='';
$data['newsertifikasi']='';
$data['newdoksatjumlah']='';
$data['newdoksatvolume']='';
$data['newdoksatberat']='';
$data['newfissatjumlah']='';
$data['newfissatvolume']='';
$data['newfissatberat']='';
$data['newdokjumlah']='';
$data['newdokvolume']='';
$data['newdokberat']='';
$data['newfisjumlah']='';
$data['newfisvolume']='';
$data['newfisberat']='';
$data['newsortimen']='';
$data['newsupplier']='';
$data['newdokumenkayu']='';
$data['newfisikkayu']='';
$data['newnegara']='';
return $data;
    }

    function indexUpdate()
    {
$get        =$this->model->showUpdateIN($_GET['tipe'],decryptURL($_GET['id']));
if(count($get)>0){
   if($get['type_penerimaan']=='lokal'){
$data['newclient_statushutan']  =$get['idhutan'].','.$get['hutan'];
$data['newsertifikasi']         =$get['idsertifikasi'].','.$get['sertifikasi'];
$data['newclient_dokumen']      =$get['iddokumen'].','.$get['dok'];
    if($get['idnegara']>0){
        $kab = rowArray('kit_kabupaten',array('kabid'=>$get['idnegara']));
        $prop = substr($kab['kode'],0,2).'00';
$getProv=rowArray('kit_propinsi',array('kode'=>$prop));
$data['newclient_propinsi']=$getProv['kode'].','.$getProv['nama'];
$data['newclient_kabupaten']=$kab['kabid'].','.$kab['nama'];
        }
    }else{
$getNeg=rowArray('kit_negara',array('idnegara'=>$get['idnegara']));
$data['newnegara']=$getNeg['idnegara'].','.$getNeg['negara'];
    }
// $data array() for value database
$data['newperiode_awal']        =encode_date($get['periode_awal']);
$data['newperiode_akhir']       =encode_date($get['periode_akhir']);
$data['newdokumenkayu']         =$get['datadokumen_kayu'].','.$get['dokumenkayu'];
$data['newdokjumlah']           =$get['datadokumen_jumlah'];
$data['newdoksatjumlah']        =$get['datadokumen_satuan_jumlah'].','.ucfirst($get['datadokumen_satuan_jumlah']);
$data['newdokvolume']           =$get['datadokumen_volume'];
$data['newdoksatvolume']        =$get['datadokumen_satuan_volume'].','.ucfirst($get['datadokumen_satuan_volume']);
$data['newdokberat']            =$get['datadokumen_berat'];
$data['newdoksatberat']         =$get['datadokumen_satuan_berat'].','.ucfirst($get['datadokumen_satuan_berat']);
$data['newfisikkayu']           =$get['datafisik_kayu'].','.$get['fisikkayu'];
$data['newfisjumlah']           =$get['datafisik_jumlah'];
$data['newfissatjumlah']        =$get['datafisik_satuan_jumlah'].','.ucfirst($get['datafisik_satuan_jumlah']);
$data['newfisvolume']           =$get['datafisik_volume'];
$data['newfissatvolume']        =$get['datafisik_satuan_volume'].','.ucfirst($get['datafisik_satuan_volume']);
$data['newfisberat']            =$get['datafisik_berat'];
$data['newfissatberat']         =$get['datafisik_satuan_berat'].','.ucfirst($get['datafisik_satuan_berat']);
$data['newsortimen']            =$get['idsortimen'].','.$get['sortimen'];
$data['newsupplier']            =$get['idsupplier'].','.$get['supplier'];
return $data;
    }else{
        redirect(site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe']);
        }
}

function saveNew(){
    // print_r($_GET['id']);exit();
if(!empty($_GET['id'])){
$update=$this->FupdateData();
    // if($update){
        $_SESSION['msg']='ToastrSukses("'.$this->label.' berhasil diubah","Info")';
        redirect(site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe']);
    // }
}else{
$insertID=$this->model->InUpItem();

        if($insertID){
            $dStock['idproduk']         = NULL;
            $dStock['idkayu']           = $this->input->post('new-fisikkayu');
            $dStock['idsortimen']       = $this->input->post('new-sortimen');
            $dStock['jenis']            = "B";
            
            $dStock['stokawal_jumlah']          = NULL;
            $dStock['stokawal_volume']          = NULL;
            $dStock['stokawal_berat']           = NULL;
            $dStock['satuan_stokawal_jumlah']   = NULL;
            $dStock['satuan_stokawal_volume']   = NULL;
            $dStock['satuan_stokawal_berat']    = NULL;
            $dStock['pemasukan_jumlah']         = $this->input->post('new-fisjumlah');
            $dStock['pemasukan_volume']         = $this->input->post('new-fisvolume');
            $dStock['pemasukan_berat']          = $this->input->post('new-fisberat');
            $dStock['satuan_pemasukan_jumlah']  = $this->input->post('new-fissatjumlah');
            $dStock['satuan_pemasukan_volume']  = $this->input->post('new-fissatvolume');
            $dStock['satuan_pemasukan_berat']   = $this->input->post('new-fissatberat');
            $dStock['pengeluaran_jumlah']       = NULL;
            $dStock['pengeluaran_volume']       = NULL;
            $dStock['pengeluaran_berat']        = NULL;
            $dStock['satuan_pengeluaran_jumlah']    = NULL;
            $dStock['satuan_pengeluaran_volume']    = NULL;
            $dStock['satuan_pengeluaran_berat']     = NULL;
            $dStock['stokakhir_jumlah']             = NULL;
            $dStock['stokakhir_volume']             = NULL;
            $dStock['stokakhir_berat']              = NULL;
            $dStock['satuan_stokakhir_jumlah']      = NULL;
            $dStock['satuan_stokakhir_volume']      = NULL;
            $dStock['satuan_stokakhir_berat']       = NULL;
            $dStock['jk']                           = NULL;
            $dStock['fk']                           = NULL;
            $dStock['client_id']        = $_SESSION['client_id'];
            $dStock['bulan']            = decode_date_m($this->input->post('new-periode_awal'));
            $dStock['tahun']            = decode_date_Y($this->input->post('new-periode_akhir'));
            $dStock['tanggal']          = date('Y-m-d');
            $dStock['periode_awal']     = decode_date($this->input->post('new-periode_awal'));
            $dStock['periode_akhir']    = decode_date($this->input->post('new-periode_akhir'));
            $dStock['contentid']        = 'm-'.$insertID;
        $insertStock=$this->stock->InUpItem($dStock);
            if($insertStock){
    $_SESSION['msg']='ToastrSukses("'.$this->label.' baru telah ditambahkan","Info")';
            redirect(site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe']);
            }else{
    $_SESSION['msg']='Toastr("Maaf, '.$this->label.' gagal ditambahkan","Info")';
            redirect(site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p=new-'.$this->label);
            }
        }
    }
}

function FupdateData(){
    // print_r($_GET['id']);exit();
$insertID=$this->model->InUpItem(array('idpenerimaan'=>decryptURL($_GET['id'])));
        if($insertID){
            $dStock['idproduk']         = NULL;
            $dStock['idkayu']           = $this->input->post('new-fisikkayu');
            $dStock['idsortimen']       = $this->input->post('new-sortimen');
            $dStock['jenis']            = "B";
            
            $dStock['stokawal_jumlah']          = NULL;
            $dStock['stokawal_volume']          = NULL;
            $dStock['stokawal_berat']           = NULL;
            $dStock['satuan_stokawal_jumlah']   = NULL;
            $dStock['satuan_stokawal_volume']   = NULL;
            $dStock['satuan_stokawal_berat']    = NULL;
            $dStock['pemasukan_jumlah']         = $this->input->post('new-fisjumlah');
            $dStock['pemasukan_volume']         = $this->input->post('new-fisvolume');
            $dStock['pemasukan_berat']          = $this->input->post('new-fisberat');
            $dStock['satuan_pemasukan_jumlah']  = $this->input->post('new-fissatjumlah');
            $dStock['satuan_pemasukan_volume']  = $this->input->post('new-fissatvolume');
            $dStock['satuan_pemasukan_berat']   = $this->input->post('new-fissatberat');
            $dStock['pengeluaran_jumlah']       = NULL;
            $dStock['pengeluaran_volume']       = NULL;
            $dStock['pengeluaran_berat']        = NULL;
            $dStock['satuan_pengeluaran_jumlah']    = NULL;
            $dStock['satuan_pengeluaran_volume']    = NULL;
            $dStock['satuan_pengeluaran_berat']     = NULL;
            $dStock['stokakhir_jumlah']             = NULL;
            $dStock['stokakhir_volume']             = NULL;
            $dStock['stokakhir_berat']              = NULL;
            $dStock['satuan_stokakhir_jumlah']      = NULL;
            $dStock['satuan_stokakhir_volume']      = NULL;
            $dStock['satuan_stokakhir_berat']       = NULL;
            $dStock['jk']                           = NULL;
            $dStock['fk']                           = NULL;
            $dStock['client_id']        = $_SESSION['client_id'];
            $dStock['bulan']            = decode_date_m($this->input->post('new-periode_awal'));
            $dStock['tahun']            = decode_date_Y($this->input->post('new-periode_awal'));
            $dStock['tanggal']          = date('Y-m-d');
            $dStock['periode_awal']     = decode_date($this->input->post('new-periode_awal'));
            $dStock['periode_akhir']    = decode_date($this->input->post('new-periode_akhir'));
            $dStock['contentid']        = 'm-'.decryptURL($_GET['id']);
            // print_r($dStock['contentid']);exit();
        $insertStock=$this->stock->InUpItem($dStock,array('contentid'=>$dStock['contentid']));
        if($insertStock){
$_SESSION['msg']='ToastrSukses("'.$this->label.' berhasil diubah","Info")';
        redirect(site_url().'setting/'.$this->field);
        }else{
$_SESSION['msg']='Toastr("Maaf, data '.$this->label.' gagal diubah","Info")';
        redirect(site_url().'setting/new-'.$this->field);
        }
}
}

public function getListDT(){
    $table      = $this->table; 
    $primaryKey = 'idpenerimaan';
    $sql_details = sql_connect();
if($_GET['tipe']=='lokal'){
$columns = array(
    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
    array('db' => 'concat(DATE_FORMAT(periode_awal, "%d %b %Y")," - ",DATE_FORMAT(periode_akhir, "%d %b %Y"))','as'=> 'periode', 'dt' => 1, 'field' => 'periode'),
    array('db' => 'supplier',  'dt' => 2, 'field' => 'supplier'),
    array('db' => 'sortimen',  'dt' => 3, 'field' => 'sortimen'),
    array('db' => '`t7`.nama','as'=> 'negara', 'dt' => 4, 'field' => 'negara'),
    array('db' => '`t8`.`nama`','as'=> 'dokumenkayu', 'dt' => 5, 'field' => 'dokumenkayu'),
    array('db' => 'sertifikasi', 'dt' => 6, 'field' => 'sertifikasi'),
    array('db' => 'concat(datadokumen_jumlah," ",datadokumen_satuan_jumlah)','as'=> 'datadokumen_jumlah', 'dt' => 7, 'field' => 'datadokumen_jumlah'),
    array('db' => 'concat(datadokumen_volume," ",datadokumen_satuan_volume)','as'=> 'datadokumen_volume', 'dt' => 8, 'field' => 'datadokumen_volume'),
    array('db' => 'concat(datadokumen_berat," ",datadokumen_satuan_berat)','as'=> 'datadokumen_berat', 'dt' => 9, 'field' => 'datadokumen_berat'),
    array('db' => 'concat(datafisik_jumlah," ",datafisik_satuan_jumlah)','as'=> 'datafisik_jumlah', 'dt' => 10, 'field' => 'datafisik_jumlah'),
    array('db' => 'concat(datafisik_volume," ",datafisik_satuan_volume)','as'=> 'datafisik_volume', 'dt' => 11, 'field' => 'datafisik_volume'),
    array('db' => 'concat(datafisik_berat," ",datafisik_satuan_berat)','as'=> 'datafisik_berat', 'dt' => 12, 'field' => 'datafisik_berat'),
   
    array('db' => $primaryKey,  'dt' => 13, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
return '<a href="'.site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p=edit-penerimaan&id='.encryptURL($d).'" class="btn btn-xs btn-info" title="Edit '.$this->label.'">
<i class="far fa-edit"></i>
</a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="'.encryptURL($d).'" title="Delete '.$this->label.'"><i class="far fa-trash-alt"></i></a>';
                   })
                );
}else{
$columns = array(
    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
    array('db' => 'concat(DATE_FORMAT(periode_awal, "%d %b %Y")," - ",DATE_FORMAT(periode_akhir, "%d %b %Y"))','as'=> 'periode', 'dt' => 1, 'field' => 'periode'),
    array('db' => 'supplier',  'dt' => 2, 'field' => 'supplier'),
    array('db' => 'sortimen',  'dt' => 3, 'field' => 'sortimen'),
    array('db' => 'negara', 'dt' => 4, 'field' => 'negara'),
    array('db' => 'concat(datadokumen_jumlah," ",datadokumen_satuan_jumlah)','as'=> 'datadokumen_jumlah', 'dt' => 5, 'field' => 'datadokumen_jumlah'),
    array('db' => 'concat(datadokumen_volume," ",datadokumen_satuan_volume)','as'=> 'datadokumen_volume', 'dt' => 6, 'field' => 'datadokumen_volume'),
    array('db' => 'concat(datadokumen_berat," ",datadokumen_satuan_berat)','as'=> 'datadokumen_berat', 'dt' => 7, 'field' => 'datadokumen_berat'),
    array('db' => 'concat(datafisik_jumlah," ",datafisik_satuan_jumlah)','as'=> 'datafisik_jumlah', 'dt' => 8, 'field' => 'datafisik_jumlah'),
    array('db' => 'concat(datafisik_volume," ",datafisik_satuan_volume)','as'=> 'datafisik_volume', 'dt' => 9, 'field' => 'datafisik_volume'),
    array('db' => 'concat(datafisik_berat," ",datafisik_satuan_berat)','as'=> 'datafisik_berat', 'dt' => 10, 'field' => 'datafisik_berat'),
   
    array('db' => $primaryKey,  'dt' => 11, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
return '<a href="'.site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p=edit-penerimaan&id='.encryptURL($d).'" class="btn btn-xs btn-info" title="Edit '.$this->label.'">
<i class="far fa-edit"></i>
</a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="'.encryptURL($d).'" title="Delete '.$this->label.'"><i class="far fa-trash-alt"></i></a>';
                   })
                );
}
    $joinQuery  = "FROM `$table` as `t1` ";
    $joinQuery .= "LEFT JOIN `kit_supplier` AS `t2` using(`idsupplier`) ";
    $joinQuery .= "LEFT JOIN `kit_hutan` AS `t3` using(`idhutan`) ";
    $joinQuery .= "LEFT JOIN `kit_sertifikasi` AS `t4` using(`idsertifikasi`) ";
    $joinQuery .= "LEFT JOIN `kit_sortimen` AS `t5` using(`idsortimen`) ";
    $joinQuery .= "LEFT JOIN `kit_jenisdokumen` AS `t6` ON (`t6`.`iddok` = `t1`.`iddokumen`) ";
        if($_GET['tipe']=='lokal'){
    $joinQuery .= "LEFT JOIN `kit_kabupaten` AS `t7` ON (`t7`.`kabid` = `t1`.`idnegara`) ";
        }else{
    $joinQuery .= "LEFT JOIN `kit_negara` AS `t7` ON (`t7`.`idnegara` = `t1`.`idnegara`) ";
        }
    $joinQuery .= "LEFT JOIN `kit_jeniskayu` AS `t8` ON (`t8`.`idkayu` = `t1`.`datadokumen_kayu`) ";
    $joinQuery .= "LEFT JOIN `kit_jeniskayu` AS `t9` ON (`t9`.`idkayu` = `t1`.`datafisik_kayu`) ";
    $extraWhere = "stdelete=1 and (type_penerimaan = '".$_GET['tipe']."' AND client_id = '".$_SESSION['client_id']."')";
    $groupBy    = "";
    $ordercus   = "ORDER BY `t1`.`idpenerimaan` DESC";
    $having     = "";
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
    );
}


// http://localhost/web/vlegalclient/ajax/delPenerimaan-lokal
function delAkun() {
$id =decryptURL($this->input->post("id")); 
$a  =$this->model->delAkun($id);
$b  =rowArray($this->table,array('idpenerimaan'=>$id));
// echo $b[''];
}
function gantiStatus() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->gantiStatus($id);
$bb = $this->model->dataStatus($id);
// $dataStatus_json = $bb;
echo stUser($bb);
}

}
