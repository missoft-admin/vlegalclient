<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Tpengeluaran extends CI_Controller {
private $table      = 'pengeluaran';
private $label      = 'pengeluaran';
private $folder     = 'bahanbaku';
public  $firstTitle = 'Bahan Baku';
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
            $this->load->model('Tbahanbaku_model','model');
            $this->load->model('Mstock_model','stock');
            $this->load->model('A_json_model','json');
    }
    
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

    function showingData()
    {
// $data array() for basic HTML
        if($_GET['tipe']=='lokal'&&empty($_GET['p'])){
            $link=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'];
            $param=array('page'=>'index','labelLink'=>'List','link'=>$link,'form_url'=>'');
                $data=$this->pageBahanbaku($param);
        }else
        if($_GET['tipe']=='import'&&empty($_GET['p'])){
            $link=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'];
            $param=array('page'=>'index','labelLink'=>'List','link'=>$link,'form_url'=>'');
                $data=$this->pageBahanbaku($param);
        }else
        if($_GET['tipe']=='lokal'&&$_GET['p']=='new-pengeluaran'){
            $param['page']='manage';
            $param['form_url']=site_url().$this->folder.'/'.$this->label.'/proses?tipe='.$_GET['tipe'];
            $param['link']=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p='.$_GET['p'];
            $param['labelLink']='Add New';
                $dat=$this->pageBahanbaku($param);
                $datnull=$this->insertBaru();
        $data = array_merge($dat, $datnull);
        }else
        if($_GET['tipe']=='lokal'&&$_GET['p']=='edit-pengeluaran'){
            $param['page']='manage';
            $param['form_url']=site_url().$this->folder.'/'.$this->label.'/proses?tipe='.$_GET['tipe'].'&id='.$_GET['id'];
            $param['link']=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p='.$_GET['p'].'&id='.$_GET['id'];
            $param['labelLink']='Edit';
                $dat=$this->pageBahanbaku($param);
                $data=$this->indexUpdate();
        $data = array_merge($dat, $data);
        }else
        if($_GET['tipe']=='import'&&$_GET['p']=='new-pengeluaran'){
            $param['page']='manage';
            $param['form_url']=site_url().$this->folder.'/'.$this->label.'/proses?tipe='.$_GET['tipe'];
            $param['link']=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p='.$_GET['p'];
            $param['labelLink']='Add New';
                $dat=$this->pageBahanbaku($param);
                $datnull=$this->insertBaru();
        $data = array_merge($dat, $datnull);
        }else
        if($_GET['tipe']=='import'&&$_GET['p']=='edit-pengeluaran'){
            $param['page']='manage';
            $param['form_url']=site_url().$this->folder.'/'.$this->label.'/proses?tipe='.$_GET['tipe'].'&id='.$_GET['id'];
            $param['link']=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p='.$_GET['p'].'&id='.$_GET['id'];
            $param['labelLink']='Edit';
                $dat=$this->pageBahanbaku($param);
                $data=$this->indexUpdate();
        $data = array_merge($dat, $data);
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function pageBahanbaku($param=array('page'=>'index','labelLink'=>'List','link'=>'','form_url'=>'')){
if(!empty($_GET['p'])&&$_GET['p']=='new-pengeluaran'){
    $t=' New ';
}else
if(!empty($_GET['p'])&&$_GET['p']=='edit-pengeluaran'){
    $t=' Edit ';
}else{
    $t=' ';
}
    $data = array();
    $data['title']        = $this->firstTitle.' -'.$t.$this->label.' '.$_GET['tipe'];
    $data['template']     = $this->folder.'/'.$this->label.'/'.$param['page'];
    $data['tJudul']       = ucfirst($this->label);
    $data['dJudul']       = $this->label;
    $data['url_index']    = site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'];
    $data['url_addnew']   = site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p=new-'.$this->label;
    $data['url_kedua']    = (!empty($_GET['p']))?$_GET['p']:'';
    $data['url_ajax']     = site_url().'ajax/'.$this->label.'?tipe='.$_GET['tipe'];
    $data['url_delete']   = site_url().'ajax/del'.ucfirst($this->label);
    $data['url_uStatus']  = site_url().'ajax/upStatus'.ucfirst($this->label);
    $data['url_proses']   = $param['form_url'];
    $data['breadcrum']    = array(
                            array($this->firstTitle,'#'),
                            array(ucfirst($this->label),'#'),
                            array($param['labelLink'],$param['link'])
                          );
    $data['json_supplier']  = site_url().'json/supplier';
    $lastperiode=$this->model->getItemsByLastPeriodeOUT($_GET['tipe']);
    $data['Laper']          = (count($lastperiode)>0)?$lastperiode:array('periode_awal'=>'0000-00-00','periode_akhir'=>'0000-00-00');
    // print_r($this->db->last_query());exit();
    $data['json_sortimen']  = site_url().'json/sortimen';
    $data['json_datakayu']  = site_url().'json/datakayu';
    return $data;
}

    function insertBaru()
    {
// $data array() for value database
$data['newperiode_awal']='';
$data['newperiode_akhir']='';
$data['newjumlah']='';
$data['newvolume']='';
$data['newberat']='';
$data['newsatjumlah']='';
$data['newsatvolume']='';
$data['newsatberat']='';
$data['newproduk']='';
$data['newlistkayu']='';
$data['newsortimen']='';
$data['newjenispengeluaran']='';
return $data;
    }

    function indexUpdate()
    {
$get        =$this->model->showUpdateOUT($_GET['tipe'],decryptURL($_GET['id']));
if(count($get)>0){

// $data array() for value database
$data['newperiode_awal']    =encode_date($get['periode_awal']);
$data['newperiode_akhir']   =encode_date($get['periode_akhir']);
$data['newproduk']          =$get['idproduk'].','.$get['produk'];
$data['newlistkayu']        =$get['idkayu'].','.$get['kayu'];;
$data['newjenispengeluaran']=$get['jenis_pengeluaran'].','.$get['pengeluaran'];;

$data['newsatjumlah']       =$get['satuan_jumlah'].','.ucfirst($get['satuan_jumlah']);;
$data['newsatvolume']       =$get['satuan_volume'].','.ucfirst($get['satuan_volume']);;
$data['newsatberat']        =$get['satuan_berat'].','.ucfirst($get['satuan_berat']);;
$data['newjumlah']          =$get['jumlah'];
$data['newvolume']          =$get['volume'];
$data['newberat']           =$get['berat'];
$data['newsortimen']        =$get['idsortimen'].','.$get['sortimen'];
return $data;
    }else{
        redirect(site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe']);
        }
}

function saveNew(){
    // print_r($_GET['id']);exit();
if(!empty($_GET['id'])){
$update=$this->FupdateData();
    // if($update){
        $_SESSION['msg']='ToastrSukses("'.$this->label.' berhasil diubah","Info")';
        redirect(site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe']);
    // }
}else{
$insertID=$this->model->InUpItemOUT();

        if($insertID){
            $dStock['idproduk']         = $this->input->post('new-produk');
            $dStock['idkayu']           = $this->input->post('new-listkayu');
            $dStock['idsortimen']       = $this->input->post('new-sortimen');
            $dStock['jenis']            = "B";

            $dStock['stokawal_jumlah']              = NULL;
            $dStock['stokawal_volume']              = NULL;
            $dStock['stokawal_berat']               = NULL;
            $dStock['satuan_stokawal_jumlah']       = NULL;
            $dStock['satuan_stokawal_volume']       = NULL;
            $dStock['satuan_stokawal_berat']        = NULL;
            $dStock['pemasukan_jumlah']             = NULL;
            $dStock['pemasukan_volume']             = NULL;
            $dStock['pemasukan_berat']              = NULL;
            $dStock['satuan_pemasukan_jumlah']      = NULL;
            $dStock['satuan_pemasukan_volume']      = NULL;
            $dStock['satuan_pemasukan_berat']       = NULL;
            $dStock['pengeluaran_jumlah']           = $this->input->post('new-jumlah');
            $dStock['pengeluaran_volume']           = $this->input->post('new-volume');
            $dStock['pengeluaran_berat']            = $this->input->post('new-berat');
            $dStock['satuan_pengeluaran_jumlah']    = $this->input->post('new-satjumlah');
            $dStock['satuan_pengeluaran_volume']    = $this->input->post('new-satvolume');
            $dStock['satuan_pengeluaran_berat']     = $this->input->post('new-satberat');

            $dStock['stokakhir_jumlah']             = NULL;
            $dStock['stokakhir_volume']             = NULL;
            $dStock['stokakhir_berat']              = NULL;
            $dStock['satuan_stokakhir_jumlah']      = NULL;
            $dStock['satuan_stokakhir_volume']      = NULL;
            $dStock['satuan_stokakhir_berat']       = NULL;
            $dStock['jk']                           = NULL;
            $dStock['fk']                           = NULL;
            $dStock['client_id']        = $_SESSION['client_id'];
            $dStock['bulan']            = decode_date_m($this->input->post('new-periode_awal'));
            $dStock['tahun']            = decode_date_Y($this->input->post('new-periode_akhir'));
            $dStock['tanggal']          = date('Y-m-d');
            $dStock['periode_awal']     = decode_date($this->input->post('new-periode_awal'));
            $dStock['periode_akhir']    = decode_date($this->input->post('new-periode_akhir'));
            $dStock['contentid']        = 'k-'.$insertID;
        $insertStock=$this->stock->InUpItem($dStock);
            if($insertStock){
    $_SESSION['msg']='ToastrSukses("'.$this->label.' baru telah ditambahkan","Info")';
            redirect(site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe']);
            }else{
    $_SESSION['msg']='Toastr("Maaf, '.$this->label.' gagal ditambahkan","Info")';
            redirect(site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p=new-'.$this->label);
            }
        }
    }
}

function FupdateData(){
    // print_r($_GET['id']);exit();
$insertID=$this->model->InUpItemOUT(array('idpengeluaran'=>decryptURL($_GET['id'])));
        if($insertID){
            $dStock['idproduk']         = $this->input->post('new-produk');
            $dStock['idkayu']           = $this->input->post('new-listkayu');
            $dStock['idsortimen']       = $this->input->post('new-sortimen');
            $dStock['jenis']            = "B";

            $dStock['stokawal_jumlah']              = NULL;
            $dStock['stokawal_volume']              = NULL;
            $dStock['stokawal_berat']               = NULL;
            $dStock['satuan_stokawal_jumlah']       = NULL;
            $dStock['satuan_stokawal_volume']       = NULL;
            $dStock['satuan_stokawal_berat']        = NULL;
            $dStock['pemasukan_jumlah']             = NULL;
            $dStock['pemasukan_volume']             = NULL;
            $dStock['pemasukan_berat']              = NULL;
            $dStock['satuan_pemasukan_jumlah']      = NULL;
            $dStock['satuan_pemasukan_volume']      = NULL;
            $dStock['satuan_pemasukan_berat']       = NULL;
            $dStock['pengeluaran_jumlah']           = $this->input->post('new-jumlah');
            $dStock['pengeluaran_volume']           = $this->input->post('new-volume');
            $dStock['pengeluaran_berat']            = $this->input->post('new-berat');
            $dStock['satuan_pengeluaran_jumlah']    = $this->input->post('new-satjumlah');
            $dStock['satuan_pengeluaran_volume']    = $this->input->post('new-satvolume');
            $dStock['satuan_pengeluaran_berat']     = $this->input->post('new-satberat');

            $dStock['stokakhir_jumlah']             = NULL;
            $dStock['stokakhir_volume']             = NULL;
            $dStock['stokakhir_berat']              = NULL;
            $dStock['satuan_stokakhir_jumlah']      = NULL;
            $dStock['satuan_stokakhir_volume']      = NULL;
            $dStock['satuan_stokakhir_berat']       = NULL;
            $dStock['jk']                           = NULL;
            $dStock['fk']                           = NULL;
            $dStock['client_id']        = $_SESSION['client_id'];
            $dStock['bulan']            = decode_date_m($this->input->post('new-periode_awal'));
            $dStock['tahun']            = decode_date_Y($this->input->post('new-periode_awal'));
            $dStock['tanggal']          = date('Y-m-d');
            $dStock['periode_awal']     = decode_date($this->input->post('new-periode_awal'));
            $dStock['periode_akhir']    = decode_date($this->input->post('new-periode_akhir'));
            $dStock['contentid']        = 'k-'.decryptURL($insertID);
            // print_r($dStock['contentid']);exit();
        $insertStock=$this->stock->InUpItem($dStock,array('contentid'=>$dStock['contentid']));
        if($insertStock){
$_SESSION['msg']='ToastrSukses("'.$this->label.' berhasil diubah","Info")';
        redirect(site_url().'setting/'.$this->field);
        }else{
$_SESSION['msg']='Toastr("Maaf, data '.$this->label.' gagal diubah","Info")';
        redirect(site_url().'setting/new-'.$this->field);
        }
}
}

public function getListDT(){
    $table      = $this->table; 
    $primaryKey = 'idpengeluaran';
    $sql_details = sql_connect();

$columns = array(
    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
    array('db' => 'concat(DATE_FORMAT(periode_awal, "%d %b %Y")," - ",DATE_FORMAT(periode_akhir, "%d %b %Y"))','as'=> 'periode', 'dt' => 1, 'field' => 'periode'),
    array('db' => '`t5`.`pengeluaran`',  'dt' => 2, 'field' => 'pengeluaran'),
    array('db' => '`t2`.`produk`',  'dt' => 3, 'field' => 'produk'),
    array('db' => '`t3`.`nama`','as'=> 'kayu', 'dt' => 4, 'field' => 'kayu'),
    array('db' => '`t4`.`sortimen`', 'dt' => 5, 'field' => 'sortimen'),
    array('db' => 'concat(jumlah," ",satuan_jumlah)','as'=> 'jumlah', 'dt' => 6, 'field' => 'jumlah'),
    array('db' => 'concat(volume," ",satuan_volume)','as'=> 'volume', 'dt' => 7, 'field' => 'volume'),
    array('db' => 'concat(berat," ",satuan_berat)','as'=> 'berat', 'dt' => 8, 'field' => 'berat'),
   
    array('db' => $primaryKey,  'dt' => 9, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
return '<a href="'.site_url().$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p=edit-pengeluaran&id='.encryptURL($d).'" class="btn btn-xs btn-info" title="Edit '.$this->label.'">
<i class="far fa-edit"></i>
</a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="'.encryptURL($d).'" title="Delete '.$this->label.'"><i class="far fa-trash-alt"></i></a>';
                   })
                );
    $joinQuery  = "FROM `$table` as `t1` ";
    $joinQuery .= "LEFT JOIN `kit_produk` as t2 using(`idproduk`) ";
    $joinQuery .= "LEFT JOIN `kit_jeniskayu` as t3 using(`idkayu`) ";
    $joinQuery .= "LEFT JOIN `kit_sortimen` AS t4 using(`idsortimen`) ";
    $joinQuery .= "LEFT JOIN `kit_jenispengeluaran` as t5 ON (`t5`.`idpeng` = `t1`.`jenis_pengeluaran`) ";
    $extraWhere = "stdelete=1 and (type_pengeluaran = '".$_GET['tipe']."' AND client_id = '".$_SESSION['client_id']."')";
    $groupBy    = "";
    $ordercus   = "ORDER BY `t1`.`idpengeluaran` DESC";
    $having     = "";
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
    );
}


// http://localhost/web/vlegalclient/ajax/delpengeluaran-lokal
function delAkun() {
$id =decryptURL($this->input->post("id")); 
$a  =$this->model->delAkun($id);
$b  =rowArray($this->table,array('idpengeluaran'=>$id));
// echo $b[''];
}
function gantiStatus() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->gantiStatus($id);
$bb = $this->model->dataStatus($id);
// $dataStatus_json = $bb;
echo stUser($bb);
}

}
