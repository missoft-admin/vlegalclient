<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Tlainlain extends CI_Controller {
private $tblLainlain   = 'pengeluaran_lain';
public  $label      = 'Lain-lain';
public  $link       = 'lain-lain';
public  $folder     = 'Tlainlain';
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
            $this->load->model($this->folder.'_model','model');
            $this->load->model('Mstock_model','stock');
    }
	
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

	function showingData()
    {
        $data = array();
        $data['title']      = 'Pengeluaran '.$this->label;
        $data['template']   = $this->folder.'/index';
        $data['tJudul']     = ucfirst($this->label);
        $data['dJudul']     = $this->label;
        $data['breadcrum']  = array(
                                array('Produk','#'),
                                array('penjualan','#'),
                                array($this->label,'produk/'.$this->link)
                              );
        $data['url_ajax']       = site_url().'ajax/'.$this->link;
        $data['url_insert']     = site_url().'produk/'.$this->link.'/proses';
        $data['getData_edit']   = site_url().'produk/'.$this->link.'/edit';
        $data['url_delete']     = site_url().'ajax/del'.ucfirst($this->link);
        $data['url_periode']    = site_url().'ajax/periode-'.$this->link;
        $data['Laper']['periode_awal']      = '';
        $data['Laper']['periode_akhir']     = '';
$data['newjumlah']='';
$data['newvolume']='';
$data['newberat'] ='';
$data['newsatjumlah']='';
$data['newsatvolume']='';
$data['newsatberat'] ='';
$data['newproduk'] ='';
$data['newlistkayu'] ='';

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function saveNew(){
parse_str($this->input->post('data'), $post);
if(!empty($post['hide-ID'])){
    $this->FupdateData();
}else{
    if(!empty($post['new-produk']) && !empty($post['new-listkayu']) && !empty($post['new-jumlah']) && !empty($post['new-satjumlah']) && !empty($post['new-volume'])&& !empty($post['new-satvolume'])&& !empty($post['new-berat'])&& !empty($post['new-satberat'])){
    $insertID=$this->model->InUpItem();
        if($insertID){
        $dStock['idproduk']     = $post['new-produk'];
        $dStock['idkayu']       = $post['new-listkayu'];
        $dStock['idsortimen']   = NULL;
        $dStock['jenis']        = "P";

        $dStock['stokawal_jumlah']              = NULL;
        $dStock['satuan_stokawal_jumlah']       = NULL;
        $dStock['stokawal_volume']              = NULL;
        $dStock['satuan_stokawal_volume']       = NULL;
        $dStock['stokawal_berat']               = NULL;
        $dStock['satuan_stokawal_berat']        = NULL;
        $dStock['stokakhir_jumlah']             = NULL;
        $dStock['stokakhir_volume']             = NULL;
        $dStock['stokakhir_berat']              = NULL;
        $dStock['satuan_stokakhir_jumlah']      = NULL;
        $dStock['satuan_stokakhir_volume']      = NULL;
        $dStock['satuan_stokakhir_berat']       = NULL;

        $dStock['pemasukan_jumlah']             = NULL;
        $dStock['satuan_pemasukan_jumlah']      = NULL;
        $dStock['pemasukan_volume']             = NULL;
        $dStock['satuan_pemasukan_volume']      = NULL;
        $dStock['pemasukan_berat']              = NULL;
        $dStock['satuan_pemasukan_berat']       = NULL;
        $dStock['pengeluaran_jumlah']           = $post['new-jumlah'];
        $dStock['satuan_pengeluaran_jumlah']    = $post['new-satjumlah'];
        $dStock['pengeluaran_volume']           = $post['new-volume'];
        $dStock['satuan_pengeluaran_volume']    = $post['new-satvolume'];
        $dStock['pengeluaran_berat']            = $post['new-berat'];
        $dStock['satuan_pengeluaran_berat']     = $post['new-satberat'];

        $dStock['jk']                           = NULL;
        $dStock['fk']                           = NULL;
        $dStock['client_id']      = $_SESSION['client_id'];
        $dStock['bulan']          = decode_date_m($post['new-periode_awal']);
        $dStock['tahun']          = decode_date_Y($post['new-periode_awal']);
        $dStock['tanggal']        = date('Y-m-d');
        $dStock['periode_awal']   = decode_date($post['new-periode_awal']);
        $dStock['periode_akhir']  = decode_date($post['new-periode_akhir']);
        $dStock['contentid']      = 'l-'.$insertID;
            $this->stock->InUpItem($dStock);
    echo 'ToastrSukses("'.$this->label.' baru telah ditambahkan.","Info")';
}else{
    echo 'Toastr("Maaf, Data gagal ditambahkan.","Info")';
    }
}else{
    echo 'Toastr("Maaf, Data tidak boleh ada yang kosong.","Info")';
    }
}
}

function FupdateData(){
parse_str($this->input->post('data'), $post);
$where =array('idlain' =>decryptURL($post['hide-ID']));
// print_r(decryptURL($post['hide-ID']));exit();
    if(!empty($post['new-produk']) && !empty($post['new-listkayu']) && !empty($post['new-jumlah']) && !empty($post['new-satjumlah']) && !empty($post['new-volume'])&& !empty($post['new-satvolume'])&& !empty($post['new-berat'])&& !empty($post['new-satberat'])){
    if($this->model->InUpItem($where)){
            $dStock['idproduk']     = $post['new-produk'];
            $dStock['idkayu']       = $post['new-listkayu'];
            $dStock['idsortimen']   = NULL;
            $dStock['jenis']        = "P";
    
            $dStock['stokawal_jumlah']              = NULL;
            $dStock['satuan_stokawal_jumlah']       = NULL;
            $dStock['stokawal_volume']              = NULL;
            $dStock['satuan_stokawal_volume']       = NULL;
            $dStock['stokawal_berat']               = NULL;
            $dStock['satuan_stokawal_berat']        = NULL;
            $dStock['stokakhir_jumlah']             = NULL;
            $dStock['stokakhir_volume']             = NULL;
            $dStock['stokakhir_berat']              = NULL;
            $dStock['satuan_stokakhir_jumlah']      = NULL;
            $dStock['satuan_stokakhir_volume']      = NULL;
            $dStock['satuan_stokakhir_berat']       = NULL;
    
            $dStock['pemasukan_jumlah']             = NULL;
            $dStock['satuan_pemasukan_jumlah']      = NULL;
            $dStock['pemasukan_volume']             = NULL;
            $dStock['satuan_pemasukan_volume']      = NULL;
            $dStock['pemasukan_berat']              = NULL;
            $dStock['satuan_pemasukan_berat']       = NULL;
            $dStock['pengeluaran_jumlah']           = $post['new-jumlah'];
            $dStock['satuan_pengeluaran_jumlah']    = $post['new-satjumlah'];
            $dStock['pengeluaran_volume']           = $post['new-volume'];
            $dStock['satuan_pengeluaran_volume']    = $post['new-satvolume'];
            $dStock['pengeluaran_berat']            = $post['new-berat'];
            $dStock['satuan_pengeluaran_berat']     = $post['new-satberat'];
    
            $dStock['jk']                           = NULL;
            $dStock['fk']                           = NULL;
            $dStock['bulan']          = decode_date_m($post['new-periode_awal']);
            $dStock['tahun']          = decode_date_Y($post['new-periode_awal']);
            $dStock['tanggal']        = date('Y-m-d');
            $dStock['client_id']      = $_SESSION['client_id'];
            $dStock['periode_awal']   = decode_date($post['new-periode_awal']);
            $dStock['periode_akhir']  = decode_date($post['new-periode_akhir']);
            $dStock['contentid']      = 'l-'.decryptURL($post['hide-ID']);
               if($this->stock->InUpItem($dStock,array('contentid'=>$dStock['contentid']))){
    echo 'ToastrSukses("'.$this->label.' berhasil diedit.","Info")';
    }
   }
    }else{
    echo 'Toastr("Maaf, Data tidak boleh ada yang kosong.","Info")';
    }
}

public function getListDT(){
    $tblLainlain       = $this->tblLainlain;
    $primaryKey     = 'idlain';
    $sql_details    = sql_connect();

$columns = array(
    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
    array('db' => 'concat(DATE_FORMAT(periode_awal, "%d %b %Y")," - ",DATE_FORMAT(periode_akhir, "%d %b %Y"))','as'=> 'periode', 'dt' => 1, 'field' => 'periode'),
    array('db' => 'concat(t3.produk," [",t3.kodehs,"]")','as'=> 'produk', 'dt' => 2, 'field' => 'produk'),
    array('db' => '`t2`.`nama`','as'=> 'kayu', 'dt' => 3, 'field' => 'kayu'),
    array('db' => 'concat(jumlah," ",satuan_jumlah)','as'=> 'jumlah', 'dt' => 4, 'field' => 'jumlah'),
    array('db' => 'concat(volume," ",satuan_volume)','as'=> 'volume', 'dt' => 5, 'field' => 'volume'),
    array('db' => 'concat(berat," ",satuan_berat)','as'=> 'berat', 'dt' => 6, 'field' => 'berat'),
    array('db' => 'keterangan', 'dt' => 7, 'field' => 'keterangan'),
    array('db' => $primaryKey, 'dt' => 8, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
        return '<a href="javascript:void(0)" class="btn btn-xs btn-info edit-row" data-periode="'.$row['periode'].'" data-id="'.encryptURL($d).'" title="Edit '.$this->label.'">
        <i class="far fa-edit"></i>
        </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="'.encryptURL($d).'" title="Delete '.$this->label.'"><i class="far fa-trash-alt"></i></a>';
                           }),
        );
    $joinQuery  = "from `$tblLainlain` as `t1` ";
    $joinQuery .= "left join `kit_jeniskayu` as `t2` using(`idkayu`) ";
    $joinQuery .= "LEFT JOIN `kit_produk` t3 using(`idproduk`) ";
    $extraWhere = "stdelete=1 and (client_id=".$_SESSION['client_id'].")";
    $groupBy    = "";
    $ordercus   = "ORDER BY periode_awal DESC ";
    $having     = "";
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $tblLainlain, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
    );
}



function getLastPeriode(){
    echo json_encode($this->model->getByLastPeriode());
}

function getData4Update(){
    echo json_encode($this->model->getDataList(decryptURL($_POST['id'])));
}

function delAkun() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->delAkun($id);
$b=rowArray($this->tblLainlain,array('idlain'=>$id));
$c=rowArray('kit_produk',array('idproduk'=>$b['idproduk']));
echo $c['produk'];
}

}
