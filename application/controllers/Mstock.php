<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Mstock extends CI_Controller {
private $tblStock   = 'stok'; 
private $tbljson    = 'kit_negara';
public  $label      = 'stock';
public  $folder     = 'Mstock';
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
            $this->load->model($this->folder.'_model','model');
    }
	
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

	function showingData()
    {
$filenya=($this->input->get('jenis')=='produk-jadi')?'produk-jadi':'index';
        $data = array();
        $data['title']      = 'Stock - '.str_replace('-', ' ', $_GET['jenis']);
        $data['template']   = $this->folder.'/'.$filenya;
        $data['tJudul']     = ucfirst($this->label);
        $data['dJudul']     = $this->label;
        $data['breadcrum']  = array(
                                array(ucfirst($this->label),'#'),
                                array($_GET['jenis'],'stock/produk?jenis='.$_GET['jenis'])
                              );
        $data['url_ajax']       = site_url().'ajax/stock-produk?jenis='.$_GET['jenis'];
        $data['url_insert']     = site_url().'stock/produk/proses?jenis='.$_GET['jenis'];
        $data['get_4update']    = site_url().'stock/produk/edit?jenis='.$_GET['jenis'];
        $data['url_delete']     = site_url().'ajax/delStock';
        $data['Laper']['periode_awal']      = '';
        $data['Laper']['periode_akhir']     = '';

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function saveNew(){
parse_str($this->input->post('data'), $post);
if(!empty($post['hide-ID'])){
    $this->FupdateData();
}else{
$dStock['periode_awal']     = decode_date($post['new-periode_awal']);
$dStock['periode_akhir']    = decode_date($post['new-periode_akhir']);

    if($_GET['jenis']=='bahan-baku'){
        $dStock['idproduk']         = NULL;
        $dStock['idkayu']           = (!isset($post['new-listkayu']))?'':$post['new-listkayu'];
        $dStock['idsortimen']       = (!isset($post['new-sortimen']))?'':$post['new-sortimen'];
        $dStock['jenis']            = "B";
    }else
    if($_GET['jenis']=='wip'){
        $dStock['idproduk']         = (!isset($post['new-produk']))?'':$post['new-produk'];
        $dStock['idkayu']           = (!isset($post['new-wip']))?'':$post['new-wip'];
        $dStock['idsortimen']       = NULL;
        $dStock['jenis']            = "W";
    }else
    if($_GET['jenis']=='produk-jadi'){
        $dStock['idproduk']         = (!isset($post['new-produk']))?'':$post['new-produk'];
        $dStock['idkayu']           = (!isset($post['new-listkayu']))?'':$post['new-listkayu'];
        $dStock['idsortimen']       = NULL;
        $dStock['jenis']            = "P";
    }
    if($post['stocknya']=='stockawal'){
        $dStock['stokawal_jumlah']              = (!isset($post['new-jumlah']))?'':$post['new-jumlah'];
        $dStock['satuan_stokawal_jumlah']       = (!isset($post['new-satjumlah']))?'':$post['new-satjumlah'];
        $dStock['stokawal_volume']              = (!isset($post['new-volume']))?'':$post['new-volume'];
        $dStock['satuan_stokawal_volume']       = (!isset($post['new-satvolume']))?'':$post['new-satvolume'];
        $dStock['stokawal_berat']               = (!isset($post['new-berat']))?'':$post['new-berat'];
        $dStock['satuan_stokawal_berat']        = (!isset($post['new-satberat']))?'':$post['new-satberat'];
    }else{
    $stockawal=$this->model->getStokWIP($dStock,'W');
$dStock['stokawal_jumlah']        = (!empty($stockawal['stokakhir_jumlah']))?$stockawal['stokakhir_jumlah']:$stockawal['stokawal_jumlah'];
$dStock['satuan_stokawal_jumlah'] = (!empty($stockawal['stokakhir_volume']))?$stockawal['stokakhir_volume']:$stockawal['stokawal_volume'];
$dStock['stokawal_volume']        = (!empty($stockawal['stokakhir_berat']))?$stockawal['stokakhir_berat']:$stockawal['stokawal_berat'];
$dStock['satuan_stokawal_volume'] = (!empty($stockawal['satuan_stokakhir_jumlah']))?$stockawal['satuan_stokakhir_jumlah']:$stockawal['satuan_stokawal_jumlah'];
$dStock['stokawal_berat']         = (!empty($stockawal['satuan_stokakhir_volume']))?$stockawal['satuan_stokakhir_volume']:$stockawal['satuan_stokawal_volume'];
$dStock['satuan_stokawal_berat']  = (!empty($stockawal['satuan_stokakhir_berat']))?$stockawal['satuan_stokakhir_berat']:$stockawal['satuan_stokawal_berat'];
    }
        $dStock['pemasukan_jumlah']             = NULL;
        $dStock['pemasukan_volume']             = NULL;
        $dStock['pemasukan_berat']              = NULL;
        $dStock['satuan_pemasukan_jumlah']      = NULL;
        $dStock['satuan_pemasukan_volume']      = NULL;
        $dStock['satuan_pemasukan_berat']       = NULL;
        $dStock['pengeluaran_jumlah']           = NULL;
        $dStock['pengeluaran_volume']           = NULL;
        $dStock['pengeluaran_berat']            = NULL;
        $dStock['satuan_pengeluaran_jumlah']    = NULL;
        $dStock['satuan_pengeluaran_volume']    = NULL;
        $dStock['satuan_pengeluaran_berat']     = NULL;

        $dStock['stokakhir_jumlah']             = (!isset($post['new-jumlah_akhir']))?'':$post['new-jumlah_akhir'];
        $dStock['satuan_stokakhir_jumlah']      = (!isset($post['new-satjumlah_akhir']))?'':$post['new-satjumlah_akhir'];
        $dStock['stokakhir_volume']             = (!isset($post['new-volume_akhir']))?'':$post['new-volume_akhir'];
        $dStock['satuan_stokakhir_volume']      = (!isset($post['new-satvolume_akhir']))?'':$post['new-satvolume_akhir'];
        $dStock['stokakhir_berat']              = (!isset($post['new-berat_akhir']))?'':$post['new-berat_akhir'];
        $dStock['satuan_stokakhir_berat']       = (!isset($post['new-satberat_akhir']))?'':$post['new-satberat_akhir'];
        $dStock['jk']                           = NULL;
        $dStock['fk']                           = NULL;
        $dStock['client_id']        = $_SESSION['client_id'];
        $dStock['bulan']            = date('m');
        $dStock['tahun']            = date('Y');
        $dStock['tanggal']          = date('Y-m-d');
        $dStock['contentid']        = NULL;
    $insertStock=$this->model->InUpItem($dStock);
    if($insertStock){
echo 'ToastrSukses("'.ucfirst($this->label).' baru telah ditambahkan.","Info")';
    }else{
echo 'Toastr("Maaf, Jenis Sortimen dan Jenis Kayu tidak boleh kosong.","Info")';
    }
}
}

function FupdateData(){
parse_str($this->input->post('data'), $post);
// exit($this->input->post('data'));
$where =array('idstok' =>decryptURL($post['hide-ID']));
    if($_GET['jenis']=='bahan-baku'){
        $dStock['idproduk']         = NULL;
        $dStock['idkayu']           = (!isset($post['new-listkayu']))?'':$post['new-listkayu'];
        $dStock['idsortimen']       = (!isset($post['new-sortimen']))?'':$post['new-sortimen'];
        $dStock['jenis']            = "B";
    }else
    if($_GET['jenis']=='wip'){
        $dStock['idproduk']         = (!isset($post['new-produk']))?'':$post['new-produk'];
        $dStock['idkayu']           = (!isset($post['new-wip']))?'':$post['new-wip'];
        $dStock['idsortimen']       = NULL;
        $dStock['jenis']            = "W";
    }else
    if($_GET['jenis']=='produk-jadi'){
        $dStock['idproduk']         = (!isset($post['new-produk']))?'':$post['new-produk'];
        $dStock['idkayu']           = (!isset($post['new-listkayu']))?'':$post['new-listkayu'];
        $dStock['idsortimen']       = NULL;
        $dStock['jenis']            = "P";        
    }

    if($post['stocknya']=='stockawal'){
        $dStock['stokawal_jumlah']              = $post['new-jumlah'];
        $dStock['satuan_stokawal_jumlah']       = $post['new-satjumlah'];
        $dStock['stokawal_volume']              = $post['new-volume'];
        $dStock['satuan_stokawal_volume']       = $post['new-satvolume'];
        $dStock['stokawal_berat']               = $post['new-berat'];
        $dStock['satuan_stokawal_berat']        = $post['new-satberat'];
        $dStock['stokakhir_jumlah']             = NULL;
        $dStock['stokakhir_volume']             = NULL;
        $dStock['stokakhir_berat']              = NULL;
        $dStock['satuan_stokakhir_jumlah']      = NULL;
        $dStock['satuan_stokakhir_volume']      = NULL;
        $dStock['satuan_stokakhir_berat']       = NULL;
    }else{
        $dStock['stokawal_jumlah']              = NULL;
        $dStock['satuan_stokawal_jumlah']       = NULL;
        $dStock['stokawal_volume']              = NULL;
        $dStock['satuan_stokawal_volume']       = NULL;
        $dStock['stokawal_berat']               = NULL;
        $dStock['satuan_stokawal_berat']        = NULL;
        $dStock['stokakhir_jumlah']             = $post['new-jumlah_akhir'];
        $dStock['satuan_stokakhir_jumlah']      = $post['new-satjumlah_akhir'];
        $dStock['stokakhir_volume']             = $post['new-volume_akhir'];
        $dStock['satuan_stokakhir_volume']      = $post['new-satvolume_akhir'];
        $dStock['stokakhir_berat']              = $post['new-berat_akhir'];
        $dStock['satuan_stokakhir_berat']       = $post['new-satberat_akhir'];
    }
        $dStock['pemasukan_jumlah']             = NULL;
        $dStock['pemasukan_volume']             = NULL;
        $dStock['pemasukan_berat']              = NULL;
        $dStock['satuan_pemasukan_jumlah']      = NULL;
        $dStock['satuan_pemasukan_volume']      = NULL;
        $dStock['satuan_pemasukan_berat']       = NULL;
        $dStock['pengeluaran_jumlah']           = NULL;
        $dStock['pengeluaran_volume']           = NULL;
        $dStock['pengeluaran_berat']            = NULL;
        $dStock['satuan_pengeluaran_jumlah']    = NULL;
        $dStock['satuan_pengeluaran_volume']    = NULL;
        $dStock['satuan_pengeluaran_berat']     = NULL;

        $dStock['jk']                           = NULL;
        $dStock['fk']                           = NULL;
        $dStock['bulan']            = '';
        $dStock['tahun']            = '';
        $dStock['tanggal']          = '';
        $dStock['client_id']        = $_SESSION['client_id'];
        $dStock['periode_awal']     = decode_date($post['new-periode_awal']);
        $dStock['periode_akhir']    = decode_date($post['new-periode_akhir']);
        $dStock['contentid']        = NULL;
    $insertStock=$this->model->InUpItem($dStock,$where);
    if($insertStock){
echo 'ToastrSukses("'.ucfirst($this->label).' berhasil diedit.","Info")';
    }else{
echo 'Toastr("Maaf, Jenis Sortimen dan Jenis Kayu tidak boleh kosong.","Info")';
    }
}

public function getListDT(){
    $tblStock       = $this->tblStock;
    $primaryKey     = 'idstok';
    $sql_details    = sql_connect();
switch ($_GET['jenis']) {
    case 'bahan-baku':  $jenis='B';
    $columns = array(
        array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
        array('db' => 'idkayu', 'dt' => 0, 'field' => 'idkayu'),
        array('db' => 'idsortimen', 'dt' => 0, 'field' => 'idsortimen'),
        array('db' => 'concat(DATE_FORMAT(periode_awal, "%d-%m-%Y")," ",DATE_FORMAT(periode_akhir, "%d-%m-%Y"))','as'=>'periode', 'dt' => 0, 'field' => 'periode'),
        array('db' => 'sortimen', 'dt' => 1, 'field' => 'sortimen'),
        array('db' => '`t2`.`nama`','as'=>'kayu', 'dt' => 2, 'field' => 'kayu'),
        array('db' => 'concat(stokawal_volume," ",satuan_stokawal_volume)','as'=>'stokawal_volume', 'dt' => 3, 'field' => 'stokawal_volume'),
        array('db' => 'concat(stokawal_berat," ",satuan_stokawal_berat)','as'=>'stokawal_berat', 'dt' => 4, 'field' => 'stokawal_berat'),
        array('db' => 'concat(stokawal_jumlah," ",satuan_stokawal_jumlah)','as'=>'stokawal_jumlah', 'dt' => 5, 'field' => 'stokawal_jumlah'),
        array('db' => 'concat(pemasukan_volume," ",satuan_pemasukan_volume)','as'=>'pemasukan_volume', 'dt' => 6, 'field' => 'pemasukan_volume'),
        array('db' => 'concat(pemasukan_berat," ",satuan_pemasukan_berat)','as'=>'pemasukan_berat', 'dt' => 7, 'field' => 'pemasukan_berat'),
        array('db' => 'concat(pemasukan_jumlah," ",satuan_pemasukan_jumlah)','as'=>'pemasukan_jumlah', 'dt' => 8, 'field' => 'pemasukan_jumlah'),
        array('db' => 'concat(pengeluaran_volume," ",satuan_pengeluaran_volume)','as'=>'pengeluaran_volume', 'dt' => 9, 'field' => 'pengeluaran_volume'),
        array('db' => 'concat(pengeluaran_berat," ",satuan_pengeluaran_berat)','as'=>'pengeluaran_berat', 'dt' => 10, 'field' => 'pengeluaran_berat'),
        array('db' => 'concat(pengeluaran_jumlah," ",satuan_pengeluaran_jumlah)','as'=>'pengeluaran_jumlah', 'dt' => 11, 'field' => 'pengeluaran_jumlah'),
        array('db' => 'concat(stokakhir_volume," ",satuan_stokakhir_volume)','as'=>'stokakhir_volume', 'dt' => 12, 'field' => 'stokakhir_volume'),
        array('db' => 'concat(stokakhir_berat," ",satuan_stokakhir_berat)','as'=>'stokakhir_berat', 'dt' => 13, 'field' => 'stokakhir_berat'),
        array('db' => 'concat(stokakhir_jumlah," ",satuan_stokakhir_jumlah)','as'=>'stokakhir_jumlah', 'dt' => 14, 'field' => 'stokakhir_jumlah'),
        array('db' => $primaryKey, 'dt' => 15, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
    if(!empty($row['stokawal_volume']) || !empty($row['stokawal_berat']) || !empty($row['stokawal_jumlah'])){
            return '<a href="javascript:void(0)" class="btn btn-xs btn-info edit-row" data-id="'.encryptURL($d).'" title="Edit '.ucfirst($this->label).'">
            <i class="far fa-edit"></i>
            </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="'.encryptURL($d).'" title="Delete '.ucfirst($this->label).'"><i class="far fa-trash-alt"></i></a>';
        }
           })
        );
    $joinQuery  = "from `$tblStock` as `t1` ";
    $joinQuery .= "left join `kit_jeniskayu` as `t2` using(`idkayu`) ";
    $joinQuery .= "left join `kit_sortimen` as `t3` using(`idsortimen`) ";
$filter='';
     break;
    case 'wip':         $jenis='W';
    $columns = array(
        array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
        array('db' => 'concat(DATE_FORMAT(periode_awal, "%d-%m-%Y")," ",DATE_FORMAT(periode_akhir, "%d-%m-%Y"))','as'=>'periode', 'dt' => 0, 'field' => 'periode'),
        array('db' => 'wip', 'dt' => 1, 'field' => 'wip'),
        array('db' => 'produk', 'dt' => 2, 'field' => 'produk'),
        array('db' => 'kodehs', 'dt' => 3, 'field' => 'kodehs'),
        array('db' => 'concat(stokawal_volume," ",satuan_stokawal_volume)','as'=>'stokawal_volume', 'dt' => 4, 'field' => 'stokawal_volume'),
        array('db' => 'concat(stokawal_berat," ",satuan_stokawal_berat)','as'=>'stokawal_berat', 'dt' => 5, 'field' => 'stokawal_berat'),
        array('db' => 'concat(stokawal_jumlah," ",satuan_stokawal_jumlah)','as'=>'stokawal_jumlah', 'dt' => 6, 'field' => 'stokawal_jumlah'),
        array('db' => 'concat(stokakhir_volume," ",satuan_stokakhir_volume)','as'=>'stokakhir_volume', 'dt' => 7, 'field' => 'stokakhir_volume'),
        array('db' => 'concat(stokakhir_berat," ",satuan_stokakhir_berat)','as'=>'stokakhir_berat', 'dt' => 8, 'field' => 'stokakhir_berat'),
        array('db' => 'concat(stokakhir_jumlah," ",satuan_stokakhir_jumlah)','as'=>'stokakhir_jumlah', 'dt' => 9, 'field' => 'stokakhir_jumlah'),
        array('db' => $primaryKey, 'dt' => 10, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
    // if(!empty($row['stokakhir_volume']) || !empty($row['stokakhir_berat']) || !empty($row['stokakhir_jumlah'])){
            return '<a href="javascript:void(0)" class="btn btn-xs btn-info edit-row" data-id="'.encryptURL($d).'" title="Edit '.ucfirst($this->label).'">
            <i class="far fa-edit"></i>
            </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="'.encryptURL($d).'" title="Delete '.ucfirst($this->label).'"><i class="far fa-trash-alt"></i></a>';
        // }
               })
            );
    $joinQuery  = "from `$tblStock` as `t1` ";
    $joinQuery .= "left join `kit_produk` as `t2` using(`idproduk`) ";
    $joinQuery .= "left join `kit_wip` as `t3` on (`t1`.`idkayu`=`t3`.`idwip`) ";
$filter='';
        break;
    case 'produk-jadi': $jenis='P';
parse_str($this->input->get('filter'), $get);
$Fawal      =(!empty(decode_date($get['new-periode_awal'])))?decode_date($get['new-periode_awal']):'0000-00-00';
$Fakhir     =(!empty(decode_date($get['new-periode_akhir'])))?decode_date($get['new-periode_akhir']):'0000-00-00';
$Fproduk    =(!empty($get['new-produk']))?$get['new-produk']:'0';
$Fkayu      =(!empty($get['new-listkayu']))?$get['new-listkayu']:'0';
    $columns = array(
        array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
        array('db' => 'concat(DATE_FORMAT(periode_awal, "%d-%m-%Y")," ",DATE_FORMAT(periode_akhir, "%d-%m-%Y"))','as'=>'periode', 'dt' => 0, 'field' => 'periode'),
        array('db' => 'produk', 'dt' => 1, 'field' => 'produk'),
        array('db' => 'kodehs', 'dt' => 2, 'field' => 'kodehs'),
        array('db' => 't3.nama','as'=> 'kayu', 'dt' => 3, 'field' => 'kayu'),
        array('db' => 'stokawal_volume', 'dt' => 4, 'field' => 'stokawal_volume'),
        array('db' => 'pemasukan_volume', 'dt' => 5, 'field' => 'pemasukan_volume'),
        array('db' => 'pengeluaran_volume', 'dt' => 6, 'field' => 'pengeluaran_volume'),
        array('db' => 'stokakhir_volume', 'dt' => 7, 'field' => 'stokakhir_volume'),
        array('db' => $primaryKey, 'dt' => 8, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
            return '<a href="javascript:void(0)" class="btn btn-xs btn-info edit-row" data-id="'.encryptURL($d).'" title="Edit '.ucfirst($this->label).'">
            <i class="far fa-edit"></i>
            </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="'.encryptURL($d).'" title="Delete '.ucfirst($this->label).'"><i class="far fa-trash-alt"></i></a>';
               })
            );
    $joinQuery  = "from `$tblStock` `t1` ";
    $joinQuery .= "left join `kit_produk` `t2` using(`idproduk`) ";
    $joinQuery .= "left join `kit_jeniskayu` `t3` using(`idkayu`) ";
$filter  =" and periode_awal='".$Fawal."' and periode_akhir='".$Fakhir."' ";
$filter .=" and idproduk='".$Fproduk."' and idkayu='".$Fkayu."' ";
    break;
}

    $extraWhere = "stdelete=1 and (client_id=".$_SESSION['client_id']." and jenis='".$jenis."' ".$filter.")";
    $groupBy    = "";
    $ordercus   = "ORDER BY idstok DESC ";
    $having     = "";
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $tblStock, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
    );
}

function getData4Update(){
    echo json_encode($this->model->getItemStock($this->input->post('idstock')));
}


function delAkun() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->delAkun($id);
$b=rowArray($this->tblStock,array('idstok'=>$id));
$c=rowArray('kit_sortimen',array('idsortimen'=>$b['idsortimen']));
echo $c['sortimen'];
}

}
