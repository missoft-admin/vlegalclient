<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Tproduk extends CI_Controller {
private $tblLokal      = 'penjualan';
private $tblEkspor     = 'realisasiekspor';
private $label      = 'penjualan';
private $label2     = 'Realisasi Ekspor';
private $folder     = 'Tproduk';
public  $firstTitle = 'Produk';
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
            $this->load->model('Tproduk_model','model');
            $this->load->model('Mstock_model','stock');
            $this->load->model('A_json_model','json');
    }
    
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

    function showingData()
    {
// $data array() for basic HTML
        if($_GET['tipe']=='lokal'&&empty($_GET['p'])){
            $link=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'];
            $param=array('page'=>'index','labelLink'=>$_GET['tipe'],'link'=>$link,'form_url'=>'');
                $data=$this->page($param);
        }else
        if($_GET['tipe']=='ekspor'&&empty($_GET['p'])){
            $link=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'];
            $param=array('page'=>'index','labelLink'=>$_GET['tipe'],'link'=>$link,'form_url'=>'');
                $data=$this->page($param);
        }else
        if($_GET['tipe']=='lokal'&&$_GET['p']=='new-penjualan'){
            $param['page']='manage';
            $param['form_url']=site_url().strtolower($this->firstTitle).'/'.$this->label.'/proses?tipe='.$_GET['tipe'];
            $param['link']='produk/'.$this->label.'?tipe='.$_GET['tipe'].'&p='.$_GET['p'];
            $param['labelLink']='Add New';
                $dat=$this->page($param);
                $datnull=$this->insertBaru();
        $data = array_merge($dat, $datnull);
        }else
        if($_GET['tipe']=='lokal'&&$_GET['p']=='edit-penjualan'){
            $param['page']='manage';
            $param['form_url']=site_url().strtolower($this->firstTitle).'/'.$this->label.'/proses?tipe='.$_GET['tipe'].'&id='.$_GET['id'];
            $param['link']='produk/'.$this->label.'?tipe='.$_GET['tipe'].'&p='.$_GET['p'].'&id='.$_GET['id'];
            $param['labelLink']='Edit';
                $dat=$this->page($param);
                $data=$this->indexUpdate();
        $data = array_merge($dat, $data);
        }else
        if($_GET['tipe']=='ekspor'&&$_GET['p']=='new-penjualan'){
            $param['page']='manage';
            $param['form_url']=site_url().strtolower($this->firstTitle).'/'.$this->label.'/proses?tipe='.$_GET['tipe'];
            $param['link']=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p='.$_GET['p'];
            $param['labelLink']='Add New';
                $dat=$this->page($param);
                $datnull=$this->insertBaru();
        $data = array_merge($dat, $datnull);
        }else
        if($_GET['tipe']=='ekspor'&&$_GET['p']=='edit-penjualan'){
            $param['page']='manage';
            $param['form_url']=site_url().strtolower($this->firstTitle).'/'.$this->label.'/proses?tipe='.$_GET['tipe'].'&id='.$_GET['id'];
            $param['link']=$this->folder.'/'.$this->label.'?tipe='.$_GET['tipe'].'&p='.$_GET['p'].'&id='.$_GET['id'];
            $param['labelLink']='Edit';
                $dat=$this->page($param);
                $data=$this->indexUpdate();
        $data = array_merge($dat, $data);
        }

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function page($param=array('page'=>'index','labelLink'=>'List','link'=>'','form_url'=>'')){
if($this->input->get('p')&&$_GET['p']=='new-penjualan'){
    $t=' New ';
}else
if($this->input->get('p')&&$_GET['p']=='edit-penjualan'){
    $t=' Edit ';
}else{
    $t=' ';
}
$title3=($_GET['tipe']=='lokal')?'dalam negeri':$_GET['tipe'];
    $data = array();
    $data['title']        = $this->firstTitle.' -'.$t.$this->label.' '.$title3;
    $data['template']     = $this->folder.'/'.$param['page'];
    $data['tJudul']       = ucfirst($this->label).' '.ucfirst($this->input->get('tipe'));
    $data['dJudul']       = $this->label.' '.ucfirst($this->input->get('tipe'));
    $data['url_index']    = site_url().strtolower($this->firstTitle).'/'.$this->label.'?tipe='.$_GET['tipe'];
    $data['url_addnew']   = site_url().strtolower($this->firstTitle).'/'.$this->label.'?tipe='.$_GET['tipe'].'&p=new-'.$this->label;
    $data['url_kedua']    = $this->input->get('p');
    $data['url_ajax']     = site_url().'ajax/'.$this->label.'?tipe='.$_GET['tipe'];
    $data['url_delete']   = site_url().'ajax/del'.ucfirst($this->label);
    $data['url_uStatus']  = site_url().'ajax/upStatus'.ucfirst($this->label);
    $data['url_proses']   = $param['form_url'];
    $data['breadcrum']    = array(
                            array($this->firstTitle,'#'),
                            array(ucfirst($this->label),'#'),
                            array($param['labelLink'],$param['link'])
                          );
    $lastperiode=$this->model->getLokalByLastPeriode($_GET['tipe']);
    $data['Laper']          = (count($lastperiode)>0)?$lastperiode:array('periode_awal'=>'0000-00-00','periode_akhir'=>'0000-00-00');
    // print_r($this->db->last_query());exit();
    return $data;
}

    function insertBaru()
    {
// $data array() for value database
$data['newperiode_awal']='';
$data['newperiode_akhir']='';
$data['newjumlah']='';
$data['newvolume']='';
$data['newberat']='';
$data['newsatjumlah']='';
$data['newsatvolume']='';
$data['newsatberat']='';
$data['newproduk']='';
$data['newnegara']='';
$data['newclient_propinsi']='';
$data['newclient_kabupaten']='';
$data['newbuyer']='';
$data['newlistkayu']='';
$data['newlistkayu2']='';
$data['newlistkayu3']='';
$data['newinvoice']='';
$data['newnilai']='';
$data['newtglterbit']='';
$data['newnonota']='';


$data['newmatauang']='';
$data['newloading']='';
$data['newdischarge']='';
$data['newstatus']='';
$data['newiso']='';
$data['newalamat']='';
$data['newketerangan']='';
$data['newetpik']='';
$data['newnpwp']='';
$data['newinvoice']='';
$data['newtglinvoice']='';
$data['newpeb']='';
$data['newbl']='';
$data['newpackinglist']='';
$data['newtglship']='';
$data['newstuffing']='';
$data['newvessel']='';
$data['newidnegara_dis']='';

return $data;
    }

    function indexUpdate()
    {
if($_GET['tipe']=='lokal'){
    $get    =$this->model->getLokalUpdate(decryptURL($_GET['id']));
    if(count($get)>0){

    // $data array() for value database
    $data['newperiode_awal']    =encode_date($get['periode_awal']);
    $data['newperiode_akhir']   =encode_date($get['periode_akhir']);
    $data['newproduk']          =$get['produk'];
    $data['newlistkayu']        =$get['kayu1'];
    $data['newlistkayu2']       =$get['kayu2'];
    $data['newlistkayu3']       =$get['kayu3'];

    $data['newsatjumlah']       =$get['satuan_jumlah'].','.ucfirst($get['satuan_jumlah']);
    $data['newsatvolume']       =$get['satuan_volume'].','.ucfirst($get['satuan_volume']);
    $data['newsatberat']        =$get['satuan_berat'].','.ucfirst($get['satuan_berat']);
    $data['newjumlah']          =$get['jumlah'];
    $data['newvolume']          =$get['volume'];
    $data['newberat']           =$get['berat'];

    $data['newclient_propinsi']     =$get['provinsi'];
    $data['newclient_kabupaten']    =$get['kabupaten'];
    $data['newbuyer']               =$get['buyer'];
    $data['newinvoice']             =$get['invoice'];
    $data['newnilai']               =$get['nilai'];
    $data['newtglterbit']           =encode_date($get['tglterbit']);
    $data['newnonota']              =$get['nonota'];
    $data['newpackinglist']         =$get['packinglist'];
        return $data;
    }else{
        redirect(site_url().strtolower($this->firstTitle).'/'.$this->label.'?tipe='.$_GET['tipe']);
        }
    }else{
    $get    =$this->model->getEksporUpdate(decryptURL($_GET['id']));
    if(count($get)>0){
$getClient=$this->json->getClientbyID();
$data['newbuyer']       =$get['idbuyer'].','.$get['buyer'].' ['.$get['negarabuyer'].']';
$data['newalamat']      =$get['alamat'];
$data['newnegara']      =$get['idnegara'].','.$get['nama_negara'];
$data['newiso']         =$get['iso'];
$data['newmatauang']    =$get['kode_uang'].','.$get['kode_uang'].' - '.$get['nama_uang'];
$data['newloading']     =$get['idloading'].','.$get['loading_uraian'].' ['.$get['loading_kode'].']';
$data['newdischarge']   =$get['iddischarge'].','.$get['dis_uraian'].' ['.$get['dis_kode'].']';
$data['newidnegara_dis']=$get['idnegara'];
$data['newketerangan']  =$get['keterangan'];
$data['newetpik']       =$getClient['client_etpik'];
$data['newnpwp']        =$getClient['client_npwp'];
$data['newinvoice']     =$get['invoice'];
$data['newtglinvoice']  =encode_date($get['tglinvoice']);
$data['newstatus']      =$get['status'];
$data['newpeb']         =$get['peb'];
$data['newbl']          =$get['bl'];
$data['newpackinglist'] =$get['packinglist'];
$data['newtglship']     =encode_date($get['tglship']);
$data['newstuffing']    =$get['stuffing'];
$data['newvessel']      =$get['vessel'];
$data['newstatus_liu']  =$get['status_liu'];

return $data;
// exit($this->db->last_query());
    }
}
}

function saveNew(){
    // exit($this->input->get('tipe'));
switch ($this->input->get('tipe')) {
    case 'lokal':
if(!empty($_GET['id'])){
$update=$this->FupdateData($_GET['tipe']);
    if($update){
        $_SESSION['msg']='ToastrSukses("'.ucfirst($this->label).' '.ucfirst($_GET['tipe']).' berhasil diubah","Info")';
        redirect(site_url().strtolower($this->firstTitle).'/'.$this->label.'?tipe='.$_GET['tipe']);
    }
}else{
$insertID=$this->model->addUpLokalItem();

        if($insertID){
            $dStock['idproduk']         = $this->input->post('new-produk');
            $dStock['idkayu']           = $this->input->post('new-listkayu');
            $dStock['idsortimen']       = NULL;
            $dStock['jenis']            = "P";

            $dStock['stokawal_jumlah']              = NULL;
            $dStock['stokawal_volume']              = NULL;
            $dStock['stokawal_berat']               = NULL;
            $dStock['satuan_stokawal_jumlah']       = NULL;
            $dStock['satuan_stokawal_volume']       = NULL;
            $dStock['satuan_stokawal_berat']        = NULL;
            $dStock['pemasukan_jumlah']             = NULL;
            $dStock['pemasukan_volume']             = NULL;
            $dStock['pemasukan_berat']              = NULL;
            $dStock['satuan_pemasukan_jumlah']      = NULL;
            $dStock['satuan_pemasukan_volume']      = NULL;
            $dStock['satuan_pemasukan_berat']       = NULL;
            $dStock['pengeluaran_jumlah']           = $this->input->post('new-jumlah');
            $dStock['satuan_pengeluaran_jumlah']    = $this->input->post('new-satjumlah');
            $dStock['pengeluaran_volume']           = $this->input->post('new-volume');
            $dStock['satuan_pengeluaran_volume']    = $this->input->post('new-satvolume');
            $dStock['pengeluaran_berat']            = $this->input->post('new-berat');
            $dStock['satuan_pengeluaran_berat']     = $this->input->post('new-satberat');

            $dStock['stokakhir_jumlah']             = NULL;
            $dStock['stokakhir_volume']             = NULL;
            $dStock['stokakhir_berat']              = NULL;
            $dStock['satuan_stokakhir_jumlah']      = NULL;
            $dStock['satuan_stokakhir_volume']      = NULL;
            $dStock['satuan_stokakhir_berat']       = NULL;
            $dStock['jk']                           = NULL;
            $dStock['fk']                           = NULL;
            $dStock['client_id']        = $_SESSION['client_id'];
            $dStock['bulan']            = decode_date_m($this->input->post('new-periode_awal'));
            $dStock['tahun']            = decode_date_Y($this->input->post('new-periode_akhir'));
            $dStock['tanggal']          = date('Y-m-d');
            $dStock['periode_awal']     = decode_date($this->input->post('new-periode_awal'));
            $dStock['periode_akhir']    = decode_date($this->input->post('new-periode_akhir'));
            $dStock['contentid']        = 'lk-'.$insertID;
        $insertStock=$this->stock->InUpItem($dStock);
            if($insertStock){
    $_SESSION['msg']='ToastrSukses("'.ucfirst($this->label).' '.ucfirst($_GET['tipe']).' baru telah ditambahkan","Info")';
            redirect(site_url().strtolower($this->firstTitle).'/'.$this->label.'?tipe='.$_GET['tipe']);
            }else{
    $_SESSION['msg']='Toastr("Maaf, '.ucfirst($this->label).' '.ucfirst($_GET['tipe']).' gagal ditambahkan","Info")';
            redirect(site_url().strtolower($this->firstTitle).'/'.$this->label.'?tipe='.$_GET['tipe'].'&p=new-'.$this->label);
            }
        }
    }
        break;
    case 'ekspor':
if(!empty($_GET['id'])){
$update=$this->FupdateData($_GET['tipe']);
    if($update){
        $_SESSION['msg']='ToastrSukses("'.ucfirst($this->label).' '.ucfirst($_GET['tipe']).' berhasil diubah","Info")';
        redirect(site_url().strtolower($this->firstTitle).'/'.$this->label.'?tipe='.$_GET['tipe']);
    }
}else{
$insertID=$this->model->addUpEkspor();

        if($insertID){
    $_SESSION['msg']='ToastrSukses("'.ucfirst($this->label).' '.ucfirst($_GET['tipe']).' baru telah ditambahkan","Info")';
            redirect(site_url().strtolower($this->firstTitle).'/'.$this->label.'?tipe='.$_GET['tipe']);
            }else{
    $_SESSION['msg']='Toastr("Maaf, '.ucfirst($this->label).' '.ucfirst($_GET['tipe']).' gagal ditambahkan","Info")';
            redirect(site_url().strtolower($this->firstTitle).'/'.$this->label.'?tipe='.$_GET['tipe'].'&p=new-'.$this->label);
            }
        }
        break;
}
}

function FupdateData($tipe){
    if($tipe=='lokal'){
        // print_r($_GET['id']);exit();
$insertID=$this->model->addUpLokalItem(array('idjual'=>decryptURL($_GET['id'])));
        if($insertID){
            return true;
        }else{
            return false;
        }
    }else{
$insertID=$this->model->addUpEkspor(array('idekspor'=>decryptURL($_GET['id'])));
        if($insertID){
            return true;
        }else{
            return false;
        }
    }
}

public function getListDT(){
if($_GET['tipe']=='lokal'){
    $tblLokal      = $this->tblLokal; 
    $primaryKey = 'idjual';
    $sql_details = sql_connect();

$buyer ='concat(`t4`.`buyer`,\' / \',`t3`.`nama`)';
$jumlah='concat(jumlah,\' \',satuan_jumlah)';
$berat ='concat(berat,\' \',satuan_berat)';
$volume='concat(volume,\' \',satuan_volume)';
$kayu  ='concat(\'1. \',COALESCE(kayu1.nama,\'-\'),"'.br(1);
$kayu .='",\'2. \',COALESCE(kayu2.nama,\'-\'),"'.br(1);
$kayu .='",\'3. \',COALESCE(kayu3.nama,\'-\'))';

$columns = array(
    array('db' => $primaryKey,          'dt' => 0, 'field' => $primaryKey),
    array('db' => $buyer,               'dt' => 1, 'field' => 'buyer',      'as'=> 'buyer'),
    array('db' => '`t1`.`invoice`',     'dt' => 2, 'field' => 'invoice'),
    array('db' => '`t5`.`produk`',      'dt' => 3, 'field' => 'produk'),
    array('db' => $jumlah,              'dt' => 4, 'field' => 'jumlah',     'as'=> 'jumlah'),
    array('db' => $berat,               'dt' => 5, 'field' => 'berat',      'as'=> 'berat'),
    array('db' => $volume,              'dt' => 6, 'field' => 'volume',    'as'=> 'volume'),
    array('db' => $kayu,                'dt' => 7, 'field' => 'kayu',       'as'=> 'kayu'),
    array('db' => '`t1`.`nonota`',      'dt' => 8, 'field' => 'nonota'),
    array('db' => '`t1`.`packinglist`', 'dt' => 9, 'field' => 'packinglist'),
    array('db' => '`t1`.`tglterbit`',   'dt' => 10, 'field' => 'tglterbit','formatter'=>function($d,$row){
        return encode_date($d);
    }),
    array('db' => '`t1`.`nilai`',       'dt' => 11, 'field' => 'nilai'),
   
    array('db' => $primaryKey, 'dt' => 12, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
return '<a href="'.site_url().strtolower($this->firstTitle).'/'.$this->label.'?tipe='.$_GET['tipe'].'&p=edit-penjualan&id='.encryptURL($d).'" class="btn btn-xs btn-info" title="Edit '.ucfirst($this->label).'">
    <i class="far fa-edit"></i>
</a>
<a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="'.encryptURL($d).'" title="Delete '.ucfirst($this->label).'"><i class="far fa-trash-alt"></i></a>';
                   })
                );
    $joinQuery  = "FROM `$tblLokal` as `t1` ";
    $joinQuery .= 'LEFT JOIN `kit_propinsi` as t2 ON (`t2`.`propid` = `t1`.`idpropinsi`) ';
    $joinQuery .= 'LEFT JOIN `kit_kabupaten` as t3 ON (`t3`.`kabid` = `t1`.`idkota`) ';
    $joinQuery .= 'LEFT JOIN `kit_buyer` as t4 using(`idbuyer`) ';
    $joinQuery .= 'LEFT JOIN `kit_produk` AS t5 using(`idproduk`) ';
    $joinQuery .= 'LEFT JOIN `kit_jeniskayu` as kayu1 ON (`kayu1`.`idkayu` = `t1`.`idkayu`) ';
    $joinQuery .= 'LEFT JOIN `kit_jeniskayu` as kayu2 ON (`kayu2`.`idkayu` = `t1`.`idkayu2`) ';
    $joinQuery .= 'LEFT JOIN `kit_jeniskayu` as kayu3 ON (`kayu3`.`idkayu` = `t1`.`idkayu3`) ';
    $extraWhere = "stdelete=1 and client_id = '".$_SESSION['client_id']."'";
    $groupBy    = '';
    $ordercus   = 'ORDER BY `t1`.`idjual` DESC';
    $having     = '';
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $tblLokal, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
    );

}else{

    $tblEkspor      = $this->tblEkspor; 
    $primaryKey = 'idekspor';
    $sql_details = sql_connect();

$invoice    ='concat(`t1`.`invoice`,\' / \',date_format(`t1`.`tglinvoice`,\'%d-%m-%Y\')) ';
$buyer      ='concat(`t5`.`buyer`,\' / \',`t2`.`negara`) ';

$columns = array(
    array('db' => $primaryKey,  'dt' => 0, 'field' => $primaryKey),
    array('db' => $invoice,     'dt' => 1, 'field' => 'invoice',    'as'=>'invoice'),
    array('db' => 'no_urut',    'dt' => 2, 'field' => 'no_urut'),
    array('db' => $buyer,       'dt' => 3, 'field' => 'buyer',      'as'=>'buyer'),
    array('db' => 'iso',        'dt' => 4, 'field' => 'iso'),
    array('db' => 't3.uraian',  'dt' => 5, 'field' => 'loading',    'as'=> 'loading'),
    array('db' => 't4.uraian',  'dt' => 6, 'field' => 'discharge',  'as'=> 'discharge'),
    array('db' => 'tglship',    'dt' => 7, 'field' => 'tglship',    'formatter'=>function($d,$row){
        return encode_date($d);
    }),
   
    array('db' => $primaryKey,  'dt' => 8, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
return '<a href="'.site_url().strtolower($this->firstTitle).'/'.$this->label.'?tipe='.$_GET['tipe'].'&p=edit-penjualan&id='.encryptURL($d).'" class="btn btn-xs btn-info" title="Edit '.ucfirst($this->label).'">
    <i class="far fa-edit"></i>
</a>
<a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="'.encryptURL($d).'" title="Delete '.ucfirst($this->label).'"><i class="far fa-trash-alt"></i></a>';
                   })
                );
    $joinQuery  = "FROM `$tblEkspor` as `t1` ";
    $joinQuery .= 'LEFT JOIN `kit_negara` as t2 using(`idnegara`) ';
    $joinQuery .= 'LEFT JOIN `kit_loading` as t3 ON (`t3`.`idloading` = `t1`.`loading`) ';
    $joinQuery .= 'LEFT JOIN `kit_discharge` as t4 ON (`t4`.`iddischarge` = `t1`.`discharge`) ';
    $joinQuery .= 'LEFT JOIN `kit_buyer` AS t5 using(`idbuyer`) ';
    $extraWhere = "stdelete=1 and client_id = '".$_SESSION['client_id']."'";
    $groupBy    = '';
    $ordercus   = 'ORDER BY no_urut DESC';
    $having     = '';
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $tblEkspor, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
    );
}
}


function delAkun() {
$id =decryptURL($this->input->post('id')); 
    if($_GET['tipe']=='lokal'){
    $a  =$this->model->delAkun($id);
        }else{
    $b  =$this->model->delEkspor($id);
        }
}

}
