<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class A_json extends CI_Controller {
private $tblNegara       = 'kit_negara';
private $tblClients      = 'clients';
private $tblSupplier     = 'kit_supplier';
private $tblLoading      = 'kit_loading';
private $tblGroupUser    = 'users_group';
private $tblPropinsi     = 'kit_propinsi';
private $tblKabupaten    = 'kit_kabupaten';
private $tblUsers        = 'users';
private $tblSetting      = 'kit_setting';
private $tblBuyer        = 'kit_buyer';
private $tblSortimen     = 'kit_sortimen';
private $tblWip          = 'kit_wip';
private $tblProduk       = 'kit_produk';
private $tblSertifikasi  = 'kit_sertifikasi';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('A_json_model','model');
    }
    
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

public function getNegara()
    {
$select='idnegara as id,concat(negara,\' (\',idnegara,\')\') as name';
$where['xstatus']=1;
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['idnegara']=$id;
    }
$like=array('negara'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblNegara,$select,$where);
    echo json_encode($q);
    }

public function getClients()
    {
$select='client_nama,client_id';
$where['client_stdelete']=1;
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['client_id']=$id;
    }else{
    $where['client_id']=0;
    }
$like=array('client_nama'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblClients,$select,$where);
// print_r($this->db->last_query());exit();
    echo json_encode($q);
    } 
 
public function getSupplier()
    {
$id='';
if(!empty($_GET['id'])){if (!is_numeric($_GET['id'])){$id.=decryptURL($_GET['id']);} else {$id.=$_GET['id'];}$id=$id;}
$limit['limit']=(!empty($_GET['page']))?$_GET['page']:'';
$limit['start']=0;
$like=(!empty($_GET['search'])?$_GET['search']:'');
    $q=$this->model->getSupplier($like,$limit,$id);
echo json_encode($q);
    }

public function getLoading()
    {
        $q=$this->model->getLoading();
    echo json_encode($q);
    }

public function getLoading_byClient()
    {
        $q=$this->model->getLoading_byClient();
    echo json_encode($q);
    }

public function getDischargeByNegara()
    {
$kode=(!empty($_GET['kode']))?substr($_GET['kode'],0,2):'';
        $q=$this->model->getDischargeByNegara($kode);
    echo json_encode($q);
    }

public function getProvinsi()
    {
$select='propid,nama,SUBSTRING(kode, 1,2) as kode';
$where=(!empty($_GET['kode']))?array('SUBSTRING(kode, 1,2)'=>$_GET['kode']):'';
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['propid']=$id;
    }
$like=array('nama'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblPropinsi,$select,$where);
    echo json_encode($q);
    }
public function getKabupaten()
    {
$select='kabid,nama,SUBSTRING(kode, 1,2) as kode';
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['kabid']=$id;
    }
$where=(!empty($_GET['kode']))?array('SUBSTRING(kode, 1,2)='=>substr($_GET['kode'],0,2)):array('kode='=>0);
$like=array('nama'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblKabupaten,$select,$where);
    echo json_encode($q);
    }

public function getGroupUsers()
    {
        echo json_encode(getData($this->tblGroupUser));
    }
  
 
public function getBuyer()
    {
        // exit($_GET['id']);
$id='';
if(!empty($_GET['id'])){if (!is_numeric($_GET['id'])){$id.=decryptURL($_GET['id']);} else {$id.=$_GET['id'];}$id=$id;}
$limit['limit']=(!empty($_GET['limitnya']))?$_GET['limitnya']:100;
$limit['start']=0;
$like=(!empty($_GET['search'])?$_GET['search']:'');
$q=$this->model->getBuyer($like,$limit,$id);
// exit($this->db->last_query()); 
    echo json_encode($q);
    }

public function getSortimen()
    {
$id='';
if(!empty($_GET['id'])){if (!is_numeric($_GET['id'])){$id.=decryptURL($_GET['id']);} else {$id.=$_GET['id'];}$id=$id;}
$limit['limit']=(!empty($_GET['page']))?$_GET['page']:'';
$limit['start']=0;
$like=(!empty($_GET['search'])?$_GET['search']:'');
$q=$this->model->getSortimen($like,$limit,$id); 
    echo json_encode($q);
    }

public function getKayu()
    {
$id='';
if(!empty($_GET['id'])){if (!is_numeric($_GET['id'])){$id.=decryptURL($_GET['id']);} else {$id.=$_GET['id'];}$id=$id;}
$limit['limit']=(!empty($_GET['page']))?$_GET['page']:'';
$limit['start']=0;
$like=(!empty($_GET['search'])?$_GET['search']:'');
$q=$this->model->getKayu($like,$limit,$id); 
    echo json_encode($q);
    }

public function getWip()
    {
$select='idwip,wip';
$where['xstatus']=1;
$where['wipstdelete']=1;
if(!empty($_GET['id'])){if (!is_numeric($_GET['id'])){$id= decryptURL($_GET['id']);}else{$id= $_GET['id'];}$where['idwip']=$id;}
$limit=(!empty($_GET['page']))?$_GET['page']:'';
$like=array('wip'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblWip,$select,$where,$limit);
    echo json_encode($q);
    }

public function getProduk()
    {
$id='';
if(!empty($_GET['id'])){if (!is_numeric($_GET['id'])){$id.=decryptURL($_GET['id']);} else {$id.=$_GET['id'];}$id=$id;}
$limit['limit']=(!empty($_GET['page']))?$_GET['page']:'';
$limit['start']=0;
$like=(!empty($_GET['search'])?$_GET['search']:'');
$q=$this->model->getProduk($like,$limit,$id);
    echo json_encode($q);
    }

public function getMataUang(){
$like=(!empty($_GET['search'])?$_GET['search']:'');
        $q=$this->model->getMataUang('',$like);
    echo json_encode($q);
}

public function getSertifikasi(){ 
        $q=$this->model->getSertifikasi();
    echo json_encode($q);
}

public function getDataPengeluaran(){ 
        $q=$this->model->getDataPengeluaran();
    echo json_encode($q);
}

public function getHutanData(){ 
        $q=$this->model->getHutan();
    echo json_encode($q);
}

public function getDokumenData(){ 
        $q=$this->model->getDokumen();
    echo json_encode($q);
}

public function getAsalKayuData(){ 
        $q=$this->model->getAsalKayu();
    echo json_encode($q);
}
public function getSatuanByOption(){
$option=$_GET['by']; 
        $q=$this->model->getSatuanByOption($option);
    echo json_encode($q);
}

    public function detailclient1($id = null)
    {
        $data = array();
        if ($id !== null) {
            $cek = rowArray($this->tblClients,array('client_id'=> decryptURL($id)));
            if ($cek) {
                $this->parser->parse('Mclient/detailsatu', array('client_id' => decryptURL($id)));
            } else {
                echo "";
            }
        } else {
            echo "";
        }
    }
 

}
