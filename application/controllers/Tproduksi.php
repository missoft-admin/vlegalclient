<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Tproduksi extends CI_Controller {
private $tblProduksi   = 'produksi';
public  $label      = 'realisasi';
public  $folder     = 'Tproduksi';
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
            $this->load->model($this->folder.'_model','model');
            $this->load->model('Mstock_model','stock');
    }
	
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

	function showingData()
    {
        $data = array();
        $data['title']      = 'Produksi - '.ucfirst($this->label);
        $data['template']   = $this->folder.'/index';
        $data['tJudul']     = ucfirst($this->label);
        $data['dJudul']     = $this->label;
        $data['breadcrum']  = array(
                                array('Produksi','#'),
                                array(ucfirst($this->label),'produksi/realisasi')
                              );
        $data['url_ajax']       = site_url().'ajax/realisasi';
        $data['url_insert']     = site_url().'produksi/realisasi/proses';
        $data['url_delete']     = site_url().'ajax/del'.ucfirst($this->label);
        $data['url_periode']    = site_url().'ajax/periode-'.$this->label;
        $data['Laper']['periode_awal']      = '';
        $data['Laper']['periode_akhir']     = '';
$data['newjumlah']='';
$data['newvolume']='';
$data['newberat'] ='';
$data['newsatjumlah']='';
$data['newsatvolume']='';
$data['newsatberat'] ='';
$data['newproduk'] ='';
$data['newlistkayu'] ='';

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function saveNew(){
parse_str($this->input->post('data'), $post);
if(!empty($post['hide-ID'])){
    $this->FupdateData();
}else{
    if(!empty($post['new-produk']) && !empty($post['new-listkayu']) && !empty($post['new-jumlah']) && !empty($post['new-satjumlah']) && !empty($post['new-volume'])&& !empty($post['new-satvolume'])&& !empty($post['new-berat'])&& !empty($post['new-satberat'])){
    $insertID=$this->model->InUpItem();
        if($insertID){
        $dStock['idproduk']     = $post['new-produk'];
        $dStock['idkayu']       = $post['new-listkayu'];
        $dStock['idsortimen']   = NULL;
        $dStock['jenis']        = "P";

        $dStock['stokawal_jumlah']              = NULL;
        $dStock['satuan_stokawal_jumlah']       = NULL;
        $dStock['stokawal_volume']              = NULL;
        $dStock['satuan_stokawal_volume']       = NULL;
        $dStock['stokawal_berat']               = NULL;
        $dStock['satuan_stokawal_berat']        = NULL;
        $dStock['stokakhir_jumlah']             = NULL;
        $dStock['stokakhir_volume']             = NULL;
        $dStock['stokakhir_berat']              = NULL;
        $dStock['satuan_stokakhir_jumlah']      = NULL;
        $dStock['satuan_stokakhir_volume']      = NULL;
        $dStock['satuan_stokakhir_berat']       = NULL;

        $dStock['pemasukan_jumlah']             = $post['new-jumlah'];
        $dStock['satuan_pemasukan_jumlah']      = $post['new-satjumlah'];
        $dStock['pemasukan_volume']             = $post['new-volume'];
        $dStock['satuan_pemasukan_volume']      = $post['new-satvolume'];
        $dStock['pemasukan_berat']              = $post['new-berat'];
        $dStock['satuan_pemasukan_berat']       = $post['new-satberat'];
        $dStock['pengeluaran_jumlah']           = NULL;
        $dStock['pengeluaran_volume']           = NULL;
        $dStock['pengeluaran_berat']            = NULL;
        $dStock['satuan_pengeluaran_jumlah']    = NULL;
        $dStock['satuan_pengeluaran_volume']    = NULL;
        $dStock['satuan_pengeluaran_berat']     = NULL;

        $dStock['jk']                           = NULL;
        $dStock['fk']                           = NULL;
        $dStock['bulan']          = decode_date_m($post['new-periode_awal']);
        $dStock['tahun']          = decode_date_Y($post['new-periode_akhir']);
        $dStock['tanggal']        = date('Y-m-d');
        $dStock['client_id']      = $_SESSION['client_id'];
        $dStock['periode_awal']   = decode_date($post['new-periode_awal']);
        $dStock['periode_akhir']  = decode_date($post['new-periode_akhir']);
        $dStock['contentid']      = 'pd-'.$insertID;
            $this->stock->InUpItem($dStock);
    echo 'ToastrSukses("'.ucfirst($this->label).' baru telah ditambahkan.","Info")';
}else{
    echo 'Toastr("Maaf, Data gagal ditambahkan.","Info")';
    }
}else{
    echo 'Toastr("Maaf, Data tidak boleh ada yang kosong.","Info")';
    }
}
}

function FupdateData(){
parse_str($this->input->post('data'), $post);
$where =array('idproduksi' =>decryptURL($post['hide-ID']));
// print_r(decryptURL($post['hide-ID']));exit();
    if(!empty($post['new-produk']) && !empty($post['new-listkayu']) && !empty($post['new-jumlah']) && !empty($post['new-satjumlah']) && !empty($post['new-volume'])&& !empty($post['new-satvolume'])&& !empty($post['new-berat'])&& !empty($post['new-satberat'])){
    if($this->model->InUpItem($where)){
            $dStock['idproduk']     = $post['new-produk'];
            $dStock['idkayu']       = $post['new-listkayu'];
            $dStock['idsortimen']   = NULL;
            $dStock['jenis']        = "P";
    
            $dStock['stokawal_jumlah']              = NULL;
            $dStock['satuan_stokawal_jumlah']       = NULL;
            $dStock['stokawal_volume']              = NULL;
            $dStock['satuan_stokawal_volume']       = NULL;
            $dStock['stokawal_berat']               = NULL;
            $dStock['satuan_stokawal_berat']        = NULL;
            $dStock['stokakhir_jumlah']             = NULL;
            $dStock['stokakhir_volume']             = NULL;
            $dStock['stokakhir_berat']              = NULL;
            $dStock['satuan_stokakhir_jumlah']      = NULL;
            $dStock['satuan_stokakhir_volume']      = NULL;
            $dStock['satuan_stokakhir_berat']       = NULL;
    
            $dStock['pemasukan_jumlah']             = $post['new-jumlah'];
            $dStock['satuan_pemasukan_jumlah']      = $post['new-satjumlah'];
            $dStock['pemasukan_volume']             = $post['new-volume'];
            $dStock['satuan_pemasukan_volume']      = $post['new-satvolume'];
            $dStock['pemasukan_berat']              = $post['new-berat'];
            $dStock['satuan_pemasukan_berat']       = $post['new-satberat'];
            $dStock['pengeluaran_jumlah']           = NULL;
            $dStock['pengeluaran_volume']           = NULL;
            $dStock['pengeluaran_berat']            = NULL;
            $dStock['satuan_pengeluaran_jumlah']    = NULL;
            $dStock['satuan_pengeluaran_volume']    = NULL;
            $dStock['satuan_pengeluaran_berat']     = NULL;
    
            $dStock['jk']                           = NULL;
            $dStock['fk']                           = NULL;
            $dStock['bulan']          = decode_date_m($post['new-periode_awal']);
            $dStock['tahun']          = decode_date_Y($post['new-periode_akhir']);
            $dStock['tanggal']        = date('Y-m-d');
            $dStock['client_id']      = $_SESSION['client_id'];
            $dStock['periode_awal']   = decode_date($post['new-periode_awal']);
            $dStock['periode_akhir']  = decode_date($post['new-periode_akhir']);
            $dStock['contentid']      = 'pd-'.decryptURL($post['hide-ID']);
               if($this->stock->InUpItem($dStock,array('contentid'=>$dStock['contentid']))){
    echo 'ToastrSukses("'.ucfirst($this->label).' berhasil diedit.","Info")';
    }
   }
    }else{
    echo 'Toastr("Maaf, Data tidak boleh ada yang kosong.","Info")';
    }
}

public function getListDT(){
    $tblProduksi       = $this->tblProduksi;
    $primaryKey     = 'idproduksi';
    $sql_details    = sql_connect();
$periode='concat(DATE_FORMAT(periode_awal, "%d %b %Y")," - ",DATE_FORMAT(periode_akhir, "%d %b %Y"))';

$columns = array(
    array('db' => $primaryKey,                          'dt' => 0, 'field' => $primaryKey),
    array('db' => 'idkayu',                             'dt' => 0, 'field' => 'idkayu'),
    array('db' => 'idproduk',                           'dt' => 0, 'field' => 'idproduk'),
    array('db' => $periode,                             'dt' => 1, 'field' => 'periode',    'as'=>'periode'),
    array('db' => 'produk',                             'dt' => 2, 'field' => 'produk'),
    array('db' => 'kodehs',                             'dt' => 3, 'field' => 'kodehs'),
    array('db' => '`t2`.`nama`',                        'dt' => 4, 'field' => 'kayu',       'as'=>'kayu'),
    array('db' => 'concat(jumlah," ",satuan_jumlah)',   'dt' => 5, 'field' => 'jumlah',     'as'=>'jumlah'),
    array('db' => 'concat(volume," ",satuan_volume)',   'dt' => 6, 'field' => 'volume',     'as'=>'volume'),
    array('db' => 'concat(berat," ",satuan_berat)',     'dt' => 7, 'field' => 'berat',      'as'=>'berat'),
    array('db' => $primaryKey,                          'dt' => 8, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
        return '<a href="javascript:void(0)" class="btn btn-xs btn-info edit-row" data-idkayu="'.$row['idkayu'].'" data-idproduk="'.$row['idproduk'].'" data-periode="'.$row['periode'].'" data-id="'.encryptURL($d).'" title="Edit '.ucfirst($this->label).'">
        <i class="far fa-edit"></i>
        </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="'.encryptURL($d).'" title="Delete '.ucfirst($this->label).'"><i class="far fa-trash-alt"></i></a>';
                           }),
        );
    $joinQuery  = "from `$tblProduksi` as `t1` ";
    $joinQuery .= 'left join `kit_jeniskayu` as `t2` using(`idkayu`) ';
    $joinQuery .= 'LEFT JOIN `kit_produk` t3 using(`idproduk`) ';
    $extraWhere = "stdelete=1 and (client_id=".$_SESSION['client_id'].")";
    $groupBy    = '';
    $ordercus   = 'ORDER BY periode_awal DESC ';
    $having     = '';
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $tblProduksi, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
    );
}



function getLastPeriode(){
    echo json_encode($this->model->getItemByLastPeriode());
}

function getData4Update(){
    echo json_encode($this->model->getItemByClient($id=decryptURL($_POST['id'])));
}

function delAkun() {
$id= decryptURL($this->input->post('id')); 
$aa=$this->model->delAkun($id);
$b=rowArray($this->tblProduksi,array('idproduksi'=>$id));
$c=rowArray('kit_produk',array('idproduk'=>$b['idproduk']));
echo $c['produk'];
}

}
