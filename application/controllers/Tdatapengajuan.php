<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Tdatapengajuan extends CI_Controller {
private $tblEkspor  = 'ekspor';
public  $label      = 'Pengajuan Dokumen V-Legal';
public  $folder     = 'Tdatapengajuan';
public  $link1      = 'dokumen';
public  $link2      = 'pengajuan';
    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
            $this->load->model($this->folder.'_model','model');
            $this->load->model('A_json_model','json');
    }
    
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

    function showingData()
    {
// $data array() for basic HTML
        $data = array();
        $data['title']        = $this->label;
        $data['template']     = $this->folder.'/index';
        $data['tJudul']       = $this->label;
        $data['dJudul']       = $this->label;
        $data['url_index']    = site_url().$this->link1.'/'.$this->link2;
        $data['url_addnew']   = site_url().$this->link1.'/new-'.$this->link2;
        $data['url_kedua']    = 'new-'.$this->link2;
        $data['url_ajax']     = site_url().'ajax/'.$this->link2;
        $data['url_delete']   = site_url().'ajax/del'.$this->label;
        $data['breadcrum']    = array(
                                array($this->link1,'#'),
                                array($this->link2,'#'),
                                array('List',$this->link1.'/'.$this->link2)
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function insertBaru()
    {
// $data array() for basic HTML
        $data = array();
        $data['title']      = $this->label;
        $data['template']   = $this->folder.'/manage';
        $data['tJudul']     = $this->label;
        $data['dJudul']     = $this->label;
        $data['url_index']  = site_url().$this->link1.'/'.$this->link2;
        $data['url_addnew'] = site_url().$this->link1.'/new-'.$this->link2;
        $data['url_kedua']  = 'new-'.$this->link2;
        $data['url_ajax']   = site_url().'ajax/'.$this->link2;
        $data['url_proses'] = site_url().$this->link1.'/new-'.$this->link2.'/proses';
        $data['breadcrum']  = array(
                                array($this->link1,'#'),
                                array($this->link2,'#'),
                                array('New',$this->link1.'/new-'.$this->link2)
                              );
$getClient=$this->json->getClientbyID();
// $data array() for value database
$data['newbuyer']='';
$data['newnegara']='';
$data['newmatauang']='';
$data['newloading']='';
$data['newdischarge']='';
$data['newstatus']='';
$data['newiso']='';
$data['newalamat']='';
$data['newketerangan']='';
$data['newetpik']='';
$data['newnpwp']=$getClient['client_npwp'];
$data['newinvoice']='';
$data['newtglinvoice']='';
$data['newpeb']='';
$data['newbl']='';
$data['newpackinglist']='';
$data['newtglship']='';
$data['newvessel']='';
$data['newstuffing']=$this->json->checkStuffingById()['name'];
$data['newidnegara_dis']='';
$data['newsertifikat']=$getClient['client_sertifikat'];
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function indexUpdate()
    {
$id=decryptURL($this->uri->segment(3));
// $data array() for basic HTML
        $data = array();
        $data['title']      = $this->label;
        $data['template']   = $this->folder.'/manage';
        $data['tJudul']     = $this->label;
        $data['dJudul']     = $this->label;
        $data['url_index']  = site_url().$this->link1.'/'.$this->link2;
        $data['url_addnew'] = site_url().$this->link1.'/new-'.$this->link2;
        $data['url_kedua']  = 'new-'.$this->link2;
        $data['url_proses'] = site_url().$this->link1.'/update-'.$this->link2.'/proses';
        $data['breadcrum']  = array(
                                array($this->link1,'#'),
                                array($this->link2,'#'),
                                array('Edit',$this->link1.'/'.$this->link2.'/'.$this->uri->segment(3))
                              );

// $data array() for value database
    $get    =$this->model->getEksporUpdate($id);
    if(count($get)>0){
$getClient=$this->json->getClientbyID();
$data['newbuyer']       =$get['idbuyer'].','.$get['buyer'].' ['.$get['negarabuyer'].']';
$data['newalamat']      =$get['alamat'];
$data['newnegara']      =$get['idnegara'].','.$get['nama_negara'].' ('.$get['idnegara'].')';
$data['newiso']         =$get['iso'];
$data['newmatauang']    =$get['kode_uang'].','.$get['kode_uang'].' - '.$get['nama_uang'];
$data['newloading']     =$get['idloading'].','.$get['loading_uraian'].' ['.$get['loading_kode'].']';
$data['newdischarge']   =$get['iddischarge'].','.$get['dis_uraian'].' ['.$get['dis_kode'].']';
$data['newidnegara_dis']=$get['idnegara'];
$data['newketerangan']  =$get['keterangan'];
$data['newetpik']       =$getClient['client_etpik'];
$data['newnpwp']        =$getClient['client_npwp'];
$data['newinvoice']     =$get['invoice'];
$data['newtglinvoice']  =encode_date($get['tglinvoice']);
$data['newstatus']      =$get['status'];
$data['newpeb']         =$get['peb'];
$data['newbl']          =$get['bl'];
$data['newpackinglist'] =$get['packinglist'];
$data['newtglship']     =encode_date($get['tglship']);
$data['newstuffing']    =$get['stuffing'];
$data['newvessel']      =$get['vessel'];
$data['newstatus_liu']  =$get['status_liu'];
$data['newsertifikat']  =$getClient['client_sertifikat'];
}else{
    $_SESSION['msg']='Toastr("Maaf, Data tidak ditemukan","Info")';
    redirect(site_url().$this->link1.'/'.$this->link2);    
}
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function saveNew(){
        $insertBaru=$this->model->addUpEkspor();
        if($insertBaru){
$_SESSION['msg']='ToastrSukses("'.$this->label.' baru telah ditambahkan","Info")';
        redirect(site_url().$this->link1.'/'.$this->link2);
        }else{
$_SESSION['msg']='Toastr("Maaf, '.$this->label.' gagal ditambahkan","Info")';
        redirect(site_url().$this->link1.'/new-'.$this->link2);
        }
}

function FupdateData(){
$where =array('idekspor' =>decryptURL($this->input->post('hide-ID')));
        $insertBaru=$this->model->addUpEkspor($where);
        if($insertBaru){
$_SESSION['msg']='ToastrSukses("'.$this->label.' berhasil diedit","Info")';
        redirect(site_url().$this->link1.'/'.$this->link2);
        }else{
$_SESSION['msg']='Toastr("Maaf, '.$this->label.' gagal diedit","Info")';
        redirect(site_url().$this->link1.'/new-'.$this->link2);
        }
}

public function getListDT(){
    $tblEkspor      = $this->tblEkspor; 
    $primaryKey = 'idekspor';
    $sql_details = sql_connect();

$invoice    ='concat(`t1`.`invoice`,\' / \',date_format(`t1`.`tglinvoice`,\'%d-%m-%Y\')) ';
$buyer      ='concat(`t5`.`buyer`,\' / \',`t2`.`negara`) ';
$columns = array(
    array('db' => 't1.'.$primaryKey, 'dt' => 0, 'field' => $primaryKey),
    array('db' => 'no_dokumen', 'dt' => 0, 'field' => 'no_dokumen'),
    array('db' => '`t1`.`invoice`', 'dt' => 0, 'field' => 'noinvoice','as'=>'noinvoice'),
    array('db' => 'received_date', 'dt' => 0, 'field' => 'received_date'),
    array('db' => 'postdate', 'dt' => 0, 'field' => 'postdate'),
    array('db' => $invoice, 'dt' => 1, 'field' => 'invoice','as'=>'invoice','formatter'=>function($d,$row){
        return anchor(site_url().$this->link1.'/detail-'.$this->link2.'/'.encryptURL($row['idekspor']),$d, 'class="link-effect"');
    }),
    array('db' => 'no_urut', 'dt' => 2, 'field' => 'no_urut'),
    array('db' => $buyer, 'dt' => 3, 'field' => 'buyer','as'=>'buyer'),
    array('db' => 'iso', 'dt' => 4, 'field' => 'iso'),
    array('db' => 't3.uraian', 'dt' => 5, 'field' => 'loading','as'=> 'loading'),
    array('db' => 't4.uraian', 'dt' => 6, 'field' => 'discharge','as'=> 'discharge'),
    array('db' => 't1.status', 'dt' => 7, 'field' => 'status'),
    array('db' => 'status_liu', 'dt' => 8, 'field' => 'status_liu'),
    array('db' => 'tglship', 'dt' => 9, 'field' => 'tglship','formatter'=>function($d,$row){
        return encode_date($d);
    }),
    array('db' => 'status_liu', 'dt' => 10, 'field' => 'status_liu','formatter'=>function($d,$row){
        return ($d=='Draft')?'-':$row['no_dokumen'];
    }),
    array('db' => 'status_dokumen', 'dt' => 11, 'field' => 'status_dokumen','formatter'=>function($d,$row){
        return ($d=='Final')?encode_long_date($row['received_date']):'-';
    }),
    array('db' => 'status_liu', 'dt' => 12, 'field' => 'status_liu','formatter'=>function($d,$row){
        return ($d=='send')?encode_date($row['postdate']):'-';
    }),
   
    array('db' => 't1.'.$primaryKey, 'dt' => 13, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
return '<a href="'.site_url().$this->link1.'/'.$this->link2.'/'.encryptURL($d).'" class="btn btn-xs btn-info" title="Edit '.ucfirst($this->label).'">
    <i class="far fa-edit"></i>
</a>
<a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="'.encryptURL($d).'" title="Delete '.ucfirst($this->label).'"><i class="far fa-trash-alt"></i></a>';
                   })
                );
    $joinQuery  = "FROM `$tblEkspor` as `t1` ";
    $joinQuery .= 'LEFT JOIN `kit_negara` as t2 using(`idnegara`) ';
    $joinQuery .= 'LEFT JOIN `kit_loading` as t3 ON (`t3`.`idloading` = `t1`.`loading`) ';
    $joinQuery .= 'LEFT JOIN `kit_discharge` as t4 ON (`t4`.`iddischarge` = `t1`.`discharge`) ';
    $joinQuery .= 'LEFT JOIN `kit_buyer` AS t5 using(`idbuyer`) ';
    $joinQuery .= 'LEFT JOIN `vlegal_status` AS t6 on t6.`idekspor`=t1.idekspor and t6.status="send" ';
    $extraWhere = "stdelete=1 and client_id ='".$_SESSION['client_id']."'";
    $groupBy    = '';
    $ordercus   = 'ORDER BY  t1.tahun desc, no_urut DESC';
    $having     = '';
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $tblEkspor, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
    );
}

/* Detail Pengajuan Dokumen */

function showDetail(){
$id=decryptURL($this->uri->segment(3));
// $data array() for basic HTML
        $data = array();
        $data['title']        = $this->label;
        $data['template']     = $this->folder.'/detail_pengajuan';
        $data['tJudul']       = $this->label;
        $data['dJudul']       = $this->label;
        $data['url_index']    = site_url().$this->link1.'/'.$this->link2;
        $data['url_addnew']   = site_url().$this->link1.'/new-'.$this->link2;
        $data['url_kedua']    = 'new-'.$this->link2;
        $data['url_delete']   = site_url().'ajax/del'.$this->label;
        $data['breadcrum']    = array(
                                array($this->link1,'#'),
                                array($this->link2,'#'),
                                array('Detail List',$this->link1.'/'.$this->link2)
                              );
        $data['get']          = $this->model->getEksporByIdEskpor($id);
$data['hide-idekspor']  =$this->uri->segment(3);
$data['newsatjumlah']   ='';
$data['newsatvolume']   ='';
$data['newsatberat']    ='';

$data['newproduk']      ='';
$data['newlistkayu']    ='';
$data['newnegara']      ='';

$data['newjumlah']      ='';
$data['newvolume']      ='';
$data['newberat']       ='';
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);    
}

function getDet4_Update(){
$idekspor=decryptURL($this->input->post('idekspor'));
    $get=$this->model->getDetail_Edit($idekspor);

    $data['newproduk']      =$get['idproduk'].','.$get['namaproduk'];
    $idkayu     = implode(array_filter(array_column(json_decode($get['jenis_kayu'],true),'id')),',');
    $namakayu   = implode(array_filter(array_column(json_decode($get['jenis_kayu'],true),'name')),',');
    $idnegara   = implode(array_filter(array_column(json_decode($get['negara_asal'],true),'id')),',');
    $namanegara = implode(array_filter(array_column(json_decode($get['negara_asal'],true),'name')),',');

    $data['newlistkayu']    = implode(array($idkayu,$namakayu),'#');
    $data['newnegara']      = implode(array($idnegara,$namanegara),'#');

    $data['newjumlah']      ='';
    $data['newsatjumlah']   ='';

    $data['newvolume']      ='';
    $data['newsatvolume']   ='';

    $data['newberat']       ='';
    $data['newsatberat']    ='';

    $data['hide-idekspor']  =$this->input->post('idekspor');
}

 function pagination_tbl()
 {
$id=decryptURL($this->input->get('idekspor'));
  $this->load->library('pagination');
  $config = array();
  $config['base_url'] = '#';
  $config['total_rows'] = $this->model->pagination_Table($id);
  $config['per_page'] = 10;
  $config['uri_segment'] = 3;
  $config['use_page_numbers'] = TRUE;
  $config['full_tag_open'] = '<ul class="pagination">';
  $config['full_tag_close'] = '</ul>';
  $config['first_tag_open'] = '<li>';
  $config['first_tag_close'] = '</li>';
  $config['last_tag_open'] = '<li>';
  $config['last_tag_close'] = '</li>';
  $config['next_link'] = '<i class="fa fa-angle-double-right"></i>';
  $config['next_tag_open'] = '<li>';
  $config['next_tag_close'] = '</li>';
  $config['prev_link'] = '<i class="fa fa-angle-double-left"></i>';
  $config['prev_tag_open'] = '<li>';
  $config['prev_tag_close'] = '</li>';
  $config['cur_tag_open'] = '<li class="active"><a href="#">';
  $config['cur_tag_close'] = '</a></li>';
  $config['num_tag_open'] = '<li>';
  $config['num_tag_close'] = '</li>';
  $config['num_links'] = 3;
  $this->pagination->initialize($config);
  $page = $this->uri->segment(3);
  $start = ($page - 1) * $config['per_page'];

  $output = array(
   'pagination_link'  => $this->pagination->create_links(),
   'dataTable'   => $this->model->pagination_Table($id,$config['per_page'], $start)
  );
  echo json_encode($output);
 }


function delAkun() {
$id= decryptURL($this->input->post('id')); 
$aa=$this->model->delAkun($id);
echo rowWhere('id'.$this->field,$id,$this->table)->wip;
}
function gantiStatus() {
$id= decryptURL($this->input->post('id')); 
$aa=$this->model->gantiStatus($id);
$bb = $this->model->dataStatus($id);
// $dataStatus_json = $bb;
echo stUser($bb);
}

}
