<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    /**
     * Dashboard controller.  
     * Developer denipurnama371@gmail.com
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
            // $this->load->model('pembeli/Dashboard_model','dash');
    }

	function index()
    {
		// print_r('Sini');exit();
        $data = array();
        $data['title']      = 'Dashboard';
        $data['template']    = 'dashboard';
        $data['breadcrum']  = array(
                                array("Dashboard",'#')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
}
