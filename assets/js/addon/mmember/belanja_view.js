var table;
$(document).ready(function(){

	
table = $('#datatable_index').DataTable({
    "pageLength": 10,  
    "processing": true,
    "serverSide": true,
    // "stateSave": true,
    "order": [ 1, 'ASC' ],
    "ajax": { "url": site_url + ajax_url, "type": "POST" },
    "columns": [
        {"data": "nomor", searchable : false, orderable: false},
        {"data": "nojual", visible: true},
        {"data": "tgljual", searchable : true, orderable: true},
        {"data": "namabarang"},
        {"data": "jumlah"},
        {"data": "hargajualmember",render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp. ' )},
        {"data": "total", searchable : true, orderable: true,render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp. ' )},
        {"data": "action", searchable : false, orderable: false, className : 'not_search'},
       
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

        var action = '<a href="'+site_url+'mmember/detail_jual/'+aData['nojual'] +'" class="btn btn-success"><i class="fa fa-search"></i></a>'	;						
								
      

        $("td:eq(7)", nRow).html(action);
		
		
        return nRow;            
    }        
})

$('#datatable_index_filter input').unbind();
$('#datatable_index_filter input').bind('keyup', function(e) {
   if(e.keyCode == 13) {
    table.search(this.value).draw();   
   }
}); 

$("#refresh_list").click(function(){
    table.state.clear();
    window.location.reload();
});        

})
