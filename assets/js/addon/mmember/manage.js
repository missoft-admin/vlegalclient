
$(document).ready(function(){
	App.initHelpers(['datepicker']);
	$(".number").number(true,0,',','');
	// $('.number').number( true, 0 );
})
$("#noidstokies").select2({
	minimumInputLength: 2,
	noResults: 'Tidak Ditemukan.',          
	// allowClear: true
	// tags: [],
	ajax: {
		url: site_url+'All/s2_stokis/',
		dataType: 'json',
		type: "POST",
		quietMillis: 50,
		
	 data: function (params) {
		  var query = {
			search: params.term,                
		  }
		  return query;
		}, 
		processResults: function (data) {
			return {
				results: $.map(data, function (item) {
					return {
						text: item.namastokies,
						id: item.noidstokies
					}
				})
			};
		}
	}
});
$("#kota").select2({
	minimumInputLength: 2,
	noResults: 'Tidak Ditemukan.',          
	// allowClear: true
	// tags: [],
	ajax: {
		url: site_url+'All/s2_kota/',
		dataType: 'json',
		type: "POST",
		quietMillis: 50,
		
	 data: function (params) {
		  var query = {
			search: params.term,                
		  }
		  return query;
		}, 
		processResults: function (data) {
			return {
				results: $.map(data, function (item) {
					return {
						text: item.kota + ' - ('+item.propinsi+')',
						id: item.idkota
					}
				})
			};
		}
	}
});