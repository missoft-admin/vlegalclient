var table;
$(document).ready(function(){
	// alert('masuk sini');

// $('#datatable_index tfoot th').each( function () {
        
        // var title = $(this).text();
        // $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    // });
	
table = $('#datatable_index').DataTable({
    "pageLength": 10,  
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [ 1, 'ASC' ],
    "ajax": { "url": site_url + ajax_url, "type": "POST" },
    "columns": [
        {"data": "nomor", visible: true, searchable : false, orderable: false},
        {"data": "namabarang", searchable : true, orderable: true},
        {"data": "hargajualstokis",searchable : false, orderable: false,render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp. ' )},
        {"data": "hargajualmember",searchable : false, orderable: false,render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp. ' )},
        {"data": "var_jenis", searchable : false, orderable: false, className : 'not_search'},
        {"data": "stok", searchable : false, orderable: false, className : 'not_search',render: $.fn.dataTable.render.number( '.', ',', 0, '' )},
        {"data": "action", searchable : false, orderable: false, className : 'not_search'},
       
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

        var action = '<div class="btn-group">';
            action += '<a href="'+site_url+'barang/editor/'+aData['kodebarang'] +'"><button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="" data-original-title="Edit Barang"><i class="fa fa-pencil"></i></button></a>';
            action += '<a href="'+site_url+'barang/stokcabang/'+aData['kodebarang'] +'"><button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="" data-original-title="Remove Client"><i class="fa fa-search"></i></button></a>';
            action += '</div>'	;						
		
        $("td:eq(6)", nRow).html(action);
		
		var var_jenis='';
		if (aData['jenis']==1){
			var_jenis='Point';
		}else{
			var_jenis='Non Point';			
		}
		$("td:eq(4)", nRow).html(var_jenis);
		
        return nRow;            
    }        
})



$('#datatable_index_filter input').unbind();
$('#datatable_index_filter input').bind('keyup', function(e) {
   if(e.keyCode == 13) {
    table.search(this.value).draw();   
   }
}); 

$("#refresh_list").click(function(){
    table.state.clear();
    window.location.reload();
});        

})
