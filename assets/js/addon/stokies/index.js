var table;
$(document).ready(function(){
	// alert('masuk sini');

// $('#datatable_index tfoot th').each( function () {
        
        // var title = $(this).text();
        // $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    // });
	
table = $('#datatable_index').DataTable({
    "pageLength": 10,  
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [ 1, 'ASC' ],
    "ajax": { "url": site_url + ajax_url, "type": "POST" },
    "columns": [
        {"data": "nomor", searchable : false, orderable: false, className : 'not_search'},
        {"data": "noidstokies", visible: true},
        {"data": "namastokies", searchable : true, orderable: true},
        {"data": "kota", searchable : true, orderable: true},
        {"data": "namamembers", searchable : true, orderable: true},
        {"data": "xstatus", searchable : false, orderable: false, className : 'not_search'},
        {"data": "stok", searchable : false, orderable: true, className : 'not_search'},
        {"data": "map", searchable : false, orderable: false, className : 'not_search'},
        {"data": "online", searchable : false, orderable: false, className : 'not_search'},
        {"data": "action", searchable : false, orderable: false, className : 'not_search'},
       
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

        var action = '<div class="btn-group"><button data-toggle="dropdown" class="btn btn-primary">Action <span class="caret"></span></button><ul class="dropdown-menu">';
        action += '<li><a href="'+ site_url +'stokies/editor/' + aData['noidstokies'] + '/1'+'">Edit</a></li>';
        action += '<li class="divider"></li>';
        action += '<li><a href="'+ site_url +'stokies/belanja/' + aData['noidstokies'] + '">Belanja</a></li>';
        action += '<li><a href="'+ site_url +'stokies/penjualan/' + aData['noidstokies'] + '">Penjualan</a></li>';
		
        action += '<li><a href="'+ site_url +'stokies/stok/' + aData['noidstokies'] + '">Stok</a></li>';
        action += '<li><a href="'+ site_url +'stokies/resetpassword/' + aData['noidstokies'] + '">Reset</a></li>';
        action += '<li><a href="'+ site_url +'stokies/editor_rekening/' + aData['noidstokies'] + '">Edit Rekening</a></li>';
		action += '<li class="divider"></li>';
		
		action += '</ul></div>'							
								
        action += '</div>';

        $("td:eq(9)", nRow).html(action);
		
		var xstatus=cek_status(aData['status'],1);
		$("td:eq(5)", nRow).html(xstatus);
		var map=cek_map(aData['lat'],aData['long'],1);
		$("td:eq(7)", nRow).html(map);
		var online=cek_ol(aData['online'],1);
		$("td:eq(8)", nRow).html(online);
        return nRow;            
    }        
})
function cek_status($status)
{	
	if ($status=='1'){
		return 'Aktif';	
	}else{
		return 'NonAktif';	
	}
		
}
function cek_map($lat,$long)
{	
	if ($lat != '' && $long != ''){
		return '<i class="fa fa-map-marker fa-2x"></i>';	
	}else{
		return '<i class="si si-close fa-2x"></i>';	
	}
		
}
function cek_ol($online)
{	
	if ($online=='1'){
		return '<i class="si si-feed fa-2x"></i>';	
	}else{
		return '<i class="si si-ban fa-2x"></i>';	
	}
		
}
$('#datatable_index_filter input').unbind();
$('#datatable_index_filter input').bind('keyup', function(e) {
   if(e.keyCode == 13) {
    table.search(this.value).draw();   
   }
}); 

$("#refresh_list").click(function(){
    table.state.clear();
    window.location.reload();
});        


	
})
