var x,x1,x2,x3,i,pi,charCode,rgx,pf,intVal,api,pageTotal;$.ajaxSetup({type:"post",cache:!1,dataType:"json"});pi=parseInt;pf=parseFloat;function getFilter(id,format,view,title){$(id).datepicker({format:format,autoclose:!0,viewMode:view,minViewMode:view,title:'Pilih '+title,orientation:'auto bottom'})}
function getDatePicker(id,sd){$(id).datepicker({format:'dd-mm-yyyy',startDate:sd,autoclose:!0})}
function addCommas(nStr){nStr+='';x3=nStr.split('.');x1=x3[0];x2=x3.length>1?'.'+x3[1]:'';rgx=/(\d+)(\d{3})/;while(rgx.test(x1)){x1=x1.replace(rgx,'$1'+','+'$2')}return x1+x2}
function hanyaAngka(evt){charCode=(evt.which)?evt.which:event.keyCode;if(charCode>31&&(charCode<48||charCode>57))return!1;return!0}
function previewIMG(idfile,idprev){$(idfile).change(function(){$(idprev).attr('src',window.URL.createObjectURL(this.files[0]))})}
function removeCommas(str){return(str.replace(/,/g,''))}
function getDataTable(id,url,rownya){$(id).DataTable({"pageLength":10,"ordering":!1,"processing":!0,"serverSide":!0,"order":[],"oLanguage":{"sProcessing":"Sedang memproses...","sLengthMenu":"Tampilkan _MENU_ entri","sZeroRecords":"Tidak ditemukan data yang sesuai","sInfo":"","sInfoEmpty":"","sInfoFiltered":"","sInfoPostFix":"","sSearch":"Cari:","sUrl":"","oPaginate":{"sFirst":"Pertama","sPrevious":"&#8678; Sebelumnya","sNext":"Selanjutnya &#8680;","sLast":"Terakhir"},},"ajax":{"url":"./"+url,"type":"POST"},"columnDefs":[{"targets":[0],"orderable":!1}],"footerCallback":function(row,data,start,end,display){api=this.api(),data;intVal=function(i){return typeof i==='string'?i.replace(/[\$,]/g,'')*1:typeof i==='number'?i:0};total=api.column(rownya).data().reduce(function(a,b){return intVal(a)+intVal(b)},0);pageTotal=api.column(rownya,{page:'current'}).data().reduce(function(a,b){return intVal(a)+intVal(b)},0);$(api.column(rownya).footer()).html('Rp. '+addCommas(pageTotal)+' (Rp. '+addCommas(total)+' total)')}})}
function getDataSSP(id,url,scroll=false,rownya='',filter=null){$(id).dataTable({"scrollX": scroll,"oLanguage":{"sProcessing":"Sedang memproses...","sLengthMenu":"Tampilkan _MENU_ entri","sZeroRecords":"Tidak ada data pada periode ini","sInfo":"","sInfoEmpty":"","sInfoFiltered":"","sInfoPostFix":"","sSearch":"Cari:","sUrl":"","oPaginate":{"sFirst":"Pertama","sPrevious":"<i class='far fa-arrow-alt-circle-left'></i> Sebelumnya","sNext":"Selanjutnya <i class='far fa-arrow-alt-circle-right'></i>","sLast":"Terakhir"},},"processing":!0,"serverSide":!0,"ordering":!1,
  "ajax":{"data":{"filter":filter},"url":url},
  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
     var page = this.fnPagingInfo().iPage;
     var length = this.fnPagingInfo().iLength;
     var index = (page * length + (iDisplayIndex +1));
     $('td:eq(0)', nRow).html(index);
  },"footerCallback":function(row,data,start,end,display){api=this.api(),data;intVal=function(i){return typeof i==='string'?i.replace(/[\$,]/g,'')*1:typeof i==='number'?i:0};total=api.column(rownya).data().reduce(function(a,b){return intVal(a)+intVal(b)},0);pageTotal=api.column(rownya,{page:'current'}).data().reduce(function(a,b){return intVal(a)+intVal(b)},0);$(api.column(rownya).footer()).html('Rp. '+addCommas(pageTotal)+' (Rp. '+addCommas(total)+' total)')}})}
function Toastr(msg,title){toastr.options.timeOut=3000;toastr.options.showDuration=500;toastr.options.closeButton=!0;toastr.options.showMethod="slideDown";toastr.options.positionClass="toast-top-right";toastr.warning(msg,title).css("width","400px")}
function ToastrSukses(msg,title){toastr.options.timeOut=3000;toastr.options.showDuration=500;toastr.options.closeButton=!0;toastr.options.showMethod="slideDown";toastr.options.positionClass="toast-top-right";toastr.success(msg,title).css("width","400px")}
function ucFirst(Str){return Str.charAt(0).toUpperCase() + Str.slice(1);}
function getSelect2Search(id,url,hideID,label=''){
    id.css('width','100%')
id.select2({
    minimumInputLength:2, 
    placeholder: "-- Pilih "+label+" --",
    tags: false,
    triggerChange: true,
    ajax: {
        url: url,
        dataType: 'json',
        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        delay: 250,
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        page:((params.page || 1)*50),
        type: 'public'
      }
      return query;
        },
        processResults: function (data,params) {
            var data = $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    };
          });
      page = params.page || 1;

      return {
        results: data,
        pagination: {
          more:(page*50)<data.length
        }
      };
    },
    cache: true
  },
    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
    escapeMarkup: function (m) { return m; }
    })
    if(hideID!=''){
        var teks = new Option(hideID[1], hideID[0], true, true); 
        id.append(teks).trigger('change');
      }
  }

function getSelect2noSearch(id,url,hideID,label=''){
id.css('width','100%')
id.select2({
    minimumResultsForSearch: -1,
    placeholder: "-- Pilih "+label+" --",
    triggerChange: true,
    ajax: {
        url: url,
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        id: item.id,
                        text: item.name
                    }
                })
            };
        }
    },
    })
    if(hideID!=''){
        var teks = new Option(hideID[1], hideID[0], true, true); 
        id.append(teks).trigger('change');
      }
    }
 
function getMultipleSelect(id,url,datahide,label,max){
id.select2({
  multiple:true,
  placeholder: "-- Pilih "+label+" --",
  triggerChange:true,
  tags: false,
  minimumInputLength: 2,
  maximumSelectionLength: max,
  tokenSeparators: [','],
  ajax: {
        url: url,
        dataType: 'json',
        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        delay: 250,
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        page:((params.page || 1)*50),
        type: 'public'
      }
      return query;
        },
        processResults: function (data,params) {
            var data = $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    };
          });
      page = params.page || 1;

      return {
        results: data,
        pagination: {
          more:(page*50)<data.length
        }
      };
    },
    cache: true
  },
    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
    escapeMarkup: function (m) { return m; }
    })
    // $data=$.parseJSON(datahide);
    $data=datahide;
    if($data!=''){
        for(var i=0;i<$data[0].split(',').length;i++){
            var teks = new Option($data[1].split(',')[i], $data[0].split(',')[i], true, true);
            id.append(teks).trigger('change');
        }
    }
}
